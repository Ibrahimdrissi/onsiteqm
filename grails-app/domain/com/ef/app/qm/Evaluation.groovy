package com.ef.app.qm

class Evaluation {

	static CREATED_STATE= 0
	static VALIDATED_STATE = 1
	static REJECTED_STATE = 2
	static REGISTERED_STATE = 3;

    String name
	String evalAnimatorComment
	String evalSupComment
	String evalCentralComment
	String filename
	Integer evalStatus
	Integer evalNotification
	Planning planning
	Questionnaire questionnaire
	User user
	Map<String,String> noteList  = new HashMap<String,String>()
	String score

	User user_validated
	User user_updated
	Date date_created
	Date date_validation
	Date date_animateur_updated
	Date date_supervisor_updated
	Date date_central_updated

	static transients = ['noteList']

	static hasMany = [evalanswers: Evalanswer]
	static belongsTo = [Planning,User]

	static mapping = {
		id column: "EVALUATION_ID"
		version false
	}

	static constraints = {
		name nullable: true, maxSize: 250
		evalAnimatorComment nullable: true, maxSize: 750
		evalSupComment nullable: true, maxSize: 750
		evalCentralComment nullable: true, maxSize: 750
		evalStatus nullable: true
		filename nullable: true
		evalNotification nullable: true
		planning  nullable: true,unique: true
		user nullable: true
		score nullable: true, maxSize: 250
		questionnaire nullable: true
		user_validated nullable: true
		user_updated nullable: true
		date_created nullable: true
		date_validation nullable: true
		date_animateur_updated nullable: true
		date_supervisor_updated nullable: true
		date_central_updated nullable: true
	}
	String toString() {
		return name
	}
}
