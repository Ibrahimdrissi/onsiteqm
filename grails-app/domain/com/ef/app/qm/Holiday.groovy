package com.ef.app.qm

class Holiday {
    static SUNDAY = 1
    static FRIDAY = 6
    static SATURDAY = 7
    String holidayDescription
    Integer holidayDuration
    Date holidayDate

    static constraints = {
        holidayDate nullable: true
        holidayDuration nullable: false
        holidayDescription nullable: false, maxSize: 250
    }
}
