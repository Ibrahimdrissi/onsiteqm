package com.ef.app.qm

class Questionnaire {

	String questionnaireName

	boolean enable_q

	static hasMany = [questionnairegroups: Questionnairegroup]

	static mapping = {
		id column: "QUESTIONNAIRE_ID"
		version false
	}

	static constraints = {
		questionnaireName nullable: false
	}
}
