package com.ef.app.qm

class Questionnairegroup {

	String questionnairegroupName
	Integer questionnairegroupPonderation
	Questionnaire questionnaire
	String color
	String groupAndQuestionnaire

	static hasMany = [questionses: Questions]
	static belongsTo = [Questionnaire]
	static transients = ['groupAndQuestionnaire']

	static mapping = {
		id column: "QUESTIONNAIREGROUP_ID"
		version false
	}

	static constraints = {
		questionnairegroupName nullable: false
		questionnairegroupPonderation nullable: false
	}

	String getGroupAndQuestionnaire(){
		return questionnairegroupName+" :: "+questionnaire.questionnaireName
	}



}
