package com.ef.app.qm

class Evalanswer {

	Integer evalanswerScore
	String evalanswerAnimatorComment
	String evalanswerSupComment
	String evalanswerCentraleComment
	Evaluation evaluation
	Questions questions

	static belongsTo = [Evaluation, Questions]

	static mapping = {
		id column: "EVALANSWER_ID"
		version false
	}

	static constraints = {
		evalanswerScore nullable: true
		evalanswerAnimatorComment nullable: true, maxSize: 250
		evalanswerSupComment nullable: true, maxSize: 250
		evalanswerCentraleComment nullable: true, maxSize: 250
	}
}
