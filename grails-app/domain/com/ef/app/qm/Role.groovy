package com.ef.app.qm

class Role {
	String name
    String description
    Date dateCreated
    Date lastUpdated

    static belongsTo = [User]
    static hasMany = [members: User, permissions: String]

    static constraints = {
        name(nullable: false, blank: false, unique: true)
        description(blank: true, maxSize: 1000,nullable: true)
       /* createdBy(nullable: true)
        updatedBy(nullable: true)*/
    }

    String toString() {
        return name
    }
  
    String toFootPrintString() {
        def str = new StringBuilder("")
        
        try {
            str.append('name:'+name).append(', ')
            str.append('description:'+description).append(', ')
            str.append('dateCreated:'+dateCreated).append(', ')
            str.append('lastUpdated:'+lastUpdated).append(', ')
            str.append('members:'+members)
        }
        catch(Exception ex) {
            println 'error: '+str
            log.info 'An exception occured. ${ex.toString()}'
        }
        
        return str
    }
}