package com.ef.app.qm

class Team {

	String teamName

	static hasMany = [utilisateurs: User]

	static mapping = {
		id column: "TEAM_ID"
		version false
	}

	static constraints = {
		teamName nullable: false
	}
	String toString() {
		return teamName
	}
}
