package com.ef.app.qm

class Visit {

	String visitName

	static hasMany = [plannings: Planning]

	static mapping = {
		table('visite')
		id column: "VISITE_ID"
		version false
	}

	static constraints = {
		visitName nullable: false
	}

	String toString() {
		return visitName
	}

}
