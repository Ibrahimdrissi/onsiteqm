package com.ef.app.qm

class Space {
	String spaceCode
	String spaceName
	int spaceNumber
	String spaceAdress
	String spacePhone
	String spaceEmail
	String zone
	String region
	User user

	static hasMany = [planning: Planning]

	static belongsTo = [User]

	static mapping = {
		id column: "SPACE_ID"
		version false
		sort spaceName: "asc"
	}

	static constraints = {
		spaceCode nullable: true
		spaceName nullable: false
		spaceNumber nullable: false
		spaceAdress nullable: true
		spacePhone nullable: true
		spaceEmail nullable: true,email: true
		user nullable: true
		zone nullable: true
		region nullable: true
	}

	String toString(){
		return spaceName
	}
}
