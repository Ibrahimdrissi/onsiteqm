package com.ef.app.qm

class Profile {

	String profilName

	static hasMany = [utilisateurs: User]

	static mapping = {
		table('profil')
		id column: "PROFIL_ID"
		version false
	}

	static constraints = {
		profilName nullable: false
	}
}
