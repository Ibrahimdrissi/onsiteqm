package com.ef.app.qm

class User {

	String userName
	String userPassword
	String userEmail
	String firstName
	String lastName
	boolean isActive
	Team team
	Profile profile

	static hasMany = [roles: Role, permissions: String,evaluations: Evaluation,spaces: Space]
	static belongsTo = [Profile, Team]

	static mapping = {
		table('utilisateur')
		id column: "USER_ID"
		version false
	}

	static constraints = {
		userName nullable: false
		userPassword nullable: false
		userEmail nullable: false,email: true
		team nullable: true
		profile nullable: true
	}

	String toString(){
		userName
	}
}
