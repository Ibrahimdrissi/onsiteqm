package com.ef.app.qm

class Questions {

	String questionName
	String questionPoints
	String questionMaturity
	Questionnairegroup questionnairegroup

	boolean na

	static hasMany = [evalanswers: Evalanswer]
	static belongsTo = [Questionnairegroup]

	static mapping = {
		id column: "QUESTION_ID"
		version false
	}

	static constraints = {
		questionName nullable: false
		questionPoints nullable: false
		questionMaturity nullable: false
	}



}
