package com.ef.app.qm

class User_holidays {

	Date startDate;
	Date endDate
	User user

	static belongsTo = [User]

	static mapping = {
		table('user_holidays')
		version false
	}

	static constraints = {
		startDate nullable: false
		endDate nullable: false
		user nullable: false
	}


}
