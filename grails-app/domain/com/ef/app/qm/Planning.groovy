package com.ef.app.qm

class Planning {
	static VALIDATED_STATE = 0
	static WAITING_CONFIRMATION_STATE= 1
	static REJECTED_STATE = 2
	static CHANGED_STATE = 3
	static EVALUATED_STATE = 4
	Date planningDate
	String planningChangeComment
	String planningChangeJoint
	Integer planningState
	Integer planningNotification
	Planning planning
	Space space
	Visit visit

	User user_validated
	Date date_updated
	Date date_validation

	static hasMany = [evaluations: Evaluation,
	                  plannings: Planning]
	static belongsTo = [Visit,Space]

	static mapping = {
		//id column: "PLANNING_ID"//, generator: 'identity'
		version false
	}

	static constraints = {
		planningDate nullable: true
		planningChangeComment nullable: true, maxSize: 250
		planningChangeJoint nullable: true
		planningState nullable: true
		planningNotification nullable: true
		evaluations nullable: true
		plannings nullable: true
		user_validated nullable: true
		date_updated nullable: true
		date_validation nullable: true
	}

	String toString() {
		return planningDate.format('dd-mm-yyyy')
	}



}
