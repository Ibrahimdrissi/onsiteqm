package com.ef.app.qm

class Permission {
  String name
  String expression
  String description
  Boolean isManaged

  static constraints = {
    name(blank: false)
    expression(blank: false, unique: true)
    description(blank: true, maxSize: 1000)
    isManaged(default: true)
  }

  String toString() {
    return name
  }
}