package com.ef.app.qm

import com.ef.app.qm.Holiday

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import java.text.SimpleDateFormat

@Transactional(readOnly = true)
class HolidayController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Holiday.list(params), model:[holidayInstanceCount: Holiday.count()]
    }

    def show(Holiday holidayInstance) {
        respond holidayInstance
    }

    def create() {
        respond new Holiday(params)
    }

    @Transactional
    def save(Holiday holidayInstance) {
        if (holidayInstance == null) {
            notFound()
            return
        }

        if(params.holidayDate_value == '' ){
            flash.type="danger"
            flash.message = message(code: 'default.null.message3')
            render view: 'create', model: [holidayInstance:holidayInstance,dateError:'error']
            return
        }

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date holidayDate = formatter.parse(params.holidayDate_value);
        holidayInstance.holidayDate=holidayDate

        if (holidayInstance.hasErrors()) {
            flash.type="danger"
            flash.message = message(code: 'default.null.message3')
            render view: 'create', model: [holidayInstance:holidayInstance]
            return
        }

        holidayInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.created.message', args: [message(code: 'holiday.label', default: 'Holiday'), holidayInstance.holidayDescription])
                redirect holidayInstance
            }
            '*' { respond holidayInstance, [status: CREATED] }
        }
    }

    def edit(Holiday holidayInstance) {
        respond holidayInstance
    }

    @Transactional
    def update(Holiday holidayInstance) {
        if (holidayInstance == null) {
            notFound()
            return
        }

        if(params.holidayDate_value == '' ){
            flash.type="danger"
            flash.message = message(code: 'default.null.message3')
            render view: 'edit', model: [holidayInstance:holidayInstance,dateError:'error']
            return
        }

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date holidayDate = formatter.parse(params.holidayDate_value);
        holidayInstance.holidayDate=holidayDate

        if (holidayInstance.hasErrors()) {
            flash.type="danger"
            flash.message = message(code: 'default.null.message3')
            render view: 'edit', model: [holidayInstance:holidayInstance]
            return
        }

        holidayInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.updated.message', args: [message(code: 'com.ef.app.qm.Holiday.label', default: 'Holiday'), holidayInstance.holidayDescription])
                redirect holidayInstance
            }
            '*'{ respond holidayInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Holiday holidayInstance) {

        if (holidayInstance == null) {
            notFound()
            return
        }

        holidayInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'com.ef.app.qm.Holiday.label', default: 'Holiday'), holidayInstance.holidayDescription])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.type="danger"
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'holiday.label', default: 'Holiday'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
