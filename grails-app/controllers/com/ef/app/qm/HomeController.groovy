package com.ef.app.qm

import grails.converters.JSON

class HomeController {
    def cookieService
    def utilityService
    def dataSource

    def index = {
        flash.message = "${params.flashMessage}"
        flash.type = "danger"
        render(view: 'index')
    }

    def home = {
        def scoreList  = [:]
        def listRegion = utilityService.listRegion()
        def spaceId = (params.spaceId == null)?Space.list()[0].id:Integer.parseInt(params.spaceId)
        def team = (params.teamId == null)?Team.list()[0].id:Integer.parseInt(params.teamId)
        def region = (params.region == null)?listRegion[0].region:params.region
        def nbVisites = utilityService.getListVisitOfPlanings().size()
        scoreList = utilityService.getScores(spaceId,team,region)
        render view: 'index',model:[scoreList:scoreList,listRegion:listRegion,
                                    spaceId:spaceId,teamId:team,region:region,
                                    nbVisites:nbVisites]
    }

    def checkFileUploading() {
        def renderJson = [:]
        def flag = UploadingCheck.flag
        renderJson.flag = flag
        if(flag==false)
            cookieService.deleteCookie('uploading')
        render renderJson as JSON

    }

    def dashboardBySpace = {

        def scoreList  = [:]
        def spaceId = (params.spaceId == null)?Space.list()[0]?.id:Integer.parseInt(params.spaceId)
        def questionnaire = ( params.questionnaire == null )?Questionnaire.findByEnable_q(true).id:params.questionnaire
        scoreList["listVisites"]=utilityService.getListVisitOfPlanings();
        def listScoresByQuestions = [];

        if (spaceId == 0)
            {
                for(group in Questionnairegroup.list())
                {
                    for(question in Questions.findAllByQuestionnairegroup(group)){
                            listScoresByQuestions = [];
                            def nameExist = true;
                            for(row in utilityService.listScoreByQuestionsForAllSpaces(question?.id)){
                                if(nameExist){
                                    scoreList["nameQuestion_"+question?.id]=question?.questionName
                                    nameExist = false;
                                }
                                listScoresByQuestions?.add(Float.parseFloat(String.valueOf(row.score)));
                            }
                            scoreList["ScoresQuestion_"+question?.id]=listScoresByQuestions
                    }
/*                    def listMoyenneScores = [];
                    for (row in utilityService.listMoyenneScoreByGroupForAllSpaces(group.id))
                    {
                        listMoyenneScores?.add(Float.parseFloat(String.valueOf(row.score)));
                    }
                    scoreList["ScoresMoyenne_"+question?.id]=listMoyenneScores
*/
                }
            }
        else
            {
                for(group in Questionnairegroup.list())
                {
                    for(question in Questions.findAllByQuestionnairegroup(group)){
                            listScoresByQuestions = [];
                            def nameExist = true;
                            for(row in utilityService.listScoreBySpaceAndQuestions(Long.parseLong(""+spaceId),question?.id)){
                                if(nameExist){
                                    scoreList["nameQuestion_"+question?.id]=question?.questionName
                                    nameExist = false;
                                }
                                listScoresByQuestions?.add(Float.parseFloat(String.valueOf(row.score)));
                            }
                            scoreList["ScoresQuestion_"+question?.id]=listScoresByQuestions
                    }
                    def listMoyenneScores = [];
                    for (row in utilityService.listScoreBySpaceAndGroupQuestions(Long.parseLong(""+spaceId),group.id))
                    {
                        listMoyenneScores?.add(Float.parseFloat(String.valueOf(row.score)));
                    }
                    scoreList["ScoresMoyenne_"+group?.id]=listMoyenneScores
                }
            }

        render view: '/dashboard/spaces',model:[questionnaire:questionnaire,spaceId:params?.spaceId,scoreList:scoreList,nbVisites:utilityService.getListVisitOfPlanings().size()]
    }

    def dashboardByTeam = {

        def scoreList  = [:]
        def teamId = (params.teamId == null)?Team.list()[0].id:Integer.parseInt(params?.teamId)
        def questionnaire = ( params.questionnaire == null )?Questionnaire.findByEnable_q(true).id:params.questionnaire
        scoreList["listVisites"]=utilityService.getListVisitOfPlanings();
        def listScoresByQuestions = [];

        if (teamId == 0)
        {
            for(group in Questionnairegroup.list())
            {
                for(question in Questions.findAllByQuestionnairegroup(group)){
                    listScoresByQuestions = [];
                    def nameExist = true;
                    for(row in utilityService.listScoreByQuestionsForAllCellule(question?.id)){
                        if(nameExist){
                            scoreList["nameQuestion_"+question?.id]=question?.questionName
                            nameExist = false;
                        }
                        listScoresByQuestions?.add(Float.parseFloat(String.valueOf(row.score)));
                    }
                    scoreList["ScoresQuestion_"+question?.id]=listScoresByQuestions
                }
/*                                  def listMoyenneScores = [];
                for (row in utilityService.listMoyenneScoreByGroupForAllTeam(group.id))
                {
                    listMoyenneScores?.add(Float.parseFloat(String.valueOf(row.score)));
                }
                scoreList["ScoresMoyenne_"+question?.id]=listMoyenneScores
*/
            }
        }
        else
        {
            for(group in Questionnairegroup.list())
            {
                for(question in Questions.findAllByQuestionnairegroup(group)){
                    listScoresByQuestions = [];
                    def nameExist = true;
                    for(row in utilityService.listScoreByCelluleAndQuestions(Long.parseLong(""+teamId),question?.id)){
                        if(nameExist){
                            scoreList["nameQuestion_"+question?.id]=question?.questionName
                            nameExist = false;
                        }
                        listScoresByQuestions?.add(Float.parseFloat(String.valueOf(row.score)));
                    }
                    scoreList["ScoresQuestion_"+question?.id]=listScoresByQuestions
                }
                def listMoyenneScores = [];
                for (row in utilityService.listScoreByTeamAndGroupQuestions(Long.parseLong(""+teamId),group.id))
                {
                    listMoyenneScores?.add(Float.parseFloat(String.valueOf(row.score)));
                }
                scoreList["ScoresMoyenne_"+group?.id]=listMoyenneScores
            }
        }

        render view: '/dashboard/teams',model:[questionnaire:questionnaire,teamId:params?.teamId,scoreList:scoreList,
                                               nbVisites:utilityService.getListVisitOfPlanings().size()]
    }

    def dashboardByUser = {

        def scoreList  = [:]
        def userId = (params.userId == null)?utilityService.listUserHasEvaluation()[0]?.id:Integer?.parseInt(params?.userId)
        def questionnaire = ( params.questionnaire == null )?Questionnaire.findByEnable_q(true).id:params.questionnaire
        scoreList["listVisites"]=utilityService.getListVisitOfPlanings();
        def listScoresByQuestions = [];

        for(group in Questionnairegroup.list())
        {
            for(question in Questions.findAllByQuestionnairegroup(group)){
                if (userId == 0)
                {
                    listScoresByQuestions = [];
                    def nameExist = true;
                    for(row in utilityService.listScoreByQuestionsForAllUsers(question?.id)){
                        if(nameExist){
                            scoreList["nameQuestion_"+question?.id]=question?.questionName
                            nameExist = false;
                        }
                        listScoresByQuestions?.add(Float.parseFloat(String.valueOf(row.score)));
                    }
                    scoreList["ScoresQuestion_"+question?.id]=listScoresByQuestions
                }
                else
                {
                    listScoresByQuestions = [];
                    def nameExist = true;
                    for(row in utilityService.listScoreByUserAndQuestions(Long.parseLong(""+((userId == null)?'0':userId)),question?.id)){
                        if(nameExist){
                            scoreList["nameQuestion_"+question?.id]=question?.questionName
                            nameExist = false;
                        }
                        listScoresByQuestions?.add(Float.parseFloat(String.valueOf(row.score)));
                    }
                    scoreList["ScoresQuestion_"+question?.id]=listScoresByQuestions
                }
            }

/*            if (userId == 0)
            {
                def listMoyenneScores = [];
                for (row in utilityService.listMoyenneScoreByGroupForAllUsers(group.id))
                {
                    listMoyenneScores?.add(Float.parseFloat(String.valueOf(row.score)));
                }
                scoreList["ScoresMoyenne_"+question?.id]=listMoyenneScores
            }
            else
            {
                def listMoyenneScores = [];
                for (row in utilityService.listScoreByUserAndGroupQuestions(Long.parseLong(""+userId),group.id))
                {
                    listMoyenneScores?.add(Float.parseFloat(String.valueOf(row.score)));
                }
                scoreList["ScoresMoyenne_"+group?.id]=listMoyenneScores
            }*/

            if (userId != 0)
            {
                def listMoyenneScores = [];
                for (row in utilityService.listScoreByUserAndGroupQuestions(Long.parseLong(""+userId),group.id))
                {
                    listMoyenneScores?.add(Float.parseFloat(String.valueOf(row.score)));
                }
                scoreList["ScoresMoyenne_"+group?.id]=listMoyenneScores
            }

        }
        render view: '/dashboard/users',model:[utilityService: utilityService,questionnaire:questionnaire,userId:params?.userId,scoreList:scoreList,nbVisites:utilityService.getListVisitOfPlanings().size()]


    }


    def dashboardByDate = {
        def scoreList  = [:]

        def monthList  = [:]
        monthList[1] = g.message(code: 'com.reporting.mois1')
        monthList[2] = g.message(code: 'com.reporting.mois2')
        monthList[3] = g.message(code: 'com.reporting.mois3')
        monthList[4] = g.message(code: 'com.reporting.mois4')
        monthList[5] = g.message(code: 'com.reporting.mois5')
        monthList[6] = g.message(code: 'com.reporting.mois6')
        monthList[7] = g.message(code: 'com.reporting.mois7')
        monthList[8] = g.message(code: 'com.reporting.mois8')
        monthList[9] = g.message(code: 'com.reporting.mois9')
        monthList[10] = g.message(code: 'com.reporting.mois10')
        monthList[11] = g.message(code: 'com.reporting.mois11')
        monthList[12] = g.message(code: 'com.reporting.mois12')


        def month = (params.month == null)?utilityService.listPlanningDate()[0]?.month:Integer?.parseInt(params?.month)
        def year = (params.year == null)?utilityService.listPlanningDate()[0]?.year:Integer?.parseInt(params?.year)

        scoreList["listVisites"]=utilityService.getListVisitOfPlanings()

        def listScoresByQuestions = [];
        for(group in Questionnairegroup.list()){
            for(question in Questions.findAllByQuestionnairegroup(group)){
                listScoresByQuestions = [];
                def nameExist = true;
                for(row in utilityService.listScoreByMonthAndYearAndQuestions(Long.parseLong(""+((month == null)?'0':month)),Long.parseLong(""+((year == null)?'0':year)),question?.id)){
                    if(nameExist){
                        scoreList["nameQuestion_"+question?.id]=question?.questionName
                        nameExist = false;
                    }
                    listScoresByQuestions?.add(Float.parseFloat(String.valueOf(row.score)));
                }
                scoreList["ScoresQuestion_"+question?.id]=listScoresByQuestions
            }
        }

        render view: '/dashboard/date',model:[questionnaire:Questionnaire.findByEnable_q(true).id,month:month,year:year,scoreList:scoreList,utilityService:utilityService,monthList:monthList,nbVisites:utilityService.getListVisitOfPlanings().size()]
    }



    def dashboardByRegion = {

        def scoreList  = [:]
        def region = (params.region == null)?(utilityService.listRegion()[0]?.region):params?.region
        def questionnaire = ( params.questionnaire == null )?Questionnaire.findByEnable_q(true).id:params.questionnaire
        scoreList["listVisites"]=utilityService.getListVisitOfPlanings();
        def listScoresByQuestions = [];

        for(group in Questionnairegroup.list())
        {
            for(question in Questions.findAllByQuestionnairegroup(group)){
                if (region == "0")
                {
                    listScoresByQuestions = [];
                    def nameExist = true;
                    for(row in utilityService.listScoreByQuestionsForAllRegion(question?.id)){
                        if(nameExist){
                            scoreList["nameQuestion_"+question?.id]=question?.questionName
                            nameExist = false;
                        }
                        listScoresByQuestions?.add(Float.parseFloat(String.valueOf(row.score)));
                    }
                    scoreList["ScoresQuestion_"+question?.id]=listScoresByQuestions
                }
                else
                {
                    listScoresByQuestions = [];
                    def nameExist = true;
                    for(row in utilityService.listScoreByRegionAndQuestions(region,question?.id)){
                        if(nameExist){
                            scoreList["nameQuestion_"+question?.id]=question?.questionName
                            nameExist = false;
                        }
                        listScoresByQuestions?.add(Float.parseFloat(String.valueOf(row.score)));
                    }
                    scoreList["ScoresQuestion_"+question?.id]=listScoresByQuestions
                }
            }

/*            if (region == "0")
            {
                def listMoyenneScores = [];
                for (row in utilityService.listMoyenneScoreByGroupForAllRegion(group.id))
                {
                    listMoyenneScores?.add(Float.parseFloat(String.valueOf(row.score)));
                }
                scoreList["ScoresMoyenne_"+question?.id]=listMoyenneScores
            }
            else
            {
                def listMoyenneScores = [];
                for (row in utilityService.listScoreByRegionAndGroupQuestions(region,group.id))
                {
                    listMoyenneScores?.add(Float.parseFloat(String.valueOf(row.score)));
                }
                scoreList["ScoresMoyenne_"+group?.id]=listMoyenneScores
            }*/

            if (region != "0")
            {
                def listMoyenneScores = [];
                for (row in utilityService.listScoreByRegionAndGroupQuestions(region,group.id))
                {
                    listMoyenneScores?.add(Float.parseFloat(String.valueOf(row.score)));
                }
                scoreList["ScoresMoyenne_"+group?.id]=listMoyenneScores
            }

        }
        render view: '/dashboard/regions',model:[listRegions:utilityService.listRegion(),questionnaire:questionnaire,region:params?.region,scoreList:scoreList,nbVisites:utilityService.getListVisitOfPlanings().size()]



    }

}