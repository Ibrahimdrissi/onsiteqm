package com.ef.app.qm

import com.ef.app.qm.Permission
import com.ef.app.qm.Role
import com.ef.app.qm.User

class RoleController {
    static allowedMethods = [save: "POST", update: "POST", delete: "GET"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
	    params.max = Math.min(params.max ? params.int('max') : 20, 100)
	    [roleInstanceList: Role.list(), roleInstanceTotal:Role.list()?.size(),controllerName:'listRole']
    }

    def rolepermissionlist = {
	    def roleInstance = Role.findById(params.id)

	    def permissions = []
	    roleInstance?.permissions?.each {
	      permissions.add(Permission.findByExpression(it))
	    }

	    [roleInstance: roleInstance, rolePermissions: permissions,controllerName:'listRole']
    }

    def create = {
	    def roleInstance = new Role()
	    roleInstance.properties = params
	    return [roleInstance: roleInstance,controllerName:'listRole']
    }

    def createModal = {
        def roleInstance = new Role()
        roleInstance.properties = params
        render(template: 'modalCreate',model: [roleInstance: roleInstance,controllerName:'listRole'])
    }

    def save = {
	    def roleInstance = new Role(params)
	    if (roleInstance.save(flush: true)) {
	        flash.message = message(code: 'default.created.message',
			                        args: [message(code: 'com.ef.cbr.entity.role',
					                               default: 'com.ef.app.qm.Role'), roleInstance.name])
	        flash.type="success"
	        redirect(action: "list")
	    }
	    else {
            /*println "Errors in ROle"+roleInstance.errors
            flash.message = roleInstance.errors
            flash.type="danger"*/
	        render(view: "create", model: [roleInstance: roleInstance,controllerName:'listRole'])
	    }
    }

	def show = {
		def roleInstance = Role.get(params.id)
		if (!roleInstance) {
			flash.message = message(code: 'default.not.found.message',
									args: [message(code: 'com.ef.cbr.entity.role',
												   default: 'com.ef.app.qm.Role'), params.id])
			flash.type = "danger"
			redirect(action: "list")
		} else {
			return [roleInstance: roleInstance,controllerName:'listRole']
		}
	}

	def edit = {
        def roleInstance = Role.get(params.id)
        if (!roleInstance) {
            flash.message = message(code: 'default.not.found.message',
                    args: [message(code: 'com.ef.cbr.entity.role',
                            default: 'com.ef.app.qm.Role'), params.id])
            flash.type = "danger"
            redirect(action: "list")
        } else {
            return [roleInstance: roleInstance,controllerName:'listRole']
        }
    }
    def editModal = {
        def roleInstance = Role.get(params.id)
        if (!roleInstance) {
            flash.message = message(code: 'default.not.found.message',
                    args: [message(code: 'com.ef.cbr.entity.role',
                            default: 'com.ef.app.qm.Role'), params.id])
            flash.type = "danger"
            redirect(action: "list")
        } else {
            render template: 'modalEdit', model:[roleInstance: roleInstance,controllerName:'listRole']
        }
    }
	def update = {
		def roleInstance = Role.get(params.id)
		if (roleInstance) {
			if (params.version) {
				def version = params.version.toLong()
				if (roleInstance.version > version) {

					roleInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
													[message(code: 'com.ef.cbr.entity.role', default: 'com.ef.app.qm.Role')] as Object[],
													"Another user has updated this com.ef.app.qm.Role while you were editing")
					render(view: "edit", model: [roleInstance: roleInstance,controllerName:'listRole'])
					return
				}
			}
			roleInstance.properties = params
			if (!roleInstance.hasErrors() && roleInstance.save(flush: true)) {
				flash.message = message(code: 'default.updated.message',
										args: [message(code: 'com.ef.cbr.entity.role',
													   default: 'com.ef.app.qm.Role'), roleInstance.name])
				flash.type = "success"
				redirect(action: "show", id: roleInstance.id)
			} else {
				render(view: "edit", model: [roleInstance: roleInstance,controllerName:'listRole'])
			}
		} else {
			flash.message = message(code: 'default.not.found.message',
									args: [message(code: 'com.ef.cbr.entity.role',
												   default: 'com.ef.app.qm.Role'), params.id])
			flash.type = "danger"
			redirect(action: "list")
		}
	}

	def delete = {
		def roleInstance = Role.get(params.id)
		if (roleInstance) {
            def permissions
			try {
                User[] userRoles = roleInstance.members
                 for(user in userRoles){
                     roleInstance.removeFromMembers(user)
                 }

				roleInstance.delete(flush: true)
				flash.message = message(code: 'default.deleted.message',
										args: [message(code: 'com.ef.cbr.entity.role',
													   default: 'com.ef.app.qm.Role'), roleInstance.name])
				flash.type = "success"
				redirect(action: "list")
			}
			catch (org.springframework.dao.DataIntegrityViolationException e) {

				flash.message = message(code: 'default.not.deleted.message',
										args: [message(code: 'com.ef.cbr.entity.role',
													   default: 'com.ef.app.qm.Role'), roleInstance.name])
				flash.type = "danger"
				redirect(action: "show", id: params.id)
			}
		} else {
			flash.message = message(code: 'default.not.found.message',
									args: [message(code: 'com.ef.cbr.entity.role',
												   default: 'com.ef.app.qm.Role'), params.id])
			flash.type = "danger"
			redirect(action: "list")
		}
	}

	def viewpermissions = {
		def rolePermissionList = []
		def roleInstance = Role.findById(params.id)

		roleInstance?.permissions?.each {
			rolePermissionList.add(Permission.findByExpression(it))
		}

		params.max = Math.min(params.max ? params.int('max') : 100, 100)
		def permissionList = Permission.list(params)
		permissionList = permissionList - rolePermissionList

		[
			permissionList: permissionList,
			rolePermissionList: rolePermissionList,
			roleInstance: roleInstance,
			permissionTotal: Permission.count(),
            controllerName:'listRole',
			rolePermissionTotal: rolePermissionList.size(),
			params: [tab: params.tab ? params.tab : 'current']
		]
	}

	def assignpermission = {
		def permission = Permission.findById(params.id)
		def role = Role.findById(params.rid)

		role.addToPermissions(permission.expression)
		flash.message = message(code: 'com.ef.cbr.message.permissionAssigned.success',
								args: [message(code: "${permission.name}"), role.name])
		flash.type = "success"
		redirect(action: 'viewpermissions', id: role.id, params: [tab: 'assign',controllerName:'listRole'])
	}

	def reovkepermission = {
		def permission = Permission.findById(params.id)
		def role = Role.findById(params.rid)

		role.removeFromPermissions(permission.expression)
		flash.message = message(code: 'com.ef.cbr.message.permissionRevoked.success',
								args: [message(code: "${permission.name}"), role.name])
		flash.type = "success"
		redirect(action: 'viewpermissions', id: role.id, params: [tab: 'current',controllerName:'listRole'])
	}

	def viewmembers = {
		def roleInstance = Role.findById(params.id)

		def membersList = roleInstance.members

		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		def usersList = User.list()

		def admin = User.findByUserName('admin')
		usersList = usersList - membersList - admin


		[
			membersList: membersList,
			usersList: usersList,
			roleInstance: roleInstance,
			membersTotal: membersList.size(),
			usersTotal: usersList.size(),
                controllerName:'listRole',
			params: [tab: params.tab ? params.tab : 'current']
		]
	}

	def reovkemembership = {
		def member = User.findById(params.id)
		def role = Role.findById(params.rid)

		role.removeFromMembers(member)
		flash.message = message(code: 'com.ef.cbr.message.membershipRevoked.success',
								args: [member.username, role.name])
		flash.type = "success"
		redirect(action: 'viewmembers', id: role.id, params: [tab: 'current',controllerName:'listRole'])
	}

	def assignmembership = {
		def member = User.findById(params.id)
		def role = Role.findById(params.rid)

		role.addToMembers(member)
		flash.message = message(code: 'com.ef.cbr.message.membershipGranted.success',
								args: [member.userName, role.name])
		flash.type = "success"
		redirect(action: 'viewmembers', id: role.id, params: [tab: 'assign',controllerName:'listRole'])
	}
}