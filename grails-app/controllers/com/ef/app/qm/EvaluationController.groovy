package com.ef.app.qm

import com.ef.app.qm.Evalanswer
import com.ef.app.qm.Evaluation
import com.ef.app.qm.Profile
import com.ef.app.qm.Questionnairegroup
import com.ef.app.qm.Questions
import com.ef.app.qm.User
import org.apache.commons.io.FilenameUtils

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class EvaluationController {
    def utilityService
    static allowedMethods = [save: "POST", update: "POST", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Evaluation.list(params), model:[evaluationInstanceCount: Evaluation.count()]
    }

    def show(Evaluation evaluationInstance) {

        def noteList  = [:]

        for(qs in Questions.findAllByQuestionnairegroupInList(Questionnairegroup.findAllByQuestionnaire(evaluationInstance.questionnaire))){

            def evalAnswer = Evalanswer.findByEvaluationAndQuestions(Evaluation.get(evaluationInstance.id),qs)
            noteList[qs.id]= (evalAnswer == null)?0:evalAnswer.evalanswerScore
            noteList['comm_animateur_'+qs.id]= (evalAnswer == null)?"":evalAnswer.evalanswerAnimatorComment
            noteList['comm_chef_'+qs.id]= (evalAnswer == null)?"":evalAnswer.evalanswerSupComment
            noteList['comm_centrale_'+qs.id]= (evalAnswer == null)?"":evalAnswer.evalanswerCentraleComment
        }

        evaluationInstance.noteList=noteList;

        respond evaluationInstance,model:[utilityService:utilityService,evaluation:evaluationInstance]
    }

    def create() {
        def noteList  = [:]
        respond new Evaluation(params),model:[noteList:noteList,id_planning:params.planning]
    }


    def create_evaluationRegistred(Evaluation evaluationInstance)
    {
        def noteList  = [:]
        def eval = Evaluation.get(Long.parseLong(""+evaluationInstance.id))
        def eval_id = evaluationInstance.id
        for(qs in Questions.findAllByQuestionnairegroupInList(Questionnairegroup.findAllByQuestionnaire(evaluationInstance.questionnaire)))
            {
                def evalAnswer =Evalanswer.findByEvaluationAndQuestions(eval,qs)

                if (evalAnswer!=null)
                {
                    if (evalAnswer.evalanswerScore == 0)
                    {
                        noteList[qs.id]= ""
                        noteList['comm_animateur_'+qs.id]= ""
                        noteList['comm_chef_'+qs.id]= ""
                        noteList['comm_centrale_'+qs.id]= ""
                    }
                    else
                    {
                        noteList[qs.id]= evalAnswer.evalanswerScore
                        noteList['comm_animateur_'+qs.id]= evalAnswer.evalanswerAnimatorComment
                        noteList['comm_chef_'+qs.id]= evalAnswer.evalanswerSupComment
                        noteList['comm_centrale_'+qs.id]= evalAnswer.evalanswerCentraleComment
                    }
                }

            }

        evaluationInstance.id = null
        evaluationInstance.evalAnimatorComment = eval.evalAnimatorComment
        evaluationInstance.evalCentralComment = eval.evalCentralComment
        evaluationInstance.evalSupComment = eval.evalSupComment

        evaluationInstance.noteList = noteList

        System.out.println(evaluationInstance.noteList);

        render view: 'create', model:[id_planning:params.planning,evaluationInstance:evaluationInstance,
                                      utilityService:utilityService,eval_id:eval_id,questionnaire:evaluationInstance.questionnaire]

    }


    @Transactional
    def save(Evaluation evaluationInstance) {

        def groupList = Questionnairegroup.findAllByQuestionnaire(Questionnaire.findByEnable_q(true))
        if(params.eval_id!=''){
            groupList = Questionnairegroup.findAllByQuestionnaire(Evaluation.get(Integer.parseInt(""+params.eval_id)).questionnaire)
        }


        def planning = Planning.get(params.id_planning)
        evaluationInstance.setPlanning(planning)

        // test instance
        if (evaluationInstance == null) {
            notFound()
            return
        }

        // test error form
        if (evaluationInstance.hasErrors()) {
            respond evaluationInstance.errors, view:'create',model:[
                    id_planning:evaluationInstance.planning.id, eval_id:params.eval_id]
            return
        }

        // -- -- -- start validate notes -- -- --
        def validateNote = true

        def noteList  = [:]

        for(qs in Questions.findAllByQuestionnairegroupInList(groupList)){
            def note = params."note_${qs.id}"
            def note_comment = params."comm_animateur_${qs.id}"
            def note_chef_comment = params."comm_chef_${qs.id}"
            def note_centrale_comment = params."comm_centrale_${qs.id}"
            if(note == ""){
                validateNote = false
            }
            noteList[qs.id]= note
            noteList['comm_animateur_'+qs.id]= note_comment
            noteList['comm_chef_'+qs.id]= note_chef_comment
            noteList['comm_centrale_'+qs.id]= note_centrale_comment

            if((Profile.findByProfilNameLike("%Animateur%")?.id == User.get(session.user?.id)?.profile?.id) && note_comment == '' ){
                noteList['comm_animateur_error_'+qs.id]= 'border-color:#a94442 !important;'
                noteList['comm_chef_error_'+qs.id]= ''
                validateNote = false
            }else if((Profile.findByProfilNameLike("%Chef%")?.id == User.get(session.user?.id)?.profile?.id) && note_chef_comment == '' ){
                noteList['comm_animateur_error_'+qs.id]= ''
                noteList['comm_chef_error_'+qs.id]= 'border-color:#a94442 !important;'
                validateNote = false
            }
        }

        evaluationInstance.noteList=noteList;
        // -- -- -- end validate notes -- -- --

        // test text comment by profil
        if (params.create2==null) {
            if (Profile.findByProfilNameLike("%Animateur%")?.id == User.get(session.user?.id)?.profile?.id && evaluationInstance.evalAnimatorComment == null) {
                evaluationInstance.errors.reject("evalAnimatorComment", [message(code: 'evaluation.evalAnimatorComment.label')] as Object[], "${g.message(code: 'default.null.message2')}")
                render view: 'create', model: [evaluationInstance: evaluationInstance, message: "border-color:red !important;", id_planning: evaluationInstance.planning.id]
                return
            } else if (Profile.findByProfilNameLike("%Chef%")?.id == User.get(session.user?.id)?.profile?.id && evaluationInstance.evalSupComment == null) {
                evaluationInstance.errors.reject("evalAnimatorComment", [message(code: 'evaluation.evalSupComment.label')] as Object[], "${g.message(code: 'default.null.message2')}")
                render view: 'create', model: [evaluationInstance: evaluationInstance, message: "border-color:red !important;", id_planning: evaluationInstance.planning.id]
                return
            } else if (Profile.findByProfilNameLike("%Central%")?.id == User.get(session.user?.id)?.profile?.id && evaluationInstance.evalSupComment == null) {
                flash.message = message(code: 'evaluation.note.error')
                evaluationInstance.errors.reject("evalAnimatorComment", [message(code: 'evaluation.evalCentralComment.label')] as Object[], "${g.message(code: 'default.null.message2')}")
                render view: 'create', model: [evaluationInstance: evaluationInstance, message: "border-color:red !important;", id_planning: evaluationInstance.planning.id]
                return
            }

            // return error validate notes
            if (validateNote == false) {
                flash.type = "danger"
                flash.message = message(code: 'evaluation.note.error')
                render view: 'create', model: [evaluationInstance: evaluationInstance, id_planning: evaluationInstance.planning.id, eval_id: params.eval_id]
                return
            }
            evaluationInstance.evalStatus= Evaluation.CREATED_STATE

        }else{
             evaluationInstance.evalStatus = Evaluation.REGISTERED_STATE
        }

        // create evaluation



        if(params.eval_id == ''){
            evaluationInstance.user = session?.user
            evaluationInstance.questionnaire = Questionnaire.findByEnable_q(true)
            evaluationInstance.date_created = new Date()
            evaluationInstance.save flush:true
        }else{

            def newEval = Evaluation.get(Long.parseLong(""+params.eval_id))
            newEval.user_updated = User.get(session.user?.id)

            if(Profile.findByProfilNameLike("%Animateur%")?.id == User.get(session.user?.id)?.profile?.id ){
                newEval.date_animateur_updated = new Date()
                newEval.evalAnimatorComment = evaluationInstance.evalAnimatorComment
            }else if(Profile.findByProfilNameLike("%Chef%")?.id == User.get(session.user?.id)?.profile?.id ){
                newEval.date_supervisor_updated = new Date()
                newEval.evalSupComment = evaluationInstance.evalSupComment
            } else if(Profile.findByProfilNameLike("%Central%")?.id == User.get(session.user?.id)?.profile?.id ){
                newEval.date_central_updated = new Date()
                newEval.evalCentralComment = evaluationInstance.evalCentralComment
            }

            newEval.save flush:true
        }



        // upload file
        if(request.getFile('evaluationJoint').getOriginalFilename() != ''){

            def file = request.getFile('evaluationJoint')

            def f = request.getFile('evaluationJoint')

            // def webrootDir = "E:"   servletContext.getRealPath("/")
            // prod C:\Program Files\Apache Software Foundation\Tomcat 7.0\webapps\telechargements\
            // dev  C:\Program Files\Apache Software Foundation\Tomcat 8.0\webapps\telechargements\
            def webrootDir = "C:\\Program Files\\Apache Software Foundation\\Tomcat 7.0\\webapps\\telechargements\\"

            File fileDest = new File(webrootDir,"Evaluation_"+evaluationInstance.id+"_"+file.getOriginalFilename())

            f.transferTo(fileDest)

            evaluationInstance.filename = "Evaluation_"+evaluationInstance.id+"_"+file.getOriginalFilename()
            //evaluationInstance.save flush:true
        }

        // add all com.ef.app.qm.Evaluation answers

        for(qs in Questions.findAllByQuestionnairegroupInList(groupList))
            {
                if (params."note_${qs.id}" != '')
                {
                    def eval = Evaluation.last()
                    def evalanswer = new Evalanswer()
                    if(params.eval_id!=''){
                        eval = Evaluation.get(Long.parseLong(""+params.eval_id))
                        evalanswer = Evalanswer.findByQuestionsAndEvaluation(qs,eval)

                        if (evalanswer == null)
                        evalanswer = new Evalanswer()
                    }

                    evalanswer.setEvaluation(eval);
                    evalanswer.setQuestions(qs)
                    evalanswer.setEvalanswerScore(Integer.parseInt(params."note_${qs.id}"))
                    evalanswer.setEvalanswerAnimatorComment(params."comm_animateur_${qs.id}")
                    evalanswer.setEvalanswerSupComment(params."comm_chef_${qs.id}")
                    evalanswer.setEvalanswerCentraleComment(params."comm_centrale_${qs.id}")
                    evalanswer.save flush:true


                }
            }

        // calcul Scores
        def totalScore = 0.0

        if (params.create2 == null)
        {
            def scoreByGroup = 0
            def evaluation
            def nbQuestionsByGroup = 0

            for(group in groupList){
                scoreByGroup = 0
                def nbqNa = 0
                for (qqs in Questions.findAllByQuestionnairegroup(group))
                {
                    def na = params."na_${qqs.id}"
                    if (na == "on") {
                        nbqNa = nbqNa+1;
                    }
                }

                nbQuestionsByGroup = Questions.findAllByQuestionnairegroup(group).size()-nbqNa

                for(question in Questions.findAllByQuestionnairegroup(group)){
                    evaluation = Evaluation.findByPlanning(evaluationInstance.planning)
                    if(evaluation != null){
                        def score = Evalanswer.findByQuestionsAndEvaluation(question,evaluation)?.evalanswerScore
                        scoreByGroup = scoreByGroup + score
                    }
                }
                if(evaluation != null){
                    totalScore = totalScore + (((scoreByGroup / nbQuestionsByGroup ) * group.questionnairegroupPonderation) / 100)
                }
            }


            if(params.eval_id == ''){
                evaluationInstance.setScore(new java.text.DecimalFormat("0.##").format(totalScore))
                evaluationInstance.save flush:true
            }else{
                def newEval = Evaluation.get(Long.parseLong(""+params.eval_id))
                newEval.user_updated = User.get(session.user?.id)
                newEval.evalStatus= Evaluation.CREATED_STATE

                if(Profile.findByProfilNameLike("%Animateur%")?.id == User.get(session.user?.id)?.profile?.id ){
                    newEval.date_animateur_updated = new Date()
                    newEval.evalAnimatorComment = evaluationInstance.evalAnimatorComment
                }else if(Profile.findByProfilNameLike("%Chef%")?.id == User.get(session.user?.id)?.profile?.id ){
                    newEval.date_supervisor_updated = new Date()
                    newEval.evalSupComment = evaluationInstance.evalSupComment
                } else if(Profile.findByProfilNameLike("%Central%")?.id == User.get(session.user?.id)?.profile?.id ){
                    newEval.date_central_updated = new Date()
                    newEval.evalCentralComment = evaluationInstance.evalCentralComment
                }


                newEval.setScore(new java.text.DecimalFormat("0.##").format(totalScore))
                newEval.save flush:true
            }

        }

        planning.planningState = Planning.EVALUATED_STATE
        planning.save()
        Planning.findAllByPlanning(planning).each
            {
                it.planningState = Planning.REJECTED_STATE
                evaluationInstance.save flush:true
            }

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.created.message', args: [message(code: 'evaluation.label', default: 'Evaluation'), evaluationInstance.evalAnimatorComment])
                redirect(uri: "/evaluation")
            }
            '*' { respond evaluationInstance, [status: CREATED] }
        }


    }

    def edit(Evaluation evaluationInstance) {

        def noteList  = [:]

        for(qs in Questions.findAllByQuestionnairegroupInList(Questionnairegroup.findAllByQuestionnaire(evaluationInstance.questionnaire))){
            def evalAnswer = Evalanswer.findByEvaluationAndQuestions(Evaluation.get(evaluationInstance.id),qs)
            noteList[qs.id]= evalAnswer.evalanswerScore
            noteList['comm_animateur_'+qs.id]= evalAnswer.evalanswerAnimatorComment
            noteList['comm_chef_'+qs.id]= evalAnswer.evalanswerSupComment
            noteList['comm_centrale_'+qs.id]= evalAnswer.evalanswerCentraleComment
        }

        evaluationInstance.noteList=noteList;
        respond evaluationInstance,model:[utilityService:utilityService]
    }

    def update(Evaluation evaluationInstance) {

        if (evaluationInstance == null) {
            notFound()
            return
        }

        if (evaluationInstance.hasErrors()) {
            respond evaluationInstance.errors, view:'edit',model:[utilityService:utilityService,id_planning:evaluationInstance.planning.id]
            return
        }
        
        def validateNote = true

        def noteList  = [:]

        for(qs in Questions.findAllByQuestionnairegroupInList(Questionnairegroup.findAllByQuestionnaire(evaluationInstance.questionnaire))){
            def note = params."note_${qs.id}"
/*            def note_comment = Evalanswer.findByEvaluationAndQuestions(evaluationInstance,qs).evalanswerAnimatorComment
            def note_chef_comment = Evalanswer.findByEvaluationAndQuestions(evaluationInstance,qs).evalanswerSupComment
            def note_centrale_comment = Evalanswer.findByEvaluationAndQuestions(evaluationInstance,qs).evalanswerCentraleComment*/
            def note_comment = params."comm_animateur_${qs.id}"
            def note_chef_comment = params."comm_chef_${qs.id}"
            def note_centrale_comment = params."comm_centrale_${qs.id}"
            if(note == ""){
                validateNote = false
            }
            noteList[qs.id]= note
            noteList['comm_animateur_'+qs.id]= note_comment


            if(evaluationInstance.evalStatus == Evaluation.CREATED_STATE){
                noteList['comm_chef_'+qs.id]= Evalanswer.findByEvaluationAndQuestions(evaluationInstance,qs).evalanswerSupComment
            }else{
                noteList['comm_chef_'+qs.id]= note_chef_comment
            }

            noteList['comm_centrale_'+qs.id]= note_centrale_comment
        }

        evaluationInstance.noteList=noteList;

/*        render noteList
        return*/

        // test text comment by profil
        if(Profile.findByProfilNameLike("%Animateur%")?.id == User.get(session.user?.id)?.profile?.id && evaluationInstance.evalAnimatorComment == null ){
            //evaluationInstance.errors.reject("evalAnimatorComment",[message(code: 'evaluation.evalAnimatorComment.label')] as Object[], "${g.message(code:'default.null.message2')}")
            flash.type="danger"
            flash.message = message(code: 'evaluation.note.error')
            render view: 'edit', model: [utilityService:utilityService,evaluationInstance:evaluationInstance,id_planning:evaluationInstance.planning.id,message: "border-color:red !important;"]
            return
        }else if(evaluationInstance.evalStatus == Evaluation.REJECTED_STATE && Profile.findByProfilNameLike("%Chef%")?.id == User.get(session.user?.id)?.profile?.id && evaluationInstance.evalSupComment == null ){
            //evaluationInstance.errors.reject("evalAnimatorComment",[message(code: 'evaluation.evalSupComment.label')] as Object[], "${g.message(code:'default.null.message2')}")
            flash.type="danger"
            flash.message = message(code: 'evaluation.note.error')
            render view: 'edit', model: [utilityService:utilityService,evaluationInstance:evaluationInstance,id_planning:evaluationInstance.planning.id,message: "border-color:red !important;"  ]
            return
        }else if(evaluationInstance.evalStatus == Evaluation.REJECTED_STATE && Profile.findByProfilNameLike("%Central%")?.id == User.get(session.user?.id)?.profile?.id && evaluationInstance.evalCentralComment == null ){
            //evaluationInstance.errors.reject("evalAnimatorComment",[message(code: 'evaluation.evalCentralComment.label')] as Object[], "${g.message(code:'default.null.message2')}")
            flash.type="danger"
            flash.message = message(code: 'evaluation.note.error')
            render view: 'edit', model: [utilityService:utilityService,evaluationInstance:evaluationInstance,id_planning:evaluationInstance.planning.id,message: "border-color:red !important;"]
            return
        }

        if(validateNote==false){
            flash.type="danger"
            flash.message = message(code: 'evaluation.note.error')
            render view: 'edit', model: [utilityService:utilityService,evaluationInstance:evaluationInstance,id_planning:evaluationInstance.planning.id]
            return
        }

        evaluationInstance.setPlanning(Evaluation.get(evaluationInstance.id)?.planning)

        def oldEval = Evaluation.get(evaluationInstance.id)

        if(Profile.findByProfilNameLike("%Animateur%")?.id == User.get(session.user?.id)?.profile?.id ){
            evaluationInstance.date_animateur_updated=new Date()
        }else if(Profile.findByProfilNameLike("%Chef%")?.id == User.get(session.user?.id)?.profile?.id ){
            evaluationInstance.date_supervisor_updated=new Date()


            if(Profile.findByProfilNameLike("%Animateur%")?.id == evaluationInstance.user.profile.id)
            {
                evaluationInstance.user_validated= User.get(session.user?.id)
                evaluationInstance.date_validation=new Date()
            }

        } else if(Profile.findByProfilNameLike("%Central%")?.id == User.get(session.user?.id)?.profile?.id ){
            evaluationInstance.date_central_updated=new Date()

            if(Profile.findByProfilNameLike("%Chef%")?.id == evaluationInstance.user.profile.id)
            {
                evaluationInstance.user_validated= User.get(session.user?.id)
                evaluationInstance.date_validation=new Date()
            }
        }

        evaluationInstance.user_updated=User.get(session.user?.id)

        evaluationInstance.save flush:true

        // upload file
        if(params.evaluationJoint != null && request.getFile('evaluationJoint').getOriginalFilename() != ''){

            def file = request.getFile('evaluationJoint')

            def f = request.getFile('evaluationJoint')
            // prod C:\Program Files\Apache Software Foundation\Tomcat 7.0\webapps\telechargements\
            // dev  C:\Program Files\Apache Software Foundation\Tomcat 8.0\webapps\telechargements\
            def webrootDir = "C:\\Program Files\\Apache Software Foundation\\Tomcat 7.0\\webapps\\telechargements\\"
            File fileDest = new File(webrootDir,"Evaluation_"+evaluationInstance.id+"_"+file.getOriginalFilename())

            f.transferTo(fileDest)

            evaluationInstance.filename = "Evaluation_"+evaluationInstance.id+"_"+file.getOriginalFilename()
            evaluationInstance.save flush:true
        }

        for(qs in Questions.findAllByQuestionnairegroupInList(Questionnairegroup.findAllByQuestionnaire(evaluationInstance.questionnaire))){
            def evalanswer = Evalanswer.findByEvaluationAndQuestions(Evaluation.get(evaluationInstance.id),qs)
            evalanswer.setEvalanswerScore(Integer.parseInt(params."note_${qs.id}"))

            if(Profile.findByProfilNameLike("%Animateur%")?.id == User.get(session.user?.id)?.profile?.id ){
                evalanswer.setEvalanswerAnimatorComment(params."comm_animateur_${qs.id}")
                evalanswer.setEvalanswerSupComment(Evalanswer.findByEvaluationAndQuestions(evaluationInstance,qs).evalanswerSupComment)

            }else if(Profile.findByProfilNameLike("%Chef%")?.id == User.get(session.user?.id)?.profile?.id){
                evalanswer.setEvalanswerAnimatorComment(Evalanswer.findByEvaluationAndQuestions(evaluationInstance,qs).evalanswerAnimatorComment)
                evalanswer.setEvalanswerSupComment(params."comm_chef_${qs.id}")

            }else if(Profile.findByProfilNameLike("%Cellule centrale%")?.id == User.get(session.user?.id)?.profile?.id){
                evalanswer.setEvalanswerScore(Integer.parseInt(params."note_${qs.id}"))



                if(params.checkstatWaitToConfirm != null){
                    evalanswer.setEvalanswerSupComment(Evalanswer.findByEvaluationAndQuestions(evaluationInstance,qs).evalanswerSupComment)
                    evalanswer.setEvalanswerAnimatorComment(Evalanswer.findByEvaluationAndQuestions(evaluationInstance,qs).evalanswerAnimatorComment)

                }else{
                    evalanswer.setEvalanswerSupComment(params."comm_chef_${qs.id}")
                    evalanswer.setEvalanswerAnimatorComment(params."comm_animateur_${qs.id}")
                }

               /* if(evaluationInstance.evalStatus == Evaluation.CREATED_STATE){
                    evalanswer.setEvalanswerSupComment(Evalanswer.findByEvaluationAndQuestions(evaluationInstance,qs).evalanswerSupComment)
                }else{
                    evalanswer.setEvalanswerSupComment(params."comm_chef_${qs.id}")
                }*/

                evalanswer.setEvalanswerCentraleComment(params."comm_centrale_${qs.id}")

            }

            evalanswer.save flush:true
        }


        /*
        // calcul Scores
        def totalScore = 0.0
        def scoreByGroup = 0
        def evaluation
        def nbQuestionsByGroup = 0

        for(group in Questionnairegroup.findAllByQuestionnaire(evaluationInstance.questionnaire)){
            scoreByGroup = 0
            def nbqNa = 0

            for(question in Questions.findAllByQuestionnairegroup(group)){
                evaluation = Evaluation.findByPlanning(evaluationInstance.planning)
                if(evaluation != null){
                    def score = Evalanswer.findByQuestionsAndEvaluation(question,evaluation)?.evalanswerScore
                    scoreByGroup = scoreByGroup + score
                }

                def na = params."na_${question.id}"
                if (na == "on") {
                    nbqNa = nbqNa+1;
                }

                nbQuestionsByGroup = Questions.findAllByQuestionnairegroup(group).size()-nbqNa

                System.out.println("\n group id : "+group.id)

                System.out.println("\n nbQuestionsByGroup : "+nbQuestionsByGroup)

                System.out.println("\n nbqNa : "+nbqNa)


            }
            if(evaluation != null){
                totalScore = totalScore + (((scoreByGroup / nbQuestionsByGroup ) * group.questionnairegroupPonderation) / 100)
            }
        }


        */

        def totalScore = 0.0
        def scoreByGroup = 0
        def evaluation
        def nbQuestionsByGroup = 0

        for(group in Questionnairegroup.findAllByQuestionnaire(evaluationInstance.questionnaire)){
            scoreByGroup = 0
            def nbqNa = 0
            System.out.println("\n group "+group.id);
            for (qqs in Questions.findAllByQuestionnairegroup(group))
            {
                def note = params."note_${qqs.id}"

                System.out.println("\n qqs "+qqs.id);
                System.out.println("\n note "+note);

                if (note == "0") {
                    nbqNa = nbqNa+1;
                }

            }


            System.out.println("\n nbqNa "+nbqNa);

            nbQuestionsByGroup = Questions.findAllByQuestionnairegroup(group).size()-nbqNa

            for(question in Questions.findAllByQuestionnairegroup(group)){
                evaluation = Evaluation.findByPlanning(evaluationInstance.planning)
                if(evaluation != null){
                    def score = Evalanswer.findByQuestionsAndEvaluation(question,evaluation)?.evalanswerScore
                    scoreByGroup = scoreByGroup + score
                }
            }
            if(evaluation != null){
                totalScore = totalScore + (((scoreByGroup / nbQuestionsByGroup ) * group.questionnairegroupPonderation) / 100)
            }
        }

        evaluationInstance.setScore(new java.text.DecimalFormat("0.##").format(totalScore))
        evaluationInstance.save flush:true


        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.updated.message')
                redirect(uri: "/evaluation")
            }
            '*'{ respond evaluationInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Evaluation evaluationInstance) {

        if (evaluationInstance == null) {
            notFound()
            return
        }

        evaluationInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'com.ef.app.qm.Evaluation.label', default: 'com.ef.app.qm.Evaluation'), evaluationInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.type="danger"
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'evaluation.label', default: 'com.ef.app.qm.Evaluation'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

}
