package com.ef.app.qm

import com.ef.app.qm.Space
import com.ef.app.qm.User

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import org.apache.shiro.crypto.hash.Sha256Hash


@Transactional(readOnly = true)
class UserController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond User.list(params), model:[userInstanceCount: User.count()]
    }

    def show(User userInstance) {
        respond userInstance
    }

    def create() {
        respond new User(params)
    }

    @Transactional
    def save(User userInstance) {
        if (userInstance == null) {
            notFound()
            return
        }

        def userByEmail = User.findByUserEmail(userInstance.userEmail)
        if(userByEmail != null){
            flash.type="danger"
            flash.message = message(code: 'com.ef.user.emailExiste')
            render view: 'create', model: [userInstance:userInstance]
            return
        }

        def userByUserName = User.findByUserName(userInstance.userName)
        if(userByUserName != null){
            flash.type="danger"
            flash.message = message(code: 'com.ef.user.userExiste')
            render view: 'create', model: [userInstance:userInstance]
            return
        }

        if((userInstance.profile == Profile.findByProfilNameLike("%Chef de cellule%") || userInstance.profile == Profile.findByProfilNameLike("%Animateur%")) && userInstance.team == null){
            flash.type="danger"
            flash.message = message(code: 'default.error.message')
            render view: 'create', model: [userInstance:userInstance,profile_error:"error"]
            return
        }

        if (userInstance.hasErrors()) {
            flash.type="danger"
            flash.message = message(code: 'default.error.message')
            render view: 'create', model: [userInstance:userInstance]
            return
        }

        def spaces =  params.spaces

        userInstance.userPassword =  new Sha256Hash(userInstance.userPassword).toHex()
        userInstance.addToPermissions("*:*")
        userInstance.isActive = true
        userInstance.save flush:true

        def id = userInstance.id

        if (spaces != null && !spaces.class.isArray()){
            def espace = Space.get(spaces);
            espace.user = User.get(id);
            espace.save flush:true
        }else{
            for(space in spaces){
                def espace = Space.get(space);
                espace.user = User.get(id);
                espace.save flush:true
            }
        }

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.created.message', args: [message(code: 'user.label', default: 'User'), userInstance.userName])
                redirect userInstance
            }
            '*' { respond userInstance, [status: CREATED] }
        }
    }

    def edit(User userInstance) {
        respond userInstance
    }


    def update(User userInstance) {

        if (userInstance == null) {
            notFound()
            return
        }

        // check userEmail Exist or not
        def userByEmail = User.findByUserEmailAndIdNotEqual(userInstance.userEmail,userInstance.id)
        if(userByEmail != null ){
            flash.type="danger"
            flash.message = message(code: 'com.ef.user.emailExiste')
            render view: 'edit', model: [userInstance:userInstance]
            return
        }
        // check userName Exist or not
        def userByUserName = User.findByUserNameAndIdNotEqual(userInstance.userName,userInstance.id)
        if(userByUserName != null ){
            flash.type="danger"
            flash.message = message(code: 'com.ef.user.userExiste')
            render view: 'edit', model: [userInstance:userInstance]
            return
        }
        // delete all spaces of the user
        def spacesOfUser = Space.findAllByUser(userInstance);
        for(sp in spacesOfUser){
            sp.user = null;
            sp.save flush:true
        }
        // add news spaces
        def spaces =  params.spaces
        def id = userInstance.id

        if (spaces != null && !spaces.class.isArray()){
            def espace = Space.get(spaces);
            espace.user = User.get(id);
            espace.save flush:true
        }else{
            for(space in spaces){
                def espace = Space.get(space);
                espace.user = User.get(id);
                espace.save flush:true
            }
        }

        if (userInstance.hasErrors()) {
            flash.type="danger"
            flash.message = message(code: 'default.error.message')
            render view: 'edit', model: [userInstance:userInstance]
            return
        }

        userInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.updated.message', args: [message(code: 'com.ef.app.qm.User.label', default: 'User'), userInstance.userName])
                redirect userInstance
            }
            '*'{ respond userInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(User userInstance) {

        if (userInstance == null) {
            notFound()
            return
        }

        userInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'com.ef.app.qm.User.label', default: 'User'), userInstance.userName])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.type="danger"
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
