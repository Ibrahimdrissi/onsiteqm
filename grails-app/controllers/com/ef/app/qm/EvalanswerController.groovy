package com.ef.app.qm

import com.ef.app.qm.Evalanswer

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class EvalanswerController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Evalanswer.list(params), model:[evalanswerInstanceCount: Evalanswer.count()]
    }

    def show(Evalanswer evalanswerInstance) {
        respond evalanswerInstance
    }

    def create() {
        respond new Evalanswer(params)
    }

    @Transactional
    def save(Evalanswer evalanswerInstance) {
        if (evalanswerInstance == null) {
            notFound()
            return
        }

        if (evalanswerInstance.hasErrors()) {
            respond evalanswerInstance.errors, view:'create'
            return
        }

        evalanswerInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.created.message', args: [message(code: 'evalanswer.label', default: 'com.ef.app.qm.Evalanswer'), evalanswerInstance.id])
                redirect evalanswerInstance
            }
            '*' { respond evalanswerInstance, [status: CREATED] }
        }
    }

    def edit(Evalanswer evalanswerInstance) {
        respond evalanswerInstance
    }


    def update(Evalanswer evalanswerInstance) {
        if (evalanswerInstance == null) {
            notFound()
            return
        }

        if (evalanswerInstance.hasErrors()) {
            respond evalanswerInstance.errors, view:'edit'
            return
        }

        evalanswerInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.updated.message', args: [message(code: 'com.ef.app.qm.Evalanswer.label', default: 'com.ef.app.qm.Evalanswer'), evalanswerInstance.id])
                redirect evalanswerInstance
            }
            '*'{ respond evalanswerInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Evalanswer evalanswerInstance) {

        if (evalanswerInstance == null) {
            notFound()
            return
        }

        evalanswerInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'com.ef.app.qm.Evalanswer.label', default: 'com.ef.app.qm.Evalanswer'), evalanswerInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.type="danger"
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'evalanswer.label', default: 'com.ef.app.qm.Evalanswer'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
