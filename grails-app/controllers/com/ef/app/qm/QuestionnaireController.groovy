package com.ef.app.qm

import com.ef.app.qm.Questionnaire

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class QuestionnaireController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def utilityService

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Questionnaire.list(params), model:[questionnaireInstanceCount: Questionnaire.count()]
    }

    def show(Questionnaire questionnaireInstance) {
        respond questionnaireInstance
    }

    def create() {
        respond new Questionnaire(params)
    }

    @Transactional
    def save(Questionnaire questionnaireInstance) {
        if (questionnaireInstance == null) {
            notFound()
            return
        }

        if (questionnaireInstance.questionnaireName != null)
        {
            def questionnaireExist = Questionnaire.findByQuestionnaireName(questionnaireInstance.questionnaireName)
            if (questionnaireExist!=null)
            {
                flash.type="danger"
                flash.message = message(code: 'default.null.message5')
                render view: 'create', model: [questionnaireInstance:questionnaireInstance]
                return
            }
        }

        if (questionnaireInstance.hasErrors()) {
            flash.type="danger"
            flash.message = message(code: 'default.null.message2')
            render view: 'create', model: [questionnaireInstance:questionnaireInstance]
            return
        }

        questionnaireInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.created.message', args: [message(code: 'questionnaire.label', default: 'com.ef.app.qm.Questionnaire'), questionnaireInstance.id])
                redirect questionnaireInstance
            }
            '*' { respond questionnaireInstance, [status: CREATED] }
        }
    }

    def edit(Questionnaire questionnaireInstance) {
        respond questionnaireInstance
    }

    @Transactional
    def update(Questionnaire questionnaireInstance) {

    def total = utilityService.getTotalPonderationByQuestionnaire(questionnaireInstance.id)

        total = (total == null)?0:Integer.parseInt(""+total)

        def allGroupsHasQs = true

        for(group in Questionnairegroup.findAllByQuestionnaire(Questionnaire.get(questionnaireInstance.id))){

            def nbQS = Questions.findAllByQuestionnairegroup(group).size()

            if(nbQS == 0){
                allGroupsHasQs=false
            }

        }

        if(questionnaireInstance.enable_q)
        {
            if ( total == 0 || total !=100 )
            {
                questionnaireInstance.enable_q = false
                flash.type="danger"
                flash.message = message(code: 'default.null.message6')
                render view: 'edit', model: [questionnaireInstance:questionnaireInstance]
                return
            }
            else if (!allGroupsHasQs)
            {
                questionnaireInstance.enable_q = false
                flash.type="danger"
                flash.message = message(code: 'default.null.message7')
                render view: 'edit', model: [questionnaireInstance:questionnaireInstance]
                return
            }else{
                questionnaireInstance.enable_q = true
                utilityService.disabledAllQuestionnaire()
            }
        }


        if (questionnaireInstance == null) {
            notFound()
            return
        }

        if (questionnaireInstance.hasErrors()) {
            flash.type="danger"
            flash.message = message(code: 'default.null.message2')
            render view: 'edit', model: [questionnaireInstance:questionnaireInstance]
            return
        }

        questionnaireInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.updated.message', args: [message(code: 'com.ef.app.qm.Questionnaire.label', default: 'com.ef.app.qm.Questionnaire'), questionnaireInstance.id])
                redirect questionnaireInstance
            }
            '*'{ respond questionnaireInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Questionnaire questionnaireInstance) {

        if (questionnaireInstance == null) {
            notFound()
            return
        }

        def listEvaluations = Evaluation.findAllByQuestionnaire(Questionnaire.get(questionnaireInstance.id))
        if (listEvaluations.size() == 0) {
            questionnaireInstance.delete flush:true
        }


        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'com.ef.app.qm.Questionnaire.label', default: 'com.ef.app.qm.Questionnaire'), questionnaireInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.type="danger"
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'questionnaire.label', default: 'com.ef.app.qm.Questionnaire'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
