package com.ef.app.qm

import com.ef.app.qm.Questions

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class QuestionsController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Questions.list(params), model:[questionsInstanceCount: Questions.count()]
    }

    def show(Questions questionsInstance) {
        (questionsInstance.questionPoints==null)?'':questionsInstance.questionPoints.replace(";",".\n")
        (questionsInstance.questionMaturity==null)?'':questionsInstance.questionMaturity.replace(";",".\n")

        respond questionsInstance
    }

    def create() {
        respond new Questions(params)
    }

    @Transactional
    def save(Questions questionsInstance) {
        if (questionsInstance == null) {
            notFound();
            return
        }

        if (questionsInstance.questionName != null)
        {
            def questionExist = Questions.findByQuestionName(questionsInstance.questionName)
            if (questionExist != null)
            {
                flash.type="danger"
                flash.message = message(code: 'default.null.message51')
                render view: 'create', model: [questionsInstance:questionsInstance]
                return
            }
        }

        if (questionsInstance.hasErrors()) {
            flash.type="danger"
            flash.message = message(code: 'default.null.message2')
            render view: 'create', model: [questionsInstance:questionsInstance]
            return
        }

        def listEvaluations = Evaluation.findAllByQuestionnaire(questionsInstance.questionnairegroup.questionnaire);

        if (listEvaluations.size()>0)
        {

            def  listGroup = Questionnairegroup.findAllByQuestionnaire(questionsInstance.questionnairegroup.questionnaire)

            def questionnaire = new Questionnaire();
            questionnaire.questionnaireName = "Espace TT "+((Questionnaire.list().size())+1)
            questionnaire.enable_q = false

            questionnaire.save flush:true


            for(quGroupe in listGroup)
            {
                def groupeQuestion = new Questionnairegroup();
                groupeQuestion.questionnaire=questionnaire
                groupeQuestion.color=quGroupe.color
                groupeQuestion.questionnairegroupName=quGroupe.questionnairegroupName
                groupeQuestion.questionnairegroupPonderation=quGroupe.questionnairegroupPonderation

                groupeQuestion.save flush:true

                def listQuestions = Questions.findAllByQuestionnairegroup(quGroupe)

                for(qs in listQuestions)
                {
                    def q = new Questions()
                    q.questionMaturity = qs.questionMaturity
                    q.questionnairegroup = qs.questionnairegroup
                    q.questionName = qs.questionName
                    q.questionPoints = qs.questionPoints
                    q.save flush:true
                }

            }

            def groupName = questionsInstance.questionnairegroup.questionnairegroupName

            def group = Questionnairegroup.findByQuestionnairegroupName(groupName);

            questionsInstance.questionnairegroup=group
        }

        questionsInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.created.message', args: [message(code: 'questions.label', default: 'com.ef.app.qm.Questions'), questionsInstance.id])
                redirect questionsInstance
            }
            '*' { respond questionsInstance, [status: CREATED] }
        }
    }

    def edit(Questions questionsInstance) {
        respond questionsInstance,model:[naExist:questionsInstance.na,questionnairegroupIdExist:questionsInstance.questionnairegroup.id,questionNameExist:questionsInstance.questionName]

    }

    @Transactional
    def update(Questions questionsInstance) {
        if (questionsInstance == null) {
            notFound();
            return
        }

        if (questionsInstance.hasErrors()) {
            flash.type="danger"
            flash.message = message(code: 'default.null.message2')
            render view: 'edit', model: [questionsInstance:questionsInstance]
            return
        }

        def idf = questionsInstance.id

        def listEvaluations = Evaluation.findAllByQuestionnaire(questionsInstance.questionnairegroup.questionnaire);

        if (listEvaluations.size() == 0)
        {
            questionsInstance.save flush:true
            idf = questionsInstance.id
        }
        else
        {

            def naExist = params.edit_naExist
            def questionnairegroupIdExist = params.edit_questionnairegroupIdExist
            def questionNameExist = params.edit_questionNameExist

            System.out.println("\n naExist "+naExist);

            if (questionsInstance.na != naExist || questionsInstance.questionnairegroup.id != questionnairegroupIdExist || questionsInstance.questionName != questionNameExist)
            {

                def  listGroup = Questionnairegroup.findAllByQuestionnaire(questionsInstance.questionnairegroup.questionnaire)

                def questionnaire = new Questionnaire();
                questionnaire.questionnaireName = "Espace TT "+((Questionnaire.list().size())+1)
                questionnaire.enable_q = false
                questionnaire.save flush:true

                for(quGroupe in listGroup)
                {

                    def groupeQuestion = new Questionnairegroup();
                    groupeQuestion.questionnaire=questionnaire
                    groupeQuestion.color=quGroupe.color
                    groupeQuestion.questionnairegroupName=quGroupe.questionnairegroupName
                    groupeQuestion.questionnairegroupPonderation=quGroupe.questionnairegroupPonderation
                    groupeQuestion.save flush:true

                    def listQuestions = Questions.findAllByQuestionnairegroup(quGroupe)

                    for(qs in listQuestions)
                    {
                        def q = new Questions()
                        q.questionMaturity = qs.questionMaturity
                        q.questionnairegroup = groupeQuestion
                        q.questionName = qs.questionName
                        q.questionPoints = qs.questionPoints
                        q.na = qs.na
                        q.save flush:true
                    }
                }
                idf = questionsInstance.id

                questionsInstance.questionName= questionNameExist
                questionsInstance.na= naExist
                questionsInstance.questionnairegroup= Questionnairegroup.get(Integer.parseInt(""+questionnairegroupIdExist))

                questionsInstance.save flush:true

            }
            else
            {
                questionsInstance.save flush:true
                idf = questionsInstance.id
            }
        }


        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.updated.message')
                redirect(uri: "/questions")
            }
            '*'{ respond questionsInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Questions questionsInstance) {

        if (questionsInstance == null) {
            notFound()
            return
        }

        questionsInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'com.ef.app.qm.Questions.label', default: 'com.ef.app.qm.Questions'), questionsInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'questions.label', default: 'com.ef.app.qm.Questions'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
