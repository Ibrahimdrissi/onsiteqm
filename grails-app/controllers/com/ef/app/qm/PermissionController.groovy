package com.ef.app.qm

import org.springframework.dao.DataIntegrityViolationException

class PermissionController {

    static allowedMethods = [create: ['GET', 'POST'], edit: ['GET', 'POST'], delete: 'GET']

    def index() {
        redirect action: 'list', params: params
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [permissionInstanceList: Permission.list(), permissionInstanceTotal: Permission.count(),controllerName:'listPermi']
    }

    def create() {
		switch (request.method) {
		case 'GET':
        	[permissionInstance: new Permission(params),controllerName:'listPermi']
			break
		case 'POST':
	        def permissionInstance = new Permission(params)
	        if (!permissionInstance.save(flush: true)) {
	            render view: 'create', model: [permissionInstance: permissionInstance,controllerName:'listPermi']
	            return
	        }

			flash.message = message(code: 'default.created.message', args: [message(code: 'com.ef.cbr.entity.permission', default: 'com.ef.app.qm.Permission'), permissionInstance.name])
	        flash.type="success"
	        redirect action: 'list'
			break
		}
    }
    def createModal() {
        switch (request.method) {
            case 'GET':
                render template:'modalCreate',model:[permissionInstance: new Permission(params),controllerName:'listPermi']
                break
            case 'POST':
                def permissionInstance = new Permission(params)
                if (!permissionInstance.save(flush: true)) {
                    render view: 'create', model: [permissionInstance: permissionInstance,controllerName:'listPermi']
                    return
                }

                flash.message = message(code: 'default.created.message', args: [message(code: 'com.ef.cbr.entity.permission', default: 'com.ef.app.qm.Permission'), permissionInstance.name])
                flash.type="success"
                redirect action: 'list'
                break
        }
    }
    def show() {
        def permissionInstance = Permission.get(params.id)
        if (!permissionInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'com.ef.cbr.entity.permission', default: 'com.ef.app.qm.Permission'), params.id])
            flash.type="danger"
            redirect action: 'list'
            return
        }

        [permissionInstance: permissionInstance,controllerName: 'listPermi']
    }

    def edit() {
		switch (request.method) {
		case 'GET':
	        def permissionInstance = Permission.get(params.id)
	        if (!permissionInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'com.ef.cbr.entity.permission', default: 'com.ef.app.qm.Permission'), params.id])
	            flash.type="danger"
	            redirect action: 'list'
	            return
	        }

	        [permissionInstance: permissionInstance,controllerName: 'listPermi']
			break
		case 'POST':
	        def permissionInstance = Permission.get(params.id)
	        if (!permissionInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'com.ef.cbr.entity.permission', default: 'com.ef.app.qm.Permission'), params.id])
	            flash.type="danger"
	            redirect action: 'list'
	            return
	        }

	        if (params.version) {
	            def version = params.version.toLong()
	            if (permissionInstance.version > version) {
	                permissionInstance.errors.rejectValue('version', 'default.optimistic.locking.failure',
	                          [message(code: 'com.ef.cbr.entity.permission', default: 'com.ef.app.qm.Permission')] as Object[],
	                          "Another user has updated this com.ef.app.qm.Permission while you were editing")
	                render view: 'edit', model: [permissionInstance: permissionInstance]
	                return
	            }
	        }

	        permissionInstance.properties = params

	        if (!permissionInstance.save(flush: true)) {
	            render view: 'edit', model: [permissionInstance: permissionInstance]
	            return
	        }

			flash.message = message(code: 'default.updated.message', args: [message(code: 'com.ef.cbr.entity.permission', default: 'com.ef.app.qm.Permission'), permissionInstance.name])
	        flash.type="success"
	        redirect action: 'list'
			break
		}
    }
    def editModal() {
        switch (request.method) {
            case 'GET':
                def permissionInstance = Permission.get(params.id)
                if (!permissionInstance) {
                    flash.message = message(code: 'default.not.found.message', args: [message(code: 'com.ef.cbr.entity.permission', default: 'com.ef.app.qm.Permission'), params.id])
                    flash.type="danger"
                    redirect action: 'list'
                    return
                }

                render(template: 'modalEdit' ,model: [permissionInstance: permissionInstance,controllerName: 'listPermi'] )
                break
            case 'POST':
                def permissionInstance = Permission.get(params.id)
                if (!permissionInstance) {
                    flash.message = message(code: 'default.not.found.message', args: [message(code: 'com.ef.cbr.entity.permission', default: 'com.ef.app.qm.Permission'), params.id])
                    flash.type="danger"
                    redirect action: 'list'
                    return
                }

                if (params.version) {
                    def version = params.version.toLong()
                    if (permissionInstance.version > version) {
                        permissionInstance.errors.rejectValue('version', 'default.optimistic.locking.failure',
                                [message(code: 'com.ef.cbr.entity.permission', default: 'com.ef.app.qm.Permission')] as Object[],
                                "Another user has updated this com.ef.app.qm.Permission while you were editing")
                        render view: 'edit', model: [permissionInstance: permissionInstance]
                        return
                    }
                }

                permissionInstance.properties = params

                if (!permissionInstance.save(flush: true)) {
                    render view: 'edit', model: [permissionInstance: permissionInstance]
                    return
                }

                flash.message = message(code: 'default.updated.message', args: [message(code: 'com.ef.cbr.entity.permission', default: 'com.ef.app.qm.Permission'), permissionInstance.name])
                flash.type="success"
                redirect action: 'list'
                break
        }
    }
    def delete() {
        def permissionInstance = Permission.get(params.id)
        if (!permissionInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'com.ef.cbr.entity.permission', default: 'com.ef.app.qm.Permission'), params.id])
            flash.type="danger"
            redirect action: 'list'
            return
        }

        try {
            permissionInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'com.ef.cbr.entity.permission', default: 'com.ef.app.qm.Permission'), params.id])
            flash.type="success"
            redirect action: 'list'
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'com.ef.cbr.entity.permission', default: 'com.ef.app.qm.Permission'), params.id])
            flash.type="danger"
            redirect action: 'show', id: params.id
        }
    }

    def addAllPermission(){

        Permission.deleteAll(Permission.getAll())
        if(Permission.count == 0){
            println "Permissions not exist"
            new Permission(isManaged: true, expression: '*:*', name: 'com.ef.cbr.bootstrap.all',
                    description: 'com.ef.cbr.bootstrap.all.permission.Admin').save()
            new Permission(isManaged: true, expression: 'settings:*', name: 'com.ef.cbr.bootstrap.config',
                    description: 'com.ef.cbr.bootstrap.config.parameter').save()

            println "user Permissions"
            // com.ef.app.qm.User related Permissions
            new Permission(isManaged: true, expression: 'user:*', name: 'com.ef.cbr.bootstrap.all.user.name',
                    description: 'com.ef.cbr.bootstrap.all.user').save()
            new Permission(isManaged: true, expression: 'user:show', name: 'com.ef.cbr.bootstrap.show.user.name',
                    description: 'com.ef.cbr.bootstrap.show.user').save()
            new Permission(isManaged: true, expression: 'user:create,createForModal,save', name: 'com.ef.cbr.bootstrap.create.user.name',
                    description: 'com.ef.cbr.bootstrap.create.user').save()
            new Permission(isManaged: true, expression: 'user:edit,editpop,update', name: 'com.ef.cbr.bootstrap.edit.user.name',
                    description: 'com.ef.cbr.bootstrap.edit.user').save()
            new Permission(isManaged: true, expression: 'user:list', name: 'com.ef.cbr.bootstrap.list.user.name',
                    description: 'com.ef.cbr.bootstrap.list.user').save()
            new Permission(isManaged: true, expression: 'user:delete', name: 'com.ef.cbr.bootstrap.delete.user.name',
                    description: 'com.ef.cbr.bootstrap.delete.user').save()
            new Permission(isManaged: true, expression: 'user:activateuser', name: 'com.ef.cbr.bootstrap.activate.user.name',
                    description: 'com.ef.cbr.bootstrap.activate.user').save()
            new Permission(isManaged: true, expression: 'user:blockuser', name: 'com.ef.cbr.bootstrap.block.user.name',
                    description: 'com.ef.cbr.bootstrap.block.user').save()
            new Permission(isManaged: true, expression: 'user:viewroles', name: 'com.ef.cbr.bootstrap.role.user.name',
                    description: 'com.ef.cbr.bootstrap.role.user').save()
            new Permission(isManaged: true, expression: 'user:assignrole', name: 'com.ef.cbr.bootstrap.assignRole.user.name',
                    description: 'com.ef.cbr.bootstrap.assignRole.user').save()
            new Permission(isManaged: true, expression: 'user:revokerole', name: 'com.ef.cbr.bootstrap.revokeRole.user.name',
                    description: 'com.ef.cbr.bootstrap.revokeRole.user').save()
            new Permission(isManaged: true, expression: 'user:viewpermissions', name: 'com.ef.cbr.bootstrap.viewPermission.user.name',
                    description: 'com.ef.cbr.bootstrap.viewPermission.user').save()
            new Permission(isManaged: true, expression: 'user:assignpermission', name: 'com.ef.cbr.bootstrap.assignPermission.user.name',
                    description: 'com.ef.cbr.bootstrap.assignPermission.user').save()
            new Permission(isManaged: true, expression: 'user:revokepermission', name: 'com.ef.cbr.bootstrap.revokePermission.user.name',
                    description: 'com.ef.cbr.bootstrap.revokePermission.user').save()
            new Permission(isManaged: true, expression: 'user:resetPassword', name: 'com.ef.cbr.bootstrap.resetPassword.user.name',
                    description: 'com.ef.cbr.bootstrap.resetPassword.user').save()
            println "com.ef.app.qm.Role Permissions"
            // com.ef.app.qm.Role related permissions
            new Permission(isManaged: true, expression: 'role:*', name: 'com.ef.cbr.bootstrap.all.role.name',
                    description: 'com.ef.cbr.bootstrap.all.role').save()
            new Permission(isManaged: true, expression: 'role:show', name: 'com.ef.cbr.bootstrap.show.role.name',
                    description: 'com.ef.cbr.bootstrap.show.role').save()
            new Permission(isManaged: true, expression: 'role:create', name: 'com.ef.cbr.bootstrap.create.role.name',
                    description: 'Can create new com.ef.app.qm.Role').save()
            new Permission(isManaged: true, expression: 'role:edit', name: 'com.ef.cbr.bootstrap.edit.role.name',
                    description: 'com.ef.cbr.bootstrap.edit.role').save()
            new Permission(isManaged: true, expression: 'role:list', name: 'com.ef.cbr.bootstrap.list.role.name',
                    description: 'com.ef.cbr.bootstrap.list.role').save()
            new Permission(isManaged: true, expression: 'role:delete', name: 'com.ef.cbr.bootstrap.delete.role.name',
                    description: 'com.ef.cbr.bootstrap.delete.role').save()
            new Permission(isManaged: true, expression: 'role:viewmembers', name: 'com.ef.cbr.bootstrap.viewMember.role.name',
                    description: 'com.ef.cbr.bootstrap.viewMember.role').save()
            new Permission(isManaged: true, expression: 'role:viewpermissions', name: 'com.ef.cbr.bootstrap.viewPermission.role.name',
                    description: 'com.ef.cbr.bootstrap.viewPermission.role').save()
            new Permission(isManaged: true, expression: 'role:assignpermission', name: 'com.ef.cbr.bootstrap.assignPermission.role.name',
                    description: 'com.ef.cbr.bootstrap.assignPermission.role').save()
            new Permission(isManaged: true, expression: 'role:revokepermission', name: 'com.ef.cbr.bootstrap.revokePermission.role.name',
                    description: 'com.ef.cbr.bootstrap.revokePermission.role').save()
            new Permission(isManaged: true, expression: 'role:assignmembership', name: 'com.ef.cbr.bootstrap.addMember.role.name',
                    description: 'com.ef.cbr.bootstrap.addMember.role').save()
            new Permission(isManaged: true, expression: 'role:revokemembership', name: 'com.ef.cbr.bootstrap.revokeMember.role.name',
                    description: 'com.ef.cbr.bootstrap.revokeMember.role').save()
            println "Customer Permissions"
            // Customer related permissions
            new Permission(isManaged: true, expression: 'businessUnit:*', name: 'com.ef.cbr.bootstrap.all.businessUnit.name',
                    description: 'com.ef.cbr.bootstrap.all.businessUnit').save()
            new Permission(isManaged: true, expression: 'businessUnit:show', name: 'com.ef.cbr.bootstrap.show.businessUnit.name',
                    description: 'com.ef.cbr.bootstrap.show.businessUnit').save()
            new Permission(isManaged: true, expression: 'businessUnit:create', name: 'com.ef.cbr.bootstrap.create.businessUnit.name',
                    description: 'com.ef.cbr.bootstrap.create.businessUnit').save()
            new Permission(isManaged: true, expression: 'v:edit', name: 'com.ef.cbr.bootstrap.edit.businessUnit.name',
                    description: 'com.ef.cbr.bootstrap.edit.businessUnit').save()
            new Permission(isManaged: true, expression: 'businessUnit:list', name: 'com.ef.cbr.bootstrap.list.businessUnit.name',
                    description: 'com.ef.cbr.bootstrap.list.businessUnit').save()
            new Permission(isManaged: true, expression: 'businessUnit:delete', name: 'com.ef.cbr.bootstrap.delete.businessUnit.name',
                    description: 'com.ef.cbr.bootstrap.delete.businessUnit').save()


           println 'Customer related permissions'
            // Customer related permissions
            new Permission(isManaged: true, expression: 'tagEntry:*', name: 'com.ef.cbr.bootstrap.all.caller.name',
                    description: 'com.ef.cbr.bootstrap.all.caller').save()
            new Permission(isManaged: true, expression: 'tagEntry:show', name: 'com.ef.cbr.bootstrap.show.caller.name',
                    description: 'com.ef.cbr.bootstrap.show.caller').save()
            new Permission(isManaged: true, expression: 'tagEntry:create', name: 'com.ef.cbr.bootstrap.create.caller.name',
                    description: 'com.ef.cbr.bootstrap.create.caller').save()
            new Permission(isManaged: true, expression: 'v:edit', name: 'com.ef.cbr.bootstrap.edit.caller.name',
                    description: 'com.ef.cbr.bootstrap.edit.caller').save()
            new Permission(isManaged: true, expression: 'tagEntry:list', name: 'com.ef.cbr.bootstrap.list.caller.name',
                    description: 'com.ef.cbr.bootstrap.list.caller').save()
            new Permission(isManaged: true, expression: 'tagEntry:delete', name: 'com.ef.cbr.bootstrap.delete.caller.name',
                    description: 'com.ef.cbr.bootstrap.delete.caller').save()




            println 'Segment related permissions'
            // Segment related permissions
            new Permission(isManaged: true, expression: 'segment:*', name: 'com.ef.cbr.bootstrap.all.segment.name',
                    description: 'com.ef.cbr.bootstrap.all.segment').save()
            new Permission(isManaged: true, expression: 'segment:show', name: 'com.ef.cbr.bootstrap.show.segment.name',
                    description: 'com.ef.cbr.bootstrap.show.segment').save()
            new Permission(isManaged: true, expression: 'segment:create', name: 'com.ef.cbr.bootstrap.create.segment.name',
                    description: 'com.ef.cbr.bootstrap.create.segment').save()
            new Permission(isManaged: true, expression: 'segment:edit', name: 'com.ef.cbr.bootstrap.edit.segment.name',
                    description: 'com.ef.cbr.bootstrap.edit.segment').save()
            new Permission(isManaged: true, expression: 'segment:list', name: 'com.ef.cbr.bootstrap.list.segment.name',
                    description: 'com.ef.cbr.bootstrap.list.segment').save()
            new Permission(isManaged: true, expression: 'segment:delete', name: 'com.ef.cbr.bootstrap.delete.segment.name',
                    description: 'com.ef.cbr.bootstrap.delete.segment').save()

            println "Region related permissions"
            // Region related permissions
            new Permission(isManaged: true, expression: 'region:*', name: 'com.ef.cbr.bootstrap.all.region.name',
                    description: 'com.ef.cbr.bootstrap.all.region').save()
            new Permission(isManaged: true, expression: 'region:show', name: 'com.ef.cbr.bootstrap.show.region.name',
                    description: 'com.ef.cbr.bootstrap.show.region').save()
            new Permission(isManaged: true, expression: 'region:create', name: 'com.ef.cbr.bootstrap.create.region.name',
                    description: 'com.ef.cbr.bootstrap.create.region').save()
            new Permission(isManaged: true, expression: 'region:edit', name: 'com.ef.cbr.bootstrap.edit.region.name',
                    description: 'com.ef.cbr.bootstrap.edit.region').save()
            new Permission(isManaged: true, expression: 'region:list', name: 'com.ef.cbr.bootstrap.list.region.name',
                    description: 'com.ef.cbr.bootstrap.list.region').save()
            new Permission(isManaged: true, expression: 'region:delete', name: 'com.ef.cbr.bootstrap.delete.region.name',
                    description: 'com.ef.cbr.bootstrap.delete.region').save()

            println "Service related permissions"
            // Service related permissions
            new Permission(isManaged: true, expression: 'service:*', name: 'com.ef.cbr.bootstrap.all.service.name',
                    description: 'com.ef.cbr.bootstrap.all.service').save()
            new Permission(isManaged: true, expression: 'service:show', name: 'com.ef.cbr.bootstrap.show.service.name',
                    description: 'com.ef.cbr.bootstrap.show.service').save()
            new Permission(isManaged: true, expression: 'service:create', name: 'com.ef.cbr.bootstrap.create.service.name',
                    description: 'com.ef.cbr.bootstrap.create.service').save()
            new Permission(isManaged: true, expression: 'service:edit', name: 'com.ef.cbr.bootstrap.edit.service.name',
                    description: 'com.ef.cbr.bootstrap.edit.service').save()
            new Permission(isManaged: true, expression: 'service:list', name: 'com.ef.cbr.bootstrap.list.service.name',
                    description: 'com.ef.cbr.bootstrap.list.service').save()
            new Permission(isManaged: true, expression: 'service:delete', name: 'com.ef.cbr.bootstrap.delete.service.name',
                    description: 'com.ef.cbr.bootstrap.delete.service').save()

            println "Announcement related permissions"
            // Announcement related permissions
            new Permission(isManaged: true, expression: 'announcement:*', name: 'com.ef.cbr.bootstrap.all.announcement.name',
                    description: 'com.ef.cbr.bootstrap.all.announcement').save()
            new Permission(isManaged: true, expression: 'announcement:show', name: 'com.ef.cbr.bootstrap.show.announcement.name',
                    description: 'com.ef.cbr.bootstrap.show.announcement').save()
            new Permission(isManaged: true, expression: 'announcement:create', name: 'com.ef.cbr.bootstrap.create.announcement.name',
                    description: 'com.ef.cbr.bootstrap.create.announcement').save()
            new Permission(isManaged: true, expression: 'announcement:edit', name: 'com.ef.cbr.bootstrap.edit.announcement.name',
                    description: 'com.ef.cbr.bootstrap.edit.announcement').save()
            new Permission(isManaged: true, expression: 'announcement:list', name: 'com.ef.cbr.bootstrap.list.announcement.name',
                    description: 'com.ef.cbr.bootstrap.list.announcement').save()
            new Permission(isManaged: true, expression: 'announcement:delete', name: 'com.ef.cbr.bootstrap.delete.announcement.name',
                    description: 'com.ef.cbr.bootstrap.delete.announcement').save()

            println "EasyAnnouncement related permissions"
            // EasyAnnouncement related permissions
            new Permission(isManaged: true, expression: 'easyAnnouncement:*', name: 'com.ef.cbr.bootstrap.all.easyAnnouncement.name',
                    description: 'com.ef.cbr.bootstrap.all.easyAnnouncement').save()
            new Permission(isManaged: true, expression: 'easyAnnouncement:show', name: 'com.ef.cbr.bootstrap.show.easyAnnouncement.name',
                    description: 'com.ef.cbr.bootstrap.show.easyAnnouncement').save()
            new Permission(isManaged: true, expression: 'easyAnnouncement:create', name: 'com.ef.cbr.bootstrap.create.easyAnnouncement.name',
                    description: 'com.ef.cbr.bootstrap.create.easyAnnouncement').save()
            new Permission(isManaged: true, expression: 'easyAnnouncement:edit', name: 'com.ef.cbr.bootstrap.edit.easyAnnouncement.name',
                    description: 'com.ef.cbr.bootstrap.edit.easyAnnouncement').save()
            new Permission(isManaged: true, expression: 'easyAnnouncement:list', name: 'com.ef.cbr.bootstrap.list.easyAnnouncement.name',
                    description: 'com.ef.cbr.bootstrap.list.easyAnnouncement').save()
            new Permission(isManaged: true, expression: 'easyAnnouncement:delete', name: 'com.ef.cbr.bootstrap.delete.easyAnnouncement.name',
                    description: 'com.ef.cbr.bootstrap.delete.easyAnnouncement').save()


            println "Tag related permissions"
            // Tag related permissions
            new Permission(isManaged: true, expression: 'tag:*', name: 'com.ef.cbr.bootstrap.all.callerList.name',
                    description: 'com.ef.cbr.bootstrap.all.callerList').save()
            new Permission(isManaged: true, expression: 'tag:show', name: 'com.ef.cbr.bootstrap.show.callerList.name',
                    description: 'com.ef.cbr.bootstrap.show.callerList').save()
            new Permission(isManaged: true, expression: 'tag:create', name: 'com.ef.cbr.bootstrap.create.callerList.name',
                    description: 'com.ef.cbr.bootstrap.create.callerList').save()
            new Permission(isManaged: true, expression: 'tag:edit', name: 'com.ef.cbr.bootstrap.edit.callerList.name',
                    description: 'com.ef.cbr.bootstrap.edit.callerList').save()
            new Permission(isManaged: true, expression: 'tag:list', name: 'com.ef.cbr.bootstrap.list.callerList.name',
                    description: 'com.ef.cbr.bootstrap.list.callerList').save()
            new Permission(isManaged: true, expression: 'tag:delete', name: 'com.ef.cbr.bootstrap.delete.callerList.name',
                    description: 'com.ef.cbr.bootstrap.delete.callerList').save()

            println "Event related permissions"
            // Event related permissions
            new Permission(isManaged: true, expression: 'event:*', name: 'com.ef.cbr.bootstrap.all.event.name',
                    description: 'com.ef.cbr.bootstrap.all.event').save()
            new Permission(isManaged: true, expression: 'event:show', name: 'com.ef.cbr.bootstrap.show.event.name',
                    description: 'com.ef.cbr.bootstrap.show.event').save()
            new Permission(isManaged: true, expression: 'event:create', name: 'com.ef.cbr.bootstrap.create.event.name',
                    description: 'com.ef.cbr.bootstrap.create.event').save()
            new Permission(isManaged: true, expression: 'event:edit', name: 'com.ef.cbr.bootstrap.edit.event.name',
                    description: 'com.ef.cbr.bootstrap.edit.event').save()
            new Permission(isManaged: true, expression: 'event:list', name: 'com.ef.cbr.bootstrap.list.event.name',
                    description: 'com.ef.cbr.bootstrap.list.event').save()
            new Permission(isManaged: true, expression: 'event:delete', name: 'com.ef.cbr.bootstrap.delete.event.name',
                    description: 'com.ef.cbr.bootstrap.delete.event').save()

            println "KPI related permissions"
            //KPI related permissions
            new Permission(isManaged: true, expression: 'kpi:*', name: 'com.ef.cbr.bootstrap.all.kpi.name',
                    description: 'com.ef.cbr.bootstrap.all.kpi').save()
            new Permission(isManaged: true, expression: 'kpi:show', name: 'com.ef.cbr.bootstrap.show.kpi.name',
                    description: 'com.ef.cbr.bootstrap.show.kpi').save()
            new Permission(isManaged: true, expression: 'kpi:create', name: 'com.ef.cbr.bootstrap.create.kpi.name',
                    description: 'com.ef.cbr.bootstrap.create.kpi').save()
            new Permission(isManaged: true, expression: 'kpi:edit', name: 'com.ef.cbr.bootstrap.edit.kpi.name',
                    description: 'com.ef.cbr.bootstrap.edit.kpi').save()
            new Permission(isManaged: true, expression: 'kpi:list', name: 'com.ef.cbr.bootstrap.list.kpi.name',
                    description: 'com.ef.cbr.bootstrap.list.kpi').save()
            new Permission(isManaged: true, expression: 'kpi:delete', name: 'com.ef.cbr.bootstrap.delete.kpi.name',
                    description: 'com.ef.cbr.bootstrap.delete.kpi').save()

            println "ServiceKPI related permissions"
            new Permission(isManaged: true, expression: 'serviceKpi:*', name: 'com.ef.cbr.bootstrap.all.servicekpi.name',
                    description: 'com.ef.cbr.bootstrap.all.servicekpi').save()
            new Permission(isManaged: true, expression: 'serviceKpi:show', name: 'com.ef.cbr.bootstrap.show.servicekpi.name',
                    description: 'com.ef.cbr.bootstrap.show.servicekpi').save()
            new Permission(isManaged: true, expression: 'serviceKpi:create', name: 'com.ef.cbr.bootstrap.create.servicekpi.name',
                    description: 'Can create new ServiceKpi').save()
            new Permission(isManaged: true, expression: 'serviceKpi:edit', name: 'com.ef.cbr.bootstrap.edit.servicekpi.name',
                    description: 'com.ef.cbr.bootstrap.edit.servicekpi').save()
            new Permission(isManaged: true, expression: 'serviceKpi:list', name: 'com.ef.cbr.bootstrap.list.servicekpi.name',
                    description: 'com.ef.cbr.bootstrap.list.servicekpi').save()
            new Permission(isManaged: true, expression: 'serviceKpi:delete', name: 'com.ef.cbr.bootstrap.delete.servicekpi.name',
                    description: 'com.ef.cbr.bootstrap.delete.servicekpi').save()
            println"configuration Setting"
            new Permission(isManaged: true, expression: 'configurationSetting:*', name: 'com.ef.cbr.bootstrap.all.configurationSetting.name',
                    description: 'com.ef.cbr.bootstrap.all.configurationSetting').save()

            new Permission(isManaged: true,expression: 'settings:*',name: 'com.ef.cbr.bootstrap.all.setting.name',description: 'com.ef.cbr.bootstrap.all.setting').save()
            // TODO DialNumber related permissions

            println "report Audit Report"
            // report Audit Report
            new Permission(isManaged: true,expression: 'report:servicesAudit,serviceAuditReport,printReport,exportToExcel',name:'com.ef.cbr.bootstrap.report.serviceAudit.name' , description: 'com.ef.cbr.bootstrap.report.serviceAudit').save()
            new Permission(isManaged: true,expression: 'report:cbrAuditReport,submitCbrAudit,printReport,auditExportToExcel',name:'com.ef.cbr.bootstrap.permission.callerListAudit.name' , description: 'com.ef.cbr.bootstrap.permission.callerListAudit').save()
            println "Purging Setting"
            // Purging Setting
            new Permission(isManaged: true, expression: 'purgingSetting:*', name: 'com.ef.cbr.bootstrap.all.purgingSetting.name', description: 'com.ef.cbr.bootstrap.all.puringSetting').save()
            // Default Administrator com.ef.app.qm.User
            redirect controller: 'permission',action: 'index'
        }
        else{
            println "Permissions exist"
            redirect controller: 'permission',action: 'index'
        }

    }
}