package com.ef.app.qm

import com.ef.app.qm.Space

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class SpaceController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Space.list(params), model:[spaceInstanceCount: Space.count()]
    }

    def show(Space spaceInstance) {
        respond spaceInstance
    }

    def create() {
        respond new Space(params)
    }

    @Transactional
    def save(Space spaceInstance) {
        if (spaceInstance == null) {
            notFound()
            return
        }

        def spaceByName = Space.findBySpaceName(spaceInstance.spaceName)
        if(spaceByName != null){
            flash.type="danger"
            flash.message = message(code: 'com.ef.user.spaceNameExiste')
            render view: 'create', model: [spaceInstance:spaceInstance]
            return
        }

        if (spaceInstance.hasErrors()) {
            flash.type="danger"
            flash.message = message(code: 'default.error.message')
            render view: 'create', model: [spaceInstance:spaceInstance]
            return
        }
        spaceInstance.spaceCode = "ETT"+ (Space.last().id+1)
        spaceInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.created.message', args: [message(code: 'space.label', default: 'Space'), spaceInstance.spaceName])
                redirect spaceInstance
            }
            '*' { respond spaceInstance, [status: CREATED] }
        }
    }

    def edit(Space spaceInstance) {
        respond spaceInstance
    }


    def update(Space spaceInstance) {
        if (spaceInstance == null) {
            notFound()
            return
        }

        def spaceExiste = Space.findBySpaceNameAndIdNotEqual(spaceInstance.spaceName,spaceInstance.id)
        if(spaceExiste != null ){
            flash.type="danger"
            flash.message = message(code: 'com.ef.user.spaceNameExiste')
            render view: 'edit', model: [spaceInstance:spaceInstance]
            return
        }

        if (spaceInstance.hasErrors()) {
            flash.type="danger"
            flash.message = message(code: 'default.error.message')
            render view: 'edit', model: [spaceInstance:spaceInstance]
            return
        }

        spaceInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.updated.message', args: [message(code: 'com.ef.app.qm.Space.label', default: 'Space'), spaceInstance.spaceName])
                redirect spaceInstance
            }
            '*'{ respond spaceInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Space spaceInstance) {

        if (spaceInstance == null) {
            notFound()
            return
        }

        spaceInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'com.ef.app.qm.Space.label', default: 'Space'), spaceInstance.spaceName])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.type="danger"
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'space.label', default: 'Space'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
