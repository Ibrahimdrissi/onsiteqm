package com.ef.app.qm

import com.ef.app.qm.Questionnairegroup

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class QuestionnairegroupController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Questionnairegroup.list(params), model:[questionnairegroupInstanceCount: Questionnairegroup.count()]
    }

    def show(Questionnairegroup questionnairegroupInstance) {
        respond questionnairegroupInstance
    }

    def create() {
        respond new Questionnairegroup(params)
    }

    @Transactional
    def save(Questionnairegroup questionnairegroupInstance) {


        if (questionnairegroupInstance == null) {
            notFound()
            return
        }

        //render questionnairegroupInstance.hasErrors()
        //return

        if (questionnairegroupInstance.hasErrors()) {
            flash.type="danger"
            flash.message = message(code: 'default.null.message2')
            render view: 'create', model: [questionnairegroupInstance:questionnairegroupInstance]
            return
        }

        def listEvaluations = Evaluation.findAllByQuestionnaire(questionnairegroupInstance.questionnaire);

        if (listEvaluations.size()>0)
        {
            def  listGroup = Questionnairegroup.findAllByQuestionnaire(questionnairegroupInstance.questionnaire)

            def questionnaire = new Questionnaire();
            questionnaire.questionnaireName = "Espace TT "+((Questionnaire.list().size())+1)
            questionnaire.enable_q = false

            questionnaire.save flush:true

            for(quGroupe in listGroup)
            {

                def groupeQuestion = new Questionnairegroup();
                groupeQuestion.questionnaire=questionnaire
                groupeQuestion.color=quGroupe.color
                groupeQuestion.questionnairegroupName=quGroupe.questionnairegroupName
                groupeQuestion.questionnairegroupPonderation=quGroupe.questionnairegroupPonderation

                groupeQuestion.save flush:true

                def listQuestions = Questions.findAllByQuestionnairegroup(quGroupe)

                for(qs in listQuestions)
                {

                    def q = new Questions()

                    q.questionMaturity = qs.questionMaturity
                    q.questionnairegroup = Questionnairegroup.get(groupeQuestion.id)
                    q.questionName = qs.questionName
                    q.questionPoints = qs.questionPoints

                    q.save flush:true

                }
            }

            questionnairegroupInstance.questionnaire=questionnaire

        }

        questionnairegroupInstance.color="#"+questionnairegroupInstance.color

        questionnairegroupInstance.save flush:true


        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.created.message', args: [message(code: 'questionnaire.label', default: 'com.ef.app.qm.Questionnairegroup'), questionnairegroupInstance.id])
                redirect questionnairegroupInstance
            }
            '*' { respond questionnairegroupInstance, [status: CREATED] }
        }

    }

    def edit(Questionnairegroup questionnairegroupInstance) {
        respond questionnairegroupInstance
    }


    def update(Questionnairegroup questionnairegroupInstance) {

        if (questionnairegroupInstance == null)
            {
                notFound();
                return
            }

            if (questionnairegroupInstance.hasErrors()) {
                flash.type="danger"
                flash.message = message(code: 'default.null.message2')
                render view: 'edit', model: [questionnairegroupInstance:questionnairegroupInstance]
                return
            }

            def idf = questionnairegroupInstance.id;
            def listEvaluations = Evaluation.findAllByQuestionnaire(questionnairegroupInstance.questionnaire);

            if (listEvaluations.size() == 0)
            {
                questionnairegroupInstance.color="#"+questionnairegroupInstance.color
                questionnairegroupInstance.save flush:true

                idf = questionnairegroupInstance.id;
            }
            else
            {
                def questionnairegroupNameExist = params.questionnairegroupNameExist
                def questionnairegroupPonderationExist = params.questionnairegroupPonderationExist



                if (questionnairegroupNameExist != questionnairegroupInstance.questionnairegroupName ||
                        Integer.parseInt(""+questionnairegroupPonderationExist) != questionnairegroupInstance.questionnairegroupPonderation)
                {

                    def  listGroup = Questionnairegroup.findAllByQuestionnaire(questionnairegroupInstance.questionnaire)

                    def questionnaire = new Questionnaire();
                    questionnaire.questionnaireName = "Espace TT "+((Questionnaire.list().size())+1)
                    questionnaire.enable_q = false
                    questionnaire.save()

                    for(quGroupe in listGroup)
                    {
                        def groupeQuestion = new Questionnairegroup();
                        groupeQuestion.questionnaire=questionnaire
                        groupeQuestion.color="#"+quGroupe.color.replace('#','')
                        groupeQuestion.questionnairegroupName=quGroupe.questionnairegroupName
                        groupeQuestion.questionnairegroupPonderation=quGroupe.questionnairegroupPonderation
                        groupeQuestion.save()

                        def listQuestions = Questions.findAllByQuestionnairegroup(quGroupe)

                        for(qs in listQuestions)
                        {
                            def q = new Questions()
                            q.questionMaturity = qs.questionMaturity
                            q.questionnairegroup = groupeQuestion
                            q.questionName = qs.questionName
                            q.questionPoints = qs.questionPoints
                            q.na = qs.na
                            q.save()
                        }
                    }


                    idf = Questionnairegroup.findAllByQuestionnairegroupName(questionnairegroupInstance.questionnairegroupName).last().id


                }
                else
                {
                    questionnairegroupInstance.color="#"+questionnairegroupInstance.color

                    questionnairegroupInstance.save flush:true
                }
            }


            request.withFormat {
                form multipartForm {
                    flash.type="success"
                    flash.message = message(code: 'default.updated.message')
                    redirect(uri: "/questionnairegroup")
                }
                '*' { respond questionnairegroupInstance, [status: OK] }
            }
    }

    @Transactional
    def delete(Questionnairegroup questionnairegroupInstance) {
        if (questionnairegroupInstance == null) {
            notFound()
            return
        }

        def listEvaluations = Evaluation.findAllByQuestionnaire(questionnairegroupInstance.questionnaire);

        if (listEvaluations.size()>0)
        {
            def  listGroup = Questionnairegroup.findAllByQuestionnaire(questionnairegroupInstance.questionnaire)

            def questionnaire = new Questionnaire();
            questionnaire.questionnaireName = "Espace TT "+((Questionnaire.list().size())+1)
            questionnaire.enable_q = false

            questionnaire.save flush:true


            for(quGroupe in listGroup)
            {
                def groupeQuestion = new Questionnairegroup();
                groupeQuestion.questionnaire=questionnaire
                groupeQuestion.color=quGroupe.color
                groupeQuestion.questionnairegroupName=quGroupe.questionnairegroupName
                groupeQuestion.questionnairegroupPonderation=quGroupe.questionnairegroupPonderation

                groupeQuestion.save flush:true

                def listQuestions = Questions.findAllByQuestionnairegroup(quGroupe)

                for(qs in listQuestions)
                {
                    def q = new Questions()

                    q.questionMaturity = qs.questionMaturity
                    q.questionnairegroup = qs.questionnairegroup
                    q.questionName = qs.questionName
                    q.questionPoints = qs.questionPoints

                    q.save flush:true
                }
            }
        }
        else
        {
            questionnairegroupInstance.delete flush:true
        }


        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'com.ef.app.qm.Questionnairegroup.label', default: 'com.ef.app.qm.Questionnairegroup'), questionnairegroupInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'questionnairegroup.label', default: 'com.ef.app.qm.Questionnairegroup'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
