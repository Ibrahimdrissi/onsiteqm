package com.ef.app.qm

import com.ef.app.qm.Profile

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ProfileController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Profile.list(params), model:[profileInstanceCount: Profile.count()]
    }

    def show(Profile profileInstance) {
        respond profileInstance
    }

    def create() {
        respond new Profile(params)
    }

    @Transactional
    def save(Profile profileInstance) {
        if (profileInstance == null) {
            notFound()
            return
        }

        def profileExiste = Profile.findByProfilName(profileInstance.profilName)
        if(profileExiste != null){
            flash.type="danger"
            flash.message = message(code: 'com.ef.team.profileExiste')
            render view: 'create', model: [profileInstance:profileInstance]
            return
        }

        if (profileInstance.hasErrors()) {
            flash.type="danger"
            flash.message = message(code: 'default.error.message')
            render view: 'create', model: [profileInstance:profileInstance]
            return
        }

        profileInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.created.message', args: [message(code: 'profile.label', default: 'Profile'), profileInstance.profilName])
                redirect profileInstance
            }
            '*' { respond profileInstance, [status: CREATED] }
        }
    }

    def edit(Profile profileInstance) {
        respond profileInstance
    }


    def update(Profile profileInstance) {
        if (profileInstance == null) {
            notFound()
            return
        }

        def profileExiste = Profile.findByProfilNameAndIdNotEqual(profileInstance.profilName,profileInstance.id)
        if(profileExiste != null ){
            flash.type="danger"
            flash.message = message(code: 'com.ef.team.profileExiste')
            render view: 'edit', model: [profileInstance:profileInstance]
            return
        }

        if (profileInstance.hasErrors()) {
            flash.type="danger"
            flash.message = message(code: 'default.error.message')
            render view: 'edit', model: [profileInstance:profileInstance]
            return
        }

        profileInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.updated.message', args: [message(code: 'com.ef.app.qm.Profile.label', default: 'Profile'), profileInstance.profilName])
                redirect profileInstance
            }
            '*'{ respond profileInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Profile profileInstance) {

        if (profileInstance == null) {
            notFound()
            return
        }

        profileInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'com.ef.app.qm.Profile.label', default: 'Profile'), profileInstance.profilName])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.type="danger"
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'profile.label', default: 'Profile'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
