package com.ef.app.qm

import org.apache.shiro.SecurityUtils
import org.apache.shiro.authc.UsernamePasswordToken
import org.apache.shiro.web.util.WebUtils
import org.apache.shiro.crypto.hash.Sha256Hash
//import org.apache.shiro.grails.ConfigUtils


class authController {
    def shiroSecurityManager

    def index = {
        redirect(action: "login", params: params)
    }


    def login = {

        //redirect(url: "http://localhost:8081/onsiteqmb")
        //redirect(url: "http://172.20.20.14:8081/Onsiteqm_testing/")


        return [ username: params.username, rememberMe: (params.rememberMe != null), targetUri: params.targetUri ]

    }

    def signIn = {
        log.info("Params com.ef.app.qm.User " +params.username)

        def userInstance
        if(params.username != null ){
            String username = params.username
            try{
                userInstance = User.findByUserName(username)
                userInstance?.lock()
                if (userInstance) {
                    if (!userInstance?.isActive) {
                        flash.message = message(code: "com.ef.qm.login.failed.userBlocked",default:"Login failed, user is blocked")
                        flash.type="danger"
                        // Keep the username and "remember me" setting so that the
                        // user doesn't have to enter them again.
                        def m = [username: params.username]
                        if (params.rememberMe) {
                            m["rememberMe"] = true
                        }
                        if (params.targetUri) {
                            m["targetUri"] = params.targetUri
                        }
                        render view: 'login'
                        return
                    }
                }

                def authToken = new UsernamePasswordToken(params.username, params.password as String)

                // Support for "remember me"
                if (params.rememberMe) {
                    authToken.rememberMe = true
                }
                else
                    authToken.rememberMe = false

                // If a controller redirected to this page, redirect back
                // to it. Otherwise redirect to the root URI.
                def targetUri = params.targetUri ?: "/"
                println params

                // Handle requests saved by Shiro filters.
                def savedRequest = WebUtils.getSavedRequest(request)
                if (savedRequest) {
                    targetUri = savedRequest.requestURI - request.contextPath

                    if (savedRequest.queryString) targetUri = targetUri + '?' + savedRequest.queryString

                }
                // Perform the actual login. An AuthenticationException
                // will be thrown if the username is unrecognised or the
                // password is incorrect.
                SecurityUtils.subject.login(authToken)

                // Update last login time
                log.info "Updating login time."

                if (userInstance){
                    session["user"] = userInstance
                    log.info "Redirecting to '${targetUri}'."
                    flash.message = "${g:message(code: "com.ef.qm.welcome",args: [userInstance.userName],default: "Welcome ${userInstance?.userName}!")}"
                    flash.type = "success"
                    String action = ""
                    if(targetUri.contains("/")){

                        ArrayList<String>  targetSplit = new ArrayList<String>()
                        targetSplit = targetUri.split("/")
                        if(targetSplit.size() != 0){
                            String targetAction = targetSplit.get(targetSplit.size()-1)
                            if(targetAction.contains("?")) {
                                action = targetAction.split("\\?")
                                if(action.size() !=0)
                                    action=targetAction.split("\\?")[0]
                            }
                            else
                                action = targetAction

                        }

                    }
                    println "Action   "    + "${action} "
                    if(action=="update" ||action=="create" || action=="edit" || action=="save" || action=="uploadCsv" || action=="approve" || action=="disapprove" )
                        redirect(controller: 'home',action: 'home')
                    else
                        redirect(uri: targetUri)
                    println "Target"+ targetUri
                }
                else{

                    // AuthenticationException ex = new AuthenticationException("${params.username} doesn't Exist in Ldap so, please Create a user with this username in CBR ")
                    //response.reset()


                    def principal = SecurityUtils.subject?.principal
                    SecurityUtils.subject?.logout()
                    // For now, redirect back to the home page.

                    flash.message =  "${params.username} Exist in Ldap so, please Create a user with this username in CBR Contact with Administrator"
                    flash.type = "warning"

                    if (ConfigUtils.getCasEnable() && ConfigUtils.isFromCas(principal)) {
                        redirect(uri:ConfigUtils.getLogoutUrl())
                    }else {
                        render view: 'login'
                    }
                    ConfigUtils.removePrincipal(principal)
                    //redirect(uri: "/")
                    //redirect(action: "login", params: [username: params.username])
                    //redirect(action: 'signOut')
                }



            }
            catch (Exception e){
                log.info "Authentication failure for user '${params.username}'."
                flash.message = message(code: "login.failed")
                flash.type = "danger"
                println e
                // Keep the username and "remember me" setting so that the
                // user doesn't have to enter them again.
                def m = [ username: params.username ]
                if (params.rememberMe) {
                    m["rememberMe"] = true
                }

                // Remember the target URI too.
                if (params.targetUri) {
                    m["targetUri"] = params.targetUri
                }

                // Now redirect back to the login page.
                render view: 'login'
                //redirect(action: "login", params: m)
            }

        }
        else{
            log.info "Authentication failure for user '${params.username}'."
            flash.message = message(code: "login.failed")
            flash.type = "danger"


            // Now redirect back to the login page.
            redirect(uri: "/")
        }



    }

    def signOut = {
        // Log the user out of the application.
        def principal = SecurityUtils.subject?.principal
        SecurityUtils.subject?.logout()
        // For now, redirect back to the home page.

        flash.message = "${g:message(code: "com.ef.qm.goodBye")}"
        flash.type = "info"

        /*if (ConfigUtils.getCasEnable() && ConfigUtils.isFromCas(principal)) {
            redirect(uri:ConfigUtils.getLogoutUrl())
        }else {
            redirect(uri: "/")
        }
        ConfigUtils.removePrincipal(principal)*/
        redirect(uri: "/")
    }

    def unauthorized = {
        //render "You do not have permission to access this page."
        render(view: 'unauthorized')
    }

    def changePassword= {
        render view: '/auth/changePassword'
    }

    def updatePassword= {

        if (params.password == "" || params.confirmationPassword == "" ) {
            flash.type="danger"
            flash.message = message(code: 'default.error.message')
            render view: '/auth/changePassword', model: [errorPassword:'error']
            return
        }

        else if (params.confirmationPassword != params.password ) {
            flash.type="danger"
            flash.message = message(code: 'default.password.message')
            render view: '/auth/changePassword'
            return
        }

        else {

            def user = User.get(session.user?.id)
            user.userPassword =  new Sha256Hash(params.password).toHex()
            user.save flush:true

            def principal = SecurityUtils.subject?.principal
            SecurityUtils.subject?.logout()
            redirect(uri: "/")

        }


    }


}
