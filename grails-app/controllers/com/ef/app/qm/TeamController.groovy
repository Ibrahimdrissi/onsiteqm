package com.ef.app.qm

import com.ef.app.qm.Team

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TeamController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Team.list(params), model:[teamInstanceCount: Team.count()]
    }

    def show(Team teamInstance) {
        respond teamInstance
    }

    def create() {
        respond new Team(params)
    }

    @Transactional
    def save(Team teamInstance) {
        if (teamInstance == null) {
            notFound()
            return
        }

        def teamExiste = Team.findByTeamName(teamInstance.teamName)
        if(teamExiste != null){
            flash.type="danger"
            flash.message = message(code: 'com.ef.team.teamExiste')
            render view: 'create', model: [teamInstance:teamInstance]
            return
        }

        if (teamInstance.hasErrors()) {
            flash.type="danger"
            flash.message = message(code: 'default.error.message')
            render view: 'create', model: [teamInstance:teamInstance]
            return
        }

        teamInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.created.message', args: [message(code: 'team.label', default: 'Team'), teamInstance.teamName])
                redirect teamInstance
            }
            '*' { respond teamInstance, [status: CREATED] }
        }
    }

    def edit(Team teamInstance) {
        respond teamInstance
    }


    def update(Team teamInstance) {
        if (teamInstance == null) {
            notFound()
            return
        }

        def teamExiste = Team.findByTeamNameAndIdNotEqual(teamInstance.teamName,teamInstance.id)
        if(teamExiste != null ){
            flash.type="danger"
            flash.message = message(code: 'com.ef.team.teamExiste')
            render view: 'edit', model: [teamInstance:teamInstance]
            return
        }

        if (teamInstance.hasErrors()) {
            flash.type="danger"
            flash.message = message(code: 'default.error.message')
            render view: 'edit', model: [teamInstance:teamInstance]
            return
        }

        teamInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.updated.message', args: [message(code: 'com.ef.app.qm.Team.label', default: 'Team'), teamInstance.teamName])
                redirect teamInstance
            }
            '*'{ respond teamInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Team teamInstance) {

        if (teamInstance == null) {
            notFound()
            return
        }

        teamInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'com.ef.app.qm.Team.label', default: 'Team'), teamInstance.teamName])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.type="danger"
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'team.label', default: 'Team'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
