package com.ef.app.qm

import com.ef.app.qm.User
import com.ef.app.qm.Team
import org.apache.shiro.crypto.hash.Sha256Hash
import groovy.sql.Sql
import grails.converters.JSON

class ReportingController {
    def dataSource
    def utilityService

    def global() {
        render view: '/reporting/global',model:[utilityService:utilityService]
    }

    def byGroup() {
        def order = [:]
        render view: '/reporting/bygroup',model:[order:order,id_group:params.id,utilityService:utilityService]
    }

    def byGroupAndCellule() {
        def totalNoteByGroup  = [:]
        def totalNoteByCellule  = [:]
        def order = [:]
        render view: '/reporting/byGroupAndCellule',model:[order:order,utilityService:utilityService,id_group:params.id,totalNoteByGroup:totalNoteByGroup,totalNoteByCellule:totalNoteByCellule]
    }

    def planning() {

        def monthList  = [:]
        monthList["1"] = g.message(code: 'com.reporting.mois1')
        monthList["2"] = g.message(code: 'com.reporting.mois2')
        monthList["3"] = g.message(code: 'com.reporting.mois3')
        monthList["4"] = g.message(code: 'com.reporting.mois4')
        monthList["5"] = g.message(code: 'com.reporting.mois5')
        monthList["6"] = g.message(code: 'com.reporting.mois6')
        monthList["7"] = g.message(code: 'com.reporting.mois7')
        monthList["8"] = g.message(code: 'com.reporting.mois8')
        monthList["9"] = g.message(code: 'com.reporting.mois9')
        monthList["10"] = g.message(code: 'com.reporting.mois10')
        monthList["11"] = g.message(code: 'com.reporting.mois11')
        monthList["12"] = g.message(code: 'com.reporting.mois12')

        render view: '/reporting/planning',model:[utilityService:utilityService,monthList:monthList]
    }

    def planningTeam() {

        def monthList  = [:]
        monthList["1"] = g.message(code: 'com.reporting.mois1')
        monthList["2"] = g.message(code: 'com.reporting.mois2')
        monthList["3"] = g.message(code: 'com.reporting.mois3')
        monthList["4"] = g.message(code: 'com.reporting.mois4')
        monthList["5"] = g.message(code: 'com.reporting.mois5')
        monthList["6"] = g.message(code: 'com.reporting.mois6')
        monthList["7"] = g.message(code: 'com.reporting.mois7')
        monthList["8"] = g.message(code: 'com.reporting.mois8')
        monthList["9"] = g.message(code: 'com.reporting.mois9')
        monthList["10"] = g.message(code: 'com.reporting.mois10')
        monthList["11"] = g.message(code: 'com.reporting.mois11')
        monthList["12"] = g.message(code: 'com.reporting.mois12')

        render view: '/reporting/planningTeam',model:[utilityService:utilityService,monthList:monthList,teamId : User.get(session?.user?.id)?.team?.id]
    }

    def myPlanning() {

        def monthList  = [:]
        monthList["1"] = g.message(code: 'com.reporting.mois1')
        monthList["2"] = g.message(code: 'com.reporting.mois2')
        monthList["3"] = g.message(code: 'com.reporting.mois3')
        monthList["4"] = g.message(code: 'com.reporting.mois4')
        monthList["5"] = g.message(code: 'com.reporting.mois5')
        monthList["6"] = g.message(code: 'com.reporting.mois6')
        monthList["7"] = g.message(code: 'com.reporting.mois7')
        monthList["8"] = g.message(code: 'com.reporting.mois8')
        monthList["9"] = g.message(code: 'com.reporting.mois9')
        monthList["10"] = g.message(code: 'com.reporting.mois10')
        monthList["11"] = g.message(code: 'com.reporting.mois11')
        monthList["12"] = g.message(code: 'com.reporting.mois12')

        render view: '/reporting/myPlanning',model:[utilityService:utilityService,monthList:monthList]
    }


    def planningCentraleJs() {
        def listUsersHasPlannings = [];
        def spaceHasVisit = [:];

        def id = utilityService.reporting()[0]['USER_ID'];
        def nbColaboratteurs = 0
        def nbSpaces = 0

        (utilityService.reporting()).eachWithIndex { row, i ->

            if(row['USER_ID']!=id ){

                spaceHasVisit = [:];
                spaceHasVisit["id"] = "";
                spaceHasVisit["utilisateur"] = "Total de nombre d'espaces"+";;none";
                spaceHasVisit["space_name"] = ""+";;none";
                spaceHasVisit["space_code"] = ""+";;none";
                spaceHasVisit["nb"] = nbSpaces+";;none";
                (utilityService.getPlanningByDay()).eachWithIndex { row2, index ->
                    spaceHasVisit["dt"+index] = ";;none";
                }
                listUsersHasPlannings.add(spaceHasVisit)

                spaceHasVisit = [:];
                spaceHasVisit["id"] = "";
                spaceHasVisit["utilisateur"] = "Total des collaborateurs"+";;none;border-bottom: 2pt solid black;";
                spaceHasVisit["space_name"] = ""+";;none;border-bottom: 2pt solid black;";
                spaceHasVisit["space_code"] = ""+";;none;border-bottom: 2pt solid black;";
                spaceHasVisit["nb"] = nbColaboratteurs+";;none;border-bottom: 2pt solid black;";
                (utilityService.getPlanningByDay()).eachWithIndex { row2, index ->
                    spaceHasVisit["dt"+index] = ";;none;border-bottom: 2pt solid black;";
                }
                listUsersHasPlannings.add(spaceHasVisit)

                id = row['USER_ID'];
            }

            spaceHasVisit = [:];
            spaceHasVisit["id"] = row['USER_ID'];
            spaceHasVisit["utilisateur"] = row['utilisateur']+";;none";
            spaceHasVisit["space_name"] = row['space_name']+";;none";
            spaceHasVisit["space_code"] = row['space_code']+";;none";
            spaceHasVisit["nb"] = row['nb']+";;none";
            (utilityService.getPlanningByDay()).eachWithIndex { row2, index ->
                def plannings = com.ef.app.qm.Planning.findAllByPlanningDateAndSpace(row2.planning_date,Space.get(Integer.parseInt(""+row['SPACE_ID'])))
                //spaceHasVisit["dt"+index] = "aaa";

                if(plannings.size() == 0){
                    spaceHasVisit["dt"+index] = ""+";;none";
                }else if(plannings.size() == 1 && ((plannings[0].planningState == Planning.VALIDATED_STATE) || (plannings[0].planningState == Planning.WAITING_CONFIRMATION_STATE) )){
                    spaceHasVisit["dt"+index] = plannings[0].space.spaceName+":"+plannings[0].visit.visitName+";;"+this.bgColorByPlanning(plannings[0])+";color:#fff;text-align: center;padding: 3px !important;vertical-align: inherit;";

                }else{
                    def res="";
                    (plannings).eachWithIndex { pl, index2 ->
                        if( ((pl.planningState == Planning.VALIDATED_STATE) || (pl.planningState == Planning.WAITING_CONFIRMATION_STATE) )){
                            res=res+"<div style='background-color:"+this.bgColorByPlanning(pl)+";color:#fff;text-align: center;padding: 3px !important;vertical-align: inherit;' >"+pl.space.spaceName+":"+pl.visit.visitName+"</div>";
                        }
                    }
                    spaceHasVisit["dt"+index] = res+";;none";
                }
            }
            listUsersHasPlannings.add(spaceHasVisit)

            if(i == utilityService.reporting().size()-1){

                spaceHasVisit = [:];
                spaceHasVisit["id"] = "";
                spaceHasVisit["utilisateur"] = "Total de nombre d'espaces"+";;none";
                spaceHasVisit["space_name"] = ""+";;none";
                spaceHasVisit["space_code"] = ""+";;none";
                spaceHasVisit["nb"] = nbSpaces+";;none";
                (utilityService.getPlanningByDay()).eachWithIndex { row2, index ->
                    spaceHasVisit["dt"+index] = ";;none";
                }
                listUsersHasPlannings.add(spaceHasVisit)

                spaceHasVisit = [:];
                spaceHasVisit["id"] = "";
                spaceHasVisit["utilisateur"] = "Total des collaborateurs"+";;none;border-bottom: 2pt solid black;";
                spaceHasVisit["space_name"] = ""+";;none;border-bottom: 2pt solid black;";
                spaceHasVisit["space_code"] = ""+";;none;border-bottom: 2pt solid black;";
                spaceHasVisit["nb"] = nbColaboratteurs+";;none;border-bottom: 2pt solid black;";
                (utilityService.getPlanningByDay()).eachWithIndex { row2, index ->
                    spaceHasVisit["dt"+index] = ";;none;border-bottom: 2pt solid black;";
                }
                listUsersHasPlannings.add(spaceHasVisit)

                id = row['USER_ID'];
            }

            nbColaboratteurs = row['total'];
            nbSpaces = row['nbSpaceHasPlanningByUser'];

        }
        render (listUsersHasPlannings as JSON)
    }

    def planningCentraleJs2() {

        def tabTitres = [];

        def    headerColumn0 = [:];
        headerColumn0["headertext"] = 'id';
        headerColumn0["datatype"] = 'number';
        headerColumn0["datafield"] = 'id';
        headerColumn0["ishidden"] = true;

        tabTitres.add(headerColumn0)

        def    headerColumn1 = [:];
        headerColumn1["headertext"] = 'utilisateur';
        headerColumn1["datatype"] = 'string';
        headerColumn1["datafield"] = 'utilisateur';
        headerColumn1["width"] = '180px';

        tabTitres.add(headerColumn1)

        def headerColumn2 = [:];
        headerColumn2["headertext"] = 'space_name';
        headerColumn2["datatype"] = 'string';
        headerColumn2["datafield"] = 'space_name';
        headerColumn2["width"] = '150px';

        tabTitres.add(headerColumn2)

        def headerColumn3 = [:];
        headerColumn3["headertext"] = 'space_code';
        headerColumn3["datatype"] = 'string';
        headerColumn3["datafield"] = 'space_code';
        headerColumn3["width"] = '70px';

        tabTitres.add(headerColumn3)

        def headerColumn4 = [:];
        headerColumn4["headertext"] = 'nb';
        headerColumn4["datatype"] = 'string';
        headerColumn4["datafield"] = 'nb';
        headerColumn4["width"] = '30px';

        tabTitres.add(headerColumn4)

        (utilityService.getPlanningByDay()).eachWithIndex { row2, index ->

            def headerColumn5 = [:];

            headerColumn5["headertext"] = row2.planning_date.format('dd/MM/yyyy');
            headerColumn5["datatype"] = 'string';
            headerColumn5["datafield"] = 'dt'+index;
            headerColumn5["width"] = '150px';

            tabTitres.add(headerColumn5)
        }

        render (tabTitres as JSON)
    }

    def planningAnimateurJs2() {

        def tabTitres = [];

        def    headerColumn0 = [:];
        headerColumn0["headertext"] = 'id';
        headerColumn0["datatype"] = 'number';
        headerColumn0["datafield"] = 'id';
        headerColumn0["ishidden"] = true;

        tabTitres.add(headerColumn0)

        def    headerColumn1 = [:];
        headerColumn1["headertext"] = 'utilisateur';
        headerColumn1["datatype"] = 'string';
        headerColumn1["datafield"] = 'utilisateur';
        headerColumn1["width"] = '180px';

        tabTitres.add(headerColumn1)

        def headerColumn2 = [:];
        headerColumn2["headertext"] = 'space_name';
        headerColumn2["datatype"] = 'string';
        headerColumn2["datafield"] = 'space_name';
        headerColumn2["width"] = '150px';

        tabTitres.add(headerColumn2)

        def headerColumn3 = [:];
        headerColumn3["headertext"] = 'space_code';
        headerColumn3["datatype"] = 'string';
        headerColumn3["datafield"] = 'space_code';
        headerColumn3["width"] = '70px';

        tabTitres.add(headerColumn3)

        def headerColumn4 = [:];
        headerColumn4["headertext"] = 'nb';
        headerColumn4["datatype"] = 'string';
        headerColumn4["datafield"] = 'nb';
        headerColumn4["width"] = '30px';

        tabTitres.add(headerColumn4)

        (utilityService.getMyPlanningByDay(Long.parseLong(""+params.id))).eachWithIndex { row2, index ->

            def headerColumn5 = [:];

            headerColumn5["headertext"] = row2.planning_date.format('dd/MM/yyyy');
            headerColumn5["datatype"] = 'string';
            headerColumn5["datafield"] = 'dt'+index;
            headerColumn5["width"] = '150px';

            tabTitres.add(headerColumn5)
        }

        render (tabTitres as JSON)
    }

    def planningAnimateurJs() {
        def listUsersHasPlannings = [];
        def spaceHasVisit = [:];

        def nbColaboratteurs = 0
        def nbSpaces = 0

        (utilityService.reportingByUser(Long.parseLong(""+params.id))).eachWithIndex { row, i ->

            spaceHasVisit = [:];
            spaceHasVisit["id"] = row['USER_ID'];
            spaceHasVisit["utilisateur"] = row['utilisateur']+";;none";
            spaceHasVisit["space_name"] = row['space_name']+";;none";
            spaceHasVisit["space_code"] = row['space_code']+";;none";
            spaceHasVisit["nb"] = row['nb']+";;none";
            (utilityService.getMyPlanningByDay(Long.parseLong(""+params.id))).eachWithIndex { row2, index ->
                def plannings = com.ef.app.qm.Planning.findAllByPlanningDateAndSpace(row2.planning_date,Space.get(Integer.parseInt(""+row['SPACE_ID'])))
                //spaceHasVisit["dt"+index] = "aaa";

                if(plannings.size() == 0){
                    spaceHasVisit["dt"+index] = ""+";;none";
                }else if(plannings.size() == 1 && ((plannings[0].planningState == Planning.VALIDATED_STATE) || (plannings[0].planningState == Planning.WAITING_CONFIRMATION_STATE) )){
                    spaceHasVisit["dt"+index] = plannings[0].space.spaceName+":"+plannings[0].visit.visitName+";;"+this.bgColorByPlanning(plannings[0])+";color:#fff;text-align: center;padding: 3px !important;vertical-align: inherit;";

                }else{
                    def res="";
                    (plannings).eachWithIndex { pl, index2 ->
                        if( ((pl.planningState == Planning.VALIDATED_STATE) || (pl.planningState == Planning.WAITING_CONFIRMATION_STATE) )){
                            res=res+"<div style='background-color:"+this.bgColorByPlanning(pl)+";color:#fff;text-align: center;padding: 3px !important;vertical-align: inherit;' >"+pl.space.spaceName+":"+pl.visit.visitName+"</div>";
                        }
                    }
                    spaceHasVisit["dt"+index] = res+";;none";
                }
            }
            listUsersHasPlannings.add(spaceHasVisit)

            if(i == utilityService.reportingByUser(Long.parseLong(""+params.id)).size()-1){

                spaceHasVisit = [:];
                spaceHasVisit["id"] = "";
                spaceHasVisit["utilisateur"] = "Total de nombre d'espaces"+";;none";
                spaceHasVisit["space_name"] = ""+";;none";
                spaceHasVisit["space_code"] = ""+";;none";
                spaceHasVisit["nb"] = nbSpaces+";;none";
                (utilityService.getPlanningByDay()).eachWithIndex { row2, index ->
                    spaceHasVisit["dt"+index] = ";;none";
                }
                listUsersHasPlannings.add(spaceHasVisit)

                spaceHasVisit = [:];
                spaceHasVisit["id"] = "";
                spaceHasVisit["utilisateur"] = "Total des collaborateurs"+";;none;border-bottom: 2pt solid black;";
                spaceHasVisit["space_name"] = ""+";;none;border-bottom: 2pt solid black;";
                spaceHasVisit["space_code"] = ""+";;none;border-bottom: 2pt solid black;";
                spaceHasVisit["nb"] = nbColaboratteurs+";;none;border-bottom: 2pt solid black;";
                (utilityService.getPlanningByDay()).eachWithIndex { row2, index ->
                    spaceHasVisit["dt"+index] = ";;none;border-bottom: 2pt solid black;";
                }
                listUsersHasPlannings.add(spaceHasVisit)


            }

            nbColaboratteurs = row['total'];
            nbSpaces = row['nbSpaceHasPlanningByUser'];

        }
        render (listUsersHasPlannings as JSON)
    }


    def planningTeamJs2() {

        def tabTitres = [];

        def    headerColumn0 = [:];
        headerColumn0["headertext"] = 'id';
        headerColumn0["datatype"] = 'number';
        headerColumn0["datafield"] = 'id';
        headerColumn0["ishidden"] = true;

        tabTitres.add(headerColumn0)

        def    headerColumn1 = [:];
        headerColumn1["headertext"] = 'utilisateur';
        headerColumn1["datatype"] = 'string';
        headerColumn1["datafield"] = 'utilisateur';
        headerColumn1["width"] = '180px';

        tabTitres.add(headerColumn1)

        def headerColumn2 = [:];
        headerColumn2["headertext"] = 'space_name';
        headerColumn2["datatype"] = 'string';
        headerColumn2["datafield"] = 'space_name';
        headerColumn2["width"] = '150px';

        tabTitres.add(headerColumn2)

        def headerColumn3 = [:];
        headerColumn3["headertext"] = 'space_code';
        headerColumn3["datatype"] = 'string';
        headerColumn3["datafield"] = 'space_code';
        headerColumn3["width"] = '70px';

        tabTitres.add(headerColumn3)

        def headerColumn4 = [:];
        headerColumn4["headertext"] = 'nb';
        headerColumn4["datatype"] = 'string';
        headerColumn4["datafield"] = 'nb';
        headerColumn4["width"] = '30px';

        tabTitres.add(headerColumn4)

        (utilityService.getMyTeamPlanningByDay(Long.parseLong(""+params.userId))).eachWithIndex { row2, index ->

            def headerColumn5 = [:];

            headerColumn5["headertext"] = row2.planning_date.format('dd/MM/yyyy');
            headerColumn5["datatype"] = 'string';
            headerColumn5["datafield"] = 'dt'+index;
            headerColumn5["width"] = '150px';

            tabTitres.add(headerColumn5)
        }

        render (tabTitres as JSON)
    }

    def planningTeamJs() {
        def listUsersHasPlannings = [];
        def spaceHasVisit = [:];

        def id = utilityService.reportingByTeam(Long.parseLong(""+params.teamId))[0]['USER_ID'];
        def nbColaboratteurs = 0
        def nbSpaces = 0

        (utilityService.reportingByTeam(Long.parseLong(""+params.teamId))).eachWithIndex { row, i ->

            if(row['USER_ID']!=id ){

                spaceHasVisit = [:];
                spaceHasVisit["id"] = "";
                spaceHasVisit["utilisateur"] = "Total de nombre d'espaces"+";;none";
                spaceHasVisit["space_name"] = ""+";;none";
                spaceHasVisit["space_code"] = ""+";;none";
                spaceHasVisit["nb"] = nbSpaces+";;none";
                (utilityService.getMyTeamPlanningByDay(Long.parseLong(""+params.userId))).eachWithIndex { row2, index ->
                    spaceHasVisit["dt"+index] = ";;none";
                }
                listUsersHasPlannings.add(spaceHasVisit)

                spaceHasVisit = [:];
                spaceHasVisit["id"] = "";
                spaceHasVisit["utilisateur"] = "Total des collaborateurs"+";;none;border-bottom: 2pt solid black;";
                spaceHasVisit["space_name"] = ""+";;none;border-bottom: 2pt solid black;";
                spaceHasVisit["space_code"] = ""+";;none;border-bottom: 2pt solid black;";
                spaceHasVisit["nb"] = nbColaboratteurs+";;none;border-bottom: 2pt solid black;";
                (utilityService.getMyTeamPlanningByDay(Long.parseLong(""+params.userId))).eachWithIndex { row2, index ->
                    spaceHasVisit["dt"+index] = ";;none;border-bottom: 2pt solid black;";
                }
                listUsersHasPlannings.add(spaceHasVisit)

                id = row['USER_ID'];
            }

            spaceHasVisit = [:];
            spaceHasVisit["id"] = row['USER_ID'];
            spaceHasVisit["utilisateur"] = row['utilisateur']+";;none";
            spaceHasVisit["space_name"] = row['space_name']+";;none";
            spaceHasVisit["space_code"] = row['space_code']+";;none";
            spaceHasVisit["nb"] = row['nb']+";;none";
            (utilityService.getMyTeamPlanningByDay(Long.parseLong(""+params.userId))).eachWithIndex { row2, index ->
                def plannings = com.ef.app.qm.Planning.findAllByPlanningDateAndSpace(row2.planning_date,Space.get(Integer.parseInt(""+row['SPACE_ID'])))
                //spaceHasVisit["dt"+index] = "aaa";

                if(plannings.size() == 0){
                    spaceHasVisit["dt"+index] = ""+";;none";
                }else if(plannings.size() == 1 && ((plannings[0].planningState == Planning.VALIDATED_STATE) || (plannings[0].planningState == Planning.WAITING_CONFIRMATION_STATE) )){
                    spaceHasVisit["dt"+index] = plannings[0].space.spaceName+":"+plannings[0].visit.visitName+";;"+this.bgColorByPlanning(plannings[0])+";color:#fff;text-align: center;padding: 3px !important;vertical-align: inherit;";

                }else{
                    def res="";
                    (plannings).eachWithIndex { pl, index2 ->
                        if( ((pl.planningState == Planning.VALIDATED_STATE) || (pl.planningState == Planning.WAITING_CONFIRMATION_STATE) )){
                            res=res+"<div style='background-color:"+this.bgColorByPlanning(pl)+";color:#fff;text-align: center;padding: 3px !important;vertical-align: inherit;' >"+pl.space.spaceName+":"+pl.visit.visitName+"</div>";
                        }
                    }
                    spaceHasVisit["dt"+index] = res+";;none";
                }
            }
            listUsersHasPlannings.add(spaceHasVisit)

            if(i == utilityService.reportingByTeam(Long.parseLong(""+params.teamId)).size()-1){

                spaceHasVisit = [:];
                spaceHasVisit["id"] = "";
                spaceHasVisit["utilisateur"] = "Total de nombre d'espaces"+";;none";
                spaceHasVisit["space_name"] = ""+";;none";
                spaceHasVisit["space_code"] = ""+";;none";
                spaceHasVisit["nb"] = nbSpaces+";;none";
                (utilityService.getMyTeamPlanningByDay(Long.parseLong(""+params.userId))).eachWithIndex { row2, index ->
                    spaceHasVisit["dt"+index] = ";;none";
                }
                listUsersHasPlannings.add(spaceHasVisit)

                spaceHasVisit = [:];
                spaceHasVisit["id"] = "";
                spaceHasVisit["utilisateur"] = "Total des collaborateurs"+";;none;border-bottom: 2pt solid black;";
                spaceHasVisit["space_name"] = ""+";;none;border-bottom: 2pt solid black;";
                spaceHasVisit["space_code"] = ""+";;none;border-bottom: 2pt solid black;";
                spaceHasVisit["nb"] = nbColaboratteurs+";;none;border-bottom: 2pt solid black;";
                (utilityService.getMyTeamPlanningByDay(Long.parseLong(""+params.userId))).eachWithIndex { row2, index ->
                    spaceHasVisit["dt"+index] = ";;none;border-bottom: 2pt solid black;";
                }
                listUsersHasPlannings.add(spaceHasVisit)

                id = row['USER_ID'];
            }

            nbColaboratteurs = row['total'];
            nbSpaces = row['nbSpaceHasPlanningByUser'];

        }
        render (listUsersHasPlannings as JSON)
    }

    def bgColorByPlanning(Planning planning){
        def res = "none"
        def today = new Date();

        if(planning != null && planning?.planningDate != null){
            if( (planning?.planningDate?.getTime() < today.getTime()) && planning?.planningState==Planning.VALIDATED_STATE)
            {
                res = "#c9302c"
            }
            else if(planning?.planningState==Planning.VALIDATED_STATE){
                res = "#5cb85c"
            }else if(planning?.planningState==Planning.WAITING_CONFIRMATION_STATE){
                res = "#f0ad4e"
            }
        }

        return res;
    }

}
