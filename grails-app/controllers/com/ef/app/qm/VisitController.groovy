package com.ef.app.qm

import com.ef.app.qm.Visit

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class VisitController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Visit.list(params), model: [visitInstanceCount: Visit.count()]
    }

    def show(Visit visitInstance) {
        respond visitInstance
    }

    def create() {
        respond new Visit(params)
    }

    @Transactional
    def save(Visit visitInstance) {
        if (visitInstance == null) {
            notFound()
            return
        }

        def visitExiste = Visit.findByVisitName(visitInstance.visitName)
        if(visitExiste != null){
            flash.type="danger"
            flash.message = message(code: 'com.ef.visit.visitExiste')
            render view: 'create', model: [visitInstance:visitInstance]
            return
        }

        if (visitInstance.hasErrors()) {
            flash.type="danger"
            flash.message = message(code: 'default.error.message')
            render view: 'create', model: [visitInstance:visitInstance]
            return
        }

        visitInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.created.message', args: [message(code: 'visit.label', default: 'Visit'), visitInstance.visitName])
                redirect visitInstance
            }
            '*' { respond visitInstance, [status: CREATED] }
        }
    }

    def edit(Visit visitInstance) {
        respond visitInstance
    }


    def update(Visit visitInstance) {
        if (visitInstance == null) {
            notFound()
            return
        }

        def visitExiste = Visit.findByVisitNameAndIdNotEqual(visitInstance.visitName,visitInstance.id)
        if(visitExiste != null){
            flash.type="danger"
            flash.message = message(code: 'com.ef.visit.visitExiste')
            render view: 'edit', model: [visitInstance:visitInstance]
            return
        }

        if (visitInstance.hasErrors()) {
            flash.type="danger"
            flash.message = message(code: 'default.error.message')
            render view: 'edit', model: [visitInstance:visitInstance]
            return
        }

        visitInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.updated.message', args: [message(code: 'com.ef.app.qm.Visit.label', default: 'Visit'), visitInstance.visitName])
                redirect visitInstance
            }
            '*' { respond visitInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Visit visitInstance) {

        if (visitInstance == null) {
            notFound()
            return
        }

        visitInstance.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'com.ef.app.qm.Visit.label', default: 'Visit'), visitInstance.visitName])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'visit.label', default: 'Visit'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
