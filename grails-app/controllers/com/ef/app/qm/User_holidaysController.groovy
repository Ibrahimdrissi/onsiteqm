package com.ef.app.qm



import static org.springframework.http.HttpStatus.*
import java.text.SimpleDateFormat
import grails.transaction.Transactional

@Transactional(readOnly = true)
class User_holidaysController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond User_holidays.list(params), model:[user_holidaysInstanceCount: User_holidays.count()]
    }

    def show(User_holidays user_holidaysInstance) {
        respond user_holidaysInstance
    }

    def create() {
        respond new User_holidays(params)
    }

    @Transactional
    def save(User_holidays user_holidaysInstance) {

        if (user_holidaysInstance == null) {
            notFound()
            return
        }

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        if (params.endDate_value == '' )
        {

            if(params.startDate_value != '' ){
                Date startDate = formatter.parse(params.startDate_value);
                user_holidaysInstance.startDate= startDate;
            }

            flash.type="danger"
            flash.message = message(code: 'default.null.message3')
            render view: 'create', model: [user_holidaysInstance:user_holidaysInstance,dateError:'error']
            return
        }
        else if (params.startDate_value == '' )
        {
            if(params.endDate_value != '' ){
                Date endDate = formatter.parse(params.endDate_value);
                user_holidaysInstance.endDate= endDate;
            }

            flash.type="danger"
            flash.message = message(code: 'default.null.message3')
            render view: 'create', model: [user_holidaysInstance:user_holidaysInstance,dateError2:'error']
            return
        }

        Date startDate = formatter.parse(params.startDate_value);
        user_holidaysInstance.startDate= startDate;

        Date endDate = formatter.parse(params.endDate_value);
        user_holidaysInstance.endDate= endDate;

        if( endDate <= startDate ){
            flash.type="danger"
            flash.message = message(code: 'default.null.message4')
            render view: 'create', model: [user_holidaysInstance:user_holidaysInstance,dateError:'error',dateError2:'error']
            return
        }

        user_holidaysInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.created.message', args: [message(code: 'user_holidays.label', default: 'User_holidays'), user_holidaysInstance.id])
                redirect user_holidaysInstance
            }
            '*' { respond visitInstance, [status: CREATED] }
        }

    }

    def edit(User_holidays user_holidaysInstance) {
        respond user_holidaysInstance
    }

    @Transactional
    def update(User_holidays user_holidaysInstance) {
        if (user_holidaysInstance == null) {
            notFound()
            return
        }

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        if (params.endDate_value == '' )
        {

            if(params.startDate_value != '' ){
                Date startDate = formatter.parse(params.startDate_value);
                user_holidaysInstance.startDate= startDate;
            }

            flash.type="danger"
            flash.message = message(code: 'default.null.message3')
            render view: 'edit', model: [user_holidaysInstance:user_holidaysInstance,dateError:'error']
            return
        }
        else if (params.startDate_value == '' )
        {
            if(params.endDate_value != '' ){
                Date endDate = formatter.parse(params.endDate_value);
                user_holidaysInstance.endDate= endDate;
            }

            flash.type="danger"
            flash.message = message(code: 'default.null.message3')
            render view: 'edit', model: [user_holidaysInstance:user_holidaysInstance,dateError2:'error']
            return
        }

        Date startDate = formatter.parse(params.startDate_value);
        user_holidaysInstance.startDate= startDate;

        Date endDate = formatter.parse(params.endDate_value);
        user_holidaysInstance.endDate= endDate;

        if( endDate <= startDate ){
            flash.type="danger"
            flash.message = message(code: 'default.null.message4')
            render view: 'edit', model: [user_holidaysInstance:user_holidaysInstance,dateError:'error',dateError2:'error']
            return
        }

        user_holidaysInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.updated.message', args: [message(code: 'User_holidays.label', default: 'User_holidays'), user_holidaysInstance.id])
                redirect user_holidaysInstance
            }
            '*' { respond user_holidaysInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(User_holidays user_holidaysInstance) {

        if (user_holidaysInstance == null) {
            notFound()
            return
        }

        user_holidaysInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'User_holidays.label', default: 'User_holidays'), user_holidaysInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }

    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'user_holidays.label', default: 'User_holidays'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
