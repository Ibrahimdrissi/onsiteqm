package com.ef.app.qm

import com.ef.app.qm.User
import org.apache.shiro.crypto.hash.Sha256Hash

class LoginController {

    def login() {
        if(session.user!=null){
            redirect(action: 'index')
        }
        if(params.username != null){
            def userByUserName = User.findByUserName(params.username)
            if(userByUserName == null){
                flash.message = message(code: "com.ef.user.undefined")
            }else{
                def password =  new Sha256Hash(params.password).toHex()
                def user = User.findByUserNameAndUserPassword(params.username,password)
                if(user == null){
                    flash.message = message(code: "com.ef.user.password.error")
                }else{
                    session["user"] = user
                    redirect(action:"index")
                }
            }
        }

    }
    def index()
    {

    }
    /*def signIn() {
        System.out.println("SignIn Method ...")
        if(params.username != null){
                def userByUserName = com.ef.app.qm.User.findByUserName(params.username)
                if(userByUserName == null){
                    flash.message = message(code: "com.ef.user.undefined")
                    System.out.println("Login 1")
                    redirect(uri: "/login")
                }else{
                    def password =  new Sha256Hash(params.password).toHex()
                    def user = com.ef.app.qm.User.findByUserNameAndUserPassword(params.username,password)
                    if(user == null){
                        flash.message = message(code: "com.ef.user.password.error")
                        System.out.println("Login 2")
                        redirect(uri: "/login")
                    }else{
                        session["user"] = user
                        //session.user = user.userName
                        System.out.println("index")
                        redirect(action:'index')
                        //render(view:"/index")
                    }
                }
            }
            else{
                System.out.println("Login 3")
                redirect(uri: "/login")
            }
    }*/

    def signOut() {
        session.removeAttribute("user");
        //session.invalidate();
        redirect(uri: "/login")
    }
}
