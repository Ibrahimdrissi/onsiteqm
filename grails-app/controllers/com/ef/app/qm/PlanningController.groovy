package com.ef.app.qm

import com.ef.app.qm.Planning
import org.apache.commons.io.FilenameUtils
import org.springframework.format.annotation.DateTimeFormat

import javax.swing.text.DateFormatter
import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class PlanningController {
    def exportService
    def csvUploaderService
    def mailSenderService
    def utilityService
    def grailsApplication
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def index(Integer max) {
        def userProfileName = User.get(session?.user?.id)?.profile?.profilName?.trim()
      //  if(userProfileName?.equalsIgnoreCase("Chef de Cellule") || userProfileName?.equalsIgnoreCase("Animateur") || userProfileName?.equalsIgnoreCase("Membre de la cellule centrale")) {
            params.max = Math.min(max ?: 10, 100)
            println Planning.count()
            respond Planning.list(params), model:[planningInstanceCount: Planning.count()]
/*        }else
            response.sendError(403)*/
    }

    def show(Planning planningInstance) {
        respond planningInstance
    }

    def create() {
            respond new Planning(params)
    }

    def VisiteForm() {
        render view: '/planning/VisiteForm',model:[val:params.val]
    }

    @Transactional
    def save(Planning planningInstance) {
        if (planningInstance == null) {
            notFound()
            return
        }

        if(Planning.findAllBySpaceAndVisit(planningInstance?.space,planningInstance?.visit)?.planningState?.contains(0) || Planning.findAllBySpaceAndVisit(planningInstance?.space,planningInstance?.visit)?.planningState?.contains(4)){
            flash.type="danger"
            flash.message = message(code: "com.ef.app.qm.planning.exists.label",default: "A planning already exists for this Space and Visit.")
            respond planningInstance, view:'create'
            return
        }

        if (planningInstance.hasErrors()) {
            respond planningInstance.errors, view:'create'
            return
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date planningDate = formatter.parse(params.planningDate_value);
        planningInstance.planningDate=planningDate
        planningInstance.planningState = Planning.VALIDATED_STATE
        System.out.println("planningDate: "+planningDate+" ")
        if(!planningInstance.save()){
            respond planningInstance.errors, view:'create'
            return
        }


        //Send notification to above planing's space's user
        //def userEmail= planningInstance?.space?.utilisateur?.userEmail
        def url = "URL: http://"+request.getServerName()+":"+8091+request.getContextPath()+"/planning/show/${planningInstance?.id}"
        def userEmail= planningInstance?.space?.user?.userEmail
        mailSenderService.sendEmailNotification(userEmail,"Nouveau planning","Bonjour,\nVeuillez trouver ci-dessous les informations du nouveau planning:\n\nEspace de la visite: ${planningInstance?.space?.spaceName}\nDate de la visite: ${planningInstance?.planningDate?.format("dd/MM/yyyy")}\nNom de la visite: ${planningInstance?.visit?.visitName} \n\n\n Programme Experience Client")


        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.created.message', args: [message(code: 'evaluation.planning.label', default: 'Planning'), planningInstance.id])
                redirect planningInstance
            }
            '*' { respond planningInstance, [status: CREATED] }
        }
    }

    def edit(Planning planningInstance) {
        respond planningInstance
    }

    def update1() {
        println params
        if(params.urgent)
        {
            def planningInstance = Planning.findById(params.id)
            def oldDate = planningInstance.planningDate
            println "Old Date:$oldDate"
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date newDate = formatter.parse(params.planningDate_value);
            planningInstance.planningDate = newDate
            println "new Date:$newDate"

            planningInstance.save(flush: true)

//            utilityService.shiftPlanning(User.get(session?.user?.id), oldDate, newDate, planningInstance)

            def userTeam = planningInstance?.space?.user?.team
            def userProfileCentralCell = Profile.findByProfilNameLike("Cellule centrale")
            def userProfileCentralLeader = Profile.findByProfilNameLike("Chef de cellule")
            def centralCellUserEmail = User.findAllByProfile(userProfileCentralCell)?.userEmail
            def cellLeaderUserEmail = User.findByTeamAndProfile(userTeam,userProfileCentralLeader)?.userEmail

            if(planningInstance.space.user.profile.profilName!="Chef de cellule")
            {
                mailSenderService.sendEmailNotification(cellLeaderUserEmail,"Changement urgent d'un planning","Bonjour,\n\nL'utilisateur ${planningInstance?.space.user.firstName} ${planningInstance?.space.user.lastName} a fait une modification urgente du planning de la ${planningInstance?.visit.visitName} pour l'espace: ${planningInstance?.space.spaceName} \n\n\n Programme Experience Client")
            }
            else
            {
                centralCellUserEmail.each{
                    mailSenderService.sendEmailNotification(it,"Changement urgent d'un planning","Bonjour,\n\nL'utilisateur ${planningInstance?.space.user.firstName} ${planningInstance?.space.user.lastName} de la ${planningInstance?.space.user.team.teamName} a fait une modification urgente du planning de la ${planningInstance?.visit.visitName} pour l'espace: ${planningInstance?.space.spaceName} \n\n\n Programme Experience Client")
                }

            }
            flash.type="info"
            flash.message = message(code: 'default.updated.message', args: [message(code: 'cevaluation.planning.label', default: 'Planning'), planningInstance.id])
            redirect(action: "show",id: planningInstance.id)
        }else {
            def editedPlanningInstance = new Planning()
            editedPlanningInstance.properties = params
            def originalPlanningInstance = Planning.findById(params.id)
            if (editedPlanningInstance == null) {
                notFound()
                return
            }
            if (editedPlanningInstance.hasErrors()) {
                respond editedPlanningInstance.errors, view:'edit'
                return
            }
            def file = request.getFile('changeFile')
            def originalFileName = file?.getOriginalFilename()
            if(originalFileName){
                def webDirectory = servletContext.getRealPath("/")
                File destinationFile = new File(webDirectory, originalFileName)
                try{
                    file?.transferTo(destinationFile)
                }catch(Exception e){
                    flash.type="danger"
                    flash.message = message(code: 'default.file.exists.message', default: "File with name $originalFileName already exists")
                    redirect(action: "index")
                }

                editedPlanningInstance.planningChangeJoint = destinationFile.getName()
            }

            editedPlanningInstance.planningState = Planning.WAITING_CONFIRMATION_STATE

            editedPlanningInstance.properties = editedPlanningInstance.properties
            editedPlanningInstance.planning = originalPlanningInstance
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date planningDate = formatter.parse(params.planningDate_value);
            editedPlanningInstance?.planningDate = planningDate
            editedPlanningInstance?.date_updated = new Date()
            editedPlanningInstance?.save flush:true

            originalPlanningInstance?.planningState = Planning.CHANGED_STATE
            originalPlanningInstance?.save()

            //Send notification about planning change request to cell leader or member of central cell
            def userTeam = originalPlanningInstance?.space?.user?.team
            def userProfileCentralCell = Profile.findByProfilNameLike("Cellule centrale")
            def userProfileCentralLeader = Profile.findByProfilNameLike("Chef de cellule")
            def centralCellUserEmail = User.findAllByProfile(userProfileCentralCell)?.userEmail
            def cellLeaderUserEmail = User.findByTeamAndProfile(userTeam,userProfileCentralLeader)?.userEmail

            if(originalPlanningInstance.space.user.profile.profilName!="Chef de cellule") // cellLeaderUserEmail)
            {
                mailSenderService.sendEmailNotification(cellLeaderUserEmail,"Changement d'un planning","Bonjour,\n\nL'utilisateur ${originalPlanningInstance?.space.user.firstName} ${originalPlanningInstance?.space.user.lastName} a soumis une demande de changement de planning de la ${originalPlanningInstance?.visit.visitName} pour l'espace: ${originalPlanningInstance?.space.spaceName} \n\n\n Programme Experience Client")
            }
            else
            {
                centralCellUserEmail.each{
                    mailSenderService.sendEmailNotification(it,"Changement d'un planning","Bonjour,\n\nL'utilisateur ${originalPlanningInstance?.space.user.firstName} ${originalPlanningInstance?.space.user.lastName} de la ${originalPlanningInstance?.space.user.team.teamName} a soumis une demande de changement de planning de la ${originalPlanningInstance?.visit.visitName} pour l'espace: ${originalPlanningInstance?.space.spaceName} \n\n\n Programme Experience Client")
                }

            }
            flash.type="info"
            flash.message = message(code: 'default.updated.message', args: [message(code: 'evaluation.planning.label', default: 'Planning'), editedPlanningInstance.id])
            redirect(action: "show",id: editedPlanningInstance.id)
        }


    }
    def approve(Planning planningInstance) {
        def changedPlanning = planningInstance?.planning
        if (planningInstance == null) {
            notFound()
            return
        }
        /*def planning = Planning.findBySpaceAndVisit(planningInstance?.space,planningInstance?.visit)
        if(planning && (planning?.planningState==Planning.VALIDATED_STATE || planning?.planningState==Planning.EVALUATED_STATE)){
            flash.type="danger"
            flash.message = message(code: "com.ef.app.qm.planning.exists.label",default: "A planning already exists for this Space and Visit.")
            respond planningInstance, view:'edit'
            return
        }*/
        if (planningInstance.hasErrors()) {
            respond planningInstance.errors, view:'edit'
            return
        }

        //changedPlanning.planningDate = planningInstance.planningDate
        planningInstance.planningState = Planning.VALIDATED_STATE
        changedPlanning.planningState = Planning.CHANGED_STATE

        planningInstance.date_validation= new Date()
        planningInstance.user_validated=User.get(session.user?.id)

        def plannings = utilityService.getPlanningToShift(changedPlanning.planningDate,changedPlanning.space.user.id,planningInstance.id);

        //Since the change for planning is approved, we need to shift all the plannings for
        //this user according to the updated planning Date.
        //utilityService.shiftPlanning(User.get(planningInstance?.space?.user?.id), changedPlanning?.planningDate, planningInstance?.planningDate, changedPlanning, planningInstance)

        if (plannings[0].oldDay != planningInstance.planningDate) {
            utilityService.shifting(plannings,planningInstance.planningDate.plus(1));
        }

        //Send notification to above planning's space's user
        // and member of central cell about approval
        def userEmail= planningInstance?.space?.user?.userEmail
        def userTeam = planningInstance?.space?.user?.team
        def userProfile = Profile.findByProfilNameLike("Cellule centrale")
        def centralCellUserEmail = User.findAllByProfile(userProfile)?.userEmail

        mailSenderService.sendEmailNotification(planningInstance.space.user.userEmail,"Changement d'un planning approuve","Bonjour,\n\nLe changement du planning de la ${planningInstance.visit.visitName} pour l'espace: ${planningInstance.space.spaceName} est approuve\n\n\nProgramme Experience Client")

        centralCellUserEmail.each{
            mailSenderService.sendEmailNotification(it,"Changement d'un planning approuve","Bonjour,\n\nLe changement du planning soumis par ${planningInstance.space.user.firstName} ${planningInstance.space.user.lastName} de la ${planningInstance.space.user.team.teamName} est appouve pour: ${planningInstance.space.spaceName},${planningInstance.visit.visitName} \n\n\n Programme Experience Client")
        }
        flash.message = message(code: "default.validated.label", default: "Planning change successfully approved")
        flash.type = "info"
        redirect(action: "show", id: planningInstance?.id)
    }
    def disapprove(Planning planningInstance) {
        println planningInstance.properties
        if (planningInstance == null) {
            notFound()
            return
        }
        if (planningInstance.hasErrors()) {
            respond planningInstance.errors, view:'edit'
            return
        }
        planningInstance.planningState = Planning.REJECTED_STATE
        planningInstance?.planning?.planningState = Planning.VALIDATED_STATE

        planningInstance.date_validation= new Date()
        planningInstance.user_validated=User.get(session.user?.id)



        planningInstance.save(flush: true)
        //Send notification to above planing's space's user about rejection of planning
        def userEmail= planningInstance?.space?.user?.userEmail
        try{
            //mailSenderService.sendEmailNotification(userEmail,"Planning rejected","Changes in planning with date:${planningInstance?.planningDate} have been rejected for space:${planningInstance?.space}")
        }catch (Exception e) {
            log.error("Unable to send email notification after creating planning with date ${planningInstance?.planningDate}, Exception:${e.getMessage()}")
        }
        flash.message = message(code: "default.rejected.label", default: "Planning change rejected")
        flash.type = "info"
        redirect(action: "show", id: planningInstance?.id)
    }

    @Transactional
    def delete(Planning planningInstance) {

        if (planningInstance == null) {
            notFound()
            return
        }

        planningInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'evaluation.planning.label', default: 'Planning'), planningInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.type="success"
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'evaluation.planning.label', default: 'Planning'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def uploadCsv() {

        try {
            def file = request.getFile('myFile')
            if (!file.isEmpty()) {
                def fileExtension = FilenameUtils.getExtension(file.getOriginalFilename())

                if (fileExtension.equals('csv') ||fileExtension.equals('xlsx') ) {
                    def webDirectory = servletContext.getRealPath("/")
                    File destinationFile = new File(webDirectory, "csvFile_${new Date().getTime()}.csv")
                    file.transferTo(destinationFile)
                    def fileName = destinationFile.getName()

                    def returnList = csvUploaderService.uploadCsv(fileName)

                    /*def userEmail= planningInstance?.space?.user?.userEmail
                    try{
                        mailSenderService.sendEmailNotification(userEmail,"Planning rejected","Changes in planning with date:${planningInstance?.planningDate} have been rejected for space:${planningInstance?.space}")
                    }catch (Exception e) {
                        log.error("Unable to send email notification after creating planning with date ${planningInstance?.planningDate}, Exception:${e.getMessage()}")
                    }*/

                    flash.type = "info"
                    flash.message = g.message(code: 'default.upload.csv.plannings.label',args : [returnList.getAt(0),returnList.getAt(1)] )
                    redirect(action: 'index')
                    return
                }else {
                    redirect(action: 'index')
                    flash.type = "danger"
                    flash.message = g.message(code: 'com.ef.app.qm.file.format', default: 'File format is not correct. Please upload only .CSV files')
                    redirect(action: 'index')
                }

            } else {
                flash.type = "danger"
                flash.message = g.message(code: 'com.ef.app.qm.file.empty', default: 'File is empty')
                redirect(action: 'index')
            }
        } catch (Exception ex) {
            println "fatal error occurred while uploading callers from csv: "+ex
            log.error "fatal error occurred while uploading callers from csv: "+ex
        }
    }

    def downloadPlannings(){
        try{
            ArrayList<PlanningResponse> plannings = utilityService?.getPlannings(User.get(session?.user?.id))

            ArrayList<String> fieldsList = ["spaceCode","spaceName","visitName", "planningDate"]
            LinkedHashMap fieldsMap = ["spaceCode":"space Code","spaceName":"space Name","visitName": "visit Name","planningDate": "planning Date"]
                response.contentType = grailsApplication.config.grails.mime.types['csv']
                response.setHeader("Content-disposition", "attachment; filename=plannings.${params.extension}")
                List fields = fieldsList
                Map labels = fieldsMap
                Map parameters = [
                        title:  "Plannings List",
                        "header.enabled":false,
                        separator:";",
                        lineEnd:"\r\n",
                        quoteCharacter:'\u0000'
                ]
                exportService.export('csv', response.outputStream, plannings, fields, labels, [:], parameters)

        }catch (Exception e){
            println "Error: $e"
            redirect(action: "index")
        }
      //  redirect(action: "index")
    }
}
