package com.ef.app.qm

import grails.transaction.Transactional

@Transactional
class MailSenderService {
    def executorService
    def mailService
    def sendEmailNotification(String mailRecipient,String mailSubject,String mailBody) {
        runAsync {
            try {
                System.out.println("I'm trying")
                mailService.sendMail {
                    from "Dashboard.notification@tunisiatelecom.tn"
                    to "${mailRecipient}"
                    subject "${mailSubject}"
                    body "${mailBody}"
                    System.out.println("done")
                }
            } catch (Exception e) {
                System.out.println("Exception: "+e)
                //log.error("Unable to send email notification after creating planning with date ${planningInstance?.planningDate}, Exception:${e.getMessage()}")
            }
        }

    }
}
