package com.ef.app.qm

import org.apache.shiro.SecurityUtils
import org.codehaus.groovy.grails.web.context.ServletContextHolder
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsWebRequest
import org.codehaus.groovy.grails.web.util.WebUtils
import javax.servlet.http.Cookie
import java.text.SimpleDateFormat

class CsvUploaderService {
    //def mailService
    def mailSenderService
    def utilityService
    def executorService
    static transactional = true

    def ArrayList uploadCsv(file) {

        def list = utilityService.listVisitBySpaces()


        def invalidPlanningsCount = 0
        def totalPlanningsCount = 0
        Cookie cookie = null
        File groovyFile = null
        ArrayList<Integer> returnCounts = new ArrayList<Integer>()
        GrailsWebRequest webUtils = WebUtils.retrieveGrailsWebRequest()
        def servletContext = ServletContextHolder.servletContext
        def webDirectory = servletContext.getRealPath("/")
        //def response = webUtils.getCurrentResponse()
        def lineNumber = 0
        def skippedCount= 0
        def user = User.findByUserName(SecurityUtils.getSubject().getPrincipal())
        groovyFile = new File(webDirectory, file)
        try {
            log.debug("Called CsvUploaderService, This service is now going to upload plannings")
            /*UploadingCheck.flag = true
            cookie = new Cookie("uploading", "true")
            cookie.setMaxAge(30 * 30 * 1)
            cookie.setPath('/')
            response.addCookie(cookie)*/
            long start = System.currentTimeMillis()
            def planningInstance
            Date planningDate
            def space
            def spaceCode
            def visit
            def spaceList
            String[] line
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy")
            boolean userIsChief = (User.findByUserName(user)?.profile?.profilName=="Chef de cellule")?true:false
            if(userIsChief)
            {
                spaceList = Space.findAllByUserInList(User.findAllByTeamAndIdNotEqual(User.findByUserName(user)?.team,User.findByUserName(user).id))
            }
            else
            {
                spaceList = Space.list()
            }
            System.out.println("spaceList: "+spaceList.spaceCode)
           // runAsync {
                try {
                    groovyFile.eachLine {
                        lineNumber++
                        try{
                            System.out.println("it: "+it)
                            line = it.split(';')
                            space = Space.findBySpaceCode(line?.getAt(0))
                            spaceCode = space.spaceCode
                            visit = Visit.findByVisitName(line?.getAt(2))
                            planningDate = formatter.parse(line?.getAt(3))


                            // my code

                            //def index = list.indexOf("ETT1,Visite 3")
                            System.out.println("search  : "+spaceCode+","+visit);
                            def index = list.indexOf(spaceCode+","+visit+","+planningDate?.format("dd/MM/yyyy"))

                            System.out.println("index  : "+index);

                            if(index != -1) {
                                skippedCount++
                            }
                            else{
                                planningInstance = Planning.findBySpaceAndVisit(space,visit)
                                System.out.println("space: "+space)
                                if(planningInstance==null)
                                {
                                    System.out.println("No space")
                                    if(spaceList?.contains(space))
                                    {
                                        System.out.println("Creating new planning")
                                        def newPlanningInstance = new Planning(visit: visit, space: space, planningDate: planningDate, planningState: Planning.VALIDATED_STATE)
                                        mailSenderService.sendEmailNotification(newPlanningInstance.space.user.userEmail,"Nouveau planning","Bonjour,\nVeuillez trouver ci-dessous les informations du nouveau planning:\n\nEspace de la visite: ${newPlanningInstance?.space?.spaceName}\nDate de la visite: ${newPlanningInstance?.planningDate?.format("dd/MM/yyyy")}\nNom de la visite: ${newPlanningInstance?.visit?.visitName}\\n\\n\\nProgramme Experience Client")
                                        newPlanningInstance.save()
                                        totalPlanningsCount++
                                    }
                                    else
                                        skippedCount++
                                }
                                else if(planningInstance.planningDate != planningDate && planningInstance.planningState!=Planning.EVALUATED_STATE )
                                {
                                    Planning.findAllByPlanning(planningInstance).each{
                                        it.planningState=Planning.CHANGED_STATE
                                        it.save flush:true
                                    }
                                    mailSenderService.sendEmailNotification(planningInstance.space.user.userEmail,"Nouveau planning","Bonjour,\nVeuillez trouver ci-dessous les informations du nouveau planning:\n\nEspace de la visite: ${planningInstance?.space?.spaceName}\nDate de la visite: ${planningInstance?.planningDate?.format("dd/MM/yyyy")}\nNom de la visite: ${planningInstance?.visit?.visitName}\n\n\nProgramme Experience Client")
                                    planningInstance?.planningDate = planningDate
                                    planningInstance?.save flush:true

                                    totalPlanningsCount++
                                }
                                else
                                    skippedCount++

                                list.add(spaceCode+","+visit)
                            }
                            // end my code


                            System.out.println("skippedCount1111111111111  : "+skippedCount);



                            /* Check here if it is same space to which user belongs
                            *  Discuss with Ibrahim * */
                            /*if(userIsChief && !spaceList?.contains(space))
                            {
                                    skippedCount++
                                    log.info("Line Number ${lineNumber} skipped as the current user is cheif and he cannot create planning for others than his team")
                            }
                            else if(userIsChief && space?.user == user)
                            {
                                skippedCount++
                                log.info("Line Number ${lineNumber} skipped as the current user cheif and cheif cannot create planning for his space")
                            }
                            /* Check if planning already exist for the same space and visit, if
                            so then create another one with same space and visit for verification*/
                            /*else if(planningInstance)
                            {
                                if(space && visit){
                                    planningInstance?.planningState = Planning.CHANGED_STATE
                                    planningInstance?.save()
                                    def newPlanningInstance = new Planning(visit: visit,space: space,planningDate: planningDate,planning: planningInstance,planningState: Planning.WAITING_CONFIRMATION_STATE)
                                    if(!newPlanningInstance?.save())
                                        skippedCount++
                                    else
                                        totalPlanningsCount++
                                }else
                                    skippedCount++
                            }
                            /* Check if a planning date specified in csv line in on holiday or not
                              if so then skip that line*/
                            /*else if(Holiday.findByHolidayDate(planningDate)){
                                skippedCount++
                                log.info("Line Number ${lineNumber} skipped as there is a holiday on the specified date.")
                            }*/
                            /* Check if the space and visit specified in csv line actually exist
                            * in our database, if any of them is missing then skip the line */
                            /*else if(space && visit)
                            {
                                planningInstance = Planning.findBySpaceAndVisit(space,visit)
                                if((planningInstance && (planningInstance?.planningState==Planning.VALIDATED_STATE || planningInstance?.planningState==Planning.EVALUATED_STATE))){
                                    skippedCount++
                                    log.info("Line Number ${lineNumber} skipped, planning already exists for space: $space with visit: $visit.")
                                }
                                else
                                {
                                    planningInstance = new Planning(visit:visit,space: space,planningDate:planningDate)
                                    if(!planningInstance?.save())
                                    {
                                        skippedCount++
                                        log.info("Line Number ${lineNumber} skipped, errors:${planningInstance?.errors}")
                                    }else
                                        totalPlanningsCount++
                                }
                            }
                            else {
                                skippedCount++
                                log.info("Line Number ${lineNumber} skipped, visit or space not found specified in the line.")
                            }*/


                        }
                        catch (Exception e)
                        {
                            System.out.println("1- Exception:"+e)
                        }
                    }
                    groovyFile.delete()
                }
                catch (Exception e) {
                    System.out.println("2- Exception:"+e)
                    /*cookie.setMaxAge(0)
                    response.addCookie(cookie)
                    UploadingCheck.flag = false*/
                    groovyFile?.delete()
                }
                /*cookie.setMaxAge(0)
                response.addCookie(cookie)
                UploadingCheck.flag = false*/

        }
        catch (Exception e) {
            System.out.println("3- Exception:"+e)
            /*cookie.setMaxAge(0)
            response.addCookie(cookie)
            UploadingCheck.flag = false*/
            groovyFile?.delete()
        }
        System.out.println("skippedCount222222222222  : "+skippedCount);

        returnCounts.add(totalPlanningsCount)
        returnCounts.add(skippedCount)
        return  returnCounts
    }
}