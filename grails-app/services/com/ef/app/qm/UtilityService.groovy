package com.ef.app.qm

import grails.transaction.Transactional
import groovy.sql.Sql
import java.util.Calendar

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.Clob;
import java.sql.SQLException;

import java.util.concurrent.TimeUnit

@Transactional
class UtilityService {
    def dataSource


    def int isItWeekend(Date date) {
        Calendar cal = Calendar.getInstance()
        cal.setTime(date)
        def day = cal.get(Calendar.DAY_OF_WEEK)
        return day
    }

    def listScoreGlobal(Long spaceId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("\tselect visite.visit_name,ISNULL(score,0) score \n" +
                "\tfrom (select visite.VISITE_ID,visite.visit_name visit_name\n" +
                "\tfrom planning join visite on planning.visit_id = visite.VISITE_ID\n" +
                "\twhere planning.planning_state = "+Planning.EVALUATED_STATE+" group by visite.VISITE_ID,visite.visit_name) visite \n" +
                "\tLeft Join \n" +
                "\t(select ROUND(AVG(CAST(score AS FLOAT)), 2) score,visit_name,visite_id from evaluation\n" +
                "\tJoin Planning on evaluation.planning_id=planning.id\n" +
                "\tJoin visite on Planning.visit_id=visite.visite_id\n" +
                "\tJoin [Space] on planning.space_id= [Space].Space_id where [Space].Space_id= "+spaceId+" \n" +
                "\tand planning.planning_state="+Planning.EVALUATED_STATE+" and evaluation.eval_status="+Evaluation.VALIDATED_STATE+" \n" +
                "\tgroup by visit_name,visite_id ) a on visite.visite_id= a.visite_id \n" +
                "\torder by visite.VISITE_ID ")
        sql.close()

        return rows
    }

    def listScoreGlobalByCellule(Long celluleId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select visite.visit_name,ISNULL(score,0) score \n" +
                "\tfrom (select visite.VISITE_ID,visite.visit_name visit_name\n" +
                "\tfrom planning join visite on planning.visit_id = visite.VISITE_ID\n" +
                "\twhere planning.planning_state = "+Planning.EVALUATED_STATE+"  group by visite.VISITE_ID,visite.visit_name) visite \n" +
                "\tLeft Join \n" +
                "\t(select  ROUND(AVG(CAST(score AS FLOAT)), 2) score,visit_name,visite_id from evaluation\n" +
                "\tJoin Planning on evaluation.planning_id=planning.id\n" +
                "\tJoin visite on Planning.visit_id=visite.visite_id\n" +
                "\tjoin utilisateur on evaluation.user_id=utilisateur.USER_ID\n" +
                "\tjoin team on utilisateur.team_id = team.TEAM_ID\n" +
                "\twhere evaluation.eval_status="+Evaluation.VALIDATED_STATE+" and  planning.planning_state="+Planning.EVALUATED_STATE+" and team.TEAM_ID="+celluleId+" \n" +
                "\tgroup by visit_name,visite_id ) a on visite.visite_id= a.visite_id order by visite.VISITE_ID ")
        sql.close()

        return rows
    }

    def listScoreGlobalByRegion(String region){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select visite.visit_name,ISNULL(score,0) score\n" +
                "\tfrom (select visite.VISITE_ID,visite.visit_name visit_name\n" +
                "\tfrom planning\tjoin visite on planning.visit_id = visite.VISITE_ID\n" +
                "\twhere planning.planning_state ="+Planning.EVALUATED_STATE+" \tgroup by visite.VISITE_ID,visite.visit_name) visite \n" +
                "\tLeft Join \n" +
                "\t(select ROUND(AVG(CAST(score AS FLOAT)), 2) score,visit_name,visite_id from evaluation\n" +
                "\tJoin Planning on evaluation.planning_id=planning.id\n" +
                "\tJoin visite on Planning.visit_id=visite.visite_id\n" +
                "\tJoin [Space] on planning.space_id= [Space].Space_id\n" +
                "\twhere [Space].region != '' and [Space].region='"+region+"' \tand planning.planning_state="+Planning.EVALUATED_STATE+" \n" +
                "\tand evaluation.eval_status="+Evaluation.VALIDATED_STATE+" group by visit_name,visite_id ) a on visite.visite_id= a.visite_id order by visite.VISITE_ID")
        sql.close()

        return rows
    }

    def listScoreForAll(){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select visite.visit_name,ISNULL(score,0) score \n" +
                "\tfrom (select visite.VISITE_ID,visite.visit_name visit_name\n" +
                "\tfrom planning join visite on planning.visit_id = visite.VISITE_ID\n" +
                "\twhere planning.planning_state = "+Planning.EVALUATED_STATE+" \n" +
                "\tgroup by visite.VISITE_ID,visite.visit_name) visite \n" +
                "\tLeft Join \n" +
                "\t(select ROUND(AVG(CAST(score AS FLOAT)), 2) score,visit_name,visite_id from evaluation\n" +
                "\tJoin Planning on evaluation.planning_id=planning.id\n" +
                "\tJoin visite on Planning.visit_id=visite.visite_id\n" +
                "\tJoin [Space] on planning.space_id= [Space].Space_id\n" +
                "\twhere planning.planning_state="+Planning.EVALUATED_STATE+" \n" +
                "\tand evaluation.eval_status="+Evaluation.VALIDATED_STATE+" \n" +
                "\tgroup by visit_name,visite_id ) a on visite.visite_id= a.visite_id order by visite.VISITE_ID")
        sql.close()

        return rows
    }

    def listScoreBySpaceAndQuestions(Long spaceId,Long questionId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select visite.visit_name,ROUND(ISNULL(score,0), 2) score \n" +
                "\tfrom (select visite.VISITE_ID,visite.visit_name visit_name\n" +
                "\tfrom planning join visite on planning.visit_id = visite.VISITE_ID\n" +
                "\twhere planning.planning_state = "+Planning.EVALUATED_STATE+" group by visite.VISITE_ID,visite.visit_name) visite \n" +
                "\tLeft Join  (select evalanswer_score score,visite_id from evaluation Join Planning on evaluation.planning_id=planning.id\n" +
                "\t\tJoin visite on Planning.visit_id=visite.visite_id Join [Space] on planning.space_id= [Space].Space_id \n" +
                "\t\tJoin evalanswer on evalanswer.evaluation_id=evaluation.evaluation_id join questions on evalanswer.questions_id=questions.question_id\n" +
                "\t\twhere [Space].Space_id="+spaceId+" and questions.QUESTION_ID = "+questionId+" \n" +
                "\t\tand evaluation.eval_status="+Evaluation.VALIDATED_STATE+" and planning.planning_state="+Planning.EVALUATED_STATE+") a on visite.visite_id= a.visite_id order by visite.visite_id ")
        sql.close()

        return rows
    }

    def listScoreBySpaceAndGroupQuestions(Long spaceId,Long groupId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select visite.visit_name,ROUND(ISNULL(score,0), 2) score \n" +
                "\tfrom (select visite.VISITE_ID,visite.visit_name visit_name\n" +
                "\tfrom planning join visite on planning.visit_id = visite.VISITE_ID \n" +
                "\twhere planning.planning_state = "+Planning.EVALUATED_STATE+" group by visite.VISITE_ID,visite.visit_name) visite  Left Join  \n" +
                "\t(select ROUND(AVG(CAST(evalanswer_score AS FLOAT)), 2) score,visite_id from evaluation Join Planning on evaluation.planning_id=planning.id\n" +
                "\tJoin visite on Planning.visit_id=visite.visite_id Join [Space] on planning.space_id= [Space].Space_id \n" +
                "\tJoin evalanswer on evalanswer.evaluation_id=evaluation.evaluation_id join questions on evalanswer.questions_id=questions.question_id\n" +
                "\twhere [Space].Space_id="+spaceId+" and questions.questionnairegroup_id="+groupId+" and evaluation.eval_status="+Evaluation.VALIDATED_STATE+" \n" +
                "\tand planning.planning_state="+Planning.EVALUATED_STATE+" group by visite_id) a on visite.visite_id= a.visite_id order by visite.visite_id")
        sql.close()

        return rows
    }

    def listScoreByQuestionsForAllSpaces(Long questionId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select visite.visit_name,ROUND(ISNULL(score,0), 2) score from (select visite.VISITE_ID,visite.visit_name visit_name from planning\n" +
                "\tjoin visite on planning.visit_id = visite.VISITE_ID where planning.planning_state = "+Planning.EVALUATED_STATE+" group by visite.VISITE_ID,visite.visit_name) visite \n" +
                "\tLeft Join (select ROUND(AVG(CAST(evalanswer_score AS FLOAT)), 2) score,visite_id from evaluation\n" +
                "\tJoin Planning on evaluation.planning_id=planning.id Join visite on Planning.visit_id=visite.visite_id \n" +
                "\tJoin evalanswer on evalanswer.evaluation_id=evaluation.evaluation_id\n" +
                "\tjoin questions on evalanswer.questions_id=questions.question_id where questions.QUESTION_ID = "+questionId+" and evaluation.eval_status= "+Evaluation.VALIDATED_STATE+" \n" +
                "\tand planning.planning_state="+Planning.EVALUATED_STATE+" group by visit_name,visite_id ) a on visite.visite_id= a.visite_id order by visite.visite_id")
        sql.close()
        return rows
    }

    def listMoyenneScoreByGroupForAllSpaces(Long groupId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select visite.visit_name,ROUND(ISNULL(score,0), 2) score from (select visite.VISITE_ID,visite.visit_name visit_name from planning\n" +
                "\tjoin visite on planning.visit_id = visite.VISITE_ID where planning.planning_state = "+Planning.EVALUATED_STATE+" group by visite.VISITE_ID,visite.visit_name) visite\n" +
                "\tLeft Join (select ROUND(AVG(CAST(evalanswer_score AS FLOAT)), 2) score,visite_id from evaluation Join Planning on evaluation.planning_id=planning.id\n" +
                "\tJoin visite on Planning.visit_id=visite.visite_id Join evalanswer on evalanswer.evaluation_id=evaluation.evaluation_id\n" +
                "\tjoin questions on evalanswer.questions_id=questions.question_id where questions.questionnairegroup_id = "+groupId+" and evaluation.eval_status="+Evaluation.VALIDATED_STATE+" \n" +
                "\tand planning.planning_state="+Planning.EVALUATED_STATE+" group by visit_name,visite_id ) a on visite.visite_id= a.visite_id order by visite.visite_id")
        sql.close()
        return rows
    }

    def listScoreByCelluleAndQuestions(Long celluleId,Long questionId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select visite.visit_name,ROUND(CAST(ISNULL(score,0) AS FLOAT),2) score from (select visite.VISITE_ID,visite.visit_name visit_name\n" +
                "\tfrom planning join visite on planning.visit_id = visite.VISITE_ID where planning.planning_state = "+Planning.EVALUATED_STATE+" \n" +
                "\tgroup by visite.VISITE_ID,visite.visit_name) visite Left Join( select ROUND(AVG(CAST(evalanswer_score AS FLOAT)), 2) score,VISITE_ID from evaluation    \n" +
                "\tjoin utilisateur on evaluation.user_id = utilisateur.USER_ID join team on utilisateur.team_id = team.TEAM_ID \n" +
                "\tJoin Planning on evaluation.planning_id=planning.id Join visite on Planning.visit_id=visite.visite_id \n" +
                "\tJoin [Space] on planning.space_id= [Space].Space_id Join evalanswer on evalanswer.evaluation_id=evaluation.evaluation_id \n" +
                "\tjoin questions on evalanswer.questions_id=questions.question_id where questions.QUESTION_ID = "+questionId+" and team.TEAM_ID = "+celluleId+" \n" +
                "\tand evaluation.eval_status="+Evaluation.VALIDATED_STATE+" and planning.planning_state="+Planning.EVALUATED_STATE+" group by VISITE_ID ) a on visite.visite_id= a.visite_id order by visite.visite_id")
        sql.close()
        return rows
    }

    def listScoreByQuestionsForAllCellule(Long questionId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("\tselect visite.visit_name,ROUND(CAST(ISNULL(score,0) AS FLOAT),2) score from (select visite.VISITE_ID,visite.visit_name visit_name from planning\n" +
                "\tjoin visite on planning.visit_id = visite.VISITE_ID where planning.planning_state = "+Planning.EVALUATED_STATE+" group by visite.VISITE_ID,visite.visit_name) visite\n" +
                "\tLeft Join ( select ROUND(AVG(CAST(evalanswer_score AS FLOAT)), 2) score,VISITE_ID from evaluation \n" +
                "\tjoin utilisateur on evaluation.user_id = utilisateur.USER_ID Join Planning on evaluation.planning_id=planning.id \n" +
                "\tJoin visite on Planning.visit_id=visite.visite_id Join [Space] on planning.space_id= [Space].Space_id \n" +
                "\tJoin evalanswer on evalanswer.evaluation_id=evaluation.evaluation_id join questions on evalanswer.questions_id=questions.question_id\n" +
                "\twhere questions.QUESTION_ID = "+questionId+" and evaluation.eval_status="+Evaluation.VALIDATED_STATE+" and planning.planning_state="+Planning.EVALUATED_STATE+" group by VISITE_ID ) a \n" +
                "\ton visite.visite_id= a.visite_id order by visite.visite_id")
        sql.close()
        return rows
    }

    def listScoreByTeamAndGroupQuestions(Long teamId,Long groupId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select visite.visit_name,ROUND(CAST(ISNULL(score,0) AS FLOAT),2) score from (select visite.VISITE_ID,visite.visit_name visit_name from planning\n" +
                "\tjoin visite on planning.visit_id = visite.VISITE_ID where planning.planning_state = "+Planning.EVALUATED_STATE+" group by visite.VISITE_ID,visite.visit_name) visite\n" +
                "\tLeft Join( select ROUND(AVG(CAST(evalanswer_score AS FLOAT)), 2) score,VISITE_ID from evaluation join utilisateur on evaluation.user_id = utilisateur.USER_ID\n" +
                "\tjoin team on utilisateur.team_id = team.TEAM_ID Join Planning on evaluation.planning_id=planning.id Join visite on Planning.visit_id=visite.visite_id\n" +
                "\tJoin [Space] on planning.space_id= [Space].Space_id Join evalanswer on evalanswer.evaluation_id=evaluation.evaluation_id \n" +
                "\tjoin questions on evalanswer.questions_id=questions.question_id\n" +
                "\twhere questions.questionnairegroup_id="+groupId+" and team.TEAM_ID ="+teamId+" and evaluation.eval_status="+Evaluation.VALIDATED_STATE+" and planning.planning_state="+Planning.EVALUATED_STATE+" group by VISITE_ID ) a \n" +
                "\ton visite.visite_id= a.visite_id order by visite.visite_id")
        sql.close()
        return rows
    }

    def listMoyenneScoreByGroupForAllTeam(Long groupId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select visite.visit_name,ROUND(CAST(ISNULL(score,0) AS FLOAT),2) score \n" +
                "from (select visite.VISITE_ID,visite.visit_name visit_name\n" +
                "from planning\n" +
                "join visite on planning.visit_id = visite.VISITE_ID\n" +
                "where planning.planning_state = "+Planning.EVALUATED_STATE+" \n" +
                "group by visite.VISITE_ID,visite.visit_name) visite \n" +
                "Left Join \n" +
                "(\n" +
                "select ROUND(AVG(CAST(evalanswer_score AS FLOAT)), 2) score,VISITE_ID\n" +
                "from evaluation\n" +
                "join utilisateur on evaluation.user_id = utilisateur.USER_ID\n" +
                "Join Planning on evaluation.planning_id=planning.id\n" +
                "Join visite on Planning.visit_id=visite.visite_id\n" +
                "Join [Space] on planning.space_id= [Space].Space_id\n" +
                "Join evalanswer on evalanswer.evaluation_id=evaluation.evaluation_id\n" +
                "join questions on evalanswer.questions_id=questions.question_id\n" +
                "where questions.questionnairegroup_id="+groupId+" and evaluation.eval_status="+Evaluation.VALIDATED_STATE+" and planning.planning_state="+Planning.EVALUATED_STATE+" \n" +
                "group by VISITE_ID ) a on visite.visite_id= a.visite_id\n" +
                "order by visite.visite_id ")
        sql.close()
        return rows
    }

    def listScoreByRegionAndQuestions(String region,Long questionId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select visite.visit_name,ISNULL(score,0) score \n" +
                "from (select visite.VISITE_ID,visite.visit_name visit_name\n" +
                "from planning\n" +
                "join visite on planning.visit_id = visite.VISITE_ID\n" +
                "where planning.planning_state = "+Planning.EVALUATED_STATE+" \n" +
                "group by visite.VISITE_ID,visite.visit_name) visite \n" +
                "Left Join \n" +
                "(\n" +
                "select ROUND(AVG(CAST(evalanswer_score AS FLOAT)), 2) score,visite_id\n" +
                "from evaluation\n" +
                "Join Planning on evaluation.planning_id=planning.id\n" +
                "Join visite on Planning.visit_id=visite.visite_id\n" +
                "Join [Space] on planning.space_id= [Space].Space_id\n" +
                "\t\tJoin evalanswer on evalanswer.evaluation_id=evaluation.evaluation_id\n" +
                "join questions on evalanswer.questions_id=questions.question_id\n" +
                "where region like '"+region+"' and questions.QUESTION_ID = "+questionId+" and evaluation.eval_status="+Evaluation.VALIDATED_STATE+" and planning.planning_state="+Planning.EVALUATED_STATE+" \n" +
                "group by visite_id ) a on visite.visite_id= a.visite_id \n" +
                "order by visite.visite_id")
        sql.close()
        return rows
    }

    def listScoreByQuestionsForAllRegion(Long questionId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select visite.visit_name,ISNULL(score,0) score \n" +
                "from (select visite.VISITE_ID,visite.visit_name visit_name\n" +
                "from planning\n" +
                "join visite on planning.visit_id = visite.VISITE_ID\n" +
                "where planning.planning_state = "+Planning.EVALUATED_STATE+" \n" +
                "group by visite.VISITE_ID,visite.visit_name) visite \n" +
                "Left Join \n" +
                "(\n" +
                "select ROUND(AVG(CAST(evalanswer_score AS FLOAT)), 2) score,visite_id\n" +
                "from evaluation\n" +
                "Join Planning on evaluation.planning_id=planning.id\n" +
                "Join visite on Planning.visit_id=visite.visite_id\n" +
                "Join evalanswer on evalanswer.evaluation_id=evaluation.evaluation_id\n" +
                "join questions on evalanswer.questions_id=questions.question_id\n" +
                "where questions.QUESTION_ID = "+questionId+" and evaluation.eval_status="+Evaluation.VALIDATED_STATE+" and planning.planning_state="+Planning.EVALUATED_STATE+" \n" +
                "group by visite_id ) a on visite.visite_id= a.visite_id \n" +
                "order by visite.visite_id")
        sql.close()
        return rows
    }

    def listScoreByRegionAndGroupQuestions(String region,Long groupId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select visite.visit_name,ISNULL(score,0) score \n" +
                "from (select visite.VISITE_ID,visite.visit_name visit_name\n" +
                "from planning\n" +
                "join visite on planning.visit_id = visite.VISITE_ID\n" +
                "where planning.planning_state = "+Planning.EVALUATED_STATE+" \n" +
                "group by visite.VISITE_ID,visite.visit_name) visite \n" +
                "Left Join \n" +
                "(\n" +
                "select ROUND(AVG(CAST(evalanswer_score AS FLOAT)), 2) score,visite_id\n" +
                "from evaluation\n" +
                "Join Planning on evaluation.planning_id=planning.id\n" +
                "Join visite on Planning.visit_id=visite.visite_id\n" +
                "Join [Space] on planning.space_id= [Space].Space_id\n" +
                "\tJoin evalanswer on evalanswer.evaluation_id=evaluation.evaluation_id\n" +
                "join questions on evalanswer.questions_id=questions.question_id\n" +
                "where questions.questionnairegroup_id="+groupId+" and region like '"+region+"' and evaluation.eval_status="+Evaluation.VALIDATED_STATE+" and planning.planning_state="+Planning.EVALUATED_STATE+" \n" +
                "group by visite_id ) a on visite.visite_id= a.visite_id \n" +
                "order by visite.visite_id")
        sql.close()
        return rows
    }

    def listMoyenneScoreByGroupForAllRegion(Long groupId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select visite.visit_name,ISNULL(score,0) score \n" +
                "from (select visite.VISITE_ID,visite.visit_name visit_name\n" +
                "from planning\n" +
                "join visite on planning.visit_id = visite.VISITE_ID\n" +
                "where planning.planning_state = "+Planning.EVALUATED_STATE+" \n" +
                "group by visite.VISITE_ID,visite.visit_name) visite \n" +
                "Left Join \n" +
                "(\n" +
                "select ROUND(AVG(CAST(evalanswer_score AS FLOAT)), 2) score,visite_id\n" +
                "from evaluation\n" +
                "Join Planning on evaluation.planning_id=planning.id\n" +
                "Join visite on Planning.visit_id=visite.visite_id\n" +
                "\t\tJoin evalanswer on evalanswer.evaluation_id=evaluation.evaluation_id\n" +
                "join questions on evalanswer.questions_id=questions.question_id\n" +
                "where questions.questionnairegroup_id="+groupId+" and evaluation.eval_status="+Evaluation.VALIDATED_STATE+" and planning.planning_state="+Planning.EVALUATED_STATE+" \n" +
                "group by visite_id ) a on visite.visite_id= a.visite_id \n" +
                "order by visite.visite_id")
        sql.close()
        return rows
    }

    def listScoreByUserAndQuestions(Long userId,Long questionId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select visite.visit_name,ISNULL(score,0) score \n" +
                "from (select visite.VISITE_ID,visite.visit_name visit_name\n" +
                "from planning\n" +
                "join visite on planning.visit_id = visite.VISITE_ID\n" +
                "where planning.planning_state = "+Planning.EVALUATED_STATE+" \n" +
                "group by visite.VISITE_ID,visite.visit_name) visite \n" +
                "Left Join \n" +
                "(\n" +
                "select ROUND(AVG(CAST(evalanswer_score AS FLOAT)), 2) score,VISITE_ID\n" +
                "\t\tfrom evaluation\n" +
                "join utilisateur on evaluation.user_id = utilisateur.USER_ID\n" +
                "\t\tJoin Planning on evaluation.planning_id=planning.id\n" +
                "Join visite on Planning.visit_id=visite.visite_id\n" +
                "Join evalanswer on evalanswer.evaluation_id=evaluation.evaluation_id\n" +
                "join questions on evalanswer.questions_id=questions.question_id\n" +
                "where questions.QUESTION_ID = "+questionId+" and evaluation.USER_ID="+userId+" \n" +
                "and evaluation.eval_status="+Evaluation.VALIDATED_STATE+" and planning.planning_state="+Planning.EVALUATED_STATE+" \n" +
                "group by VISITE_ID ) a on visite.visite_id= a.visite_id\n" +
                "order by visite.visite_id")
        sql.close()
        return rows
    }

    def listScoreByQuestionsForAllUsers(Long questionId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select visite.visit_name,ISNULL(score,0) score \n" +
                "from (select visite.VISITE_ID,visite.visit_name visit_name\n" +
                "from planning\n" +
                "join visite on planning.visit_id = visite.VISITE_ID\n" +
                "where planning.planning_state = "+Planning.EVALUATED_STATE+" \n" +
                "group by visite.VISITE_ID,visite.visit_name) visite \n" +
                "Left Join \n" +
                "(\n" +
                "select ROUND(AVG(CAST(evalanswer_score AS FLOAT)), 2) score,VISITE_ID\n" +
                "\t\tfrom evaluation\n" +
                "Join Planning on evaluation.planning_id=planning.id\n" +
                "Join visite on Planning.visit_id=visite.visite_id\n" +
                "Join evalanswer on evalanswer.evaluation_id=evaluation.evaluation_id\n" +
                "join questions on evalanswer.questions_id=questions.question_id\n" +
                "where questions.QUESTION_ID = "+questionId+" \n" +
                "and evaluation.eval_status="+Evaluation.VALIDATED_STATE+" and planning.planning_state="+Planning.EVALUATED_STATE+" \n" +
                "group by VISITE_ID ) a on visite.visite_id= a.visite_id\n" +
                "order by visite.visite_id")
        sql.close()
        return rows
    }

    def listScoreByUserAndGroupQuestions(Long userId,Long groupId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select visite.visit_name,ISNULL(score,0) score \n" +
                "from (select visite.VISITE_ID,visite.visit_name visit_name\n" +
                "from planning\n" +
                "join visite on planning.visit_id = visite.VISITE_ID\n" +
                "where planning.planning_state = "+Planning.EVALUATED_STATE+" \n" +
                "group by visite.VISITE_ID,visite.visit_name) visite \n" +
                "Left Join \n" +
                "(\n" +
                "select ROUND(AVG(CAST(evalanswer_score AS FLOAT)), 2) score,VISITE_ID\n" +
                "\t\tfrom evaluation\n" +
                "join utilisateur on evaluation.user_id = utilisateur.USER_ID\n" +
                "\t\tJoin Planning on evaluation.planning_id=planning.id\n" +
                "Join visite on Planning.visit_id=visite.visite_id\n" +
                "Join evalanswer on evalanswer.evaluation_id=evaluation.evaluation_id\n" +
                "join questions on evalanswer.questions_id=questions.question_id\n" +
                "where questions.questionnairegroup_id="+groupId+" and evaluation.USER_ID="+userId+" \n" +
                "and evaluation.eval_status="+Evaluation.VALIDATED_STATE+" and planning.planning_state="+Planning.EVALUATED_STATE+" \n" +
                "group by VISITE_ID ) a on visite.visite_id= a.visite_id\n" +
                "order by visite.visite_id")
        sql.close()
        return rows
    }

    def listMoyenneScoreByGroupForAllUsers(Long groupId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select visite.visit_name,ISNULL(score,0) score \n" +
                "from (select visite.VISITE_ID,visite.visit_name visit_name\n" +
                "from planning\n" +
                "join visite on planning.visit_id = visite.VISITE_ID\n" +
                "where planning.planning_state = "+Planning.EVALUATED_STATE+" \n" +
                "group by visite.VISITE_ID,visite.visit_name) visite \n" +
                "Left Join \n" +
                "(\n" +
                "select ROUND(AVG(CAST(evalanswer_score AS FLOAT)), 2) score,VISITE_ID\n" +
                "\t\tfrom evaluation\n" +
                "Join Planning on evaluation.planning_id=planning.id\n" +
                "Join visite on Planning.visit_id=visite.visite_id\n" +
                "Join evalanswer on evalanswer.evaluation_id=evaluation.evaluation_id\n" +
                "join questions on evalanswer.questions_id=questions.question_id\n" +
                "where questions.questionnairegroup_id="+groupId+" \n" +
                "and evaluation.eval_status="+Evaluation.VALIDATED_STATE+" and planning.planning_state="+Planning.EVALUATED_STATE+" \n" +
                "group by VISITE_ID ) a on visite.visite_id= a.visite_id\n" +
                "order by visite.visite_id")
        sql.close()
        return rows
    }

    def listRegion(){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select distinct region from space where region!='' and region is not null order by region")
        sql.close()
        return rows
    }

    def listUserHasEvaluation(){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select utilisateur.USER_ID id,utilisateur.first_name+' '+utilisateur.last_name name\n" +
                "    from evaluation\n" +
                "    join utilisateur on evaluation.user_id = utilisateur.USER_ID\n" +
                "    where evaluation.eval_status="+Evaluation.VALIDATED_STATE+" group by utilisateur.USER_ID,utilisateur.first_name+' '+utilisateur.last_name\n" +
                "    order by name")
        sql.close()
        return rows
    }

    def listAnswersByEvaluationToExport(Long evaluationId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select questionnairegroup_name,question_name,evalanswer_score,evalanswer_animator_comment,\n" +
                "    evalanswer_sup_comment,evalanswer_centrale_comment\n" +
                "    from evalanswer\n" +
                "    join evaluation on evalanswer.evaluation_id = evaluation.EVALUATION_ID\n" +
                "    join questions on questions.QUESTION_ID = evalanswer.questions_id\n" +
                "    join questionnairegroup on questionnairegroup.QUESTIONNAIREGROUP_ID = questions.questionnairegroup_id\n" +
                "    where evaluation.EVALUATION_ID = "+evaluationId+" order by question_id")
        sql.close()
        return rows
    }

    def getPlanningByYear(){
        def sql = new Sql(dataSource)
        def rows = sql.rows("WITH mycte AS\n" +
                "(\n" +
                "  SELECT CAST('2010-01-01' AS DATETIME) DateValue\n" +
                "  UNION ALL\n" +
                "  SELECT  DateValue + 1\n" +
                "  FROM    mycte   \n" +
                "  WHERE   DateValue + 1 < '2030-12-31'\n" +
                ")\n" +
                "\n" +
                "select COUNT(YEAR (pl.planning_date)) planning_counter,YEAR (pl.planning_date) planning_year\n" +
                "from \n" +
                "(\n" +
                "SELECT DateValue planning_date,'1' holiday,p.user_id user_holiday\n" +
                "FROM    mycte,\n" +
                "( select start_date,end_date,user_id\n" +
                "from user_holidays) p\n" +
                "where DateValue between p.start_date and p.end_date\n" +
                "union\n" +
                "select planning_date,'0' holiday,cast('0' AS numeric) user_holiday from planning \n" +
                "where planning_state in ("+Planning.VALIDATED_STATE+","+Planning.WAITING_CONFIRMATION_STATE+") \n" +
                "and planning_date is not null\n" +
                "and planning_date not in \n" +
                "(\n" +
                "SELECT DateValue\n" +
                "FROM    mycte,\n" +
                "( select start_date,end_date\n" +
                "from user_holidays) p\n" +
                "where DateValue between p.start_date and p.end_date\n" +
                ")\n" +
                "group by planning_date\n" +
                ") pl\n" +
                "group by YEAR (pl.planning_date)\n" +
                "order by YEAR (pl.planning_date)\n" +
                "OPTION (MAXRECURSION 0)")
        sql.close()
        return rows
    }

    def getMyPlanningByYear(Long userId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("WITH mycte AS\n" +
                "(\n" +
                "  SELECT CAST('2010-01-01' AS DATETIME) DateValue\n" +
                "  UNION ALL\n" +
                "  SELECT  DateValue + 1\n" +
                "  FROM    mycte   \n" +
                "  WHERE   DateValue + 1 < '2030-12-31'\n" +
                ")\n" +
                "\n" +
                "select COUNT(Month (pl.planning_date)) planning_counter,YEAR (pl.planning_date) year\n" +
                "from \n" +
                "(\n" +
                "SELECT DateValue planning_date,'1' holiday,p.user_id user_holiday\n" +
                "FROM    mycte,\n" +
                "( select start_date,end_date,user_id\n" +
                "from user_holidays) p\n" +
                "where DateValue between p.start_date and p.end_date\n" +
                "union\n" +
                "select planning_date,'0' holiday,cast('0' AS numeric) user_holiday \n" +
                "from planning \n" +
                "join (select * from space where space.user_id= "+userId+" ) space on space.SPACE_ID = planning.space_id \n" +
                "where planning_state in ("+Planning.VALIDATED_STATE+","+Planning.WAITING_CONFIRMATION_STATE+") \n" +
                "and planning_date is not null\n" +
                "and planning_date not in \n" +
                "(\n" +
                "SELECT DateValue\n" +
                "FROM    mycte,\n" +
                "( select start_date,end_date\n" +
                "from user_holidays) p\n" +
                "where DateValue between p.start_date and p.end_date\n" +
                ")\n" +
                "group by planning_date\n" +
                ") pl\n" +
                "group by YEAR (pl.planning_date) \n" +
                "order by YEAR (pl.planning_date)\n" +
                "OPTION (MAXRECURSION 0)")
        sql.close()
        return rows
    }

    def getMyTeamPlanningByYear(Long userId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("WITH mycte AS\n" +
                "(\n" +
                "  SELECT CAST('2010-01-01' AS DATETIME) DateValue\n" +
                "  UNION ALL\n" +
                "  SELECT  DateValue + 1\n" +
                "  FROM    mycte   \n" +
                "  WHERE   DateValue + 1 < '2030-12-31'\n" +
                ")\n" +
                "\n" +
                "select COUNT(Month (pl.planning_date)) planning_counter,YEAR (pl.planning_date) year\n" +
                "from \n" +
                "(\n" +
                "SELECT DateValue planning_date,'1' holiday,p.user_id user_holiday\n" +
                "FROM    mycte,\n" +
                "( select start_date,end_date,user_id\n" +
                "from user_holidays) p\n" +
                "where DateValue between p.start_date and p.end_date\n" +
                "union\n" +
                "select planning_date,'0' holiday,cast('0' AS numeric) user_holiday \n" +
                "from planning \n" +
                "join (\n" +
                "select * from space \n" +
                "where space.user_id in \n" +
                "(\n" +
                " select user_id from utilisateur\n" +
                " where team_id = (select team_id from utilisateur where user_id= "+userId+" )\n" +
                ")\n" +
                ") space on space.SPACE_ID = planning.space_id\n" +
                "where planning_state in ("+Planning.VALIDATED_STATE+","+Planning.WAITING_CONFIRMATION_STATE+") \n" +
                "and planning_date is not null\n" +
                "and planning_date not in \n" +
                "(\n" +
                "SELECT DateValue\n" +
                "FROM    mycte,\n" +
                "( select start_date,end_date\n" +
                "from user_holidays) p\n" +
                "where DateValue between p.start_date and p.end_date\n" +
                ")\n" +
                "group by planning_date\n" +
                ") pl\n" +
                "group by YEAR (pl.planning_date) \n" +
                "order by YEAR (pl.planning_date)\n" +
                "OPTION (MAXRECURSION 0)")
        sql.close()
        return rows
    }

    def getPlanningByMonth(){
        def sql = new Sql(dataSource)
        def rows = sql.rows("WITH mycte AS\n" +
                "(\n" +
                "  SELECT CAST('2010-01-01' AS DATETIME) DateValue\n" +
                "  UNION ALL\n" +
                "  SELECT  DateValue + 1\n" +
                "  FROM    mycte   \n" +
                "  WHERE   DateValue + 1 < '2030-12-31'\n" +
                ")\n" +
                "\n" +
                "select COUNT(Month (pl.planning_date)) planning_counter,Month (pl.planning_date) planning_month,YEAR (pl.planning_date) year\n" +
                "from \n" +
                "(\n" +
                "SELECT DateValue planning_date,'1' holiday,p.user_id user_holiday\n" +
                "FROM    mycte,\n" +
                "( select start_date,end_date,user_id\n" +
                "from user_holidays) p\n" +
                "where DateValue between p.start_date and p.end_date\n" +
                "union\n" +
                "select planning_date,'0' holiday,cast('0' AS numeric) user_holiday from planning \n" +
                "where planning_state in ("+Planning.VALIDATED_STATE+","+Planning.WAITING_CONFIRMATION_STATE+") \n" +
                "and planning_date is not null\n" +
                "and planning_date not in \n" +
                "(\n" +
                "SELECT DateValue\n" +
                "FROM    mycte,\n" +
                "( select start_date,end_date\n" +
                "from user_holidays) p\n" +
                "where DateValue between p.start_date and p.end_date\n" +
                ")\n" +
                "group by planning_date\n" +
                ") pl\n" +
                "group by Month (pl.planning_date),YEAR (pl.planning_date) \n" +
                "order by YEAR (pl.planning_date),Month (pl.planning_date)\n" +
                "OPTION (MAXRECURSION 0)")
        sql.close()
        return rows
    }

    def getMyPlanningByMonth(Long userId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("WITH mycte AS\n" +
                "(\n" +
                "  SELECT CAST('2010-01-01' AS DATETIME) DateValue\n" +
                "  UNION ALL\n" +
                "  SELECT  DateValue + 1\n" +
                "  FROM    mycte   \n" +
                "  WHERE   DateValue + 1 < '2030-12-31'\n" +
                ")\n" +
                "\n" +
                "select COUNT(Month (pl.planning_date)) planning_counter,Month (pl.planning_date) planning_month,YEAR (pl.planning_date) year\n" +
                "from \n" +
                "(\n" +
                "SELECT DateValue planning_date,'1' holiday,p.user_id user_holiday\n" +
                "FROM    mycte,\n" +
                "( select start_date,end_date,user_id\n" +
                "from user_holidays) p\n" +
                "where DateValue between p.start_date and p.end_date\n" +
                "union\n" +
                "select planning_date,'0' holiday,cast('0' AS numeric) user_holiday \n" +
                "from planning \n" +
                "join (select * from space where space.user_id= "+userId+" ) space on space.SPACE_ID = planning.space_id \n" +
                "where planning_state in ("+Planning.VALIDATED_STATE+","+Planning.WAITING_CONFIRMATION_STATE+") \n" +
                "and planning_date is not null\n" +
                "and planning_date not in \n" +
                "(\n" +
                "SELECT DateValue\n" +
                "FROM    mycte,\n" +
                "( select start_date,end_date\n" +
                "from user_holidays) p\n" +
                "where DateValue between p.start_date and p.end_date\n" +
                ")\n" +
                "group by planning_date\n" +
                ") pl\n" +
                "group by Month (pl.planning_date),YEAR (pl.planning_date) \n" +
                "order by YEAR (pl.planning_date),Month (pl.planning_date)\n" +
                "OPTION (MAXRECURSION 0)")
        sql.close()
        return rows
    }

    def getMyTeamPlanningByMonth(Long userId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("WITH mycte AS\n" +
                "(\n" +
                "  SELECT CAST('2010-01-01' AS DATETIME) DateValue\n" +
                "  UNION ALL\n" +
                "  SELECT  DateValue + 1\n" +
                "  FROM    mycte   \n" +
                "  WHERE   DateValue + 1 < '2030-12-31'\n" +
                ")\n" +
                "\n" +
                "select COUNT(Month (pl.planning_date)) planning_counter,Month (pl.planning_date) planning_month,YEAR (pl.planning_date) year\n" +
                "from \n" +
                "(\n" +
                "SELECT DateValue planning_date,'1' holiday,p.user_id user_holiday\n" +
                "FROM    mycte,\n" +
                "( select start_date,end_date,user_id\n" +
                "from user_holidays) p\n" +
                "where DateValue between p.start_date and p.end_date\n" +
                "union\n" +
                "select planning_date,'0' holiday,cast('0' AS numeric) user_holiday \n" +
                "from planning \n" +
                "join (\n" +
                "select * from space \n" +
                "where space.user_id in \n" +
                "(\n" +
                " select user_id from utilisateur\n" +
                " where team_id = (select team_id from utilisateur where user_id= "+userId+" )\n" +
                ")\n" +
                ") space on space.SPACE_ID = planning.space_id\n" +
                "where planning_state in ("+Planning.VALIDATED_STATE+","+Planning.WAITING_CONFIRMATION_STATE+") \n" +
                "and planning_date is not null\n" +
                "and planning_date not in \n" +
                "(\n" +
                "SELECT DateValue\n" +
                "FROM    mycte,\n" +
                "( select start_date,end_date\n" +
                "from user_holidays) p\n" +
                "where DateValue between p.start_date and p.end_date\n" +
                ")\n" +
                "group by planning_date\n" +
                ") pl\n" +
                "group by Month (pl.planning_date),YEAR (pl.planning_date) \n" +
                "order by YEAR (pl.planning_date),Month (pl.planning_date)\n" +
                "OPTION (MAXRECURSION 0)")
        sql.close()
        return rows
    }

    def getPlanningByDay(){
        def sql = new Sql(dataSource)
        def rows = sql.rows("WITH mycte AS\n" +
                "(\n" +
                "  SELECT CAST('2010-01-01' AS DATETIME) DateValue\n" +
                "  UNION ALL\n" +
                "  SELECT  DateValue + 1\n" +
                "  FROM    mycte   \n" +
                "  WHERE   DateValue + 1 < '2030-12-31'\n" +
                ")\n" +
                "\n" +
                "SELECT DateValue planning_date,Month (DateValue) planning_month,Day (DateValue) planning_day,'1' holiday,p.user_id user_holiday\n" +
                "FROM    mycte,\n" +
                "( select start_date,end_date,user_id\n" +
                "from user_holidays) p\n" +
                "where DateValue between p.start_date and p.end_date\n" +
                "union\n" +
                "select planning_date,Month (planning_date) planning_month,Day (planning_date) planning_day,'0' holiday,cast('0' AS numeric) user_holiday from planning \n" +
                "where planning_state in ("+Planning.VALIDATED_STATE+","+Planning.WAITING_CONFIRMATION_STATE+") \n" +
                "and planning_date is not null\n" +
                "and planning_date not in \n" +
                "(\n" +
                "SELECT DateValue\n" +
                "FROM    mycte,\n" +
                "( select start_date,end_date\n" +
                "from user_holidays) p\n" +
                "where DateValue between p.start_date and p.end_date\n" +
                ")\n" +
                "group by planning_date\n" +
                "order by planning_date\n" +
                "OPTION (MAXRECURSION 0)")
        sql.close()
        return rows
    }

    def getMyPlanningByDay(Long userId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("WITH mycte AS\n" +
                "(\n" +
                "  SELECT CAST('2010-01-01' AS DATETIME) DateValue\n" +
                "  UNION ALL\n" +
                "  SELECT  DateValue + 1\n" +
                "  FROM    mycte   \n" +
                "  WHERE   DateValue + 1 < '2030-12-31'\n" +
                ")\n" +
                "\n" +
                "SELECT DateValue planning_date,Month (DateValue) planning_month,Day (DateValue) planning_day,'1' holiday,p.user_id user_holiday\n" +
                "FROM    mycte,\n" +
                "( select start_date,end_date,user_id\n" +
                "from user_holidays) p\n" +
                "where DateValue between p.start_date and p.end_date\n" +
                "union\n" +
                "select planning_date,Month (planning_date) planning_month,Day (planning_date) planning_day,'0' holiday,cast('0' AS numeric) user_holiday \n" +
                "from planning \n" +
                "join (select * from space where space.user_id= "+userId+" ) space on space.SPACE_ID = planning.space_id \n" +
                "where planning_state in ("+Planning.VALIDATED_STATE+","+Planning.WAITING_CONFIRMATION_STATE+") \n" +
                "and planning_date is not null\n" +
                "and planning_date not in \n" +
                "(\n" +
                "SELECT DateValue\n" +
                "FROM    mycte,\n" +
                "( select start_date,end_date\n" +
                "from user_holidays) p\n" +
                "where DateValue between p.start_date and p.end_date\n" +
                ")\n" +
                "group by planning_date\n" +
                "order by planning_date\n" +
                "OPTION (MAXRECURSION 0)")
        sql.close()
        return rows
    }

    def getMyTeamPlanningByDay(Long userId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("WITH mycte AS\n" +
                "(\n" +
                "  SELECT CAST('2010-01-01' AS DATETIME) DateValue\n" +
                "  UNION ALL\n" +
                "  SELECT  DateValue + 1\n" +
                "  FROM    mycte   \n" +
                "  WHERE   DateValue + 1 < '2030-12-31'\n" +
                ")\n" +
                "\n" +
                "SELECT DateValue planning_date,Month (DateValue) planning_month,Day (DateValue) planning_day,'1' holiday,p.user_id user_holiday\n" +
                "FROM    mycte,\n" +
                "( select start_date,end_date,user_id\n" +
                "from user_holidays) p\n" +
                "where DateValue between p.start_date and p.end_date\n" +
                "union\n" +
                "select planning_date,Month (planning_date) planning_month,Day (planning_date) planning_day,'0' holiday,cast('0' AS numeric) user_holiday \n" +
                "from planning \n" +
                "join (\n" +
                "select * from space \n" +
                "where space.user_id in \n" +
                "(\n" +
                " select user_id from utilisateur\n" +
                " where team_id = (select team_id from utilisateur where user_id= "+userId+" )\n" +
                ")\n" +
                ") space on space.SPACE_ID = planning.space_id\n" +
                "where planning_state in ("+Planning.VALIDATED_STATE+","+Planning.WAITING_CONFIRMATION_STATE+") \n" +
                "and planning_date is not null\n" +
                "and planning_date not in \n" +
                "(\n" +
                "SELECT DateValue\n" +
                "FROM    mycte,\n" +
                "( select start_date,end_date\n" +
                "from user_holidays) p\n" +
                "where DateValue between p.start_date and p.end_date\n" +
                ")\n" +
                "group by planning_date\n" +
                "order by planning_date\n" +
                "OPTION (MAXRECURSION 0)")
        sql.close()
        return rows
    }

    def getListUsersHasPlanning(){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select u.USER_ID userId\n" +
                "    from planning join [space] on planning.space_id = space.SPACE_ID join utilisateur u on space.user_id = u.USER_ID\n" +
                "    join (select utilisateur.USER_ID userId,utilisateur.first_name+' '+utilisateur.last_name utilisateur,count(distinct space.space_name)\n" +
                "    nbSpaceHasPlanningByUser from planning join [space] on planning.space_id = space.SPACE_ID join utilisateur on space.user_id = utilisateur.USER_ID\n" +
                "            where planning_state in ("+Planning.VALIDATED_STATE+","+Planning.WAITING_CONFIRMATION_STATE+") group by utilisateur.USER_ID,utilisateur.first_name+' '+utilisateur.last_name) a on a.userId = u.USER_ID\n" +
                "    where planning_state in ("+Planning.VALIDATED_STATE+","+Planning.WAITING_CONFIRMATION_STATE+") \n" +
                "    group by u.USER_ID,u.first_name+' '+u.last_name \n" +
                "    order by u.first_name+' '+u.last_name")
        sql.close()
        return rows
    }

    def getListUsersHasPlanningInMyCellule(User user){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select u.USER_ID userId\n" +
                "    from planning join [space] on planning.space_id = space.SPACE_ID join utilisateur u on space.user_id = u.USER_ID\n" +
                "    join (select utilisateur.USER_ID userId,utilisateur.first_name+' '+utilisateur.last_name utilisateur,count(distinct space.space_name)\n" +
                "    nbSpaceHasPlanningByUser from planning join [space] on planning.space_id = space.SPACE_ID join utilisateur on space.user_id = utilisateur.USER_ID\n" +
                "            where planning_state in ("+Planning.VALIDATED_STATE+","+Planning.WAITING_CONFIRMATION_STATE+") group by utilisateur.USER_ID,utilisateur.first_name+' '+utilisateur.last_name) a on a.userId = u.USER_ID\n" +
                "    where planning_state in ("+Planning.VALIDATED_STATE+","+Planning.WAITING_CONFIRMATION_STATE+")" +
                "    and u.USER_ID in (select ut.user_id from utilisateur ut where ut.team_id = "+user?.team?.id+" ) \n" +
                "    group by u.USER_ID,u.first_name+' '+u.last_name \n" +
                "    order by u.first_name+' '+u.last_name")
        sql.close()
        return rows
    }

    def getListPlanningByUser(Long userId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select u.first_name+' '+u.last_name utilisateur,space.SPACE_ID,space.space_name,space.space_code,space.space_number nb,\n" +
                "a.nbSpaceHasPlanningByUser nbSpaceHasPlanningByUser \n" +
                "from planning join [space] on planning.space_id = space.SPACE_ID join utilisateur u on space.user_id = u.USER_ID \n" +
                "join (select utilisateur.USER_ID userId,utilisateur.first_name+' '+utilisateur.last_name utilisateur,count(distinct space.space_name)\n" +
                " nbSpaceHasPlanningByUser from planning join [space] on planning.space_id = space.SPACE_ID join utilisateur on space.user_id = utilisateur.USER_ID\n" +
                "  where planning_state in ("+Planning.VALIDATED_STATE+","+Planning.WAITING_CONFIRMATION_STATE+") group by utilisateur.USER_ID,utilisateur.first_name+' '+utilisateur.last_name) a on a.userId = u.USER_ID \n" +
                "  where planning_state in ("+Planning.VALIDATED_STATE+","+Planning.WAITING_CONFIRMATION_STATE+") \n" +
                "  and u.USER_ID = "+userId+" \n" +
                "  group by u.first_name+' '+u.last_name,space.SPACE_ID,space.space_name,space.space_code,space.space_number,a.nbSpaceHasPlanningByUser \n" +
                "  order by utilisateur")
        sql.close()
        return rows
    }

    def getNombreSpaceCollaborateursByUser(Long userId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select SUM(nb) nbCollaborateurs\n" +
                "    from (\n" +
                "    select u.first_name+' '+u.last_name utilisateur,space.SPACE_ID,space.space_name,space.space_code,space.space_number nb,\n" +
                "    a.nbSpaceHasPlanningByUser nbSpaceHasPlanningByUser\n" +
                "    from planning join [space] on planning.space_id = space.SPACE_ID join utilisateur u on space.user_id = u.USER_ID\n" +
                "    join (select utilisateur.USER_ID userId,utilisateur.first_name+' '+utilisateur.last_name utilisateur,count(distinct space.space_name)\n" +
                "    nbSpaceHasPlanningByUser from planning join [space] on planning.space_id = space.SPACE_ID join utilisateur on space.user_id = utilisateur.USER_ID\n" +
                "            where planning_state in ("+Planning.VALIDATED_STATE+","+Planning.WAITING_CONFIRMATION_STATE+") group by utilisateur.USER_ID,utilisateur.first_name+' '+utilisateur.last_name) a on a.userId = u.USER_ID\n" +
                "            where planning_state in ("+Planning.VALIDATED_STATE+","+Planning.WAITING_CONFIRMATION_STATE+") \n" +
                "    and u.USER_ID = "+userId+" \n" +
                "    group by u.first_name+' '+u.last_name,space.SPACE_ID,space.space_name,space.space_code,space.space_number,a.nbSpaceHasPlanningByUser\n" +
                "    ) t ")
        sql.close()
        return rows[0].nbCollaborateurs
    }



    def getScoresBySpaceAndQuestions(Long spaceId,Long questionId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select isNull(evalAnswer.evalanswer_score,0) score,CASE \n" +
                "            WHEN isNull(evalAnswer.evalanswer_score,0) >= 4\n" +
                "               THEN '#5cb85c'\n" +
                "               ELSE CASE \n" +
                "\t\t\t\t\tWHEN isNull(evalAnswer.evalanswer_score,0) < 4 and isNull(evalAnswer.evalanswer_score,0) > 2.5\n" +
                "\t\t\t\t\t   THEN '#f0ad4e'\n" +
                "\t\t\t\t\t   ELSE '#d9534f' \n" +
                "\t\t\t   END\n" +
                "\t\t\tEND as color,CASE \n" +
                "            WHEN isNull(evalAnswer.evalanswer_score,0) >= 4\n" +
                "               THEN 'btn-success'\n" +
                "               ELSE CASE \n" +
                "\t\t\t\t\tWHEN isNull(evalAnswer.evalanswer_score,0) < 4 and isNull(evalAnswer.evalanswer_score,0) > 2.5\n" +
                "\t\t\t\t\t   THEN 'btn-warning'\n" +
                "\t\t\t\t\t   ELSE 'btn-danger' \n" +
                "\t\t\t   END\n" +
                "\t\t\tEND as typebtn,visite.VISITE_ID id \n" +
                "from (select visite.VISITE_ID,visite.visit_name visit_name\n" +
                "from planning\n" +
                "join visite on planning.visit_id = visite.VISITE_ID\n" +
                "where planning.planning_state = "+Planning.EVALUATED_STATE+" \n" +
                "group by visite.VISITE_ID,visite.visit_name) visite \n" +
                "left join (\n" +
                "select evalanswer.evalanswer_score,planning.visit_id\n" +
                "from evalanswer\n" +
                "join questions on questions.QUESTION_ID = evalanswer.questions_id\n" +
                "join (\n" +
                "select * from evaluation\n" +
                "where planning_id in (\n" +
                "select pl.id\n" +
                "from planning pl\n" +
                "join [space] s on pl.space_id = s.SPACE_ID\n" +
                "where s.SPACE_ID="+spaceId+" and planning_state="+Planning.EVALUATED_STATE+" \n" +
                " )) evaluation on evaluation.EVALUATION_ID = evalanswer.evaluation_id\n" +
                " join planning on evaluation.planning_id = planning.id\n" +
                " where questions.QUESTION_ID = "+questionId+" and evaluation.eval_status="+Evaluation.VALIDATED_STATE+" \n" +
                "  ) evalAnswer on evalAnswer.visit_id = visite.VISITE_ID \n" +
                "  order by visite.VISITE_ID")
        sql.close()
        return rows
    }

    def getScoresByQuestionId(Long questionId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select p.space_id,REPLACE(space.space_name,'Espace ','') space_name,space.region region,team_name,last_name+' '+first_name nameComplete,\n" +
                "scores= substring(  \n" +
                "\t\t\t( \n" +
                "\t\t\tSELECT ( ',' + score)\n" +
                "FROM \n" +
                "(select \n" +
                "'V:'+\n" +
                "CAST(visite.VISITE_ID AS nvarchar(255))+\n" +
                "':'+\n" +
                "CAST(isNull(visite.evalanswer_score,0) AS nvarchar(255))+\n" +
                "':'+\n" +
                "CASE WHEN isNull(visite.evalanswer_score,0) >= 4 THEN '#5cb85c'\n" +
                "ELSE CASE\n" +
                "WHEN isNull(visite.evalanswer_score,0) < 4 and isNull(visite.evalanswer_score,0) > 2.5 \n" +
                "THEN '#f0ad4e'\n" +
                "ELSE '#d9534f'END \n" +
                "END +\n" +
                "':'+\n" +
                "CASE  WHEN isNull(visite.evalanswer_score,0) >= 4 THEN 'btn-success'\n" +
                "ELSE CASE \n" +
                "WHEN isNull(visite.evalanswer_score,0) < 4 and isNull(visite.evalanswer_score,0) > 2.5 \n" +
                "THEN 'btn-warning'\n" +
                "ELSE 'btn-danger'  END \n" +
                "END +\n" +
                "':'+ isNull(CONVERT(VARCHAR(10),visite.dt,103),' ') score\n" +
                "from \n" +
                "( select visite.VISITE_ID VISITE_ID,v.evalanswer_score,v.dt \n" +
                "from planning p\n" +
                "join visite on p.visit_id = visite.VISITE_ID \n" +
                "left join (select planning.visit_id VISITE_ID,evalanswer.evalanswer_score,planning.planning_date dt \n" +
                "from evalanswer\n" +
                "join questions on questions.QUESTION_ID = evalanswer.questions_id\n" +
                "join evaluation on evaluation.EVALUATION_ID = evalanswer.evaluation_id   \n" +
                "join planning on evaluation.planning_id = planning.id\n" +
                "join [space] s on planning.space_id = s.SPACE_ID \n" +
                "where questions.QUESTION_ID = "+questionId+" and evaluation.eval_status="+Evaluation.VALIDATED_STATE+" \n" +
                "and s.SPACE_ID=p.space_id \n" +
                "and planning_state="+Planning.EVALUATED_STATE+") v on v.VISITE_ID = p.visit_id\n" +
                "where p.planning_state ="+Planning.EVALUATED_STATE+" \n" +
                "group by visite.VISITE_ID,v.VISITE_ID,v.evalanswer_score,v.dt ) visite\n" +
                ") t\n" +
                "\t\t\tFOR XML PATH( '' )\n" +
                "\t\t\t\t\t\t\t\t  ), 2, 2000 )\n" +
                "from planning p\n" +
                "join evaluation on evaluation.planning_id = p.id\n" +
                "join space on p.space_id = space.SPACE_ID\n" +
                "join utilisateur on space.user_id= utilisateur.USER_ID\n" +
                "join team on utilisateur.team_id = team.TEAM_ID\n" +
                "where evaluation.eval_status = "+Evaluation.VALIDATED_STATE+" \n" +
                "and p.planning_state="+Planning.EVALUATED_STATE+" \n" +
                "group by p.space_id,space_name,region,team_name,last_name+' '+first_name")
        sql.close()
        return rows
    }

    def getScoresByVisitAndQuestions(Long visit,Long questionId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("    select SUM(evalanswer.evalanswer_score) total,COUNT(evalanswer.evalanswer_score) nb,\n" +
                " 1.0 * SUM(evalanswer.evalanswer_score)/COUNT(evalanswer.evalanswer_score) as moyenne,\n" +
                " CASE \n" +
                "            WHEN (1.0 * SUM(evalanswer.evalanswer_score)/COUNT(evalanswer.evalanswer_score)) >= 4\n" +
                "               THEN '#5cb85c'\n" +
                "               ELSE CASE \n" +
                "\t\t\t\t\tWHEN (1.0 * SUM(evalanswer.evalanswer_score)/COUNT(evalanswer.evalanswer_score)) < 4 and (1.0 * SUM(evalanswer.evalanswer_score)/COUNT(evalanswer.evalanswer_score)) > 2.5\n" +
                "\t\t\t\t\t   THEN '#f0ad4e'\n" +
                "\t\t\t\t\t   ELSE '#d9534f' \n" +
                "\t\t\t   END\n" +
                "\t\t\tEND as color,CASE \n" +
                "            WHEN (1.0 * SUM(evalanswer.evalanswer_score)/COUNT(evalanswer.evalanswer_score)) >= 4\n" +
                "               THEN 'btn-success'\n" +
                "               ELSE CASE \n" +
                "\t\t\t\t\tWHEN (1.0 * SUM(evalanswer.evalanswer_score)/COUNT(evalanswer.evalanswer_score)) < 4 and (1.0 * SUM(evalanswer.evalanswer_score)/COUNT(evalanswer.evalanswer_score)) > 2.5\n" +
                "\t\t\t\t\t   THEN 'btn-warning'\n" +
                "\t\t\t\t\t   ELSE 'btn-danger' \n" +
                "\t\t\t   END\n" +
                "\t\t\tEND as typebtn \n" +
                "    from evalanswer\n" +
                "    join questions on questions.QUESTION_ID = evalanswer.questions_id\n" +
                "    join (\n" +
                "    select * from evaluation\n" +
                "    where planning_id in (\n" +
                "    select pl.id\n" +
                "    from planning pl\n" +
                "    join visite on pl.visit_id=visite.VISITE_ID\n" +
                "            where visite.VISITE_ID="+visit+" and planning_state="+Planning.EVALUATED_STATE+" \n" +
                "    )) evaluation on evaluation.EVALUATION_ID = evalanswer.evaluation_id\n" +
                "    join planning on evaluation.planning_id = planning.id\n" +
                "    where evaluation.eval_status="+Evaluation.VALIDATED_STATE+" and questions.QUESTION_ID = "+questionId);

        sql.close()
        return rows[0]
    }

    def getScoresBySpaceAndGroup(Long spaceId,Long groupQuestionId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("    select isNull(total,0) total,isNull(nb,0) nb,visite.VISITE_ID id " +
                "   from (select visite.VISITE_ID,visite.visit_name visit_name\n" +
                "   from planning\n" +
                "   join visite on planning.visit_id = visite.VISITE_ID\n" +
                "   where planning.planning_state = 4\n" +
                "   group by visite.VISITE_ID,visite.visit_name) visite \n" +
                "    left join (\n" +
                "    select SUM(evalanswer.evalanswer_score) total,COUNT(evalanswer.evalanswer_score) nb,planning.visit_id\n" +
                "    from evalanswer\n" +
                "    join questions on questions.QUESTION_ID = evalanswer.questions_id\n" +
                "    join (\n" +
                "    select * from evaluation\n" +
                "    where planning_id in (\n" +
                "    select pl.id\n" +
                "    from planning pl\n" +
                "    join [space] s on pl.space_id = s.SPACE_ID\n" +
                "            where s.SPACE_ID="+spaceId+" and planning_state="+Planning.EVALUATED_STATE+" \n" +
                "    )) evaluation on evaluation.EVALUATION_ID = evalanswer.evaluation_id\n" +
                "    join planning on evaluation.planning_id = planning.id\n" +
                "    where questions.questionnairegroup_id= "+groupQuestionId+" and evaluation.eval_status="+Evaluation.VALIDATED_STATE+" \n" +
                "    group by planning.visit_id\n" +
                "    ) evalAnswer on evalAnswer.visit_id = visite.VISITE_ID order by visite.VISITE_ID")
        sql.close()
        return rows
    }

    def getScoresByVisitAndGroup(Long visitId,Long groupQuestionId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select ROUND(AVG(CAST(t.total AS FLOAT)), 2) moyenne\n" +
                "    from\n" +
                "    (select ROUND(AVG(CAST(evalanswer.evalanswer_score AS FLOAT)), 2) total\n" +
                "    from evalanswer\n" +
                "    join questions on questions.QUESTION_ID = evalanswer.questions_id\n" +
                "    join (\n" +
                "    select * from evaluation\n" +
                "    where planning_id in (\n" +
                "    select pl.id\n" +
                "    from planning pl\n" +
                "    join [space] s on pl.space_id = s.SPACE_ID\n" +
                "            where planning_state="+Planning.EVALUATED_STATE+" \n" +
                "    )) evaluation on evaluation.EVALUATION_ID = evalanswer.evaluation_id\n" +
                "    join planning on evaluation.planning_id = planning.id\n" +
                "    where questions.questionnairegroup_id= "+groupQuestionId+" and evaluation.eval_status="+Evaluation.VALIDATED_STATE+" and planning.visit_id = "+visitId+" \n" +
                "    group by planning.space_id ) t")
        sql.close()

        return rows[0].moyenne
    }

    def listSpacesHasPlanning(){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select space.space_id,space_name,zone,region,team_name,last_name+' '+first_name nameComplete \n" +
                "from space\n" +
                "join planning on planning.space_id = space.SPACE_ID\n" +
                "join evaluation on evaluation.planning_id = planning.id\n" +
                "join utilisateur on evaluation.user_id= utilisateur.USER_ID\n" +
                "join team on utilisateur.team_id = team.TEAM_ID\n" +
                "where evaluation.eval_status = "+Evaluation.VALIDATED_STATE+" and planning.planning_state="+Planning.EVALUATED_STATE+" \n" +
                "group by space.space_id,space_name,zone,region,team_name,last_name+' '+first_name ")
        sql.close()
        return rows
    }

    def getScoresByCelluleAndVisiteAndQuestionForCelluleReport(Long celluleId,Long visiteId,Long questionId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("    select ROUND(AVG(CAST(evalanswer.evalanswer_score AS FLOAT)), 2) moyenne\n" +
                "    from evalanswer\n" +
                "    join questions on questions.QUESTION_ID = evalanswer.questions_id\n" +
                "    join (\n" +
                "    select * from evaluation\n" +
                "    where planning_id in (\n" +
                "    select pl.id\n" +
                "    from planning pl\n" +
                "    join [space] s on pl.space_id = s.SPACE_ID\n" +
                "            join utilisateur u on s.user_id = u.USER_ID\n" +
                "            join team t on t.TEAM_ID = u.team_id\n" +
                "    where t.TEAM_ID="+celluleId+" and pl.visit_id="+visiteId+" and planning_state="+Planning.EVALUATED_STATE+" \n" +
                "    )) evaluation on evaluation.EVALUATION_ID = evalanswer.evaluation_id\n" +
                "    join planning on evaluation.planning_id = planning.id\n" +
                "    where evaluation.eval_status = "+Evaluation.VALIDATED_STATE+" and questions.QUESTION_ID = "+questionId)
        sql.close()

        return rows[0].moyenne
    }

    def getScoresCelluleByQuestion(Long questionId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select team.team_name teamName, moyenne= substring((SELECT ( ';' + moyenne)\n" +
                "                       FROM (\n" +
                "select 'V:'+cast(visite.VISITE_ID as nvarchar(50))+':'+cast(ISNULL(sc.moyenne,0) as nvarchar(50))+':'+\n" +
                "CASE WHEN isNull(sc.moyenne,0) >= 4 THEN '#5cb85c'\n" +
                "ELSE CASE WHEN isNull(sc.moyenne,0) < 4 and isNull(sc.moyenne,0) > 2.5 THEN '#f0ad4e'\n" +
                "ELSE '#d9534f' \n" +
                "END \n" +
                "END+':'+\n" +
                "CASE  WHEN isNull(sc.moyenne,0) >= 4 THEN 'btn-success'\n" +
                "ELSE CASE WHEN isNull(sc.moyenne,0) < 4 and isNull(sc.moyenne,0) > 2.5 THEN 'btn-warning'\n" +
                "ELSE 'btn-danger'  END END moyenne\n" +
                "from ( select ROUND(AVG(CAST(evalanswer.evalanswer_score AS FLOAT)), 2) moyenne, planning.visit_id\n" +
                "from evalanswer\n" +
                "join questions on questions.QUESTION_ID = evalanswer.questions_id\n" +
                "join ( select * from evaluation \n" +
                "where planning_id in ( select pl.id \n" +
                "from planning pl\n" +
                "join [space] s on pl.space_id = s.SPACE_ID\n" +
                "join utilisateur u on s.user_id = u.USER_ID\n" +
                "join team t on t.TEAM_ID = u.team_id\n" +
                "where t.TEAM_ID=team.TEAM_ID  and planning_state="+Planning.EVALUATED_STATE+" \n" +
                ")\n" +
                ") evaluation on evaluation.EVALUATION_ID = evalanswer.evaluation_id\n" +
                "join planning on evaluation.planning_id = planning.id\n" +
                "where evaluation.eval_status = "+Evaluation.VALIDATED_STATE+" and questions.QUESTION_ID = "+questionId+"   \n" +
                "group by planning.visit_id\n" +
                ") sc\n" +
                "right join (select visite.VISITE_ID\n" +
                "from planning\n" +
                "join visite on planning.visit_id = visite.VISITE_ID\n" +
                "where planning.planning_state = "+Planning.EVALUATED_STATE+" \n" +
                "group by visite.VISITE_ID,visite.visit_name) visite \n" +
                "on visite.VISITE_ID = sc.visit_id\n" +
                ") t\n" +
                "                       FOR XML PATH( '' )\n" +
                "                      ), 2, 1000 )\n" +
                "from team")
        sql.close()

        return rows
    }

    def getScoresByVisiteAndQuestionForCelluleReport(Long visiteId,Long questionId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select  ROUND(AVG(CAST(t.moyenneCellule AS FLOAT)), 2) moyenne\n" +
                "from (\n" +
                "\tselect ROUND(AVG(CAST(evalanswer.evalanswer_score AS FLOAT)), 2) moyenneCellule,u.team_id\n" +
                "\tfrom evalanswer\n" +
                "\tjoin questions on questions.QUESTION_ID = evalanswer.questions_id\n" +
                "\tjoin (\n" +
                "\tselect * from evaluation\n" +
                "\twhere planning_id in (\n" +
                "\tselect pl.id\n" +
                "\tfrom planning pl\n" +
                "\t where pl.visit_id="+visiteId+" and planning_state="+Planning.EVALUATED_STATE+" \n" +
                "\t )) evaluation on evaluation.EVALUATION_ID = evalanswer.evaluation_id\n" +
                "\t join planning on evaluation.planning_id = planning.id\n" +
                "\t join utilisateur u on evaluation.user_id = u.USER_ID\n" +
                "\t where evaluation.eval_status = "+Evaluation.VALIDATED_STATE+" and questions.QUESTION_ID = "+questionId+" \n" +
                "\t group by u.team_id\n" +
                ") t")
        sql.close()

        return rows[0].moyenne
    }

    def getTotalMoyenneScoreByQuestion(Long questionId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select  ROUND(AVG(CAST(t.moyenneCellule AS FLOAT)), 2) moyenne,t.visitId visitId,\n" +
                "CASE WHEN isNull(ROUND(AVG(CAST(t.moyenneCellule AS FLOAT)), 2),0) >= 4 THEN '#5cb85c'\n" +
                "ELSE CASE WHEN isNull(ROUND(AVG(CAST(t.moyenneCellule AS FLOAT)), 2),0) < 4 \n" +
                "and isNull(ROUND(AVG(CAST(t.moyenneCellule AS FLOAT)), 2),0) > 2.5 THEN '#f0ad4e'\n" +
                "ELSE '#d9534f' END  END color,\n" +
                "CASE  WHEN isNull(ROUND(AVG(CAST(t.moyenneCellule AS FLOAT)), 2),0) >= 4 THEN 'btn-success'\n" +
                "ELSE CASE WHEN isNull(ROUND(AVG(CAST(t.moyenneCellule AS FLOAT)), 2),0) < 4 and isNull(ROUND(AVG(CAST(t.moyenneCellule AS FLOAT)), 2),0) > 2.5 THEN 'btn-warning'\n" +
                "ELSE 'btn-danger'  END END btn_type\n" +
                "from (\n" +
                "select ROUND(AVG(CAST(evalanswer.evalanswer_score AS FLOAT)), 2) moyenneCellule,u.team_id,planning.visit_id visitId\n" +
                "from evalanswer\n" +
                "join questions on questions.QUESTION_ID = evalanswer.questions_id\n" +
                "join (\n" +
                "select * from evaluation\n" +
                "where planning_id in (\n" +
                "select pl.id\n" +
                "from planning pl\n" +
                " where planning_state="+Planning.EVALUATED_STATE+" \n" +
                " )) evaluation on evaluation.EVALUATION_ID = evalanswer.evaluation_id\n" +
                " join planning on evaluation.planning_id = planning.id\n" +
                " right join (select visite.VISITE_ID\n" +
                "from planning\n" +
                "join visite on planning.visit_id = visite.VISITE_ID\n" +
                "where planning.planning_state = "+Planning.EVALUATED_STATE+" \n" +
                "group by visite.VISITE_ID,visite.visit_name) visite \n" +
                "on visite.VISITE_ID = planning.visit_id\n" +
                " join utilisateur u on evaluation.user_id = u.USER_ID\n" +
                " where evaluation.eval_status = "+Evaluation.VALIDATED_STATE+" and questions.QUESTION_ID = "+questionId+" \n" +
                " group by u.team_id,planning.visit_id ) t\n" +
                " group by t.visitId ")
        sql.close()

        return rows
    }

    def getBackgroundColorByPlanning(Planning planning){
        def res = "none"
        def today = new Date();

        if(planning != null && planning?.planningDate != null){
            if((new Date()).getTime() - planning?.planningDate?.getTime() > 86400000 && planning?.planningState==Planning.VALIDATED_STATE)
            {
                res = "#c9302c"
            }
            else if(planning?.planningState==Planning.VALIDATED_STATE){
                res = "#5cb85c"
            }else if(planning?.planningState==Planning.WAITING_CONFIRMATION_STATE){
                res = "#f0ad4e"
            }
        }

        return res;
    }

    def getListVisitOfPlanings(){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select visite.VISITE_ID,visite.visit_name visit_name\n" +
                " from planning\n" +
                " join visite on planning.visit_id = visite.VISITE_ID " +
                " where planning.planning_state ="+Planning.EVALUATED_STATE+"  \n" +
                " group by visite.VISITE_ID,visite.visit_name\n" +
                " order by visite.VISITE_ID")
        sql.close()
        return rows.visit_name
    }

    def getListVisitBySpace(Space space){
        def sql = new Sql(dataSource)

        def rows = sql.rows("select visite.VISITE_ID,\"CASE WHEN pl.planning_date IS NOT NULL    THEN visite.visit_name+' : '+CONVERT(VARCHAR(10),\n" +
                "pl.planning_date,105)    ELSE visite.visit_name END visit_name , pl.planning_date dt from planning\n" +
                "join visite on planning.visit_id = visite.VISITE_ID  left join ( select * from planning where space_id="+space.id+" \n" +
                "and planning_state in ("+Planning.VALIDATED_STATE+","+Planning.WAITING_CONFIRMATION_STATE+") ) pl on visite.VISITE_ID=pl.visit_id  where planning.planning_state = "+Planning.EVALUATED_STATE+" \n" +
                "group by visite.VISITE_ID,visite.visit_name,pl.planning_date\n" +
                "order by visite.VISITE_ID ")

        sql.close()

        return rows
    }

    def reporting(){

        def sql = new Sql(dataSource)
        def rows = sql.rows(" select  u.USER_ID,u.first_name+' '+u.last_name utilisateur,space.SPACE_ID,space.space_name,space.space_code,space.space_number nb,\n" +
                "    a.nbSpaceHasPlanningByUser nbSpaceHasPlanningByUser,t.total total\n" +
                "    from planning\n" +
                "    join [space] on planning.space_id = space.SPACE_ID\n" +
                "    join utilisateur u on space.user_id = u.USER_ID\n" +
                "    join\n" +
                "    (select utilisateur.USER_ID userId,utilisateur.first_name+' '+utilisateur.last_name utilisateur,count(distinct space.space_name)\n" +
                "    nbSpaceHasPlanningByUser from planning join [space] on planning.space_id = space.SPACE_ID join utilisateur on space.user_id = utilisateur.USER_ID\n" +
                "            where planning_state in ("+Planning.VALIDATED_STATE+","+Planning.WAITING_CONFIRMATION_STATE+") group by utilisateur.USER_ID,utilisateur.first_name+' '+utilisateur.last_name) a\n" +
                "\n" +
                "    on a.userId = u.USER_ID\n" +
                "\n" +
                "    join\n" +
                "    (select space.user_id, SUM(space.space_number) total from space group by space.user_id) t on t.user_id = u.USER_ID\n" +
                "\n" +
                "    where planning_state in ("+Planning.VALIDATED_STATE+","+Planning.WAITING_CONFIRMATION_STATE+") \n" +
                "    group by u.USER_ID,u.first_name+' '+u.last_name,space.SPACE_ID,space.space_name,space.space_code,space.space_number,a.nbSpaceHasPlanningByUser,t.total\n" +
                "    order by utilisateur")
        sql.close()
        return rows
    }

    def reportingByUser(Long user){

        def sql = new Sql(dataSource)
        def rows = sql.rows(" select  u.USER_ID,u.first_name+' '+u.last_name utilisateur,space.SPACE_ID,space.space_name,space.space_code,space.space_number nb,\n" +
                "    a.nbSpaceHasPlanningByUser nbSpaceHasPlanningByUser,t.total total\n" +
                "    from planning\n" +
                "    join [space] on planning.space_id = space.SPACE_ID\n" +
                "    join utilisateur u on space.user_id = u.USER_ID\n" +
                "    join\n" +
                "    (select utilisateur.USER_ID userId,utilisateur.first_name+' '+utilisateur.last_name utilisateur,count(distinct space.space_name)\n" +
                "    nbSpaceHasPlanningByUser from planning join [space] on planning.space_id = space.SPACE_ID join utilisateur on space.user_id = utilisateur.USER_ID\n" +
                "            where planning_state in ("+Planning.VALIDATED_STATE+","+Planning.WAITING_CONFIRMATION_STATE+") group by utilisateur.USER_ID,utilisateur.first_name+' '+utilisateur.last_name) a\n" +
                "\n" +
                "    on a.userId = u.USER_ID\n" +
                "\n" +
                "    join\n" +
                "    (select space.user_id, SUM(space.space_number) total from space group by space.user_id) t on t.user_id = u.USER_ID\n" +
                "\n" +
                "    where planning_state in ("+Planning.VALIDATED_STATE+","+Planning.WAITING_CONFIRMATION_STATE+") and u.USER_ID = "+user+"  \n" +
                "    group by u.USER_ID,u.first_name+' '+u.last_name,space.SPACE_ID,space.space_name,space.space_code,space.space_number,a.nbSpaceHasPlanningByUser,t.total\n" +
                "    order by utilisateur")
        sql.close()
        return rows
    }

    def reportingByTeam(Long team){

        def sql = new Sql(dataSource)
        def rows = sql.rows(" select  u.USER_ID,u.first_name+' '+u.last_name utilisateur,space.SPACE_ID,space.space_name,space.space_code,space.space_number nb,\n" +
                "    a.nbSpaceHasPlanningByUser nbSpaceHasPlanningByUser,t.total total\n" +
                "    from planning\n" +
                "    join [space] on planning.space_id = space.SPACE_ID\n" +
                "    join utilisateur u on space.user_id = u.USER_ID\n" +
                "    join\n" +
                "    (select utilisateur.USER_ID userId,utilisateur.first_name+' '+utilisateur.last_name utilisateur,count(distinct space.space_name)\n" +
                "    nbSpaceHasPlanningByUser from planning join [space] on planning.space_id = space.SPACE_ID join utilisateur on space.user_id = utilisateur.USER_ID\n" +
                "            where planning_state in ("+Planning.VALIDATED_STATE+","+Planning.WAITING_CONFIRMATION_STATE+") group by utilisateur.USER_ID,utilisateur.first_name+' '+utilisateur.last_name) a\n" +
                "\n" +
                "    on a.userId = u.USER_ID\n" +
                "\n" +
                "    join\n" +
                "    (select space.user_id, SUM(space.space_number) total from space group by space.user_id) t on t.user_id = u.USER_ID\n" +
                "\n" +
                "    where planning_state in ("+Planning.VALIDATED_STATE+","+Planning.WAITING_CONFIRMATION_STATE+") and u.USER_ID in (select ut.user_id from utilisateur ut where ut.team_id = "+team+" )  \n" +
                "    group by u.USER_ID,u.first_name+' '+u.last_name,space.SPACE_ID,space.space_name,space.space_code,space.space_number,a.nbSpaceHasPlanningByUser,t.total\n" +
                "    order by utilisateur")
        sql.close()
        return rows
    }

    def getPlannings(User user)
    {
        ArrayList<PlanningResponse> planningResponses = new ArrayList<PlanningResponse>()
        PlanningResponse planningResponse
        boolean isUserChief = (user?.profile?.profilName?.trim().equalsIgnoreCase("Chef de cellule"))?true:false

        if(isUserChief){
            def userList = User.findAllByTeamAndIdNotEqual(user?.team,user?.id)
            def spaceList = Space.findAllByUserInList(userList)
            def planningList = Planning.findAllBySpaceInListAndPlanningState(spaceList,Planning.VALIDATED_STATE)
            planningList.each {
                planningResponse = new PlanningResponse()
                planningResponse.planningDate = it?.planningDate?.format("dd/MM/yyyy")
                planningResponse.spaceName = it?.space?.spaceName
                planningResponse.spaceCode = it?.space?.spaceCode
                planningResponse.visitName = it?.visit?.visitName
                planningResponses.add(planningResponse)
            }
        }
        else
        {
            def planningList = Planning.findAllByPlanningState(Planning.VALIDATED_STATE)
            planningList.each {
                planningResponse = new PlanningResponse()
                planningResponse.planningDate = it?.planningDate?.format("dd/MM/yyyy")
                planningResponse.spaceName = it?.space?.spaceName
                planningResponse.spaceCode = it?.space?.spaceCode
                planningResponse.visitName = it?.visit?.visitName
                planningResponses.add(planningResponse)
            }
        }

        /*plannings.each {
            if(isUserChief){
                println "User is chief"
                if(spaceList.contains(it.space)){
                    println "This planning has space of team"
                   planningResponse.planningDate = it?.planningDate?.format("dd/MM/yyyy")
                   planningResponse.spaceCode = it?.space?.spaceCode
                   planningResponse.visitName = it?.visit?.visitName
                   planningResponses.add(planningResponse)
                }
            }else{
                planningResponse.planningDate = it?.planningDate?.format("dd/MM/yyyy")
                planningResponse.spaceCode = it?.space?.spaceCode
                planningResponse.visitName = it?.visit?.visitName
                planningResponses.add(planningResponse)
            }
        }*/
        return planningResponses
    }

    def getPlanningToShift(Date datedepart,Long userId,Long newId){

        def sql = new Sql(dataSource)
        def rows = sql.rows("select id,space.space_name,planning_date oldDay from planning join space on planning.space_id=space.SPACE_ID where planning.space_id \n" +
                "in (select SPACE_ID from space where user_id="+userId+" ) \n" +
                "and planning_date >= "+datedepart.format('dd-mm-yyyy')+" \n" +
                "and planning_state in ("+Planning.VALIDATED_STATE+","+Planning.WAITING_CONFIRMATION_STATE+") \n" +
                "and planning.id != "+newId+" \n" +
                "order by planning_date")
        sql.close()

        return rows
    }

    def dateValideForShifting(Date dt){

        def sql = new Sql(dataSource)
        def rows = sql.rows("select holiday_user.nb+holiday.nb+weekend.nb dateDisponible \n" +
                "from     (select count(0) nb from user_holidays where user_id=6 and ( start_date <= "+dt.format('dd-mm-yyyy')+" and end_date >= "+dt.format('dd-mm-yyyy')+" )) holiday_user,\n" +
                "  (select count(0) nb from holiday where holiday_date = "+dt.format('dd-mm-yyyy')+") holiday,\n" +
                "  (select (case when ((DATEPART(dw, "+dt.format('dd-mm-yyyy')+") + @@DATEFIRST) % 7)  IN (0, 1,6) then 1 else 0 end) nb) weekend")
        sql.close()

        return rows[0].dateDisponible
    }

    def shiftPlanning(User user, Date oldDate, Date newDate, Planning oldplanning , Planning newplanning) {

        def dateDifference = newDate?.getTime() - oldDate?.getTime()

        Calendar cal = java.util.Calendar.getInstance();
        Calendar cal1 = java.util.Calendar.getInstance();
        Calendar cal2 = java.util.Calendar.getInstance();
        int sign = (newDate?.getTime()>oldDate?.getTime())?1:-1    // differenceInDays/(int)Math.abs(differenceInDays)
        if(sign==1)
        {
            cal1.setTime(oldDate);
            cal2.setTime(newDate);
        }
        else
        {
            cal1.setTime(newDate);
            cal2.setTime(oldDate);
        }
        dateDifference=0
        System.out.println("*********")
        System.out.println("Diff: "+cal1.before(cal2))
        while (cal1.before(cal2))
        {
            System.out.println("In")
            if ((!Holiday?.findByHolidayDate(newDate)) && (Calendar.FRIDAY != cal1.get(Calendar.DAY_OF_WEEK)) && (Calendar.SATURDAY != cal1.get(Calendar.DAY_OF_WEEK)) && (Calendar.SUNDAY != cal1.get(Calendar.DAY_OF_WEEK)))
            {
                dateDifference++;
                System.out.println("dateDifference: "+dateDifference)
            }
            cal1.add(Calendar.DATE,1);
        }

        int differenceInDays = dateDifference*sign

        System.out.println("differenceInDays:"+differenceInDays)
        System.out.println("planning:"+oldplanning?.planningDate)
        System.out.println("*********")
        try{
            def currentUserOtherPlannings = Planning.findAllBySpaceInListAndPlanningState(Space.findAllByUser(user),Planning.VALIDATED_STATE)
            Long day = 86400000
            def holiday
            Date nextDate = null
            currentUserOtherPlannings.each {
                if(it.id != newplanning.id)
                {
                    System.out.println("new: "+it.planningDate.getTime())
                    System.out.println("old: "+oldDate.getTime())
                    if(it.planningDate.getTime()>=oldDate.getTime())
                    {
                        System.out.println("Shifting: Space:"+it.space+" Visite:"+it.visit+" Date:"+it.planningDate)
                        cal.setTime(it.planningDate);
                        nextDate = new Date(it.planningDate.getTime())
                        System.out.println("###dateDifference: "+dateDifference)
                        System.out.println("***differenceInDays: "+differenceInDays)
                        differenceInDays = dateDifference
                        while(differenceInDays!=0)
                        {
                            System.out.println("Previous date:"+nextDate)
                            nextDate = new Date(nextDate.getTime()+1*day)
                            System.out.println("Adter date:"+nextDate)
                            if(Holiday?.findByHolidayDate(nextDate)?.id!=null || isItWeekend(nextDate) == Holiday.FRIDAY || isItWeekend(nextDate) == Holiday.SATURDAY || isItWeekend(nextDate) == Holiday.SUNDAY )
                            {
                                while (Holiday?.findByHolidayDate(nextDate)?.id != null || isItWeekend(nextDate) == Holiday.FRIDAY || isItWeekend(nextDate) == Holiday.SATURDAY || isItWeekend(nextDate) == Holiday.SUNDAY)
                                {
                                    holiday = Holiday?.findByHolidayDate(nextDate)
                                    if (!holiday) {
                                        println("NextDate:$nextDate and it is not holiday.")
                                        if (isItWeekend(nextDate) == Holiday.FRIDAY) {
                                            nextDate = new Date(nextDate?.getTime() + (sign) * (day * 3))
                                            differenceInDays--
                                        } else if (isItWeekend(nextDate) == Holiday.SATURDAY) {
                                            nextDate = new Date(nextDate?.getTime() + (sign) * (day * 3))
                                            differenceInDays--
                                        } else if (isItWeekend(nextDate) == Holiday.SUNDAY) {
                                            nextDate = new Date(nextDate?.getTime() + (sign) * day * 3)
                                            differenceInDays--
                                        }

                                    } else {
                                        println("NextDate:$nextDate and it is holiday, now shifting planning ahead of holiday")
                                        nextDate = new Date(nextDate?.getTime() + 1 * day)
                                        dateDifference = dateDifference + (sign) * day
                                        differenceInDays--
                                    }
                                };
                            }
                            else
                            {
                                differenceInDays--
                            }
                        };
                        System.out.println("Shifted: Space:"+it.space+" Visite:"+it.visit+" Date:"+nextDate)
                        System.out.println("Planning: "+ it.id  +"## nextDate: "+nextDate)
                        it.planningDate = nextDate
                        it.save(flush: true)
                    }


                    //nextDate = new Date(it.planningDate.getTime()+differenceInDays*day)
                    //System.out.println("it.planningDate.getTime(): "+it.planningDate.getTime()+ " (sign)*dateDifference: "+dateDifference)
                    /*while (Holiday?.findByHolidayDate(nextDate)?.id!=null || isItWeekend(nextDate) == Holiday.FRIDAY || isItWeekend(nextDate) == Holiday.SATURDAY || isItWeekend(nextDate) == Holiday.SUNDAY )
                    {
                        holiday = Holiday?.findByHolidayDate(nextDate)
                        if(!holiday)
                        {
                            println("NextDate:$nextDate and it is not holiday.")
                            if(isItWeekend(nextDate) == Holiday.FRIDAY)
                                nextDate = new Date(nextDate?.getTime()+(sign)*(day*3))
                            else if(isItWeekend(nextDate) == Holiday.SATURDAY)
                                nextDate = new Date(nextDate?.getTime()+(sign)*(day*3))
                            else if (isItWeekend(nextDate) == Holiday.SUNDAY)
                                nextDate = new Date(nextDate?.getTime()+(sign)*day*3)

                        }
                        else
                        {
                            println("NextDate:$nextDate and it is holiday, now shifting planning ahead of holiday")
                            nextDate = new Date(nextDate?.getTime()+(sign)*day)
                            dateDifference = dateDifference+(sign)*day
                        }
                    };*/
                    /*System.out.println("Planning: "+ it.id  +"## nextDate: "+nextDate)
                    it.planningDate = nextDate
                    it.save(flush: true)*/
                }
            }

        }catch (Exception e) {
            log.error("Fatal error occurred while shifting planning with date $planning,error: $e")
            println("Fatal error occurred while shifting planning with date $planning,error: $e")
        }

    }

    def disabledAllQuestionnaire(){
        def sql = new Sql(dataSource)
        sql.executeUpdate("update questionnaire set enable_q = 0;")
        sql.close()
    }

    def checkHolidayUser(Date date,Long user){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select * from user_holidays where user_id="+user+" and "+date+" between start_date and end_date")
        sql.close()

        return rows
    }

    def getAllUsersHolidaysDay(){
        def sql = new Sql(dataSource)
        def rows = sql.rows("   WITH mycte AS\n" +
                "(\n" +
                "  SELECT CAST('2010-01-01' AS DATETIME) DateValue\n" +
                "  UNION ALL\n" +
                "  SELECT  DateValue + 1\n" +
                "  FROM    mycte   \n" +
                "  WHERE   DateValue + 1 < '2030-12-31'\n" +
                ")\n" +
                "\n" +
                "\n" +
                "SELECT  DateValue,Month (DateValue) planning_month,Day (DateValue) planning_day\n" +
                "FROM    mycte\n" +
                "where DateValue between (select min(start_date) from user_holidays) and (select max(end_date) from user_holidays)\n" +
                "OPTION (MAXRECURSION 0)")
        sql.close()

        return rows
    }

    def getTotalPonderationByQuestionnaire(Long idQuestionnaire){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select SUM(qg.questionnairegroup_ponderation) total\n" +
                "  from questionnairegroup qg\n" +
                "  where qg.questionnaire_id= "+idQuestionnaire)
        sql.close()

        return rows[0].total
    }

    def getNbGroupHasQuestions(Long idQuestionnaire){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select count(qs.questions) nbGroupHasQs,\n" +
                "(select count(qr.QUESTIONNAIREGROUP_ID) from questionnairegroup qr where qr.questionnaire_id=1) nbGroupByQuestionnaire\n" +
                "from (\n" +
                "select count(q.questionnairegroup_id) questions\n" +
                "from questions q\n" +
                "join questionnairegroup qg on q.questionnairegroup_id = qg.QUESTIONNAIREGROUP_ID\n" +
                "join questionnaire qr on qg.questionnaire_id = qr.QUESTIONNAIRE_ID\n" +
                "where qr.questionnaire_id="+idQuestionnaire+" \n" +
                "group by q.questionnairegroup_id\n" +
                ") qs")
        sql.close()

        return rows
    }

    /**
     *    def getNbGroupHasQuestionsAndTotalNbGroup(Long idQuestionnaire){
     def sql = new Sql(dataSource)
     def rows = sql.rows("select count(qs.questions) nbGroupHasQs,\n" +
     "(select count(qr.QUESTIONNAIREGROUP_ID) from questionnairegroup qr where qr.questionnaire_id=1) nbGroupByQuestionnaire\n" +
     "from (\n" +
     "select count(q.questionnairegroup_id) questions\n" +
     "from questions q\n" +
     "join questionnairegroup qg on q.questionnairegroup_id = qg.QUESTIONNAIREGROUP_ID\n" +
     "join questionnaire qr on qg.questionnaire_id = qr.QUESTIONNAIRE_ID\n" +
     "where qr.questionnaire_id="+idQuestionnaire+" \n" +
     "group by q.questionnairegroup_id\n" +
     ") qs")
     sql.close()

     return rows
     }
     */


    def listVisitBySpaces(){
        def sql = new Sql(dataSource)
/*        def rows = sql.rows("select space.space_code+','+visite.visit_name spaceVisite\n" +
                "\t\tfrom planning\n" +
                "\t\tjoin space on space.SPACE_ID=planning.space_id\n" +
                "\t\tjoin visite on visite.VISITE_ID = planning.visit_id\n" +
                "\t\tgroup by space.space_code,visite.visit_name,space.SPACE_ID,visite.VISITE_ID")*/

        def rows = sql.rows("select space.space_code+','+visite.visit_name+','+CONVERT(VARCHAR(10),planning.planning_date,103) spaceVisite\n" +
                "from planning\n" +
                "join space on space.SPACE_ID=planning.space_id\n" +
                "join visite on visite.VISITE_ID = planning.visit_id\n" +
                "group by space.space_code,visite.visit_name,space.SPACE_ID,visite.VISITE_ID,planning.planning_date")

        sql.close()
        //return rows[0].listVisitBySpaces

        def result = [];
            for(row in rows){
                result.add(row.spaceVisite)
            }
        return result
    }


    def getScores(Long spaceId,Long celluleId, String region){
        def scoreList  = [:]

        def listScores = []; // listScores by space for home page
        def listCelluleScores = []; // listScores by cellule for home page
        def listRegionScores = []; // listScores by region for home page

        if(spaceId != 0 ) {
            for(row in listScoreGlobal(spaceId)){
                listScores.add(row.score);
            }
        }else{
            for(row in listScoreForAll()){
                listScores.add(row.score);
            }
        }

        if(celluleId != 0 ) {
            for(row in listScoreGlobalByCellule(celluleId)){
                listCelluleScores.add(row.score);
            }
        }else{
            for(row in listScoreForAll()){
                listCelluleScores.add(row.score);
            }
        }

        if(region != '0' ) {
            for(row in listScoreGlobalByRegion(region)){
                listRegionScores.add(row.score);
            }
        }else{
            for(row in listScoreForAll()){
                listRegionScores.add(row.score);
            }
        }

        scoreList["listVisites"]=getListVisitOfPlanings() // list visit
        scoreList["listScores"]=listScores // add list scores by space
        scoreList["listScoresRegion"]=listRegionScores // add list scores by region
        scoreList["listScoresCellule"]=listCelluleScores // add list scores by cellule

        return scoreList
    }

    def Date oldDate;
    def Date NewDate;

    def shifting(List rows,Date dt)
    {
        Date newDate = dt;
        int addDate = 0;
        if (oldDate != null &&  rows[0].oldDay == oldDate)
        {
            dt = NewDate
            addDate = 0;
        }
        else
        {
            int i = 1;
            while (dateValideForShifting(dt.plus(i)) != 0)
            {
                newDate = dt.plus(i)
                i=i+1
            }
            addDate = 1;
        }

        Planning p = Planning.get(Long.parseLong(""+rows[0].id))
        p.planningDate = dt
        p.save flush:true

        oldDate = rows[0].oldDay
        NewDate = dt

        rows.remove(0)

        if (rows.size() != 0) {
            shifting(rows,newDate.plus(addDate));
        }
    }


    def String clobToString(Clob data) {
        StringBuilder sb = new StringBuilder();
        try {
            Reader reader = data.getCharacterStream();
            BufferedReader br = new BufferedReader(reader);

            String line;
            while(null != (line = br.readLine())) {
                sb.append(line);
            }
            br.close();
        } catch (SQLException e) {
            // handle this exception
        } catch (IOException e) {
            // handle this exception
        }
        return sb.toString();
    }

    def listPlanningDate(){
        def sql = new Sql(dataSource)
        def rows = sql.rows(" select MONTH(planning.planning_date) month,year(planning.planning_date) year \n" +
                "from Planning \n" +
                "join evaluation on planning.id = evaluation.planning_id\n" +
                "where planning.planning_state="+Planning.EVALUATED_STATE+" and evaluation.eval_status = "+Evaluation.VALIDATED_STATE+" \n" +
                "group by MONTH(planning.planning_date),year(planning.planning_date) \n" +
                "order by year,month asc ")
        sql.close()
        return rows
    }


    def listScoreByMonthAndYearAndQuestions(Long month,Long year,Long questionId){
        def sql = new Sql(dataSource)
        def rows = sql.rows("select visite.visit_name,ISNULL(score,0) score \n" +
                "from (select visite.VISITE_ID,visite.visit_name visit_name\n" +
                "from planning\n" +
                "join visite on planning.visit_id = visite.VISITE_ID\n" +
                "where planning.planning_state = "+Planning.EVALUATED_STATE+" \n" +
                "group by visite.VISITE_ID,visite.visit_name) visite \n" +
                "Left Join \n" +
                "    (\n" +
                "    select ROUND(AVG(CAST(evalanswer_score AS FLOAT)), 2) score,VISITE_ID\n" +
                "            from evaluation\n" +
                "    Join Planning on evaluation.planning_id=planning.id\n" +
                "    Join visite on Planning.visit_id=visite.visite_id\n" +
                "    Join evalanswer on evalanswer.evaluation_id=evaluation.evaluation_id\n" +
                "    join questions on evalanswer.questions_id=questions.question_id\n" +
                "    where questions.QUESTION_ID = "+questionId+" \n" +
                "    and MONTH(planning.planning_date) = "+month+" \n" +
                "    and YEAR(planning.planning_date) = "+year+" \n" +
                "    and evaluation.eval_status="+Evaluation.VALIDATED_STATE+" and planning.planning_state="+Planning.EVALUATED_STATE+" \n" +
                "    group by VISITE_ID ) a on visite.visite_id= a.visite_id\n" +
                "    order by visite.visite_id ")
        sql.close()
        return rows
    }





}
