import com.ef.app.qm.Permission
import com.ef.app.qm.User
import grails.util.Environment
import org.apache.shiro.crypto.hash.Sha256Hash

class BootStrap {
    def init = { servletContext ->
        switch(Environment.getCurrent()) {
            case "DEVELOPMENT":
                def user = new User(userName: "admin", password: new Sha256Hash("admin").toHex())
                user.addToPermissions("*:*")
                user.save()

                //if(com.ef.app.qm.Permission.count == 0) {
                    // Create Default permissions
                    new Permission(isManaged: true, expression: '*:*', name: 'com.ef.cbr.bootstrap.all',
                            description: 'com.ef.cbr.bootstrap.all.permission.Admin').save()
                    new Permission(isManaged: true, expression: 'settings:*', name: 'com.ef.cbr.bootstrap.config',
                            description: 'com.ef.cbr.bootstrap.config.parameter').save()

                    // com.ef.app.qm.User related Permissions
                    new Permission(isManaged: true, expression: 'user:*', name: 'com.ef.cbr.bootstrap.all.user.name',
                            description: 'com.ef.cbr.bootstrap.all.user').save()
                    new Permission(isManaged: true, expression: 'user:show', name: 'com.ef.cbr.bootstrap.show.user.name',
                            description: 'com.ef.cbr.bootstrap.show.user').save()
                    new Permission(isManaged: true, expression: 'user:create,createForModal,save', name: 'com.ef.cbr.bootstrap.create.user.name',
                            description: 'com.ef.cbr.bootstrap.create.user').save()
                    new Permission(isManaged: true, expression: 'user:edit,editpop,update', name: 'com.ef.cbr.bootstrap.edit.user.name',
                            description: 'com.ef.cbr.bootstrap.edit.user').save()
                    new Permission(isManaged: true, expression: 'user:list', name: 'com.ef.cbr.bootstrap.list.user.name',
                            description: 'com.ef.cbr.bootstrap.list.user').save()
                    new Permission(isManaged: true, expression: 'user:delete', name: 'com.ef.cbr.bootstrap.delete.user.name',
                            description: 'com.ef.cbr.bootstrap.delete.user').save()
                    new Permission(isManaged: true, expression: 'user:activateuser', name: 'com.ef.cbr.bootstrap.activate.user.name',
                            description: 'com.ef.cbr.bootstrap.activate.user').save()
                    new Permission(isManaged: true, expression: 'user:blockuser', name: 'com.ef.cbr.bootstrap.block.user.name',
                            description: 'com.ef.cbr.bootstrap.block.user').save()
                    new Permission(isManaged: true, expression: 'user:viewroles', name: 'com.ef.cbr.bootstrap.role.user.name',
                            description: 'com.ef.cbr.bootstrap.role.user').save()
                    new Permission(isManaged: true, expression: 'user:assignrole', name: 'com.ef.cbr.bootstrap.assignRole.user.name',
                            description: 'com.ef.cbr.bootstrap.assignRole.user').save()
                    new Permission(isManaged: true, expression: 'user:revokerole', name: 'com.ef.cbr.bootstrap.revokeRole.user.name',
                            description: 'com.ef.cbr.bootstrap.revokeRole.user').save()
                    new Permission(isManaged: true, expression: 'user:viewpermissions', name: 'com.ef.cbr.bootstrap.viewPermission.user.name',
                            description: 'com.ef.cbr.bootstrap.viewPermission.user').save()
                    new Permission(isManaged: true, expression: 'user:assignpermission', name: 'com.ef.cbr.bootstrap.assignPermission.user.name',
                            description: 'com.ef.cbr.bootstrap.assignPermission.user').save()
                    new Permission(isManaged: true, expression: 'user:revokepermission', name: 'com.ef.cbr.bootstrap.revokePermission.user.name',
                            description: 'com.ef.cbr.bootstrap.revokePermission.user').save()
                    new Permission(isManaged: true, expression: 'user:resetPassword', name: 'com.ef.cbr.bootstrap.resetPassword.user.name',
                            description: 'com.ef.cbr.bootstrap.resetPassword.user').save()

                    // com.ef.app.qm.Role related permissions
                    new Permission(isManaged: true, expression: 'role:*', name: 'com.ef.cbr.bootstrap.all.role.name',
                            description: 'com.ef.cbr.bootstrap.all.role').save()
                    new Permission(isManaged: true, expression: 'role:show', name: 'com.ef.cbr.bootstrap.show.role.name',
                            description: 'com.ef.cbr.bootstrap.show.role').save()
                    new Permission(isManaged: true, expression: 'role:create', name: 'com.ef.cbr.bootstrap.create.role.name',
                            description: 'Can create new com.ef.app.qm.Role').save()
                    new Permission(isManaged: true, expression: 'role:edit', name: 'com.ef.cbr.bootstrap.edit.role.name',
                            description: 'com.ef.cbr.bootstrap.edit.role').save()
                    new Permission(isManaged: true, expression: 'role:list', name: 'com.ef.cbr.bootstrap.list.role.name',
                            description: 'com.ef.cbr.bootstrap.list.role').save()
                    new Permission(isManaged: true, expression: 'role:delete', name: 'com.ef.cbr.bootstrap.delete.role.name',
                            description: 'com.ef.cbr.bootstrap.delete.role').save()
                    new Permission(isManaged: true, expression: 'role:viewmembers', name: 'com.ef.cbr.bootstrap.viewMember.role.name',
                            description: 'com.ef.cbr.bootstrap.viewMember.role').save()
                    new Permission(isManaged: true, expression: 'role:viewpermissions', name: 'com.ef.cbr.bootstrap.viewPermission.role.name',
                            description: 'com.ef.cbr.bootstrap.viewPermission.role').save()
                    new Permission(isManaged: true, expression: 'role:assignpermission', name: 'com.ef.cbr.bootstrap.assignPermission.role.name',
                            description: 'com.ef.cbr.bootstrap.assignPermission.role').save()
                    new Permission(isManaged: true, expression: 'role:revokepermission', name: 'com.ef.cbr.bootstrap.revokePermission.role.name',
                            description: 'com.ef.cbr.bootstrap.revokePermission.role').save()
                    new Permission(isManaged: true, expression: 'role:assignmembership', name: 'com.ef.cbr.bootstrap.addMember.role.name',
                            description: 'com.ef.cbr.bootstrap.addMember.role').save()
                    new Permission(isManaged: true, expression: 'role:revokemembership', name: 'com.ef.cbr.bootstrap.revokeMember.role.name',
                            description: 'com.ef.cbr.bootstrap.revokeMember.role').save()
               // }
                break
        }
    }
    def destroy = {
    }
}
