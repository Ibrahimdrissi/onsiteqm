class UrlMappings {

	static mappings = {
    /*    if(request.getHeader('referer')==null)
           "/" {
               controller = 'home'
               action = {'home'}
           }*/


        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }
        "/" view: "login"
        /*"/" {
            controller = "auth"
            action = "index"
        }*/
        "/"  {
            controller = 'home'
            action = {'home'}
            view = {'index'}
        }
        "/index" {
            controller = "login"
            action = "index"
        }
        "/login" {
            controller = "login"
            action = "index"
        }
        "/logout" {
            controller = "login"
            action = "signOut"
        }
        "/reportingGlobal" {
            controller = "reporting"
            action = "global"
        }
        "500"(view:'/error')
		"/basic"(view:"/basic")
        "403"	(view:'/errors/403')
        "404"	(view:'/errors/404')
        "503"	(view:'/errors/503')
	}
}
