

<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'evalanswer.label', default: 'Evalanswer')}" />
		<title><g:message code="com.expertflow.ma.onsiteqm.applicationName"/></title>
	</head>
	<body>
		<a href="#list-evalanswer" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-evalanswer" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="evalanswerScore" title="${message(code: 'evalanswer.evalanswerScore.label', default: 'Evalanswer Score')}" />
					
						<g:sortableColumn property="evalanswerAnimatorComment" title="${message(code: 'evalanswer.evalanswerAnimatorComment.label', default: 'Evalanswer Animator Comment')}" />
					
						<g:sortableColumn property="evalanswerSupComment" title="${message(code: 'evalanswer.evalanswerSupComment.label', default: 'Evalanswer Sup Comment')}" />
					
						<th><g:message code="evalanswer.evaluation.label" default="Evaluation" /></th>
					
						<th><g:message code="evalanswer.questions.label" default="Questions" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${evalanswerInstanceList}" status="i" var="evalanswerInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${evalanswerInstance.id}">${fieldValue(bean: evalanswerInstance, field: "evalanswerScore")}</g:link></td>
					
						<td>${fieldValue(bean: evalanswerInstance, field: "evalanswerAnimatorComment")}</td>
					
						<td>${fieldValue(bean: evalanswerInstance, field: "evalanswerSupComment")}</td>

						<td>${fieldValue(bean: evalanswerInstance, field: "evaluation")}</td>
					
						<td>${fieldValue(bean: evalanswerInstance, field: "questions")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${evalanswerInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
