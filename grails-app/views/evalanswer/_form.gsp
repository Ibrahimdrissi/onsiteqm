<%@ page import="com.ef.app.qm.Questions; com.ef.app.qm.Evaluation" %>




<div class="fieldcontain ${hasErrors(bean: evalanswerInstance, field: 'evalanswerScore', 'error')} ">
	<label for="evalanswerScore">
		<g:message code="evalanswer.evalanswerScore.label" default="Evalanswer Score" />
		
	</label>
	<g:field name="evalanswerScore" type="number" value="${evalanswerInstance.evalanswerScore}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: evalanswerInstance, field: 'evalanswerAnimatorComment', 'error')} ">
	<label for="evalanswerAnimatorComment">
		<g:message code="evalanswer.evalanswerAnimatorComment.label" default="Evalanswer Animator Comment" />
		
	</label>
	<g:textField name="evalanswerAnimatorComment" maxlength="250" value="${evalanswerInstance?.evalanswerAnimatorComment}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: evalanswerInstance, field: 'evalanswerSupComment', 'error')} ">
	<label for="evalanswerSupComment">
		<g:message code="evalanswer.evalanswerSupComment.label" default="Evalanswer Sup Comment" />
		
	</label>
	<g:textField name="evalanswerSupComment" maxlength="250" value="${evalanswerInstance?.evalanswerSupComment}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: evalanswerInstance, field: 'evaluation', 'error')} required">
	<label for="evaluation">
		<g:message code="evalanswer.evaluation.label" default="Evaluation" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="evaluation" name="evaluation.id" from="${com.ef.app.qm.Evaluation.list()}" optionKey="id" required="" value="${evalanswerInstance?.evaluation?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: evalanswerInstance, field: 'questions', 'error')} required">
	<label for="questions">
		<g:message code="evalanswer.questions.label" default="Questions" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="questions" name="questions.id" from="${com.ef.app.qm.Questions.list()}" optionKey="id" required="" value="${evalanswerInstance?.questions?.id}" class="many-to-one"/>

</div>

