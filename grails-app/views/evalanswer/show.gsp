

<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'evalanswer.label', default: 'Evalanswer')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-evalanswer" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-evalanswer" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list evalanswer">
			
				<g:if test="${evalanswerInstance?.evalanswerScore}">
				<li class="fieldcontain">
					<span id="evalanswerScore-label" class="property-label"><g:message code="evalanswer.evalanswerScore.label" default="Evalanswer Score" /></span>
					
						<span class="property-value" aria-labelledby="evalanswerScore-label"><g:fieldValue bean="${evalanswerInstance}" field="evalanswerScore"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${evalanswerInstance?.evalanswerAnimatorComment}">
				<li class="fieldcontain">
					<span id="evalanswerAnimatorComment-label" class="property-label"><g:message code="evalanswer.evalanswerAnimatorComment.label" default="Evalanswer Animator Comment" /></span>
					
						<span class="property-value" aria-labelledby="evalanswerAnimatorComment-label"><g:fieldValue bean="${evalanswerInstance}" field="evalanswerAnimatorComment"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${evalanswerInstance?.evalanswerSupComment}">
				<li class="fieldcontain">
					<span id="evalanswerSupComment-label" class="property-label"><g:message code="evalanswer.evalanswerSupComment.label" default="Evalanswer Sup Comment" /></span>
					
						<span class="property-value" aria-labelledby="evalanswerSupComment-label"><g:fieldValue bean="${evalanswerInstance}" field="evalanswerSupComment"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${evalanswerInstance?.evaluation}">
				<li class="fieldcontain">
					<span id="evaluation-label" class="property-label"><g:message code="evalanswer.evaluation.label" default="Evaluation" /></span>
					
						<span class="property-value" aria-labelledby="evaluation-label"><g:link controller="evaluation" action="show" id="${evalanswerInstance?.evaluation?.id}">${evalanswerInstance?.evaluation?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${evalanswerInstance?.questions}">
				<li class="fieldcontain">
					<span id="questions-label" class="property-label"><g:message code="evalanswer.questions.label" default="Questions" /></span>
					
						<span class="property-value" aria-labelledby="questions-label"><g:link controller="questions" action="show" id="${evalanswerInstance?.questions?.id}">${evalanswerInstance?.questions?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:evalanswerInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${evalanswerInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
