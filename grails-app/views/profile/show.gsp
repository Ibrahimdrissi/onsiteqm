
<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'profile.label', default: 'Profile')}" />
	<title><g:message code="default.show.label" args="${[message(code: 'com.ef.domain.name.profile')]}" /></title>
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<g:message code="default.show.label" args="${[message(code: 'com.ef.domain.name.profile')]}" />
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row" style="padding-bottom: 20px">
					<div class="col-lg-2" role="navigation">
						<g:link class="btn btn-primary btn-block" action="index"><g:message code="default.list.label" args="${[message(code: 'com.ef.domain.name.profile')]}" /></g:link>
					</div>
					<div class="col-lg-2" role="navigation">
						<g:link class="btn btn-primary btn-block" action="create"><g:message code="default.new.label" args="${[message(code: 'com.ef.domain.name.profile')]}" /></g:link></div>
				</div>
				<div id="show-profil" class="content scaffold-show" role="main">

					<g:if test="${profileInstance?.profilName}">
						<div class="row form-group">
							<div class="col-lg-2" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="profil.profileName.label" default="Profile Name" />
							</div>
							<div class="col-lg-3">
								<pre><g:fieldValue bean="${profileInstance}" field="profilName"/></pre>
							</div>
						</div>
					</g:if>

				</div>

				<div class="row" style="padding-top: 20px">
					<g:form url="[resource:profileInstance, action:'delete']" method="DELETE">
						<div class="col-lg-2" role="navigation">
							<g:link class="btn btn-success btn-block" action="edit" resource="${profileInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
						</div>
					</g:form>
				</div>

			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>

</body>

</html>

