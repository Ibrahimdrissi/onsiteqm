<%@ page import="com.ef.app.qm.Profile; com.ef.app.qm.User" %>

<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'profile.label', default: 'com.ef.app.qm.Profile')}" />
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<g:message code="default.list.label" args="${[message(code: 'com.ef.domain.name.profile')]}" />
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="col-lg-3" role="main">
					<g:link class="btn btn-primary btn-block" action="create"><g:message code="default.new.label" args="${[message(code: 'com.ef.domain.name.profile')]}" /></g:link>
					<br/>
				</div>
				<div class="dataTable_wrapper">
					<table class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
						<tr>
							<th>${message(code: 'profil.profileName.label', default: 'com.ef.app.qm.Profile Name')}</th>
							<th>${message(code: 'user.userName.label', default: 'Utilisateur Name')}</th>
						</tr>
						</thead>
						<tbody>
						<g:each in="${com.ef.app.qm.Profile.list()}" status="i" var="profile">
							<g:each in="${User.findAllByProfile(profile)}" status="j" var="user">
								<tr class="${(j % 2) == 0 ? 'even gradeC' : 'odd gradeX'}">
									<td><g:link controller="profile" action="show" id="${user.profile.id}">${user.profile.profilName}</g:link></td>
									<td><g:link controller="user" action="show" id="${user.id}">${user.userName}</g:link></td>
								</tr>
							</g:each>
							<g:if test="${com.ef.app.qm.User.findAllByProfile(profile).size() == 0}">
								<tr class="${(i % 2) == 0 ? 'even gradeC' : 'odd gradeX'}">
									<td ><g:link controller="profile" action="show" id="${profile.id}">${profile.profilName}</g:link></td>
									<td></td>
								</tr>
							</g:if>
						</g:each>

						</tbody>
					</table>
				</div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>

<script>
	$(document).ready(function() {
		$('#dataTables-example').DataTable({
			responsive: true
		});
	});
</script>

</body>

</html>
