
<div class="row form-group has-${hasErrors(bean: profileInstance, field: 'profilName', 'error')}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="profil.profileName.label" default="Profile Name" />
		<span class="required-indicator" style="color:red" >*</span>
	</div>
	<div class="col-lg-3">
		<g:textField  class="form-control" name="profilName" value="${profileInstance?.profilName}"/>
	</div>
</div>
