<%@ page import="com.ef.app.qm.Questionnaire;com.ef.app.qm.User; com.ef.app.qm.Questions; com.ef.app.qm.Questionnairegroup; com.ef.app.qm.Space; com.ef.app.qm.Planning; com.ef.app.qm.Evalanswer; com.ef.app.qm.Evaluation; com.ef.app.qm.Visit" %>

<html lang="en">

<head>
	<meta name="layout" content="layout"/>
	<title>Projet TT</title>
</head>

<body>

<g:set var="questionsGroup" value="${com.ef.app.qm.Questionnairegroup.findAllByQuestionnaire(Questionnaire.get(questionnaire))}" />

<div class="row">
	<div class="col-lg-12">
		<h4 class="page-header"><g:message code="com.ef.menu.Dashboard" />
		<g:message code="com.ef.menu.reporting.user" />
			<div class="pull-right">
				<div class="btn-group">
					<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
						${Questionnaire.get(questionnaire).questionnaireName}
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu pull-right" role="menu">
						<g:each var="qs" status="i" in="${com.ef.app.qm.Questionnaire.list()}">
							<li><g:link url="${request.forwardURI}?questionnaire=${qs.id}"  >${qs.questionnaireName}</g:link></li>
						</g:each>
					</ul>
				</div>
			</div>
		</h4>
	</div>
	<!-- /.col-lg-12 -->
</div>

<div class="row">

	<g:each var="groupe"  in="${questionsGroup}">

		<!-- /.panel -->
		<div class="panel panel-default">
			<!-- /.panel-heading -->
			<a name="Group_${groupe.id}" href="#Group_${groupe.id}" ></a>
			<div class="panel-heading" style="color: #fff !important;background-color: ${groupe.color} !important;" >
				<i class="fa fa-bar-chart-o fa-fw"></i> ${groupe.questionnairegroupName}
				<div class="pull-right">
					<div class="btn-group">
						<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
							${(userId == null)?utilityService.listUserHasEvaluation()[0]?.name:(userId == '0')?'Tous':com.ef.app.qm.User.get(userId)?.firstName+" "+com.ef.app.qm.User.get(userId)?.lastName}
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu pull-right" role="menu">
							<g:each var="user" status="i" in="${utilityService.listUserHasEvaluation()}">
								<li><g:link url="${request.forwardURI}?userId=${user?.id}&questionnaire=${questionnaire}#Group_${groupe.id}"  >${user?.name}</g:link></li>
							</g:each>
							<li>
								<g:link url="${request.forwardURI}?userId=0#Group_${groupe.id}"  >
									<g:message code="com.ef.menu.liste.all" />
								</g:link>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<!-- /.panel-heading -->
			<!-- /.panel-body -->
			<div class="panel-body" style="border: 1pt solid ${groupe.color} !important;border-top: none !important;">

				<script type="text/javascript">
					$(function () {

						var listVisiteString = '${scoreList["listVisites"].toString().substring(1,scoreList["listVisites"].toString().size()-1)}';
						var listVisiteArray = listVisiteString.split(',');
						// http://localhost:8090/
						var id = 'container'+'${groupe.id}';
						$('#'+id).highcharts({
							chart: { zoomType: 'x' },
							title: {text: ""},
							xAxis: {categories: listVisiteArray},
							exporting: {
								server: "http://ezzoumohammed.com/export/excel/",
								filename: "questionGroup_${groupe.id}",
								nbColumn: "${com.ef.app.qm.Questions.findAllByQuestionnairegroup(groupe).size()}",
								nbVisites: "${nbVisites}",
								chartTitre: $('<div />').html( '${groupe.questionnairegroupName}' ).text()
							},
							labels: {
								items: [{
									style: {
										left: '50px',
										top: '18px',
										color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
									}
								}]
							},series: []
						});
					});
				</script>

				<g:each var="questions" status="k"  in="${com.ef.app.qm.Questions.findAllByQuestionnairegroup(groupe)}">

					<script type="text/javascript">
						$(function () {
							var id = 'container'+'${groupe.id}';
							var chart = $('#'+id).highcharts();
							chart.addSeries( {
								type: 'column',
								name: $('<div />').html( '${scoreList["nameQuestion_"+questions.id]}' ).text(),
								data: ${scoreList["ScoresQuestion_"+questions.id]}
							}, false);

							/*
							 chart.addSeries( {
							 type: 'spline',
							 name: $('<div />').html( '%{--${scoreList['questionName'+questions.id]}--}%' ).text(),
							 data: [%{--${scoreList['questionScore'+questions.id]}--}%],
							 marker: {
							 lineWidth: 2,
							 lineColor: Highcharts.getOptions().colors[0],
							 fillColor: 'white'
							 }
							 }, false);

							 */
							chart.redraw();
						});
					</script>

				</g:each>

				<script type="text/javascript">
					$(function () {
						var id = 'container'+'${groupe.id}';
						var chart = $('#'+id).highcharts();
						chart.addSeries( {
							type: 'column',
							name: 'Moyenne',
							data: ${scoreList["ScoresMoyenne_"+groupe.id]}
						}, false);
						chart.redraw();
					});
				</script>

				<div id="container${groupe.id}" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</g:each>

<!-- /.panel -->
</div>
<!-- /.col-lg-8 -->

</div>
<!-- /.row -->


</body>

</html>