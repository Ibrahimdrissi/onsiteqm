<html>
	<head>
		<title><g:message code="error.500.callout"/> </title>
		<meta name="layout" content="layoutContent" />

	</head>

  <body>
  %{--
	<section id="overview" class="">
    	<div class="alert alert-danger">
			${request.'javax.servlet.error.message'.indexOf(':') != -1 ? request.'javax.servlet.error.message'?.substring(0, request.'javax.servlet.error.message'?.indexOf(':')).encodeAsHTML() : request.'javax.servlet.error.message'?.encodeAsHTML()}
			<g:if test="${request.'javax.servlet.error.message' == null}">
			</g:if>
			<g:elseif test="${request.'javax.servlet.error.message'.indexOf(':') != -1}">
				${request.'javax.servlet.error.message'?.substring(0, request.'javax.servlet.error.message'?.indexOf(':')).encodeAsHTML()}
			</g:elseif>
			<g:else>
				${request.'javax.servlet.error.message'?.encodeAsHTML()}
			</g:else>
			<g:if test="${exception}">
				${exception.className}
				has problem at line ${exception.lineNumber}
			</g:if>
 	   </div>
	</section>
		
	<section id="details" class="">
	    <h2>Error Details</h2>
	  	<div class="message">
			<table class="table">
				<tbody>
					<tr>
						<td><strong>Error ${request.'javax.servlet.error.status_code'}</strong></td>
						<td>
							${request.'javax.servlet.error.message'.encodeAsHTML()}
						</td>
					</tr>
					<tr>
						<td><strong>Servlet</strong></td>
						<td>
							${request.'javax.servlet.error.servlet_name'}
						</td>
					</tr>
					<tr>
						<td><strong>URI</strong></td>
						<td>
							${request.'javax.servlet.error.request_uri'}
						</td>
					</tr>
				<g:if test="${exception}">
					<tr>
						<td><strong>Exception Message:</strong></td>
						<td>
							${exception.message?.encodeAsHTML()}
						</td>
					</tr>
					<tr>
						<td><strong>Caused by:</strong></td>
						<td>
							${exception.cause?.message?.encodeAsHTML()}
						</td>
					</tr>
					<tr>
						<td><strong>Class:</strong></td>
						<td>
							${exception.className}
						</td>
					</tr>
					<tr>
						<td><strong>At Line:</strong>
						</td>
						<td> [${exception.lineNumber}]</td>
					</tr>
			  		<tr>
						<td><strong>Code Snippet:</strong></td>
						<td>
				  		<div class="snippet">
				  			<g:each var="cs" in="${exception.codeSnippet}">
				  				${cs?.encodeAsHTML()}<br />
				  			</g:each>
				  		</div>
				  		</td>
					</tr>
				</g:if>
				</tbody>
			</table>
	  	</div>
	</section>

	<g:if test="${exception}">
		<section id="exception">
		    <h2>Stack Trace</h2>
		    <div class="stack">
		      <pre><g:each in="${exception.stackTraceLines}">${it.encodeAsHTML()}<br/></g:each></pre>
		    </div>
		</section>
	</g:if>--}%
  <div class="container">
      <h1>
          <g:message code="error.500.callout"/>
      </h1>
      <h2>
          <g:message code="error.500.title"/>
      </h2>
      <p>
          <g:message code="error.500.message"/>
      </p>

      <div class="actions">
          <a href="${createLink(uri: '/')}" class="btn btn-large btn-primary">
              <i class="icon-chevron-left icon-white"></i>
              <g:message code="error.button.backToHome"/>
          </a>

      </div>
  </div>
  <div class="panel-group" id="accordion">
      <div class="panel panel-default">
          <div class="panel-heading">
              <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                      --
                  </a>
              </h4>
          </div>

          <div id="collapseOne" class="panel-collapse collapse">
              <div class="panel-body">
                  <section id="overview" class="">
                      <div class="alert alert-danger">
                          ${request.'javax.servlet.error.message'.indexOf(':') != -1 ? request.'javax.servlet.error.message'?.substring(0, request.'javax.servlet.error.message'?.indexOf(':')).encodeAsHTML() : request.'javax.servlet.error.message'?.encodeAsHTML()}
                          <g:if test="${request.'javax.servlet.error.message' == null}">
                          </g:if>
                          <g:elseif test="${request.'javax.servlet.error.message'.indexOf(':') != -1}">
                              ${request.'javax.servlet.error.message'?.substring(0, request.'javax.servlet.error.message'?.indexOf(':')).encodeAsHTML()}
                          </g:elseif>
                          <g:else>
                              ${request.'javax.servlet.error.message'?.encodeAsHTML()}
                          </g:else>
                          <g:if test="${exception}">
                              ${exception.className}
                              has problem at line ${exception.lineNumber}
                          </g:if>
                      </div>
                  </section>

                  <section id="details" class="">
                      <h2>Error Details</h2>

                      <div class="message">
                          <table class="table">
                              <tbody>
                              <tr>
                                  <td><strong>Error ${request.'javax.servlet.error.status_code'}</strong></td>
                                  <td>
                                      ${request.'javax.servlet.error.message'.encodeAsHTML()}
                                  </td>
                              </tr>
                              <tr>
                                  <td><strong>Servlet</strong></td>
                                  <td>
                                      ${request.'javax.servlet.error.servlet_name'}
                                  </td>
                              </tr>
                              <tr>
                                  <td><strong>URI</strong></td>
                                  <td>
                                      ${request.'javax.servlet.error.request_uri'}
                                  </td>
                              </tr>
                              <g:if test="${exception}">
                                  <tr>
                                      <td><strong>Exception Message:</strong></td>
                                      <td>
                                          ${exception.message?.encodeAsHTML()}
                                      </td>
                                  </tr>
                                  <tr>
                                      <td><strong>Caused by:</strong></td>
                                      <td>
                                          ${exception.cause?.message?.encodeAsHTML()}
                                      </td>
                                  </tr>
                                  <tr>
                                      <td><strong>Class:</strong></td>
                                      <td>
                                          ${exception.className}
                                      </td>
                                  </tr>
                                  <tr>
                                      <td><strong>At Line:</strong>
                                      </td>
                                      <td>[${exception.lineNumber}]</td>
                                  </tr>
                                  <tr>
                                      <td><strong>Code Snippet:</strong></td>
                                      <td>
                                          <div class="snippet">
                                              <g:each var="cs" in="${exception.codeSnippet}">
                                                  ${cs?.encodeAsHTML()}<br/>
                                              </g:each>
                                          </div>
                                      </td>
                                  </tr>
                              </g:if>
                              </tbody>
                          </table>
                      </div>
                  </section>

                  <g:if test="${exception}">
                      <section id="exception">
                          <h2>Stack Trace</h2>

                          <div class="stack">
                              <pre><g:each in="${exception.stackTraceLines}">${it.encodeAsHTML()}<br/></g:each></pre>
                          </div>
                      </section>
                  </g:if>
              </div>
          </div>
      </div>
  </div>
  </body>
</html>
