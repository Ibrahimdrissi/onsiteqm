<html>
	<head>
		<title><g:message code="error.403.callout"/></title>
		<meta name="layout" content="layoutContent" />

	</head>

<body>
	<content tag="header">
		<!-- Empty Header -->
	</content>
	
  			<div class="container">
				<h1>
					<g:message code="error.403.callout"/>
				</h1>
				<h2>
					<g:message code="error.403.title"/>
				</h2>
				<p>
					<g:message code="error.403.message"/>
				</p>
				
				<div class="actions">
					<a href="${createLink(uri: '/')}" class="btn btn-large btn-primary">
						<i class="icon-chevron-left icon-white"></i>
						<g:message code="error.button.backToHome" default="Back to Home"/>
					</a>

				</div>
			</div>

  
  
  </body>
</html>