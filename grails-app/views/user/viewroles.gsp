<%@ page import="org.apache.shiro.SecurityUtils" %>

<%--
  Author: Akeel
  Date: 12/20/12
  Time: 12:20 AM
--%>

<!doctype html>
<html>
<head>
    <meta name="layout" content="qm_main"/>
    <g:set var="userEntity" value="${message(code: 'com.ef.qm.entity.user', default: 'User')}"/>
    <g:set var="roleEntity" value="${message(code: 'com.ef.qm.entity.role', default: 'Role')}"/>
    <g:set var="permissionEntity" value="${message(code: 'com.ef.qm.entity.permission', default: 'Permission')}"/>
    <title><g:message code="cbr.form.rolesAssignedTo.label" args="[userInstance.userName]" default="Roles assigned to ${userInstance?.username}"/></title>
</head>

<body>
    <div class="row">
        <div class="col-lg-3">
            <g:render template="/templates/usermanagement" />
        </div>

        <div class="col-lg-9">
            
            <div class="page-header">
                <h1><g:message code="cbr.form.rolesAssignedTo.label" args="[userInstance.username]" default="Roles assigned to ${userInstance?.username}"/></h1>
            </div>

            <div class="tabbable tabs-left">
                <ul class="nav nav-tabs">
                    <li class="${params.tab=='current' ? 'active' : ''}">
                        <g:link action="viewroles" id="${userInstance.id}"  params="[tab:'current']">
                            ${message(code:'cbr.form.tabs.currentRoles.label',default: "Current roles")}
                        </g:link>
                    </li>
                    <li class="${params.tab=='assign' ? 'active' : ''}">
                        <g:link action="viewroles" id="${userInstance.id}"  params="[tab:'assign']">
                            ${message(code:'cbr.form.tabs.addMoreRoles.label',default: "Add roles")}
                        </g:link>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane ${params.tab=='current' ? 'active' : ''}" id="current">
                        <table class="table table-striped table-bordered table-condensed mytable" >
                            <thead>
                                <tr>
                                    <th>${message(code: 'com.ef.qm.label.name',default: "Name")}</th>
                                    <th>${message(code: 'com.ef.qm.label.description',default: "Description")}</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <g:each in="${userRoleList}" var="role">
                                    <tr>
                                        <td>
                                            <g:link controller="role" action="show" id="${role.id}">${role?.name}</g:link>
                                        </td>
                                        <td>${role?.description}</td>
                                        <td width="20%">
                                            <g:if test="${SecurityUtils.subject.isPermitted('user:reovkerole')}">
                                                <g:link action="revokerole" class="btn btn-xs btn-info" id="${role?.id}" params="[uid:userInstance.id, tab:'current']">
                                                    <g:message code="com.ef.qm.label.revokeRole" default="Revoke Rules"/>
                                                </g:link>
                                            </g:if>
                                            <g:else>
                                                <g:link class="btn btn-xs" disabled="disabled">
                                                    <g:message code="com.ef.qm.label.revokeRole" default="Revoke Rules"/>
                                                </g:link>
                                            </g:else>
                                        </td>
                                    </tr>
                                </g:each>
                            </tbody>
                        </table>
                       %{-- <div class=" pull-right">
                            <bootstrap:paginate  total="${userRoleTotal}" id="${userInstance.id}" params="[tab:'current']"/>
                        </div>--}%
                    </div>
                    <div class="tab-pane ${params.tab=='assign' ? 'active' : ''}" id="assign">
                        <table class="table table-striped table-bordered table-condensed mytable">
                            <thead>
                                <tr>
                                    <g:sortableColumn property="name" title="${message(code: 'com.ef.qm.label.name',default: "Name")}"/>
                                    <th>${message(code: 'com.ef.qm.label.description',default: "Description")}</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <g:each in="${roleList}" var="role">
                                    <tr>
                                        <td>
                                            <g:link controller="role" action="show" id="${role.id}">${role?.name}</g:link>
                                        </td>
                                        <td>${role?.description}</td>
                                        <td width="20%">
                                            <g:if test="${SecurityUtils.subject.isPermitted('user:assignrole')}">
                                                <g:link action="assignrole" class="btn btn-xs btn-info" id="${role?.id}" params="[uid:userInstance.id, tab:'assign']">
                                                    <g:message code="com.ef.qm.label.assignRole" default="Assign role"/>
                                                </g:link>
                                            </g:if>
                                            <g:else>
                                                <g:link class="btn btn-xs btn-info" disabled="disabled">
                                                    <g:message code="com.ef.qm.label.assignRole" default="Assign role"/>
                                                </g:link>
                                            </g:else>
                                        </td>
                                    </tr>
                                </g:each>
                            </tbody>
                        </table>
                        %{--<div class=" pull-right">
                            <bootstrap:paginate  total="${roleTotal}" id="${userInstance.id}" params="[tab:'assign']" />
                        </div> --}%
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

<script>
    var myParam = location.search.split('lang=')[1] ? location.search.split('lang=')[1] : 'en';;
    $('.mytable').dataTable({
        "bInfo": false,
        "bAutoWidth": false,
        "oLanguage": {
            "sUrl": "../../dataTableLang/dataTables_"+myParam+".txt"
        }
    });

</script>