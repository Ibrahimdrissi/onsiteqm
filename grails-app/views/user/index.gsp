
<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
	<title><g:message code="com.expertflow.ma.onsiteqm.applicationName"/></title>
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<g:message code="default.list.label" args="${[message(code: 'com.ef.domain.name.user')]}" />
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="col-lg-3" role="main">
					<g:link class="btn btn-primary btn-block" action="create"><g:message code="default.new.label" args="${[message(code: 'com.ef.domain.name.user')]}" /></g:link>
					<br/>
				</div>
				<div class="dataTable_wrapper">
					<table class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
						<tr>
							<th>${message(code: 'user.userName.label', default: 'User Name')}</th>
							<th>${message(code: 'user.firstName.label', default: 'Nom')}</th>
							<th>${message(code: 'user.lastName.label', default: 'Prenom')}</th>
							<th>${message(code: 'user.userEmail.label', default: 'User Email')}</th>
							<th><g:message code="user.profil.label" default="Profil" /></th>
							<th><g:message code="user.team.label" default="Team" /></th>

						</tr>
						</thead>
						<tbody>
						<g:each in="${com.ef.app.qm.User.list()}" status="i" var="userInstance">
							<tr class="${(i % 2) == 0 ? 'even gradeC' : 'odd gradeX'}">

								<td><g:link action="show" id="${userInstance.id}">${fieldValue(bean: userInstance, field: "userName")}</g:link></td>

								<td>${fieldValue(bean: userInstance, field: "firstName")}</td>

								<td>${fieldValue(bean: userInstance, field: "lastName")}</td>

								<td>${fieldValue(bean: userInstance, field: "userEmail")}</td>

								<td>${fieldValue(bean: userInstance, field: "profile.profilName")}</td>

								<td><g:link action="show" controller="team" id="${userInstance?.team?.id}">${userInstance?.team}</g:link></td>

							</tr>
						</g:each>

						</tbody>
					</table>
				</div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>

<script>
	$(document).ready(function() {
		$('#dataTables-example').DataTable({
			responsive: true
		});
	});
</script>

</body>

</html>
