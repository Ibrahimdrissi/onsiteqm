<%@ page import="com.ef.app.qm.Space" %>

<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'com.ef.app.qm.user.label', default: 'com.ef.app.qm.User')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<g:message code="default.show.label" args="[entityName]" />
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row" style="padding-bottom: 20px">
					<div class="col-lg-2" role="navigation">
						<g:link class="btn btn-primary btn-block" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link>
					</div>
					<div class="col-lg-2" role="navigation">
						<g:link class="btn btn-primary btn-block" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></div>
				</div>
				<div id="show-utilisateur" class="content scaffold-show" role="main">

					<g:if test="${userInstance?.userName}">
						<div class="row form-group">
							<div class="col-lg-2" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="user.userName.label" default="com.ef.app.qm.User Name" />
							</div>
							<div class="col-lg-3">
								<pre><g:fieldValue bean="${userInstance}" field="userName"/></pre>
							</div>
						</div>
					</g:if>

					<g:if test="${userInstance?.firstName}">
						<div class="row form-group">
							<div class="col-lg-2" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="user.firstName.label" default="com.ef.app.qm.User Name" />
							</div>
							<div class="col-lg-3">
								<pre><g:fieldValue bean="${userInstance}" field="firstName"/></pre>
							</div>
						</div>
					</g:if>

					<g:if test="${userInstance?.lastName}">
						<div class="row form-group">
							<div class="col-lg-2" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="user.lastName.label" default="com.ef.app.qm.User Name" />
							</div>
							<div class="col-lg-3">
								<pre><g:fieldValue bean="${userInstance}" field="lastName"/></pre>
							</div>
						</div>
					</g:if>

					<g:if test="${userInstance?.userEmail}">
						<div class="row form-group">
							<div class="col-lg-2" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="user.userEmail.label" default="com.ef.app.qm.User Email" />
							</div>
							<div class="col-lg-3">
								<pre><g:fieldValue bean="${userInstance}" field="userEmail"/></pre>
							</div>
						</div>
					</g:if>

					<g:if test="${userInstance?.profile}">
						<div class="row form-group">
							<div class="col-lg-2" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="user.profil.label" default="com.ef.app.qm.Profile" />
							</div>
							<div class="col-lg-3">
								<pre><g:link controller="profile" action="show" id="${userInstance?.profile?.id}">${userInstance?.profile?.profilName.encodeAsHTML()}</g:link></pre>
							</div>
						</div>
					</g:if>

					<g:if test="${userInstance?.team}">
						<div class="row form-group">
							<div class="col-lg-2" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="user.team.label" default="com.ef.app.qm.Team" />
							</div>
							<div class="col-lg-3">
								<pre><g:link controller="team" action="show" id="${userInstance?.team?.id}">${userInstance?.team?.teamName.encodeAsHTML()}</g:link></pre>
							</div>
						</div>
					</g:if>

					<g:if test="${com.ef.app.qm.Space.findByUser(userInstance) != null}">
					<div class="row form-group">
						<div class="col-lg-2" style="font-weight: bold;padding-top: 8px;" >
							<g:message code="user.space.label" default="Espaces" />
						</div>
						<div class="col-lg-3">
							<g:each var="space" in="${Space.findAllByUser(userInstance)}">
								<pre><g:link controller="space" action="show" id="${space.id}">${space?.spaceName.encodeAsHTML()}</g:link></pre>
							</g:each>
						</div>
					</div>
					</g:if>

				</div>

				<div class="row" style="padding-top: 20px">
					<g:form url="[resource:userInstance, action:'delete']" method="DELETE">
						<div class="col-lg-2" role="navigation">
							<g:link class="btn btn-success btn-block" action="edit" resource="${userInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
						</div>
					</g:form>
				</div>

			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>


</body>

</html>
