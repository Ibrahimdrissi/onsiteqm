<%@ page import="com.ef.app.qm.Profile" %>

<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'com.ef.app.qm.user.label', default: 'com.ef.app.qm.User')}" />
	<title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<g:message code="default.edit.label" args="[entityName]" />
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row" style="padding-bottom: 20px">
					<div class="col-lg-2" role="navigation">
						<g:link class="btn btn-primary btn-block" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link>
					</div>
					<div class="col-lg-2" role="navigation">
						<g:link class="btn btn-primary btn-block" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></div>
				</div>
				<div id="edit-utilisateur" class="content scaffold-edit" role="main">


					<g:form url="[resource:userInstance, action:'update']" method="PUT" >
						<g:hiddenField name="version" value="${userInstance?.version}" />
						<fieldset class="form">
							<g:render template="formEdit"/>
						</fieldset>
						<fieldset class="buttons">
							<br/>
							<g:actionSubmit class="btn btn-success" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />

						</fieldset>
					</g:form>


				</div>

			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>

<script >

	$(function(){

		var id_admin = '${Profile.findByProfilNameLike('%Administrateur%')?.id}';
		var id_membre = '${com.ef.app.qm.Profile.findByProfilNameLike('%entrale%')?.id}';
		var id_consultation = '${Profile.findByProfilNameLike('%Consultation%')?.id}';

		var profile = '${userInstance?.profile?.id}';
		if(profile == id_admin || profile == id_membre || profile == id_consultation){
			$(".profileAdmin").hide();
		}else{
			$(".profileAdmin").show();
		}

		$("#profile").change(function(){
			// id chef de cellule
			if($(this).val() == id_admin || $(this).val() == id_membre || $(this).val() == id_consultation){
				$(".profileAdmin").hide();
			}else{
				$(".profileAdmin").show();
			}
		});

	});

</script>

</body>

</html>
