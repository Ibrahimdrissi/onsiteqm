<%@ page import="com.ef.app.qm.Team; com.ef.app.qm.Profile; com.ef.app.qm.Space; javax.validation.constraints.Null" %>

<div class="row form-group has-${hasErrors(bean: userInstance, field: 'userName', 'error')}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="user.userName.label" default="com.ef.app.qm.User Name" />
		<span class="required-indicator" style="color:red" >*</span>
	</div>
	<div class="col-lg-3">
		<g:textField  class="form-control" name="userName" value="${userInstance?.userName}"/>
	</div>
</div>

<div class="row form-group has-${hasErrors(bean: userInstance, field: 'firstName', 'error')}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="user.firstName.label" default="com.ef.app.qm.User Name" />
		<span class="required-indicator" style="color:red" >*</span>
	</div>
	<div class="col-lg-3">
		<g:textField  class="form-control" name="firstName" value="${userInstance?.firstName}"/>
	</div>
</div>

<div class="row form-group has-${hasErrors(bean: userInstance, field: 'lastName', 'error')}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="user.lastName.label" default="com.ef.app.qm.User Name" />
		<span class="required-indicator" style="color:red" >*</span>
	</div>
	<div class="col-lg-3">
		<g:textField  class="form-control" name="lastName" value="${userInstance?.lastName}"/>
	</div>
</div>


<div class="row form-group has-${hasErrors(bean: userInstance, field: 'userPassword', 'error')}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="user.userPassword.label" default="com.ef.app.qm.User Password" />
		<span class="required-indicator" style="color:red" >*</span>
	</div>
	<div class="col-lg-3">
		<g:passwordField  class="form-control" name="userPassword" value="${userInstance?.userPassword}"/>
	</div>
</div>

<div class="row form-group has-${hasErrors(bean: userInstance, field: 'userEmail', 'error')}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="user.userEmail.label" default="com.ef.app.qm.User Email" />
		<span class="required-indicator" style="color:red" >*</span>
	</div>
	<div class="col-lg-3">
		<g:textField  class="form-control" name="userEmail" value="${userInstance?.userEmail}"/>
	</div>
</div>

<div class="row form-group ">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="user.profil.label" default="com.ef.app.qm.Profile" />
	</div>
	<div class="col-lg-3">
		<g:select value="${userInstance?.profile?.id}" class="form-control" id="profile" name="profile.id" from="${com.ef.app.qm.Profile.list()}" optionKey="id" optionValue="profilName" />
	</div>
</div>

<div class="row profileAdmin form-group has-${(profile_error == null)?'':profile_error}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="user.team.label" default="com.ef.app.qm.Team" />
		<span class="required-indicator" style="color:red" >*</span>
	</div>
	<div class="col-lg-3">
		<g:select value="${userInstance?.team?.id}" class="form-control" id="team" name="team.id" from="${com.ef.app.qm.Team.list()}" noSelection="${['null':'']}" optionKey="id" optionValue="teamName" />
	</div>
</div>

<g:if test="${userInstance?.id == null}">
	<div class="row form-group profileAdmin ">
		<div class="col-lg-2" style="font-weight: bold" >
			<g:message code="user.space.label" default="com.ef.app.qm.Team" />
		</div>
		<div class="col-lg-3 ">
			<p style="  height: 100px;overflow-y: scroll;border: 1pt solid #ccc;border-radius: 3pt;padding: 5px;">
				<g:each var="space" in="${com.ef.app.qm.Space.findAllByUser(null)}">
					<label><input style="vertical-align: top;" type="checkbox" name="spaces" value="${space.id}" />&nbsp;${space.spaceName}</label><br>
				</g:each>
			</p>
		</div>
	</div>
</g:if>
<g:else>
	<div class="row form-group profileAdmin ">
		<div class="col-lg-2" style="font-weight: bold" >
			<g:message code="user.space.label" default="com.ef.app.qm.Team" />
		</div>
		<div class="col-lg-3 ">
			<p style="  height: 100px;overflow-y: scroll;border: 1pt solid #ccc;border-radius: 3pt;padding: 5px;">
				<g:each var="space" in="${Space.findAllByUserOrUser(userInstance,null)}">
					<g:if test="${space?.user == null}">
						<label><input style="vertical-align: top;" type="checkbox" name="spaces" value="${space.id}" />&nbsp;${space.spaceName}</label><br>
					</g:if>
					<g:else>
						<label><input checked="checked" style="vertical-align: top;" type="checkbox" name="spaces" value="${space.id}" />&nbsp;${space.spaceName}</label><br>
					</g:else>
				</g:each>
			</p>
		</div>
	</div>
</g:else>
