<%@ page import="org.apache.shiro.SecurityUtils" %>


<%--
  Author: Akeel
  Date: 12/20/12
  Time: 12:20 AM
--%>

<!doctype html>
<html>
<head>
    <meta name="layout" content="qm_main"/>
    <g:set var="userEntity" value="${message(code: 'com.ef.qm.entity.user', default: 'User')}"/>
    <g:set var="roleEntity" value="${message(code: 'com.ef.qm.entity.role', default: 'Role')}"/>
    <g:set var="permissionEntity" value="${message(code: 'com.ef.qm.entity.permission', default: 'Permission')}"/>
    <title><g:message code="cbr.form.permissionsAssignedTo.label" args="[userInstance.username]" default="Assign permission"/></title>
</head>

<body>
    <div class="row">
        <div class="col-lg-3">
            <g:render template="/templates/usermanagement" />
        </div>

        <div class="col-lg-9">
            
            <div class="page-header">
                <h1><g:message code="cbr.form.permissionsAssignedTo.label" args="[userInstance.username]" default="Assign Permission to ${userInstance?.username}"/></h1>
            </div>

            <div class="tab  tabs-left">
                <ul class="nav nav-tabs">                    
                    <li class="${params.tab=='current' ? 'active' : ''}">
                        <g:link action="viewpermissions" id="${userInstance.id}" params="[tab:'current']">
                            ${message(code:'com.ef.qm.currentPermissions.label',default: "Current Permissions")}
                        </g:link>
                    </li>
                    <li class="${params.tab=='assign' ? 'active' : ''}">
                        <g:link action="viewpermissions" id="${userInstance.id}" params="[tab:'assign']">
                            ${message(code:'com.ef.qm.assignPermissions.label',default: "Assign Permissions")}
                        </g:link>
                    </li>
                    <li class="${params.tab=='rolePermission' ? 'active' : ''}">
                        <g:link action="viewpermissions" id="${userInstance.id}" params="[tab:'rolePermission']">
                            ${message(code:'com.ef.qm.roleUserPermissions.label',default: "Permissions from rules")}
                        </g:link>
                    </li>

                </ul>
                <div class="tab-content">
                    <div class="tab-pane ${params.tab=='current' ? 'active' : ''}" id="current">
                        <table class="table table-striped table-bordered table-condensed mytable">
                            <thead>
                                <tr>
                                    <th>${message(code: 'com.ef.qm.label.name',default: "Name")}</th>
                                    <th>${message(code: 'com.ef.qm.label.description',default: "Description")}</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <g:each in="${userPermissionList}" status="i" var="userPermission">
                                    <tr>
                                        <td><g:message code="${userPermission?.name}"/> </td>
                                        <td><g:message code="${userPermission?.description}"/> </td>

                                        <td width="20%">
                                            <g:if test="${SecurityUtils.subject.isPermitted('user:reovkepermission')}">
                                                <g:link action="reovkepermission" class="btn btn-xs btn-info" id="${userPermission?.id}" params="[uid:userInstance.id, tab:'current']">
                                                    <g:message code="com.ef.qm.label.revokePermission" default="Revoke permissions"/>
                                                </g:link>
                                            </g:if>
                                            <g:else>
                                                <g:link class="btn btn-xs" disabled="disabled">
                                                    <g:message code="com.ef.qm.label.revokePermission" default="Revoke permissions"/>
                                                </g:link>
                                            </g:else>
                                        </td>
                                    </tr>
                                </g:each>
                            </tbody>
                        </table>
                        %{--<div class="pull-right">
                            <bootstrap:paginate  total="${userPermissionTotal}" id="${userInstance.id}" params="[tab:'current']" />
                            </div>--}%
                    </div>
                    <div class="tab-pane ${params.tab=='assign' ? 'active' : ''}" id="assign">
                        <table class="table table-striped table-bordered table-condensed mytable" >
                            <thead>
                                <tr>
                                    <g:sortableColumn property="name" title="${message(code: 'com.ef.qm.label.name',default: "Name")}"/>
                                    <th>${message(code: 'com.ef.cbr.label.description',default: "Description")}</th>
                                     <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <g:each in="${permissionList}" var="permission">
                                    <tr>
                                        <td><g:message code="${permission?.name}"/> </td>
                                        <td><g:message code="${permission?.description}"/> </td>
                                        <td width="20%">
                                            <g:if test="${SecurityUtils.subject.isPermitted('user:assignpermission')}">
                                                <g:link action="assignpermission" class="btn btn-xs btn-info" id="${permission.id}" params="[uid:userInstance.id, tab:'assign']">
                                                    <g:message code="com.ef.qm.label.assignPermission" default="Assign Permission"/>
                                                </g:link>
                                            </g:if>
                                            <g:else>
                                                <g:link class="btn btn-xs btn-info" disabled="disabled">
                                                    <g:message code="com.ef.qm.label.assignPermission" default="Assign Permission"/>
                                                </g:link>
                                            </g:else>
                                        </td>
                                    </tr>
                                </g:each>
                            </tbody>
                        </table>
                        %{--<div class="pull-right">--}%
                            %{--<bootstrap:paginate   total="${permissionTotal}" id="${userInstance.id}" params="[tab:'assign']" />--}%
                        %{--</div>--}%
                    </div>
                    <div class="tab-pane ${params.tab=='rolePermission' ? 'active' : ''}" id="rolePermission">
                        <table class="table table-striped table-bordered table-condensed mytable" >
                            <thead>
                            <tr>
                                <g:sortableColumn property="name" title="${message(code: 'com.ef.qm.label.name',default: "Name")}"/>
                                <th>${message(code: 'com.ef.qm.label.description',default: "Description")}</th>

                            </tr>
                            </thead>
                            <tbody>
                            <g:each in="${roleUserPermission}" var="permission">
                                <tr>
                                    <td><g:message code="${permission?.name}"/> </td>
                                    <td><g:message code="${permission?.description}"/> </td>
                                    %{--<td width="20%" class="hidden">
                                        <g:if test="${SecurityUtils.subject.isPermitted('user:assignpermission')}">
                                            <g:link action="assignpermission" class="btn btn-xs btn-info" id="${permission.id}" params="[uid:userInstance.id, tab:'assign']">
                                                <g:message code="com.ef.cbr.label.assignPermission"/>
                                            </g:link>
                                        </g:if>
                                        <g:else>
                                            <g:link class="btn btn-xs btn-info" disabled="disabled">
                                                <g:message code="com.ef.cbr.label.assignPermission"/>
                                            </g:link>
                                        </g:else>
                                    </td>--}%
                                </tr>
                            </g:each>
                            </tbody>
                        </table>
                        %{--<div class="pull-right">--}%
                        %{--<bootstrap:paginate   total="${permissionTotal}" id="${userInstance.id}" params="[tab:'assign']" />--}%
                        %{--</div>--}%
                    </div>

                </div>
            </div>
        </div>
    </div>
</body>
</html>
<script>
    var myParam = location.search.split('lang=')[1] ? location.search.split('lang=')[1] : 'en';;
    $('.mytable').dataTable({
        "bInfo": false,
        "bAutoWidth": false,
        "oLanguage": {
            "sUrl": "../../dataTableLang/dataTables_"+myParam+".txt"
        }
    });

</script>