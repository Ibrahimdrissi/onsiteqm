<%@ page import="com.ef.app.qm.Planning; com.ef.app.qm.Evaluation; com.ef.app.qm.Visit; com.ef.app.qm.Questionnairegroup; com.ef.app.qm.Space" %>

<html lang="en">

<head>
	<meta name="layout" content="layoutReporting"/>
	<g:set var="entityName" value="${message(code: 'evaluation.label', default: 'Evaluation')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">

		<g:set var="user" value="${session?.user}" ></g:set>

		<div id="show-evaluation" class="content scaffold-show" role="main">

			<div class="panel panel-primary">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseTotal"><g:message code="com.ef.menu.reporting.myplannings" /></a>
						<div class="pull-right">
							<div class="btn-group">
								<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
									Export
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu pull-right" role="menu">
									<li id="btnExport" ><a href="#">XLS</a></li>
								</ul>
							</div>
						</div>
					</h4>
				</div>
				<div id="collapseTotal" class="panel-collapse collapse in ">
					<div class="panel-body">

						<div class="table-responsive" style="overflow-x: hidden !important;">
							<g:set var="listDate" value="${utilityService.getMyPlanningByDay(session?.user?.id)}"></g:set>
							<table class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
								<tr >
									<th rowspan="2" colspan="4" style="padding: 3px !important;min-width: 70px !important;" ></th>
									<g:each var="row" status="j" in="${utilityService.getMyPlanningByYear(session?.user?.id)}">
										<th colspan="${row.planning_counter}" style="${(j % 2) == 0 ? 'background-color: #ddd' : 'background-color: #FFF'}  ;text-align: center;padding: 3px !important;min-width: 70px !important;" >${row.year}</th>
									</g:each>
								</tr>
								<tr>
									<g:each var="row" status="j" in="${utilityService.getMyPlanningByMonth(session?.user?.id)}">
										<th colspan="${row.planning_counter}" style="${(j % 2) == 0 ? 'background-color: #f9f9f9' : 'background-color: #FFF'}  ;text-align: center;padding: 3px !important;min-width: 70px !important;" >
											${monthList[""+row.planning_month]}
										</th>
									</g:each>
								</tr>
								<tr>
									<th style="padding: 3px !important;min-width: 98px !important ;max-width: 98px !important ;" ><g:message code="reporting.animateur.label" /></th>
									<th style="padding: 3px !important;min-width: 98px !important ;max-width: 98px !important ;" ><g:message code="reporting.space.label" /></th>
									<th style="padding: 3px !important;min-width: 98px !important ;max-width: 98px !important ;" ><g:message code="reporting.Gspace.label" /></th>
									<th style="padding: 3px !important;min-width: 98px !important ;max-width: 98px !important ;" ><g:message code="reporting.TotalCollaborateur.label" /></th>
									<g:each var="row" in="${listDate}">
										<th style="padding: 3px !important;min-width: 98px !important ;max-width: 98px !important ;text-align: center;" >${row.planning_day}</th>
									</g:each>
								</tr>
								</thead>
								<tbody>

									<g:set var="total" value="${0}" ></g:set>

									<g:set var="nbColaboratteurs" value="${0}"></g:set>
									<g:set var="nbSpaces" value="${0}"></g:set>

									<g:each var="planning" in="${utilityService.reportingByUser(Long.parseLong(""+user.id))}">

										<g:set var="nbColaboratteurs" value="${planning.total}"></g:set>
										<g:set var="nbSpaces" value="${planning.nbSpaceHasPlanningByUser}"></g:set>

										<tr>

											%{--background-color: ${(com.ef.app.qm.Planning.findByPlanningDateAndSpace(row.planning_date,Space.get(planning.SPACE_ID)).planningState == 0)?'#5cb85c':'#f0ad4e'};--}%
											<td style="padding: 3px !important;min-width: 98px !important ;max-width: 98px !important ;" >${planning.utilisateur}</td>
											<td style="padding: 3px !important;min-width: 98px !important ;max-width: 98px !important ;" >${planning.space_name}</td>
											<td style="padding: 3px !important;min-width: 98px !important ;max-width: 98px !important ;" >${planning.space_code}</td>
											<td style="padding: 3px !important;min-width: 98px !important ;max-width: 98px !important ;" >${planning.nb}</td>
											<g:each var="row" in="${listDate}">
												<g:if test="${((row.user_holiday).equals(planning['USER_ID']) ) && ((row.holiday).equals("1"))}" >
													<td style="background-image: url(../images/path-to-stripe.png);background-repeat: repeat;vertical-align: middle;text-align: center;padding: 3px !important;min-width: 70px !important;" ><b>Cong&eacute;</b></td>
												</g:if>
												<g:else>
													<g:set var="plannings" value="${com.ef.app.qm.Planning.findAllByPlanningDateAndSpace(row.planning_date,Space.get(planning.SPACE_ID))}" ></g:set>
													<g:if test="${plannings.size() == 0}">
														<td style="padding: 3px !important;min-width: 98px !important ;max-width: 98px !important ;" ></td>
													</g:if>
													<g:elseif test="${plannings.size() == 1 && ((plannings[0].planningState == Planning.VALIDATED_STATE) || (plannings[0].planningState == Planning.WAITING_CONFIRMATION_STATE) )}">
														<td style="background-color:${utilityService.getBackgroundColorByPlanning(plannings[0])};color:#fff;text-align: center;padding: 3px !important;min-width: 98px !important ;max-width: 98px !important ;vertical-align: inherit;" >
															<g:link style="color:#fff;" controller="planning" action="show" id="${plannings[0].id}">${plannings[0].space.spaceName+":"}${plannings[0].visit.visitName}</g:link>
														</td>
													</g:elseif>
													<g:else>
														<td style="color:#fff;text-align: center;padding: 3px !important;min-width: 98px !important ;max-width: 98px !important ;vertical-align: inherit;" >
															<table style="width: 100%;" >
																<g:each var="pl" in="${plannings}">
																	<g:if test="${pl.planningState == Planning.VALIDATED_STATE || pl.planningState == Planning.WAITING_CONFIRMATION_STATE}">

																		<tr><td style="color:#fff;text-align: center;vertical-align: inherit;margin: 2px;background-color:${utilityService.getBackgroundColorByPlanning(pl)}" >
																			<g:link style="color:#fff;" controller="planning" action="show" id="${pl.id}">${pl.space.spaceName+":"}${pl.visit.visitName}</g:link>
																		</td></tr>
																	</g:if>
																</g:each>
															</table>
														</td>
													</g:else>
												</g:else>
											</g:each>
										</tr>
								</g:each>

								<tr style="" >
									<td colspan="3" style="background-color: #ddd;padding: 3px !important;min-width: 70px !important;" >Total de nombre d'espaces &agrave; visiter</td>
									<td style="display: none" >${nbSpaces}</td>
									<td style="display: none" ></td>
									<td style="padding: 3px !important;min-width: 70px !important;" ><input style="padding: 0;background: none;border: none" disabled type="button" value="${nbSpaces}" /></td>
									<g:each var="row" in="${listDate}">
										<g:if test="${ planning!=null && ((row.user_holiday).equals(planning['USER_ID']) ) && ((row.holiday).equals("1"))}" >
											<td style="background-color: green;padding: 3px !important;min-width: 70px !important;" ></td>
										</g:if>
										<g:else>
											<td style="padding: 3px !important;min-width: 70px !important;" ></td>
										</g:else>
									</g:each>
								</tr>
								<tr>
									<td colspan="3" style="background-color: #eee;padding: 3px !important;min-width: 70px !important;" >Total des collaborateurs &agrave; coacher</td>
									<td style="display: none" >${nbColaboratteurs}</td>
									<td style="display: none" ></td>
									<td style="padding: 3px !important;min-width: 70px !important;" ><input style="padding: 0;background: none;border: none" disabled type="button" value="${nbColaboratteurs}" /></td>
									<g:each var="row" in="${listDate}">
										<g:if test="${ planning!=null && ((row.user_holiday).equals(planning['USER_ID']) ) && ((row.holiday).equals("1"))}" >
											<td style="background-color: green;padding: 3px !important;min-width: 70px !important;" ></td>
										</g:if>
										<g:else>
											<td style="padding: 3px !important;min-width: 70px !important;" ></td>
										</g:else>
									</g:each>
								</tr>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

		</div>

	</div>
	<!-- /.col-lg-12 -->
</div>

<script src="${resource(dir: 'theme/bower_components/bootstrap/dist/js/', file: 'bootstrap.min.js')}"></script>
<script src="${resource(dir: 'theme/bower_components/metisMenu/dist/', file: 'metisMenu.min.js')}"></script>

<script src="${resource(dir: 'theme/bower_components/datatables/media/js/', file: 'jquery.dataTables.min.js')}"></script>
<script src="${resource(dir: 'theme/bower_components/datatables-plugins/integration/bootstrap/3/', file: 'dataTables.bootstrap.min.js')}"></script>

<script src="${resource(dir: 'theme/bower_components/datatables/extensions/FixedColumns/js/', file: 'dataTables.fixedColumns.js')}"></script>

<script src="${resource(dir: 'theme/dist/js/', file: 'sb-admin-2.js')}"></script>

<script src="${resource(dir: 'js/', file: 'jquery.btechco.excelexport.js')}"></script>
<script src="${resource(dir: 'js/', file: 'jquery.base64.js')}"></script>

<script>
	$(document).ready(function() {
		var table = $('#dataTables-example').DataTable({
			responsive: true,
			"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"ordering": false,
			scrollY:        "500px",
			scrollX:        true,
			scrollCollapse: true,
		});

		new $.fn.dataTable.FixedColumns( table, {
			leftColumns: 4
		} );

		var id = '${session?.user?.id}';
		var dataHeader = (function () {
			var json = null;
			$.ajax({
				'async': false,
				'global': false,
				'url': "${createLink(action: 'planningAnimateurJs2', controller: 'reporting'  )}?id="+id,
				'dataType': "json",
				'success': function (data) {
					json = data;
				}
			});
			return json;
		})();


		var dataobj = (function () {

			var json = null;
			$.ajax({
				'async': false,
				'global': false,
				'url': "${createLink(action: 'planningAnimateurJs', controller: 'reporting'  )}?id="+id,
				'dataType': "json",
				'success': function (data) {
					json = data;
				}
			});
			return json;
		})();

		$("#btnExport").click(function () {
			$("#dvjson").battatech_excelexport({
				containerid: "dvjson"
				, datatype: 'json'
				, dataset: dataobj
				, columns: dataHeader
			});
		});
	});
</script>

</body>

</html>
