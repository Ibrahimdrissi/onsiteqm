<%@ page import="com.ef.app.qm.Questionnaire; com.ef.app.qm.Evalanswer; com.ef.app.qm.Planning; com.ef.app.qm.Evaluation; com.ef.app.qm.Visit; com.ef.app.qm.Questionnairegroup; com.ef.app.qm.Space" %>

<html lang="en">

<head>
	<meta name="layout" content="layoutReporting"/>
	<g:set var="entityName" value="${message(code: 'evaluation.label', default: 'Evaluation')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">

		<div id="show-evaluation" class="content scaffold-show" role="main">

			<div class="panel panel-primary">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseTotal"><g:message code="evaluation.note.ponderation.libelle" /></a>
						<div class="pull-right">
							<div class="btn-group">
								<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
									Export
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu pull-right" role="menu">
									<li id="btnExport" ><a href="#">XLS</a></li>
								</ul>
							</div>
						</div>
					</h4>
				</div>
				<div id="collapseTotal" class="panel-collapse collapse in ">
					<div class="panel-body" >

						<div class="table-responsive" style="overflow-x: hidden;">
							<table class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
								<tr>
									<th style="padding: 3px !important;min-width: 120px !important;" >R&eacute;f&eacute;rence</th>
									%{--<th style="padding: 3px !important;min-width: 82px !important;"><g:message code="reporting.zone.label" /></th>--}%
									<th style="padding: 3px !important;min-width: 82px !important;"><g:message code="reporting.region.label" /></th>
									<th style="padding: 3px !important;min-width: 82px !important;"><g:message code="reporting.Cellule.label" /></th>
									<th style="padding: 3px !important;min-width: 82px !important;"><g:message code="reporting.animateur.label" /></th>
									<g:each var="row"  in="${utilityService.getListVisitOfPlanings()}">
										<th  style="padding: 3px !important;min-width: 70px !important;" >${row}</th>
									</g:each>
									<th style="padding: 3px !important;min-width: 82px !important;" >Moyenne</th>
								</tr>
								</thead>
								<g:each var="espace" in="${utilityService.listSpacesHasPlanning()}">
									<tr>
										<td>${espace.space_name}</td>
										%{--<td>${espace.zone}</td>--}%
										<td>${espace.region}</td>
										<td>${espace.team_name}</td>
										<td>${espace.nameComplete}</td>

										<g:set var="counter" value="${0}" />
										<g:set var="totalScore" value="${0}" />

										<g:each var="row"  in="${utilityService.getListVisitOfPlanings()}">
											<g:set var="planning" value="${Planning.findByVisitAndSpaceAndPlanningState(Visit.findByVisitName(row),Space.get(Integer.parseInt(""+espace.SPACE_ID)),Planning.EVALUATED_STATE)}" />
											<g:set var="note" value="${Evaluation.findByPlanningAndEvalStatus(planning,com.ef.app.qm.Evaluation.VALIDATED_STATE)?.score}" />
											%{--visite: ${visit} / space: ${espace.SPACE_ID} ${espace.space_name} Planning: ${Planning.findByVisitAndSpace(visit,Space.get(Integer.parseInt(""+espace.SPACE_ID))).id} evaluation ${Evaluation.findByPlanning(Planning.findByVisitAndSpace(visit,Space.get(Integer.parseInt(""+espace.SPACE_ID))))?.id}  note ${note} <br/>--}%
											<g:if test="${note != null }">
												<g:set var="note" value="${(note.indexOf(",") > 0)?note.replace(",","."):note}" />

												<g:if test="${Double.parseDouble(note) >= 4 }">
													<td style='background-color:#5cb85c;background: none;' >
                                                    <button title="<g:formatDate format="dd/MM/yyyy" date="${planning.planningDate}"/>" data-toggle="tooltip" data-placement="top" style="margin-left: 20%;" type="button" class="btn btn-success btn-circle">
													${note}
													</button>
												</g:if>

												<g:elseif test="${Double.parseDouble(note) < 4 &&  Double.parseDouble(note) >= 2.5 }">
													<td style='background-color:#f0ad4e;background: none;' >
                                                    <button title="<g:formatDate format="dd/MM/yyyy" date="${planning.planningDate}"/>" data-toggle="tooltip" data-placement="top" style="margin-left: 20%;" type="button" class="btn btn-warning btn-circle">
													${note}
													</button>
												</g:elseif>

												<g:elseif test="${ Double.parseDouble(note)  < 2.5 }">
													<td style='background-color:#d9534f;background: none;' >

                                                    <button title="<g:formatDate format="dd/MM/yyyy" date="${planning.planningDate}"/>" data-toggle="tooltip" data-placement="top" style="margin-left: 20%;" type="button" class="btn btn-danger btn-circle">
													${note}
													</button>
												</g:elseif>

												<g:set var="counter" value="${counter + 1}" />
												<g:set var="totalScore" value="${totalScore + Double.parseDouble(note)}" />
												</td>
											</g:if>

											<g:else>
												<td> </td>
											</g:else>
											%{--((double)Math.round((totalScore) * 100) / 100)--}%
										</g:each>

											%{--${(totalScore == 0)?'':((double)Math.round((totalScore/Integer.parseInt(String.valueOf(counter))) * 100) / 100)}--}%
											<g:if test="${totalScore != 0 }">
												<g:set var="moyenne" value="${((double)Math.round((totalScore/Integer.parseInt(String.valueOf(counter))) * 100) / 100)}" />

												<g:if test="${moyenne >= 4 }">
													<td style='background-color:#5cb85c;background: none;' >
													<button style="margin-left: 20%;" type="button" class="btn btn-success btn-circle">
													${moyenne}
													</button>
												</g:if>

												<g:elseif test="${moyenne < 4 &&  moyenne >= 2.5 }">
													<td style='background-color:#f0ad4e;background: none;' >
													<button style="margin-left: 20%;" type="button" class="btn btn-warning btn-circle">
													${moyenne}
													</button>
												</g:elseif>

												<g:elseif test="${ moyenne  < 2.5 }">
													<td style='background-color:#d9534f;background: none;' >
													<button style="margin-left: 20%;" type="button" class="btn btn-danger btn-circle">
													${moyenne}
													</button>
												</g:elseif>
											</g:if>
										</td>
									</tr>
								</g:each>
							</table>
						</div>

						<br/>
						<div class="row" >
							<div class="col-lg-6" >
								<div class="alert alert-info">
									<g:each var="groupe" in="${com.ef.app.qm.Questionnairegroup.findAllByQuestionnaireLike(com.ef.app.qm.Questionnaire.findByEnable_q(true))}">
										<div class="row" >
											<div class="col-lg-9" >
												<p>${groupe.questionnairegroupName}</p>
											</div>
											<div class="col-lg-3" >
												<p>${groupe.questionnairegroupPonderation}%</p>
											</div>
										</div>
									</g:each>
								</div>
								<div class="row" >
									<div class="col-lg-4" >
										<button type="button" class="btn btn-success disabled" style="width: 100%"><g:message code="evaluation.score.niveau1.libelle" /></button>
									</div>
									<div class="col-lg-4" >
										<button type="button" class="btn btn-warning disabled" style="width: 100%"><g:message code="evaluation.score.niveau2.libelle" /></button>
									</div>
									<div class="col-lg-4" >
										<button type="button" class="btn btn-danger disabled" style="width: 100%"><g:message code="evaluation.score.niveau3.libelle" /></button>
									</div>
								</div>

							</div>
						</div>

					</div>
				</div>
			</div>

		</div>

	</div>
	<!-- /.col-lg-12 -->
</div>

<script src="${resource(dir: 'theme/bower_components/bootstrap/dist/js/', file: 'bootstrap.min.js')}"></script>
<script src="${resource(dir: 'theme/bower_components/metisMenu/dist/', file: 'metisMenu.min.js')}"></script>

<script src="${resource(dir: 'theme/bower_components/datatables/media/js/', file: 'jquery.dataTables.min.js')}"></script>
<script src="${resource(dir: 'theme/bower_components/datatables-plugins/integration/bootstrap/3/', file: 'dataTables.bootstrap.min.js')}"></script>

<script src="${resource(dir: 'theme/bower_components/datatables/extensions/FixedColumns/js/', file: 'dataTables.fixedColumns.js')}"></script>

<script src="${resource(dir: 'theme/dist/js/', file: 'sb-admin-2.js')}"></script>

<script src="${resource(dir: 'js/', file: 'jquery.btechco.excelexport.js')}"></script>
<script src="${resource(dir: 'js/', file: 'jquery.base64.js')}"></script>

<script>
	$(document).ready(function() {
		var table = $('#dataTables-example').DataTable({
			responsive: true,
			"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		//	"ordering": false,
			scrollY:        "500px",
			scrollX:        true,
			scrollCollapse: true,
			"fnDrawCallback": function (oSettings) {
				$('#dataTables-example tr').eq(0).css('background-color','#f9f9f9');
			}
		});

		new $.fn.dataTable.FixedColumns( table, {
			leftColumns: 4
		} );

		table.fnDeleteRow( 0 );

		$(function () {
			$('[data-toggle="tooltip"]').tooltip()
		})

		$("#btnExport").click(function () {
			$("#dataTables-example").battatech_excelexport({
				containerid: "dataTables-example"
			});
		});



	});
</script>

</body>

</html>
