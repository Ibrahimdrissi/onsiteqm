
<%@ page import="com.ef.app.qm.Questions; com.ef.app.qm.Questionnairegroup;com.ef.app.qm.Visit;com.ef.app.qm.Evaluation;com.ef.app.qm.Planning;com.ef.app.qm.Space;com.ef.app.qm.Evalanswer;com.ef.app.qm.User;" %>

<html lang="en">

<head>
	<meta name="layout" content="layoutReporting"/>
<g:set var="entityName" value="${message(code: 'evaluation.label', default: 'Evaluation')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">

		<div id="show-evaluation" class="content scaffold-show" role="main">

			<div class="panel panel-primary" style="border:0 !important;">
				<div class="panel-heading" style="color: #fff !important;background-color: ${Questionnairegroup.get(id_group).color} !important;border:0 !important;" >
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseTotal">
							${Questionnairegroup.get(id_group).questionnairegroupName}
						</a>
						<div class="pull-right">
							<div class="btn-group">
								<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
									Export XLS
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu pull-right" role="menu">

									<g:each var="question" in="${Questions.findAllByQuestionnairegroup(Questionnairegroup.get(id_group))}">
										<li id="${question.id}" class="btnExport" ><a href="#">${question.questionName}</a></li>
									</g:each>
									%{--<li class="divider"></li>
									<li class="btnExportAll" ><a href="#">Export Tous</a></li>--}%
								</ul>
							</div>
						</div>
					</h4>
				</div>

				<g:set var="listVisit" value="${Visit.list()}" ></g:set>
				<g:set var="listQuestion" value="${Questions.findAllByQuestionnairegroup(Questionnairegroup.get(id_group))}" ></g:set>

				<div id="collapseTotal" class="panel-collapse collapse in ">
					<div class="panel-body" style="padding-right: 0 !important;border: 1pt solid ${Questionnairegroup.get(id_group).color} !important;border-top: none !important;">
						<div class="panel-body">
							<g:each var="question" in="${listQuestion}">
								<g:set var="add" value="${totalNoteByCellule = [:] }" ></g:set>
								<h4>${question.questionName}</h4>
								<div class="table-responsive"   style="overflow-x: hidden !important;">
									<table class="table table-striped table-bordered table-hover dataTables" id="dataTables-example_${question.id}" ltr="${question.questionName}" >
										<thead>
										<tr style="background-color: rgb(144, 237, 125);background: none"  class="headTr_${question.id}" >
											<th  style="padding: 3px !important;min-width: 100px !important;" >Cellule</th>
											<g:each var="row"  in="${utilityService.getListVisitOfPlanings()}">
												<th  style="padding: 3px !important;min-width: 70px !important ;max-width: 70px !important ;" >${row}</th>
											</g:each>
										</tr>
										</thead>
										<tbody>
										<g:set var="listScoresCelluleByQuestion" value="${utilityService.getScoresCelluleByQuestion(Long.parseLong(""+question.id))}" ></g:set>

										<g:each var="team" in="${listScoresCelluleByQuestion}" status="i">
											<tr>
												<td>${team.teamName}</td>

												<g:set var="listMoyenneString" value="${utilityService.clobToString(team.moyenne)}" ></g:set>
												<g:set var="listMoyenneArray" value="${listMoyenneString.split(";")}" ></g:set>
												<g:each var="row"  in="${listMoyenneArray}">
													<g:set var="element" value="${row.split(":")}" ></g:set>
													<g:set var="x" value="${order[element[1]] = element[2]+':'+element[3]+':'+element[4]}" ></g:set>
												</g:each>

												<g:each var="row"  in="${order.entrySet().sort{it.key as Integer}}" >
													<g:set var="moyenne_infos" value="${row.value}" ></g:set>

													<g:set var="moyenne_infos" value="${moyenne_infos.split(":")}" ></g:set>
												%{-- note_infos = score:color:typebutton --}%
													<g:if test="${Float.parseFloat(moyenne_infos[0]) != 0}" >
														<td style='background-color:${moyenne_infos[1]};background: none;' >
															<button style="margin-left: 20%;" type="button" class="btn ${moyenne_infos[2]} btn-circle">
																${moyenne_infos[0]}
															</button>
														</td>
													</g:if>
													<g:else>
														<td></td>
													</g:else>

												</g:each>

											</tr>
										</g:each>

										</tbody>
										<tfoot>
										<tr  style="background-color: rgb(144, 237, 125);background: none" >
											<th style="padding: 3px !important;border-bottom: 0;border-left: 0;" >Moyenne</th>
											<g:each var="row" in="${utilityService.getTotalMoyenneScoreByQuestion(Long.parseLong(""+question.id))}" status="i">
												<th style='background-color:${row.color};background: none;' >
													<button style="margin-left: 20%;" type="button" class="btn ${row.btn_type} btn-circle">
														<g:set var="score" value="${((double)Math.round((row.moyenne) * 100) / 100)}" ></g:set>
														${score}
													</button>
												</th>
											</g:each>
										</tr>
										</tfoot>
									</table>
								</div>
								<br/>
								<br/>

							</g:each>
								<br/>
								<br/>
								<div class="row" >
									<div class="col-lg-6" >
										<div class="row" >
											<div class="col-lg-4" >
												<button type="button" class="btn btn-success disabled" style="width: 100%">[4 - 5]</button>
											</div>
											<div class="col-lg-4" >
												<button type="button" class="btn btn-warning disabled" style="width: 100%">[2.5 - 4[</button>
											</div>
											<div class="col-lg-4" >
												<button type="button" class="btn btn-danger disabled" style="width: 100%">[1 - 2.5[</button>
											</div>
										</div>

									</div>
								</div>
						</div>

						<table id="tables-total" style="display: none">

						</table>

					</div>
				</div>
			</div>

		</div>

	</div>
	<!-- /.col-lg-12 -->
</div>

<script src="${resource(dir: 'theme/bower_components/bootstrap/dist/js/', file: 'bootstrap.min.js')}"></script>
<script src="${resource(dir: 'theme/bower_components/metisMenu/dist/', file: 'metisMenu.min.js')}"></script>

<script src="${resource(dir: 'theme/bower_components/datatables/media/js/', file: 'jquery.dataTablesReporting.min.js')}"></script>
<script src="${resource(dir: 'theme/bower_components/datatables-plugins/integration/bootstrap/3/', file: 'dataTables.bootstrap.min.js')}"></script>

<script src="${resource(dir: 'theme/bower_components/datatables/extensions/FixedColumns/js/', file: 'dataTables.fixedColumns.js')}"></script>

<script src="${resource(dir: 'theme/dist/js/', file: 'sb-admin-2.js')}"></script>

<script src="${resource(dir: 'js/', file: 'jquery.btechco.excelexport.js')}"></script>
<script src="${resource(dir: 'js/', file: 'jquery.base64.js')}"></script>


<g:each var="question" in="${listQuestion}">
	<script>
		$(document).ready(function() {
			var table = $('#dataTables-example_${question.id}').DataTable({
				responsive: true,
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Tous"]],
				"order": [[ 0, "asc" ]],
			//	"ordering": false,
				scrollY:        "500px",
				scrollX:        true,
				scrollCollapse: true,
				"fnDrawCallback": function (oSettings) {
					$('#dataTables-example_${question.id} tr').eq(0).css('background-color','#f9f9f9');
				}
			});

			new $.fn.dataTable.FixedColumns( table, {
				leftColumns: 1
			} );

		});
	</script>
</g:each>
<script>
	$(document).ready(function() {

		$(".btnExport").click(function () {
			var id = $(this).attr('id');
			$('#tables-total').html('');
			var nbColumn = $('#dataTables-example_'+id).children('thead').eq(0).children('tr').children('th').length
			$('#tables-total').append('<tr><td colspan="'+nbColumn+'" ></td></tr>');
			$('#tables-total').append('<tr><td colspan="'+nbColumn+'" style="text-align:center" >'+$('#dataTables-example_'+id).attr('ltr')+'</td></tr>');
			$('#tables-total').append('<tr><td colspan="'+nbColumn+'" ></td></tr>');

			var head = '<tr>'+$('.headTr_'+id).html()+'</tr>';
			$('#tables-total').append(head);

			$('#tables-total').append($('#dataTables-example_'+id).html());

			$('#tables-total').battatech_excelexport({
				containerid: 'tables-total'
			});
		});

		$(".btnExportAll").click(function () {
			$('#tables-total').html('');
			var nbColumn = $('.dataTables').children('thead').eq(0).children('tr').children('th').length
			$('#tables-total ').append('<tr><td colspan="'+nbColumn+'" ></td></tr>');
			$( ".dataTables" ).each(function() {
				$('#tables-total tr:last').after('<tr><td colspan="'+nbColumn+'" ></td></tr>');
				$('#tables-total tr:last').after('<tr><td colspan="'+nbColumn+'" >'+$(this).attr("ltr")+'</td></tr>');
				$('#tables-total tr:last').after('<tr><td colspan="'+nbColumn+'" ></td></tr>');
				$('#tables-total').append($(this).html());
			});
			$('#tables-total').battatech_excelexport({
				containerid: 'tables-total'
			});
		});

		$(".search-in-all").click(function () {
			var value = $(this).prev().val();
			$(".search").val(value).trigger('keyup');
		});
	});
</script>

</body>

</html>