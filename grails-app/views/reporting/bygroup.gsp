
<%@ page import="com.ef.app.qm.Questions; com.ef.app.qm.Questionnairegroup;com.ef.app.qm.Visit;com.ef.app.qm.Evaluation;com.ef.app.qm.Planning;com.ef.app.qm.Space;com.ef.app.qm.Evalanswer;com.ef.app.qm.User" %>

<html lang="en">

<head>
	<meta name="layout" content="layoutReporting"/>
<g:set var="entityName" value="" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">

		<div id="show-evaluation" class="content scaffold-show" role="main">

			<g:set var="listVisit" value="${utilityService.getListVisitOfPlanings()}" ></g:set>
			<g:set var="listQuestion" value="${Questions.findAllByQuestionnairegroup(Questionnairegroup.get(id_group))}" ></g:set>

			<div class="panel panel-primary" style="border:0 !important;">
				<div class="panel-heading" style="color: #fff !important;background-color: ${Questionnairegroup.get(id_group).color} !important;border:0 !important;" >
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseTotal">
							${Questionnairegroup.get(id_group).questionnairegroupName}
						</a>
						<div class="pull-right">
							<div class="btn-group">
								<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
									Export XLS
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu pull-right" role="menu">
									<g:each var="question" in="${listQuestion}">
										<li id="${question.id}" class="btnExport" ><a href="#">${question.questionName}</a></li>
									</g:each>
									%{--<li class="divider"></li>
									<li class="btnExportAll" ><a href="#">Export Tous</a></li>--}%
								</ul>
							</div>
						</div>
					</h4>
				</div>



				<div id="collapseTotal" class="panel-collapse collapse in ">
				<div class="panel-body" style="padding-right: 0 !important;border: 1pt solid ${Questionnairegroup.get(id_group).color} !important;border-top: none !important;">
						<div class="panel-body">
							<g:each var="question" in="${listQuestion}">
								<h4>${question.questionName}</h4>
								<div class="table-responsive"  style="overflow-x: hidden !important;">

									<g:set var="listScoresByQuestionId" value="${utilityService.getScoresByQuestionId(Long.parseLong(""+question.id))}" ></g:set>
									<table class="table table-striped table-bordered table-hover dataTables" id="dataTables-example_${question.id}" ltr="${question.questionName}" >
										<thead>
										<tr style="background-color: rgb(144, 237, 125);background: none" class="headTr_${question.id}" >
											<th style="padding: 3px !important;min-width: 120px !important ;max-width: 120px !important ;" >Espace</th>
											%{--<th style="padding: 3px !important;min-width: 60px !important ;max-width: 60px !important ;"><g:message code="reporting.zone.label" /></th>--}%
											<th style="padding: 3px !important;min-width: 120px !important ;max-width: 120px !important ;"><g:message code="reporting.region.label" /></th>
											<th style="padding: 3px !important;min-width: 120px !important ;max-width: 120px !important ;"><g:message code="reporting.Cellule.label" /></th>
											<th style="padding: 3px !important;min-width: 120px !important ;max-width: 120px !important ;"><g:message code="reporting.animateur.label" /></th>
											<g:each var="row"  in="${listVisit}">
												<th  style="padding: 3px !important;min-width: 60px !important ;max-width: 60px !important ;" >${row}</th>
											</g:each>

										</tr>

										</thead>
										<tbody>

										<g:each var="espace" in="${listScoresByQuestionId}" status="i">
												<tr>
													<td>${espace.space_name}</td>
													<td>${espace.region}</td>
													<td>${espace.team_name}</td>
													<td >${espace.nameComplete}</td>

													<g:set var="listVisitesString" value="${utilityService.clobToString(espace.scores)}" ></g:set>
													<g:set var="listVisitArray" value="${listVisitesString.split(",")}" ></g:set>

													<g:each var="row"  in="${listVisit}" status="k" >
														<g:set var="element" value="${listVisitArray[k].split(":")}" ></g:set>
														<td style="padding: 3px !important;min-width: 60px !important ;
														max-width: 60px !important ;background-color:${element[3]};background: none;" >

														<g:if test="${Integer.parseInt(element[2]) != 0}" >
																<button title="${element[5]}" data-toggle="tooltip" data-placement="top" style="margin-left: 20%;" type="button" class="btn ${element[4]} btn-circle">
																	${element[2]}
																</button>
														</g:if>
														</td>
													</g:each>
												</tr>
										</g:each>

										</tbody>

									</table>
								</div>
								<br/>
								<br/>
							</g:each>

							<br/>
							<br/>
							<div class="row" >
								<div class="col-lg-6" >
									<div class="row" >
										<div class="col-lg-4" >
											<button type="button" class="btn btn-success disabled" style="width: 100%">[4 - 5]</button>
										</div>
										<div class="col-lg-4" >
											<button type="button" class="btn btn-warning disabled" style="width: 100%">[2.5 - 4[</button>
										</div>
										<div class="col-lg-4" >
											<button type="button" class="btn btn-danger disabled" style="width: 100%">[1 - 2.5[</button>
										</div>
									</div>

								</div>
							</div>

						</div>

						<table id="tables-total" style="display: none">

						</table>

					</div>
				</div>
			</div>

		</div>

	</div>
	<!-- /.col-lg-12 -->
</div>

<script src="${resource(dir: 'theme/bower_components/bootstrap/dist/js/', file: 'bootstrap.min.js')}"></script>
<script src="${resource(dir: 'theme/bower_components/metisMenu/dist/', file: 'metisMenu.min.js')}"></script>

<script src="${resource(dir: 'theme/bower_components/datatables/media/js/', file: 'jquery.dataTablesReporting.min.js')}"></script>
<script src="${resource(dir: 'theme/bower_components/datatables-plugins/integration/bootstrap/3/', file: 'dataTables.bootstrap.min.js')}"></script>

<script src="${resource(dir: 'theme/bower_components/datatables/extensions/FixedColumns/js/', file: 'dataTables.fixedColumns.js')}"></script>

<script src="${resource(dir: 'theme/dist/js/', file: 'sb-admin-2.js')}"></script>

<script src="${resource(dir: 'js/', file: 'jquery.btechco.excelexport.js')}"></script>
<script src="${resource(dir: 'js/', file: 'jquery.base64.js')}"></script>


<g:each var="question" in="${listQuestion}" status="i">
	<script>
		$(document).ready(function() {

			var table = $('#dataTables-example_${question.id}').DataTable({
				responsive: true,
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Tous"]],
			//	"ordering": false,
				scrollY:        "500px",
				scrollX:        true,
				scrollCollapse: true,
				"fnDrawCallback": function (oSettings) {
					//	$('.DTFC_LeftHeadWrapper').css('display','none');
					//	$('.DTFC_LeftBodyWrapper').css('display','none');

					$('#dataTables-example_${question.id} tr').eq(0).css('background-color','#f9f9f9');
				}
			});

			new $.fn.dataTable.FixedColumns( table, {
				leftColumns: 4
			} );

			$(function () {
				$('[data-toggle="tooltip"]').tooltip()
			})

		});
	</script>
</g:each>
<script>
	$(document).ready(function() {
		$(".btnExport").click(function () {
			var id = $(this).attr('id');
			$('#tables-total').html('');
			var nbColumn = $('#dataTables-example_'+id).children('thead').eq(0).children('tr').children('th').length
			$('#tables-total').append('<tr><td colspan="'+nbColumn+'" ></td></tr>');
			$('#tables-total').append('<tr><td colspan="'+nbColumn+'" style="text-align:center" >'+$('#dataTables-example_'+id).attr('ltr')+'</td></tr>');
			$('#tables-total').append('<tr><td colspan="'+nbColumn+'" ></td></tr>');

			var head = '<tr>'+$('.headTr_'+id).html()+'</tr>';
			$('#tables-total').append(head);

			/*var footer = '<tr><td colspan="'+nbColumn+'" ></td></tr>';
			footer = footer + '<tr><td colspan="5" >'+$('#dataTables-example_'+id+' tfoot').attr('class')+'</td>';

			$('#dataTables-example_'+id+' .footerNotes').each(function() {
				footer = footer + '<td >'+$(this).attr("title")+'</td>';
			});

			footer = footer + '</tr>';*/

			$('#tables-total').append($('#dataTables-example_'+id).html());

			$('#tables-total').battatech_excelexport({
				containerid: 'tables-total'
			});

		});

		$(".btnExportAll").click(function () {
			$('#tables-total').html('');
			var nbColumn = $('.dataTables').children('thead').eq(0).children('tr').children('th').length
			$('#tables-total ').append('<tr><td colspan="'+nbColumn+'" ></td></tr>');
			$( ".dataTables" ).each(function() {

				$('#tables-total tr:last').after('<tr><td colspan="'+nbColumn+'" ></td></tr>');
				$('#tables-total tr:last').after('<tr><td colspan="'+nbColumn+'" >'+$(this).attr("ltr")+'</td></tr>');
				$('#tables-total tr:last').after('<tr><td colspan="'+nbColumn+'" ></td></tr>');

				$('#tables-total').append($(this).html());
			});
			$('#tables-total').battatech_excelexport({
				containerid: 'tables-total'
			});
		});

		$(".search-in-all").click(function () {
			var value = $(this).prev().val();
			$(".search").val(value).trigger('keyup');
		});
	});
</script>

</body>

</html>