<%@ page import="com.ef.app.qm.Space" %>

<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'space.label', default: 'Space')}" />
	<title><g:message code="default.list.label" args="${[message(code: 'com.ef.domain.name.space')]}" /></title>
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<g:message code="default.list.label" args="${[message(code: 'com.ef.domain.name.space')]}" />
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="col-lg-3" role="main">
					<g:link class="btn btn-primary btn-block" action="create"><g:message code="default.new.label" args="${[message(code: 'com.ef.domain.name.space')]}" /></g:link>
					<br/>
				</div>
				<div class="dataTable_wrapper">
					<table class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
						<tr>
							<th>Code de l'espace</th>
							<th>${message(code: 'space.spaceName.label', default: 'Space Name')} </th>
							<th>${message(code: 'space.spaceAdress.label', default: 'Space Adress')}</th>
							<!--th>Nombre de collaborateurs</th-->
							<th>${message(code: 'space.spacePhone.label', default: 'Space Phone')}</th>
							<th>${message(code: 'space.spaceEmail.label', default: 'Space Email')}</th>
							<th><g:message code="space.user.label" default="User" /></th>
							<th><g:message code="space.zone.label" default="zone" /></th>
							<th><g:message code="space.region.label" default="region" /></th>
						</tr>
						</thead>
						<tbody>
						<g:each in="${com.ef.app.qm.Space.list()}" status="i" var="spaceInstance">
							<tr class="${(i % 2) == 0 ? 'even gradeC' : 'odd gradeX'}">
								<td><g:link action="show" id="${spaceInstance.id}">${fieldValue(bean: spaceInstance, field: "spaceCode")}</g:link></td>
								<td><g:link action="show" id="${spaceInstance.id}">${fieldValue(bean: spaceInstance, field: "spaceName")}</g:link></td>
								<td>${fieldValue(bean: spaceInstance, field: "spaceAdress")}</td>
								<td>${fieldValue(bean: spaceInstance, field: "spacePhone")}</td>
								<td>${fieldValue(bean: spaceInstance, field: "spaceEmail")}</td>
								<td>${fieldValue(bean: spaceInstance, field: "user.userName")}</td>
								<td>${fieldValue(bean: spaceInstance, field: "zone")}</td>
								<td>${fieldValue(bean: spaceInstance, field: "region")}</td>
							</tr>
						</g:each>

						</tbody>
					</table>
				</div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>

<script>
	$(document).ready(function() {
		$('#dataTables-example').DataTable({
			responsive: true,
			"order": [[ 2, "asc" ]]
		});
	});
</script>

</body>

</html>
