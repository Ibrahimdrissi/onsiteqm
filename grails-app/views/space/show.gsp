
<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'space.label', default: 'Space')}" />
	<title><g:message code="default.show.label" args="${[message(code: 'com.ef.domain.name.space')]}" /></title>
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<g:message code="default.show.label" args="${[message(code: 'com.ef.domain.name.space')]}" />
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row" style="padding-bottom: 20px">
					<div class="col-lg-2" role="navigation">
						<g:link class="btn btn-primary btn-block" action="index"><g:message code="default.list.label" args="${[message(code: 'com.ef.domain.name.space')]}" /></g:link>
					</div>
					<div class="col-lg-2" role="navigation">
						<g:link class="btn btn-primary btn-block" action="create"><g:message code="default.new.label" args="${[message(code: 'com.ef.domain.name.space')]}" /></g:link></div>
				</div>
				<div id="show-space" class="content scaffold-show" role="main">



					<g:if test="${spaceInstance?.spaceName}">
						<div class="row form-group">
							<div class="col-lg-2" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="space.spaceName.label" default="Space Name" />
							</div>
							<div class="col-lg-3">
								<pre><g:fieldValue bean="${spaceInstance}" field="spaceName"/></pre>
							</div>
						</div>
					</g:if>


					<g:if test="${spaceInstance?.spaceAdress}">
						<div class="row form-group">
							<div class="col-lg-2" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="space.spaceAdress.label" default="spaceAdress" />
							</div>
							<div class="col-lg-3">
								<pre><g:fieldValue bean="${spaceInstance}" field="spaceAdress"/></pre>
							</div>
						</div>
					</g:if>

					<g:if test="${spaceInstance?.spacePhone}">
						<div class="row form-group">
							<div class="col-lg-2" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="space.spacePhone.label" default="spacePhone" />
							</div>
							<div class="col-lg-3">
								<pre><g:fieldValue bean="${spaceInstance}" field="spacePhone"/></pre>
							</div>
						</div>
					</g:if>

					<g:if test="${spaceInstance?.spaceEmail}">
						<div class="row form-group">
							<div class="col-lg-2" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="space.spaceEmail.label" default="spaceEmail" />
							</div>
							<div class="col-lg-3">
								<pre><g:fieldValue bean="${spaceInstance}" field="spaceEmail"/></pre>
							</div>
						</div>
					</g:if>

					<g:if test="${spaceInstance?.user}">
						<div class="row form-group">
							<div class="col-lg-2" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="space.user.label" default="User" />
							</div>
							<div class="col-lg-3">
								<pre><g:link controller="user" action="show" id="${spaceInstance?.user?.id}">${spaceInstance?.user?.userName.encodeAsHTML()}</g:link></pre>
							</div>
						</div>
					</g:if>

					<g:if test="${spaceInstance?.zone}">
						<div class="row form-group">
							<div class="col-lg-2" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="space.zone.label" default="spaceEmail" />
							</div>
							<div class="col-lg-3">
								<pre><g:fieldValue bean="${spaceInstance}" field="zone"/></pre>
							</div>
						</div>
					</g:if>

					<g:if test="${spaceInstance?.region}">
						<div class="row form-group">
							<div class="col-lg-2" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="space.region.label" default="spaceEmail" />
							</div>
							<div class="col-lg-3">
								<pre><g:fieldValue bean="${spaceInstance}" field="region"/></pre>
							</div>
						</div>
					</g:if>


				</div>

				<div class="row" style="padding-top: 20px">
					<g:form url="[resource:spaceInstance, action:'delete']" method="DELETE">
						<div class="col-lg-2" role="navigation">
							<g:link class="btn btn-success btn-block" action="edit" resource="${spaceInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
						</div>
					</g:form>
				</div>

			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>


</body>

</html>

