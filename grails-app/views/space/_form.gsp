<%@ page import="com.ef.app.qm.Profile; com.ef.app.qm.Space; com.ef.app.qm.User" %>


<div class="row form-group has-${hasErrors(bean: spaceInstance, field: 'spaceCode', 'error')}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="space.spaceCode.label" default="Code de l'espace" />
		<span class="required-indicator" style="color:red" >*</span>
	</div>
	<div class="col-lg-3">
		<g:if test="${spaceInstance?.spaceCode==null}">
			<% spaceInstance?.spaceCode='ETT'+(com.ef.app.qm.Space.last().id+1) %>
		</g:if>
		<g:textField  class="form-control" name="spaceCode" value="${spaceInstance?.spaceCode}" readonly="readonly"/>
	</div>
</div>

<div class="row form-group has-${hasErrors(bean: spaceInstance, field: 'spaceName', 'error')}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="space.spaceName.label" default="Space Name" />
		<span class="required-indicator" style="color:red" >*</span>
	</div>
	<div class="col-lg-3">
		<g:textField  class="form-control" name="spaceName" value="${spaceInstance?.spaceName}"/>
	</div>
</div>

<div class="row form-group has-${hasErrors(bean: spaceInstance, field: 'spaceNumber', 'error')}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="space.spaceNumber.label" default="Nombre de collaborateurs" />
	</div>
	<div class="col-lg-3">
		<g:field type="number" min="0" max="200"  class="form-control" name="spaceNumber" value="${spaceInstance?.spaceNumber}"/>
	</div>
</div>

<div class="row form-group has-${hasErrors(bean: spaceInstance, field: 'spaceAdress', 'error')}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="space.spaceAdress.label" default="Space Adress" />
	</div>
	<div class="col-lg-3">
		<g:textField  class="form-control" name="spaceAdress" value="${spaceInstance?.spaceAdress}"/>
	</div>
</div>

<div class="row form-group has-${hasErrors(bean: spaceInstance, field: 'spacePhone', 'error')}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="space.spacePhone.label" default="Space Phone" />
	</div>
	<div class="col-lg-3">
		<g:textField  class="form-control" name="spacePhone" value="${spaceInstance?.spacePhone}"/>
	</div>
</div>

<div class="row form-group has-${hasErrors(bean: spaceInstance, field: 'spaceEmail', 'error')}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="space.spaceEmail.label" default="Space Email" />
	</div>
	<div class="col-lg-3">
		<g:textField  class="form-control" name="spaceEmail" value="${spaceInstance?.spaceEmail}"/>
	</div>
</div>

<div class="row form-group has-${hasErrors(bean: spaceInstance, field: 'zone', 'error')}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="space.zone.label" default="Zone" />
		<span class="required-indicator" style="color:red" >*</span>
	</div>
	<div class="col-lg-3">
		<select class="form-control" name="zone"  value="${spaceInstance?.zone}"  >
			<option ${(spaceInstance.zone == 'Zone 1')?'selected':''} value="Zone 1">Zone 1</option>
			<option ${(spaceInstance.zone == 'Zone 2')?'selected':''}  value="Zone 2">Zone 2</option>
			<option ${(spaceInstance.zone == 'Zone 3')?'selected':''} value="Zone 3">Zone 3</option>
			<option ${(spaceInstance.zone == 'Zone 4')?'selected':''} value="Zone 4">Zone 4</option>
			<option ${(spaceInstance.zone == 'Zone 5')?'selected':''} value="Zone 5">Zone 5</option>
		</select>
	</div>
</div>

<div class="row form-group has-${hasErrors(bean: spaceInstance, field: 'region', 'error')}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="space.region.label" default="region" />
		<span class="required-indicator" style="color:red" >*</span>
	</div>
	<div class="col-lg-3">
		<select class="form-control" name="region"  value="${spaceInstance?.region}"  >
			<option ${(spaceInstance.region == 'Ariana')?'selected':''}  value="Ariana">Ariana</option>
			<option ${(spaceInstance.region == 'Beja')?'selected':''} value="Beja">Beja</option>
			<option ${(spaceInstance.region == 'Ben Arous')?'selected':''} value="Ben Arous">Ben Arous</option>
			<option ${(spaceInstance.region == 'Bizerte')?'selected':''} value="Bizerte">Bizerte</option>

			<option ${(spaceInstance.region == 'Gabes')?'selected':''} value="Gabes">Gabes</option>
			<option ${(spaceInstance.region == 'Gafsa')?'selected':''} value="Gafsa">Gafsa</option>
			<option ${(spaceInstance.region == 'Jendouba')?'selected':''} value="Jendouba">Jendouba</option>
			<option ${(spaceInstance.region == 'Kairouan')?'selected':''} value="Kairouan">Kairouan</option>
			<option ${(spaceInstance.region == 'Kasserine')?'selected':''} value="Kasserine">Kasserine</option>

			<option ${(spaceInstance.region == 'Kebili')?'selected':''} value="Kebili">Kebili</option>
			<option ${(spaceInstance.region == 'Kef')?'selected':''} value="Kef">Kef</option>
			<option ${(spaceInstance.region == 'Mahdia')?'selected':''} value="Mahdia">Mahdia</option>
			<option ${(spaceInstance.region == 'Medenine')?'selected':''} value="Medenine">Medenine</option>

			<option ${(spaceInstance.region == 'Monastir')?'selected':''} value="Monastir">Monastir</option>
			<option ${(spaceInstance.region == 'Nabeul')?'selected':''} value="Nabeul">Nabeul</option>
			<option ${(spaceInstance.region == 'Seliana')?'selected':''} value="Seliana">Seliana</option>
			<option ${(spaceInstance.region == 'Sfax')?'selected':''} value="Sfax">Sfax</option>
			<option ${(spaceInstance.region == 'Sidi Bouzid')?'selected':''} value="Sidi Bouzid">Sidi Bouzid</option>
			<option ${(spaceInstance.region == 'Sousse')?'selected':''} value="Sousse">Sousse</option>
			<option ${(spaceInstance.region == 'Tataouine')?'selected':''} value="Tataouine">Tataouine</option>
			<option ${(spaceInstance.region == 'Tozeur')?'selected':''} value="Tozeur">Tozeur</option>
			<option ${(spaceInstance.region == 'Tunis')?'selected':''} value="Tunis">Tunis</option>
			<option ${(spaceInstance.region == 'Zaghouen')?'selected':''} value="Zaghouen">Zaghouen</option>
		</select>
	</div>
</div>

<div class="row form-group has-${hasErrors(bean: spaceInstance, field: 'user', 'error')}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="space.user.label" default="User" />

	</div>
	<div class="col-lg-3">
		<g:select class="form-control" value="${spaceInstance?.user?.id}" id="user" name="user.id" from="${com.ef.app.qm.User.findAllByProfileOrProfile(com.ef.app.qm.Profile.findByProfilNameLike("%Animateur%"),com.ef.app.qm.Profile.findByProfilNameLike("%Chef%"))}" noSelection="${['null':'']}" optionKey="id" optionValue="userName" />
	</div>
</div>
