
<div class="row form-group has-${hasErrors(bean: visitInstance, field: 'visitName', 'error')}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="user.visitName.label" default="Visit Name" />
		<span class="required-indicator" style="color:red" >*</span>
	</div>
	<div class="col-lg-3">
		<g:textField  class="form-control" name="visitName" value="${visitInstance?.visitName}"/>
	</div>
</div>