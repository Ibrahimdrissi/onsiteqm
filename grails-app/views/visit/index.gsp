<%@ page import="com.ef.app.qm.Visit" %>

<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'visit.label', default: 'Visit')}" />
	<title><g:message code="com.expertflow.ma.onsiteqm.applicationName"/></title>
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<g:message code="default.list.label" args="[entityName]" />
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="col-lg-3" role="main">
					<g:link class="btn btn-primary btn-block" action="create"><g:message code="default.new2.label" args="${[message(code: 'user.visitName.label')]}" /></g:link>
					<br/>
				</div>
				<div class="dataTable_wrapper">
					<table class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
						<tr>
							<th>${message(code: 'user.visitName.label', default: 'Visit Name')}</th>


						</tr>
						</thead>
						<tbody>
						<g:each in="${com.ef.app.qm.Visit.list()}" status="i" var="visitInstance">
							<tr class="${(i % 2) == 0 ? 'even gradeC' : 'odd gradeX'}">

								<td><g:link action="show" id="${visitInstance.id}">${fieldValue(bean: visitInstance, field: "visitName")}</g:link></td>

							</tr>
						</g:each>

						</tbody>
					</table>
				</div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<script>
	$(document).ready(function() {
		$('#dataTables-example').DataTable({
			responsive: true
		});
	});
</script>

</body>

</html>
