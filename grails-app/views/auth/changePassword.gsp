
<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'profile.label', default: 'Profile')}" />
	<title><g:message code="default.create.label" args="${[message(code: 'com.ef.domain.name.profile')]}" /></title>
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				Changer mot de passe
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">

			<div id="create-passwordChange" class="content scaffold-create" role="main">

					<g:form controller="auth" action="updatePassword" method="POST" >
						<fieldset class="form">
							<div class="row form-group has-${errorPassword} ">
								<div class="col-lg-3" style="font-weight: bold" >
									Le nouveau mot de passe
									<span class="required-indicator" style="color:red" >*</span>
								</div>
								<div class="col-lg-3">
									<g:passwordField  class="form-control" name="password" />
								</div>
							</div>
							<div class="row form-group has-${errorPassword} ">
								<div class="col-lg-3" style="font-weight: bold" >
									Confirmation du mot de passe
									<span class="required-indicator" style="color:red" >*</span>
								</div>
								<div class="col-lg-3">
									<g:passwordField  class="form-control" name="confirmationPassword" />
								</div>
							</div>
						</fieldset>
						<fieldset class="buttons">
							<br/>
							<g:submitButton name="create" class="btn btn-success" value="modifier"  />
						</fieldset>
					</g:form>
				</div>

			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>


</body>

</html>

