<%--
  Created by IntelliJ IDEA.
  com.ef.app.qm.User: akeel
  Date: 4/10/13
  Time: 6:34 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title>Permission Denied</title>
</head>
<body>
<g:if test="${flash.message}">
    <bootstrap:alert class="alert alert-${flash.type}">${flash.message}</bootstrap:alert>
</g:if>
	<h1><g:message code="com.ef.app.cbr.unauthorized"/></h1>
</body>
</html>