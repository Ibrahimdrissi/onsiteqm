<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'questions.label', default: 'Questions')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				${message(code: 'question.label.list' )}
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="col-lg-3" role="main">
					<g:link class="btn btn-primary btn-block" action="create"><g:message code="questions.new" /></g:link>
					<br/>
				</div>
				<div class="dataTable_wrapper">
					<table class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
						<tr>
							<th>${message(code: 'questions.questionName.label', default: 'Question Name')}</th>
							<th><g:message code="questions.questionnairegroup.label" default="Questionnairegroup" /></th>
							<th><g:message code="questionnaire.label" default="Questionnaire" /></th>
						</tr>
						</thead>
						<tbody>

						<g:each in="${com.ef.app.qm.Questions.list()}" status="i" var="questionsInstance">
							<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

								<td><g:link action="show" id="${questionsInstance.id}">${fieldValue(bean: questionsInstance, field: "questionName")}</g:link></td>

								<td><g:link controller="questionnairegroup" action="show" id="${questionsInstance?.questionnairegroup?.id}">${questionsInstance?.questionnairegroup?.questionnairegroupName}</g:link></td>

								<td><g:link controller="questionnaire"  action="show" id="${questionsInstance?.questionnairegroup?.questionnaire?.id}">${questionsInstance?.questionnairegroup?.questionnaire?.questionnaireName}</g:link></td>

							</tr>
						</g:each>

						</tbody>
					</table>
				</div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>

<script>
	$(document).ready(function() {
		$('#dataTables-example').DataTable({
			responsive: true,
			"order": [[ 2, "asc" ]]
		});


	});
</script>

</body>

</html>