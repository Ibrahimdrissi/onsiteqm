<%@ page import="com.ef.app.qm.Questionnairegroup" %>

<div class="row form-group has-${hasErrors(bean: questionsInstance, field: 'questionName', 'error')} ">
	<div class="col-lg-3" style="font-weight: bold" >
		<g:message code="questions.questionName.label" default="Question Name" />
		<span class="required-indicator" style="color:red" >*</span>
	</div>
	<div class="col-lg-5">
		<g:textField  class="form-control" name="questionName" value="${questionsInstance?.questionName}"/>
		<g:hiddenField  class="form-control" name="questionNameExist" value="${questionsInstance?.questionName}"/>

	</div>
</div>


<div class="row form-group has-${hasErrors(bean: questionsInstance, field: 'questionPoints', 'error')} ">
	<div class="col-lg-3" style="font-weight: bold" >
		<g:message code="questions.questionPoints.label" default="Question Points" />
		<span class="required-indicator" style="color:red" >*</span>
	</div>
	<div class="col-lg-5">
		<g:textArea  rows="5" class="form-control" name="questionPoints" value="${questionsInstance?.questionPoints}"/>
	</div>
</div>

<div class="row form-group has-${hasErrors(bean: questionsInstance, field: 'questionMaturity', 'error')} ">
	<div class="col-lg-3" style="font-weight: bold" >
		<g:message code="questions.questionMaturity.label" default="Question Maturity" />
		<span class="required-indicator" style="color:red" >*</span>
	</div>
	<div class="col-lg-5">
		<g:textArea rows="5" class="form-control" name="questionMaturity" value="${questionsInstance?.questionMaturity}"/>
	</div>
</div>

<div class="row form-group has-${hasErrors(bean: questionsInstance, field: 'questionnairegroup', 'error')}">
	<div class="col-lg-3" style="font-weight: bold" >
		<g:message code="questions.questionnairegroup.label" default="Questionnairegroup" />
		<span class="required-indicator" style="color:red" >*</span>
	</div>
	<div class="col-lg-5">
		<g:hiddenField  class="form-control" name="questionnairegroupIdExist" value="${questionsInstance?.questionnairegroup?.id}"/>
		<g:select  class="form-control" value="${questionsInstance?.questionnairegroup?.id}" id="questionnairegroup"
				  name="questionnairegroup.id" from="${com.ef.app.qm.Questionnairegroup.list()}" optionKey="id"
				  optionValue="groupAndQuestionnaire" />
	</div>
</div>

<div class="row form-group has-${hasErrors(bean: questionsInstance, field: 'na', 'error')}">
	<div class="col-lg-3" style="font-weight: bold" >
		NA
	</div>
	<div class="col-lg-5">
		<g:hiddenField  class="form-control" name="naExist" value="${questionsInstance?.na}"/>
		<g:checkBox style="margin: 0;width: 35px;" class="form-control" name="na" value="${questionsInstance?.na}"/>
	</div>
</div>


<g:hiddenField value="${naExist}"  name="edit_naExist" />

<g:hiddenField value="${questionnairegroupIdExist}"  name="edit_questionnairegroupIdExist" />

<g:hiddenField value="${questionNameExist}"  name="edit_questionNameExist" />
