<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'questions.label', default: 'Questions')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<g:message code="question.label" />
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row" style="padding-bottom: 20px">
					<div class="col-lg-3" role="navigation">
						<g:link class="btn btn-primary btn-block" action="index"><g:message code="questions.questionName.list" /></g:link>
					</div>
					<div class="col-lg-3" role="navigation">
						<g:link class="btn btn-primary btn-block" action="create"><g:message code="questions.new" /></g:link></div>
				</div>
				<div id="show-questions" class="content scaffold-show" role="main">

					<g:if test="${questionsInstance?.questionName}">
						<div class="row form-group">
							<div class="col-lg-3" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="question.label" />
							</div>
							<div class="col-lg-6">
								<pre><g:fieldValue bean="${questionsInstance}" field="questionName"/></pre>
							</div>
						</div>
					</g:if>

					<g:if test="${questionsInstance?.questionPoints}">
						<div class="row form-group">
							<div class="col-lg-3" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="questions.questionPoints.label" default="Question Points" />
							</div>
							<div class="col-lg-6">
								<pre>${questionsInstance?.questionPoints}</pre>
							</div>
						</div>
					</g:if>

					<g:if test="${questionsInstance?.questionMaturity}">
						<div class="row form-group">
							<div class="col-lg-3" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="questions.questionMaturity.label" default="Question Maturity" />
							</div>
							<div class="col-lg-6">
								<pre><g:fieldValue bean="${questionsInstance}" field="questionMaturity"/></pre>
							</div>
						</div>
					</g:if>

					<g:if test="${questionsInstance?.questionnairegroup}">
						<div class="row form-group">
							<div class="col-lg-3" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="questions.questionnairegroup.label" default="Questionnairegroup" />
							</div>
							<div class="col-lg-6">
								<pre>${questionsInstance?.questionnairegroup?.questionnairegroupName}</pre>
							</div>
						</div>
					</g:if>

					<g:if test="${questionsInstance?.questionnairegroup?.questionnaire}">
						<div class="row form-group">
							<div class="col-lg-3" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="questionnaire.label" default="questionnaire" />
							</div>
							<div class="col-lg-6">
								<pre>${questionsInstance?.questionnairegroup?.questionnaire?.questionnaireName}</pre>
							</div>
						</div>
					</g:if>

						<div class="row form-group">
							<div class="col-lg-3" style="font-weight: bold;padding-top: 8px;" >
								NA
							</div>
							<div class="col-lg-6">
								<pre><g:checkBox disabled="disabled" style="margin: 0;width: 35px;" class="form-control" name="na" value="${questionsInstance?.na}"/></pre>
							</div>
						</div>


				</div>

				<div class="row" style="padding-top: 20px">
					<g:form url="[resource:questionsInstance, action:'delete']" method="DELETE">
						<div class="col-lg-2" role="navigation">
							<g:link class="btn btn-success btn-block" action="edit" resource="${questionsInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
						</div>
%{--						<div class="col-lg-2" role="navigation">
							<g:actionSubmit class="btn btn-danger btn-block" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
						</div>--}%
					</g:form>
				</div>

			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>

</body>

</html>
