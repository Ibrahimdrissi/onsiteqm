<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'questionnairegroup.label', default: 'Questionnairegroup')}" />
	<title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<g:message code="questionnairegroup.label" />
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row" style="padding-bottom: 20px">
					<div class="col-lg-3" role="navigation">
						<g:link class="btn btn-primary btn-block" action="index"><g:message code="questionnairegroup.label.list"  /></g:link>
					</div>
					<div class="col-lg-3" role="navigation">
						<g:link class="btn btn-primary btn-block" action="create"><g:message code="questionnairegroup.label.new" /></g:link></div>
				</div>
				<div id="edit-questionnairegroup" class="content scaffold-edit" role="main">
					<g:form url="[resource:questionnairegroupInstance, action:'update']" method="PUT" >
						<g:hiddenField name="version" value="${questionnairegroupInstance?.version}" />
						<fieldset class="form">
							<div class="row form-group has-${hasErrors(bean: questionnairegroupInstance, field: 'questionnairegroupName', 'error')}">
								<div class="col-lg-3" style="font-weight: bold" >
									<g:message code="questionnairegroup.name.label" default="Questionnaire Name" />
									<span class="required-indicator" style="color:red" >*</span>
								</div>
								<div class="col-lg-3">
									<g:textField  class="form-control" name="questionnairegroupName" value="${questionnairegroupInstance?.questionnairegroupName}"/>
								</div>
							</div>


							<div class="row form-group has-${hasErrors(bean: questionnairegroupInstance, field: 'questionnairegroupPonderation', 'error')}">
								<div class="col-lg-3" style="font-weight: bold" >
									<g:message code="questionnairegroup.ponderation.label" default="Questionnairegroup Ponderation" />
									<span class="required-indicator" style="color:red" >*</span>
								</div>
								<div class="col-lg-3">
									<g:textField  class="form-control" name="questionnairegroupPonderation" value="${questionnairegroupInstance?.questionnairegroupPonderation}"/>
								</div>
							</div>

							<div class="row form-group ">
								<div class="col-lg-3" style="font-weight: bold" >

									<g:message code="questionnairegroup.color.label" default="Couleur" />
									<span class="required-indicator" style="color:red" >*</span>
								</div>
								<div class="col-lg-3">
									<g:textField  class="jscolor form-control" name="color" value="${questionnairegroupInstance?.color}"/>
									<g:hiddenField name="questionnaire" value="${questionnairegroupInstance?.questionnaire?.id}"/>
									<g:hiddenField  class="form-control" name="questionnairegroupNameExist" value="${questionnairegroupInstance?.questionnairegroupName}"/>
									<g:hiddenField  class="form-control" name="questionnairegroupPonderationExist" value="${questionnairegroupInstance?.questionnairegroupPonderation}"/>
								</div>
							</div>
						</fieldset>
						<fieldset class="buttons">
							<br/>
							<g:actionSubmit class="btn btn-success" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />

						</fieldset>
					</g:form>


				</div>

			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>

</body>

</html>