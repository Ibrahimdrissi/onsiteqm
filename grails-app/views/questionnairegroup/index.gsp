<%@ page import="com.ef.app.qm.User_holidays" %>

<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'questionnairegroup.label', default: 'Questionnairegroup')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				${message(code: 'questionnairegroup.label.list' )}
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="col-lg-3" role="main">
					<g:link class="btn btn-primary btn-block" action="create">${message(code: 'questionnairegroup.label.new')}</g:link>
					<br/>
				</div>
				<div class="dataTable_wrapper">
					<table class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
						<tr>
							<th>${message(code: 'questionnairegroup.name.label')}</th>
							<th>${message(code: 'questionnairegroup.ponderation.label')}</th>
							<th>${message(code: 'questionnairegroup.questionnaire.label')}</th>
						</tr>
						</thead>
						<tbody>

						<g:each in="${com.ef.app.qm.Questionnairegroup.list()}" status="i" var="questionnairegroupInstance">
							<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

								<td><g:link action="show" id="${questionnairegroupInstance.id}">
									${fieldValue(bean: questionnairegroupInstance, field: "questionnairegroupName")}</g:link></td>

								<td>${fieldValue(bean: questionnairegroupInstance, field: "questionnairegroupPonderation")}</td>

								<td>${questionnairegroupInstance?.questionnaire?.questionnaireName}</td>

							</tr>
						</g:each>

						</tbody>
					</table>
				</div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>

<script>
	$(document).ready(function() {
		$('#dataTables-example').DataTable({
			responsive: true,
			"order": [[ 2, "asc" ]]
		});


	});
</script>

</body>

</html>