<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'questionnairegroup.label', default: 'Questionnairegroup')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<g:message code="questionnairegroup.label.show" />
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row" style="padding-bottom: 20px">
					<div class="col-lg-3" role="navigation">
						<g:link class="btn btn-primary btn-block" action="index"><g:message code="questionnairegroup.label.list" /></g:link>
					</div>
					<div class="col-lg-3" role="navigation">
						<g:link class="btn btn-primary btn-block" action="create"><g:message code="questionnairegroup.label.new"  /></g:link></div>
				</div>
				<div id="show-questionnairegroup" class="content scaffold-show" role="main">

%{--					<g:if test="${questionnaireInstance?.questionnaireName}">
						<div class="row form-group">
							<div class="col-lg-3" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="questionnaire.questionnaireName.label" default="Questionnaire Name" />
							</div>
							<div class="col-lg-3">
								<pre><g:fieldValue bean="${questionnaireInstance}" field="questionnaireName"/></pre>
							</div>
						</div>
					</g:if>

					<g:if test="${questionnaireInstance?.questionnairegroups}">
						<div class="row form-group">
							<div class="col-lg-3" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="questionnaire.questionnairegroups.label" default="Questionnairegroups" />
							</div>
							<div class="col-lg-3">
								<g:each in="${questionnaireInstance.questionnairegroups}" var="q">
									<span class="property-value" aria-labelledby="questionnairegroups-label"><g:link controller="questionnairegroup" action="show" id="${q.id}">${q.questionnairegroupName}</g:link></span><br/><br/>
								</g:each>
							</div>
						</div>
					</g:if>--}%

					<g:if test="${questionnairegroupInstance?.questionnairegroupName}">
						<div class="row form-group">
							<div class="col-lg-3" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="questionnairegroup.name.label" default="Nom du groupe questions" />
							</div>
							<div class="col-lg-4">
								<pre><g:fieldValue bean="${questionnairegroupInstance}" field="questionnairegroupName"/></pre>
							</div>
						</div>
					</g:if>

					<g:if test="${questionnairegroupInstance?.questionnairegroupPonderation}">
						<div class="row form-group">
							<div class="col-lg-3" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="questionnairegroup.ponderation.label" default="Ponderation" />
							</div>
							<div class="col-lg-4">
								<pre><g:fieldValue bean="${questionnairegroupInstance}" field="questionnairegroupPonderation"/></pre>
							</div>
						</div>
					</g:if>

					<g:if test="${questionnairegroupInstance?.questionnaire}">
						<div class="row form-group">
							<div class="col-lg-3" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="questionnairegroup.questionnaire.label" default="Ponderation" />
							</div>
							<div class="col-lg-4">
								<pre><g:link controller="questionnaire" action="show" id="${questionnairegroupInstance?.questionnaire?.id}">${questionnairegroupInstance?.questionnaire?.questionnaireName}</g:link></pre>
							</div>
						</div>
					</g:if>

					<g:if test="${questionnairegroupInstance.color}">
						<div class="row form-group">
							<div class="col-lg-3" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="questionnairegroup.color.label" default="Ponderation" />
							</div>
							<div class="col-lg-4">
								<pre style="background-color: #${questionnairegroupInstance.color}" ><g:fieldValue bean="${questionnairegroupInstance}" field="color"/></pre>
							</div>
						</div>
					</g:if>

					<g:if test="${questionnairegroupInstance?.questionses}">

						<div class="row form-group">
							<div class="col-lg-3" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="questionnairegroup.questions.label" default="Questions" />
							</div>
							<div class="col-lg-4">
								<g:each in="${questionnairegroupInstance.questionses}" var="q">
								<li><g:link controller="questions" action="show" id="${q.id}">${q?.questionName}</g:link></li>
								</g:each>
							</div>
						</div>

					</g:if>

				</div>

				<div class="row" style="padding-top: 20px">
					<g:form url="[resource:questionnairegroupInstance, action:'delete']" method="DELETE">
						<div class="col-lg-2" role="navigation">
							<g:link class="btn btn-success btn-block" action="edit" resource="${questionnairegroupInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
						</div>
%{--						<div class="col-lg-2" role="navigation">
							<g:actionSubmit class="btn btn-danger btn-block" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
						</div>--}%
					</g:form>

				</div>

			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>

</body>

</html>
