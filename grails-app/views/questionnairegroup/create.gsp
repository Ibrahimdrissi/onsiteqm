<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'questionnairegroup.label', default: 'Questionnairegroup')}" />
	<title><g:message code="default.create.label" args="[entityName]" /></title>
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<g:message code="questionnairegroup.label" />
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row" style="padding-bottom: 20px">
					<div class="col-lg-3" role="navigation">
						<g:link class="btn btn-primary btn-block" action="index"><g:message code="questionnairegroup.label.list" /></g:link>
					</div>
				</div>
				<div id="create-questionnairegroup" class="content scaffold-create" role="main">

					<g:form url="[resource:questionnairegroupInstance, action:'save']" >
						<fieldset class="form">
							<g:render template="form"/>
						</fieldset>
						<fieldset class="buttons">
							<br/>
							<g:submitButton name="create" class="btn btn-success" value="${message(code: 'default.button.create.label', default: 'Create')}" />
						</fieldset>
					</g:form>
				</div>

			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>


</body>

<script >

	$(function(){
		$(".ponderation").keyup(function(){
			if(!$(this).val().match(/^\d+$/) || parseInt($(this).val(),10)>100 || parseInt($(this).val(),10)<0 ){
				$(this).val("");
			}
		});
	});

</script>

</html>