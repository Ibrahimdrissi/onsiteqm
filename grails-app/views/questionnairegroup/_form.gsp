<%@ page import="com.ef.app.qm.Questionnaire" %>

<div class="row form-group has-${hasErrors(bean: questionnairegroupInstance, field: 'questionnairegroupName', 'error')}">
	<div class="col-lg-3" style="font-weight: bold" >
		<g:message code="questionnairegroup.name.label" default="Questionnaire Name" />
		<span class="required-indicator" style="color:red" >*</span>
	</div>
	<div class="col-lg-3">
		<g:textField  class="form-control" name="questionnairegroupName" value="${questionnairegroupInstance?.questionnairegroupName}"/>

	</div>
</div>


<div class="row form-group has-${hasErrors(bean: questionnairegroupInstance, field: 'questionnairegroupPonderation', 'error')}">
	<div class="col-lg-3" style="font-weight: bold" >
		<g:message code="questionnairegroup.ponderation.label" default="Questionnairegroup Ponderation" />
		<span class="required-indicator" style="color:red" >*</span>
	</div>
	<div class="col-lg-3">
		<g:textField  class="form-control" name="questionnairegroupPonderation" value="${questionnairegroupInstance?.questionnairegroupPonderation}"/>

	</div>
</div>

<div class="row form-group ">
	<div class="col-lg-3" style="font-weight: bold" >

		<g:message code="questionnairegroup.color.label" default="Couleur" />
		<span class="required-indicator" style="color:red" >*</span>
	</div>
	<div class="col-lg-3">
		<g:textField  class="jscolor form-control" name="color" value="${(questionnairegroupInstance?.color == null)?'337ab7':questionnairegroupInstance?.color}"/>
	</div>
</div>


<div class="row form-group has-${hasErrors(bean: questionnairegroupInstance, field: 'questionnaire', 'error')}">
	<div class="col-lg-3" style="font-weight: bold" >
		<g:message code="questionnairegroup.questionnaire.label" default="Questionnaire" />
		<span class="required-indicator" style="color:red" >*</span>
	</div>
	<div class="col-lg-3">
		<g:select class="form-control" value="${spaceInstance?.user?.id}" id="questionnaire" name="questionnaire.id" from="${com.ef.app.qm.Questionnaire.list()}" optionKey="id" optionValue="questionnaireName" />
	</div>
</div>
