<%@ page import="com.ef.app.qm.User; org.apache.shiro.SecurityUtils" %>

<%--
  Author: Akeel
  Date: 12/20/12
  Time: 1:32 AM
--%>

<!doctype html>
<html>
<head>
    <meta name="layout" content="layoutContent"/>
    <g:set var="loggedUser" value="${com.ef.app.qm.User.findByUserName(SecurityUtils.getSubject().getPrincipal())}"/>
    <g:set var="userEntity" value="${message(code: 'com.ef.qm.entity.user', default: 'User')}"/>
    <g:set var="roleEntity" value="${message(code: 'com.ef.qm.entity.role', default: 'Role')}"/>
    <g:set var="permissionEntity" value="${message(code: 'com.ef.qm.entity.permission', default: 'Permission')}"/>
    <title><g:message code="default.edit.label" args="[roleEntity]"/></title>
</head>

<body>
    <div class="row">
        <div class="col-lg-9">
            <div class="page-header">
                <h1><g:message code="default.edit.label" args="[roleEntity]" /></h1>
            </div>

            <g:form class="form-horizontal" action="update">
                <fieldset>
                    <g:hiddenField name="id" value="${roleInstance?.id}"/>

                    <div class="form-group ${hasErrors(bean:userInstance,field:'name','error')}">
                        <label class="col-lg-4 control-label"   for="name">
                            <g:message code="com.ef.qm.label.name" default="Name"/>
                        </label>
                        <div class="col-lg-6">
                            <input class="form-control"type="text" name="name" id="name" value="${roleInstance?.name}"
                                   required="required"
                                   placeholder="${message(code: 'com.ef.qm.label.name',default: "Name")}">
                            <span class="help-inline label label-danger">${fieldError(bean:roleInstance,field:'name')}</span>
                        </div>                          
                    </div>

                    <div class="form-group ${hasErrors(bean:roleInstance,field:'name','error')}">
                        <label class="col-lg-4 control-label"   for="description">
                            <g:message code="com.ef.qm.label.description" default="Description"/>
                        </label>
                        <div class="col-lg-6">
                            <textarea class="form-control" name="description" rows="3" id="description" >${roleInstance?.description}</textarea>
                            <span class="help-inline label label-danger">${fieldError(bean:roleInstance,field:'description')}</span>
                        </div>
                    </div>
                
                    <div class="form-actions col-lg-7 pull-right">
                        <shiro:hasPermission permission="role:list">
                            <g:link action="list" class="btn btn-default">
                                <i class="glyphicon glyphicon-circle-arrow-left"></i>
                                <g:message code="com.ef.qm.cancel.label" default="Cancel"/>
                            </g:link>
                        </shiro:hasPermission>
                        <button type="submit" class="btn btn-primary">
                            <i class="glyphicon glyphicon-pencil glyphicon glyphicon-white"></i>
                            <g:message code="default.button.update.label" default="Update" />
                        </button>
	                    <shiro:hasPermission permission="role:delete">
                            <g:link class="btn btn-danger confirmDelete" confirmMessage="${g.message(code: 'default.delete.Confirm',default: 'Are you sure to delete ?')}" action="delete" id="${roleInstance?.id}">
                                <i class="glyphicon glyphicon-remove"></i>
                                <g:message code="default.button.delete.label"/>
                            </g:link>
	                    </shiro:hasPermission>

                    </div>
                </fieldset>  
            </g:form>
        </div>
    </div>
</body>
</html>