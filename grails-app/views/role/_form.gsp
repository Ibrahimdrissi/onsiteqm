
<div class="row form-group has-${hasErrors(bean: roleInstance, field: 'name', 'error')}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="user.name.label" default="Name" />
		<span class="required-indicator" style="color:red" >*</span>
	</div>
	<div class="col-lg-3">
		<g:textField  required="" class="form-control" name="name" value="${roleInstance?.name}"/>
	</div>
</div>
<div class="row form-group has-${hasErrors(bean: roleInstance, field: 'description', 'error')}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="user.description.label" default="Description" />
	</div>
	<div class="col-lg-3">
		<g:textArea  class="form-control" name="description" value="${roleInstance?.description}"/>
	</div>
</div>



