 <%@ page import="org.apache.shiro.SecurityUtils" %>

<%--
  Author: Akeel
  Date: 12/20/12
  Time: 12:20 AM
--%>

<!doctype html>
<html>
<head>
    <meta name="layout" content="layoutContent"/>
    <g:set var="userEntity" value="${message(code: 'com.ef.qm.entity.user', default: 'User')}"/>
    <g:set var="roleEntity" value="${message(code: 'com.ef.qm.entity.role', default: 'Role')}"/>
    <g:set var="permissionEntity" value="${message(code: 'com.ef.qm.entity.permission', default: 'Permission')}"/>
    <title><g:message code="default.show.label" args="[roleEntity]"/></title>
</head>

<body>
    <div class="row">
        <div class="col-lg-9">
            <div class="page-header">
                <h1><g:message code="com.ef.qm.roleDetails.label" args="[roleInstance.name]" default="${roleInstance?.name} Details"/>
                </h1>        
            </div>

            <dl>
                <g:if test="${roleInstance?.name}">
                    <dt><g:message code="com.ef.qm.label.name" default="Name"/></dt>
                    <dd>${fieldValue(bean: roleInstance, field: "name")}</dd>
                </g:if>
                <div class="divider"></div>

                <g:if test="${roleInstance?.description}">
                    <dt><g:message code="com.ef.qm.label.description" default="Description"/></dt>
                    <dd>${fieldValue(bean: roleInstance, field: "description")}</dd>
                </g:if>
                <div class="divider"></div>

                <dt><g:message code="com.ef.qm.label.lastUpdated" default="Last Updated"/></dt>
                <dd><g:formatDate date="${roleInstance?.lastUpdated}" format="${g.message(code: 'default.date.format')}"/></dd>
                <div class="divider"></div>
                %{--<div class="divider"></div>
                <g:if test="${roleInstance?.createdBy}">
                    <dt><g:message code="com.ef.cbr.label.createdBy" default="Created By" /></dt>

                    <dd><g:fieldValue bean="${roleInstance}" field="createdBy"/></dd>

                </g:if>
                <div class="divider"></div>
                <g:if test="${roleInstance?.updatedBy}">
                    <dt><g:message code="com.ef.cbr.label.updatedBy" default="Updated By" /></dt>

                    <dd><g:fieldValue bean="${roleInstance}" field="createdBy"/></dd>

                </g:if>--}%
                <dt><g:message code="com.ef.qm.label.dateCreated" default="Date Created" format="${g.message(code: 'default.date.format')}"/></dt>
                <dd><g:formatDate date="${roleInstance?.dateCreated}"/></dd>
            </dl>
        </div>
    </div>
</body>
</html>