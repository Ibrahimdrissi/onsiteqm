<%@ page import="com.ef.app.qm.User; org.apache.shiro.SecurityUtils" %>
<g:set var="loggedUser" value="${com.ef.app.qm.User.findByUserName(SecurityUtils.getSubject().getPrincipal())}"/>
<g:set var="userEntity" value="${message(code: 'com.ef.qm.entity.user', default: 'User')}"/>
<g:set var="roleEntity" value="${message(code: 'com.ef.qm.entity.role', default: 'Role')}"/>
<g:set var="permissionEntity" value="${message(code: 'com.ef.qm.entity.permission', default: 'Permission')}"/>
<title><g:message code="default.edit.label" args="[roleEntity]"/></title>
<div class="modal-header">
    <h1><g:message code="default.edit.label" args="[roleEntity]" /></h1>
</div>
<div class="modal-body">
    <g:form class="form-horizontal" action="update">
        <fieldset>
            <g:hiddenField name="updatedBy.id" value="${loggedUser.id}"/>
            <g:hiddenField name="id" value="${roleInstance?.id}"/>
            <g:hiddenField name="version" value="${roleInstance?.version}"/>

            <div class="form-group ${hasErrors(bean:roleInstance,field:'name','error')}">
                <label class="col-lg-4 control-label"   for="name">
                    <g:message code="com.ef.qm.label.name" default="Name"/>
                    <span class="required-indicator">*</span>
                </label>
                <div class="col-lg-6">
                    <input class="form-control"type="text" name="name" id="name" value="${roleInstance?.name}"
                           required="required"
                           placeholder="${message(code: 'com.ef.qm.label.name',default: "Name")}">
                    <span class="help-inline label label-danger">${fieldError(bean:roleInstance,field:'name')}</span>
                </div>
            </div>

            <div class="form-group ${hasErrors(bean:roleInstance,field:'description','error')}">
                <label class="col-lg-4 control-label"   for="description">
                    <g:message code="com.ef.qm.label.description" default="Description"/>
                </label>
                <div class="col-lg-6">
                    <textarea class="form-control" name="description" rows="3" id="description" >${roleInstance?.description}</textarea>
                    <span class="help-inline label label-danger">${fieldError(bean:roleInstance,field:'description')}</span>
                </div>
            </div>

            <div class="modal-footer row">
                <shiro:hasPermission permission="role:list">
                    <a data-dismiss="modal" class="btn btn-default">
                        <i class="glyphicon glyphicon-circle-arrow-left"></i>
                        <g:message code="com.ef.qm.cancel.label" default="Cancel"/>
                    </a>
                </shiro:hasPermission>
                <shiro:hasPermission permission="region:delete">
                    <g:link class="btn btn-danger confirmDelete" confirmMessage="${g.message(code: 'default.delete.Confirm',default: 'Are you sure to delete ?')}" action="delete" id="${roleInstance?.id}">
                        <i class="glyphicon glyphicon-remove"></i>
                        <g:message code="default.button.delete.label"/>
                    </g:link>
                </shiro:hasPermission>
                <button type="submit" class="btn btn-primary">
                    <i class="glyphicon glyphicon-pencil glyphicon glyphicon-white"></i>
                    <g:message code="default.button.update.label" default="Update" />
                </button>
            </div>
        </fieldset>
    </g:form>
</div>