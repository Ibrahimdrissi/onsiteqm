<%@ page import="org.apache.shiro.SecurityUtils" %>

<html lang="en">

<head>
    <meta name="layout" content="layoutContent"/>
    <g:set var="entityName" value="${message(code: 'role.label', default: 'Role')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>

<br/>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <g:message code="default.list.label" args="[entityName]" />
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="col-lg-3" role="main">
                    <g:link class="btn btn-primary btn-block" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link>
                    <br/>
                    <g:if test="${flash.message}">
                        <div class="message" role="status">${flash.message}</div>
                    </g:if>
                </div>
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>${message(code: 'com.ef.qm.label.name',default: "Name")}</th>
                            <th>${message(code: 'com.ef.qm.label.description',default: "Description")}</th>
                            <th style="width:100px;"><g:message code="com.ef.qm.label.action" default="Action"/></th>

                        </tr>
                        </thead>
                        <tbody>
                        <g:each in="${roleInstanceList}" status="i" var="roleInstance">
                            <tr class="${(i % 2) == 0 ? 'even gradeC' : 'odd gradeX'}">

                                <td><g:link action="show" id="${roleInstance.id}">${roleInstance?.name}</g:link></td>

                                <td>${fieldValue(bean: roleInstance, field: "description")}</td>

                                <td>
                                    <div class="btn-group">
                                        <a class="btn btn-xs  btn-primary dropdown-toggle" data-hover="dropdown" data-delay="100" data-close-others="false" data-toggle="dropdown" href="#">
                                            <i class="glyphicon glyphicon-cog"></i>
                                            <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu" id="myUll">
                                            <li>
                                                <g:if test="${SecurityUtils.subject.isPermitted('role:edit:'+roleInstance?.id)}">
                                                    <g:link action="edit" class="btn btn-xs" id="${roleInstance?.id}">
                                                        <i class="glyphicon glyphicon-edit"></i>
                                                        <g:message code="default.button.edit.label" default="Edit"/>
                                                    </g:link>
                                                </g:if>
                                                <g:else>
                                                    <button class="btn btn-xs" disabled="disabled">
                                                        <i class="glyphicon glyphicon-edit"></i>
                                                        <g:message code="default.button.edit.label" default="Edit"/>
                                                    </button>
                                                </g:else>
                                            </li>
                                            <li>
                                                <g:if test="${SecurityUtils.subject.isPermitted('role:show:'+roleInstance?.id)}">
                                                    <g:link class="btn btn-xs" action="show" id="${roleInstance?.id}">
                                                        <i class="glyphicon glyphicon-info-sign"></i>
                                                        <g:message code="com.ef.qm.detail.button" default="Details"/>
                                                    </g:link>
                                                </g:if>
                                                <g:else>
                                                    <button class="btn btn-xs" disabled="disabled">
                                                        <i class="glyphicon glyphicon-edit"></i>
                                                        <g:message code="default.button.edit.label" default="Edit"/>
                                                    </button>
                                                </g:else>
                                            </li>
                                            <li>
                                                <g:if test="${SecurityUtils.subject.isPermitted('role:delete')}">
                                                    <g:link class="btn btn-xs confirmDelete" confirmMessage="${g.message(code: 'default.delete.Confirm',default: 'Are you sure to delete ?')}" action="delete" id="${roleInstance?.id}">
                                                        <i class="glyphicon glyphicon-remove"></i>
                                                        <g:message code="default.button.delete.label"/>
                                                    </g:link>
                                                </g:if>
                                                <g:else>
                                                    <button class="btn btn-xs" disabled="disabled">
                                                        <i class="glyphicon glyphicon-remove"></i>
                                                        <g:message code="default.button.delete.label" default="Delete"/>
                                                    </button>
                                                </g:else>

                                            </li>
                                            <li>
                                                <g:if test="${SecurityUtils.subject.isPermitted('role:viewmembers')}">
                                                    <g:link class="btn btn-xs" controller="role" action="viewmembers" id="${roleInstance.id}">
                                                        <i class="glyphicon glyphicon-user"></i>
                                                        <g:message code="com.ef.qm.entity.members.plural" default="View Members"/>
                                                    </g:link>
                                                </g:if>
                                                <g:else>
                                                    <button class="btn btn-xs" disabled="disabled">
                                                        <i class="glyphicon glyphicon-user"></i>
                                                        <g:message code="com.ef.qm.entity.members.plural" default="View Members"/>
                                                    </button>
                                                </g:else>
                                            </li>
                                            <li>
                                                <g:if test="${SecurityUtils.subject.isPermitted('role:viewpermissions')}">
                                                    <g:link class="btn btn-xs" controller="role" action="viewpermissions" id="${roleInstance.id}">
                                                        <i class="glyphicon glyphicon-certificate"></i>
                                                        <g:message code="com.ef.qm.entity.permissions.plural" default="View Permissions"/>
                                                    </g:link>
                                                </g:if>
                                                <g:else>
                                                    <button class="btn btn-xs" disabled="disabled">
                                                        <i class="glyphicon glyphicon-certificate"></i>
                                                        <g:message code="com.ef.qm.entity.permissions.plural" default="View Permissions"/>
                                                    </button>
                                                </g:else>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<script src="${resource(dir: 'theme/bower_components/jquery/dist/', file: 'jquery.min.js')}"></script>
<script src="${resource(dir: 'theme/bower_components/bootstrap/dist/js/', file: 'bootstrap.min.js')}"></script>
<script src="${resource(dir: 'theme/bower_components/metisMenu/dist/', file: 'metisMenu.min.js')}"></script>

<script src="${resource(dir: 'theme/bower_components/datatables/media/js/', file: 'jquery.dataTables.min.js')}"></script>
<script src="${resource(dir: 'theme/bower_components/datatables-plugins/integration/bootstrap/3/', file: 'dataTables.bootstrap.min.js')}"></script>

<script src="${resource(dir: 'theme/dist/js/', file: 'sb-admin-2.js')}"></script>
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true,
            "aoColumnDefs": [
                { 'bSortable': false, 'aTargets': [ 2 ] }
            ]
        });
    });
</script>

</body>

</html>