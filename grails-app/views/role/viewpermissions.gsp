<%@ page import="org.apache.shiro.SecurityUtils" %>
<%--
  Author: Akeel
  Date: 12/20/12
  Time: 12:20 AM
--%>

<!doctype html>
<html>
<head>
    <meta name="layout" content="layoutContent"/>
    <g:set var="userEntity" value="${message(code: 'com.ef.qm.entity.user', default: 'User')}"/>
    <g:set var="roleEntity" value="${message(code: 'com.ef.qm.entity.role', default: 'Role')}"/>
    <g:set var="permissionEntity" value="${message(code: 'com.ef.qm.entity.permission', default: 'Permission')}"/>
    <title><g:message code="com.ef.qm.permissionsAssignedTo.label" args="[roleInstance.name]" default="Permissions assigned to ${roleInstance.name}"/></title>
</head>

<body>
    <div class="row">

        <div class="col-lg-9">
            <div class="page-header">
                <h1><g:message code="com.ef.qm.permissionsAssignedTo.label" default="Permissions assigned to ${roleInstance.name}" args="[roleInstance.name]"/></h1>
            </div>

            <div class="tabbable tabs-left">
                <ul class="nav nav-tabs">                    
                    <li class="${params.tab=='current' ? 'active' : ''}">
                        <g:link action="viewpermissions" id="${roleInstance.id}" params="[tab:'current']">
                            ${message(code:'com.ef.qm.currentPermissions.label',default: "Current permissions")}
                        </g:link>
                    </li>
                    <li class="${params.tab=='assign' ? 'active' : ''}">
                        <g:link action="viewpermissions" id="${roleInstance.id}" params="[tab:'assign']">
                            ${message(code:'com.ef.qm.assignPermissions.label',default: "Assign permissions")}
                        </g:link>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane ${params.tab=='current' ? 'active' : ''}" id="current">
                        <table class="table table-striped table-bordered table-condensed mytable" >
                            <thead>
                                <tr>
                                    <th>${message(code: 'com.ef.qm.label.name',default: "Name")}</th>
                                    <th>${message(code: 'com.ef.qm.label.description',default: "Description")}</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <g:each in="${rolePermissionList}" var="rolePermission">
                                    <g:if test="${rolePermission?.name != null}">
                                        <tr>
                                            <td><g:message code="${rolePermission?.name}" /> </td>
                                            <td><g:message code="${rolePermission?.description}"/> </td>

                                            <td width="20%">
                                                <g:if test="${org.apache.shiro.SecurityUtils.subject.isPermitted('role:reovkepermission')}">
                                                    <g:link action="reovkepermission" class="btn btn-xs btn-info" id="${rolePermission?.id}" params="[rid:roleInstance.id, tab:'current']">
                                                        <g:message code="com.ef.qm.label.revokePermission" default="Revoke permission"/>
                                                    </g:link>
                                                </g:if>
                                                <g:else>
                                                    <g:link class="btn btn-xs" disabled="disabled">
                                                        <g:message code="com.ef.qm.label.revokePermission" default="Revoke permission"/>
                                                    </g:link>
                                                </g:else>
                                            </td>
                                        </tr>
                                    </g:if>

                                </g:each>
                            </tbody>
                        </table>
                        <div class="pagination pull-right">
                            <bootstrap:paginate total="${rolePermissionTotal}" id="${roleInstance.id}" tab="current" />
                        </div>
                    </div>
                    <div class="tab-pane ${params.tab=='assign' ? 'active' : ''}" id="assign">
                        <table class="table table-striped table-bordered table-condensed mytable">
                            <thead>
                                <tr>
                                    <th>${message(code: 'com.ef.qm.label.name',default: "Name")}</th>
                                    <th>${message(code: 'com.ef.qm.label.description',default: "Description")}</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <g:each in="${permissionList}" var="permission">
                                    <tr>
                                        <td><g:message code="${permission?.name}" /> </td>
                                        <td><g:message code="${permission?.description}"/> </td>

                                        <td width="20%">
                                            <g:if test="${SecurityUtils.subject.isPermitted('role:assignpermission')}">
                                                <g:link action="assignpermission" class="btn btn-xs btn-info" id="${permission.id}" params="[rid:roleInstance.id, tab:'assign']">
                                                    <g:message code="com.ef.qm.label.assignPermission" default="Assign permissions"/>
                                                </g:link>
                                            </g:if>
                                            <g:else>
                                                <g:link class="btn btn-xs" disabled="disabled">
                                                    <g:message code="com.ef.qm.label.assignPermission" default="Assign permissions"/>
                                                </g:link>
                                            </g:else>
                                        </td>
                                    </tr>
                                </g:each>
                            </tbody>
                        </table>
                        <div class="pagination pull-right">
                            <bootstrap:paginate total="${permissionTotal}" id="${roleInstance.id}" tab="assign" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

<script>
    var myParam = location.search.split('lang=')[1] ? location.search.split('lang=')[1] : 'en';
    $('.mytable').dataTable({
        "bInfo": false,
        "bAutoWidth": false,
        "oLanguage": {
            "sUrl": "../../dataTableLang/dataTables_"+myParam+".txt"
        }
    });

</script>