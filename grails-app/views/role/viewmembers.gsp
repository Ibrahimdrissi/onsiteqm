<%@ page import="org.apache.shiro.SecurityUtils" %>

<%--
  Author: Akeel
  Date: 12/20/12
  Time: 12:20 AM
--%>

<!doctype html>
<html>
<head>
    <meta name="layout" content="layoutContent"/>
    <g:set var="userEntity" value="${message(code: 'com.ef.qm.entity.user', default: 'User')}"/>
    <g:set var="roleEntity" value="${message(code: 'com.ef.qm.entity.role', default: 'Role')}"/>
    <g:set var="permissionEntity" value="${message(code: 'com.ef.qm.entity.permission', default: 'Permission')}"/>
    <title><g:message code="com.ef.qm.roleMembers.label" default="${roleInstance?.name} members" args="[roleInstance.name]"/></title>
</head>

<body>
    <div class="row">
        <div class="col-lg-9">
            <div class="page-header">
                <h1><g:message code="com.ef.qm.membersInRole.label" default="${roleInstance?.name} members" args="[roleInstance.name]"/></h1>
            </div>

            <div class="tabbable tabs-left">
                <ul class="nav nav-tabs">                    
                    <li class="${params.tab=='current' ? 'active' : ''}">
                        <g:link action="viewmembers" id="${roleInstance.id}" params="[tab:'current']">
                            ${message(code:'om.ef.qm.currentMembers.label',default: "Current members")}
                        </g:link>
                    </li>
                    <li class="${params.tab=='assign' ? 'active' : ''}">
                        <g:link action="viewmembers" id="${roleInstance.id}" params="[tab:'assign']">
                            ${message(code:'om.ef.qm.addMoreMembers.label',default: "Add members")}
                        </g:link>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane ${params.tab=='current' ? 'active' : ''}" id="current">
                        <table class="table table-striped table-bordered table-condensed mytable" >
                            <thead>
                                <tr>
                                    <th>${message(code: 'com.ef.qm.label.userName',default: "Username")}</th>
                                    <th>${message(code: 'com.ef.qm.label.fullName',default: "FullName")}</th>
                                    <th>${message(code: 'com.ef.qm.label.email',default: "Email")}</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <g:each in="${membersList}" var="member">
                                    <tr>
                                        <td>
                                            <g:if test="${org.apache.shiro.SecurityUtils.subject.isPermitted('user:show:'+member.id)}">
                                                <g:link controller="user" action="show" id="${member.id}">${member.userName}</g:link>
                                            </g:if>
                                            <g:else>${member.userName}</g:else>
                                        </td>
                                        <td>${member?.firstName}</td>
                                        <td>${member?.userEmail}</td>

                                        <td width="20%">
                                            <g:if test="${SecurityUtils.subject.isPermitted('role:reovkemembership')}">
                                                <g:link action="reovkemembership" class="btn btn-xs btn-info" id="${member?.id}" params="[rid:roleInstance.id, tab:'current']">
                                                    <g:message code="com.ef.qm.revokeMembership.label" default="Revoke Membership"/>
                                                </g:link>
                                            </g:if>
                                            <g:else>
                                                <g:link class="btn btn-xs" disabled="disabled">
                                                    <g:message code="com.ef.qm.revokeMembership.label" default="Revoke Membership"/>
                                                </g:link>
                                            </g:else>
                                        </td>                     
                                    </tr>
                                </g:each>
                            </tbody>
                        </table>
                        %{--<div class="pagination pull-right">
                            <bootstrap:paginate total="${membersTotal}" id="${roleInstance.id}" tab="current" />
                        </div>--}%
                    </div>
                    <div class="tab-pane ${params.tab=='assign' ? 'active' : ''}" id="assign">
                        <table class="table table-striped table-bordered table-condensed mytable">
                            <thead>
                                <tr>
                                    <g:sortableColumn property="userName" title="${message(code: 'com.ef.qm.label.userName',default: "Username")}"/>
                                    <g:sortableColumn property="userFirstName" title="${message(code: 'com.ef.qm.label.fullName',default: "FullName")}"/>
                                    <g:sortableColumn property="userEmail" title="${message(code: 'com.ef.qm.label.email',default: "Email")}"/>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <g:each in="${usersList}" status="i" var="user">
                                    <tr>
                                        <td>
                                            <g:if test="${SecurityUtils.subject.isPermitted('user:show:'+user.id)}">
                                                <g:link controller="user" action="show" id="${user.id}">${user.userName}</g:link>
                                            </g:if>
                                            <g:else>${user.userName}</g:else>
                                        </td>
                                        <td>${user?.firstName}</td>
                                        <td>${user?.userEmail}</td>
                                        <td width="20%">
                                            <g:if test="${SecurityUtils.subject.isPermitted('role:assignmembership')}">
                                                <g:link action="assignmembership" class="btn btn-xs btn-info" id="${user.id}" params="[rid:roleInstance.id, tab:'assign']">
                                                    <g:message code="com.ef.qm.label.grantMembership" default="Grant Membership"/>
                                                </g:link>
                                            </g:if>
                                            <g:else>
                                                <g:link class="btn btn-xs" disabled="disabled">
                                                    <g:message code="com.ef.qm.label.grantMembership" default="Grant Membership"/>
                                                </g:link>
                                            </g:else>
                                        </td>
                                    </tr>
                                </g:each>
                            </tbody>
                        </table>
                        %{--<div class="pagination pull-right">
                            <bootstrap:paginate total="${usersTotal}" id="${roleInstance.id}" tab="assign" />
                        </div>--}%
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<script>
    var myParam = location.search.split('lang=')[1] ? location.search.split('lang=')[1] : 'en';
    $('.mytable').dataTable({
        "bInfo": false,
        "bAutoWidth": false,
        "oLanguage": {
            "sUrl": "../../dataTableLang/dataTables_"+myParam+".txt"
        }
    });

</script>