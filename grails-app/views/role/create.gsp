
<html lang="en">

<head>
    <meta name="layout" content="layoutContent"/>
    <g:set var="entityName" value="${message(code: 'role.label', default: 'Role')}" />
    <title><g:message code="default.create.label" args="${[message(code: 'role.label', default: 'Role')]}" /></title>
</head>

<body><br/>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <g:message code="default.create.label" args="${[message(code: 'role.label', default: 'Role')]}" />
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="row" style="padding-bottom: 20px">
                    <div class="col-lg-3" role="navigation">
                        <g:link class="btn btn-primary btn-block" action="index"><g:message code="default.list.label" args="${[message(code: 'role.label', default: 'Role')]}" /></g:link>
                    </div>
                </div>
                <div id="create-space" class="content scaffold-create" role="main">
                    <g:if test="${flash.message}">
                        <div class="message" role="status"  >${flash.message}</div>
                    </g:if>
                    <g:hasErrors bean="${roleInstance}">
                        <div class="alert alert-danger">
                            <g:eachError bean="${roleInstance}" var="error">
                                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                            </g:eachError>
                        </div>
                    </g:hasErrors>
                    <g:form url="[resource:roleInstance, action:'save']" >
                        <fieldset class="form">
                            <g:render template="form"/>
                        </fieldset>
                        <fieldset class="buttons">
                            <br/>
                            <g:submitButton name="create" class="btn btn-success" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                        </fieldset>
                    </g:form>
                </div>

            </div>
        </div>
    </div>
</div>
</body>

</html>
