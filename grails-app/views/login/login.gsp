<?xml version='1.0' encoding='UTF-8' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
	  xmlns:h="http://java.sun.com/jsf/html"
	  xmlns:f="http://java.sun.com/jsf/core">
<head>


	<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
	<link rel="apple-touch-icon" href="${assetPath(src: 'apple-touch-icon.png')}">
	<link rel="apple-touch-icon" sizes="114x114" href="${assetPath(src: 'apple-touch-icon-retina.png')}">

	<title><g:message code="com.expertflow.ma.onsiteqm.applicationName"/></title>

	<link rel="stylesheet" type="text/css"  href="${resource(dir: 'theme/bower_components/bootstrap/dist/css/', file: 'bootstrap.min.css')}" />
	<!-- MetisMenu CSS -->
	<link rel="stylesheet" type="text/css"  href="${resource(dir: 'theme/bower_components/metisMenu/dist/', file: 'metisMenu.min.css')}"  />
	<!-- Custom CSS -->
	<link rel="stylesheet" type="text/css"  href="${resource(dir: 'theme/dist/css/', file: 'sb-admin-2.css')}"  />
	<!-- Custom Fonts -->
	<link rel="stylesheet" type="text/css"  href="${resource(dir: 'theme/bower_components/font-awesome/css/', file: 'font-awesome.min.css')}" />

</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="login-panel panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Please Sign In</h3>
					</div>
					<div class="panel-body">
						<g:form class="simpleform" controller="login" action="login" >
							<fieldset>
								<p class="info">
									Please login with your username and password. <br />
									<g:if test="${flash.message}">
										<div class="message" style="color:red">${flash.message}</div>
									</g:if>
								</p>
%{--								<g:if test="${flash.message}">
									<div class="message">${flash.message}</div>
								</g:if>--}%
								<div class="form-group">
									<label for="username">Username</label>
									<g:textField class="form-control" required="required" name="username" />
								</div>
								<div class="form-group">
									<label for="password">Password</label>
									<g:passwordField class="form-control"  required="required" name="password" />
								</div>
								<g:submitButton class="btn btn-lg btn-success btn-block" name="submitButton" value="Login" />
							</fieldset>
						</g:form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="${resource(dir: 'theme/bower_components/jquery/dist/', file: 'jquery.min.js')}"></script>
	<script src="${resource(dir: 'theme/bower_components/bootstrap/dist/js/', file: 'bootstrap.min.js')}"></script>
	<script src="${resource(dir: 'theme/bower_components/metisMenu/dist/', file: 'metisMenu.min.js')}"></script>
	<script src="${resource(dir: 'theme/dist/js/', file: 'sb-admin-2.js')}"></script>

</body>
</html>

