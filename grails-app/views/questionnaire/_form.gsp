
<div class="row form-group has-${hasErrors(bean: questionnaireInstance, field: 'questionnaireName', 'error')}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="questionnaire.questionnaireName.label" default="Questionnaire Name" />
		<span class="required-indicator" style="color:red" >*</span>
	</div>
	<div class="col-lg-3">
		<g:textField  class="form-control" name="questionnaireName" value="${questionnaireInstance?.questionnaireName}"/>
	</div>
	<div class="col-lg-1">
		<g:checkBox  class="form-control" name="enable_q" value="${questionnaireInstance?.enable_q}"/>
	</div>
</div>

%{--<div class="row form-group ">
	<div class="col-lg-2" style="font-weight: bold" >
		Enable
	</div>
	<div class="col-lg-3">
		<g:checkBox  class="form-control" name="priority_q" value="${questionnaireInstance?.questionnaireName}"/>
	</div>
</div>

<div class="row form-group ">
	<div class="col-lg-2" style="font-weight: bold" >
		Prioritaire
	</div>
	<div class="col-lg-3">
		<g:checkBox  class="form-control" name="enable_q" value="${questionnaireInstance?.questionnaireName}"/>
	</div>
</div>--}%


