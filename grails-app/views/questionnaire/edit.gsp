
<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'questionnaire.label', default: 'Questionnaire')}" />
	<title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<g:message code="questionnaire.label" />
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row" style="padding-bottom: 20px">
					<div class="col-lg-3" role="navigation">
						<g:link class="btn btn-primary btn-block" action="index"><g:message code="questionnaire.label.list"  /></g:link>
					</div>
					<div class="col-lg-3" role="navigation">
						<g:link class="btn btn-primary btn-block" action="create"><g:message code="questionnaire.label.new" /></g:link></div>
				</div>
				<div id="edit-questionnaire" class="content scaffold-edit" role="main">
					<g:form url="[resource:questionnaireInstance, action:'update']" method="PUT" >
						<fieldset class="form">
							<div class="row form-group has-${hasErrors(bean: questionnaireInstance, field: 'questionnaireName', 'error')}">
								<div class="col-lg-2" style="font-weight: bold" >
									<g:message code="questionnaire.questionnaireName.label" default="Questionnaire Name" />
									<span class="required-indicator" style="color:red" >*</span>
								</div>
								<div class="col-lg-3">
									<g:textField  class="form-control" name="questionnaireName" value="${questionnaireInstance?.questionnaireName}"/>
								</div>
								<div class="col-lg-1">
									<g:checkBox style="margin: 0;width: 35px;" class="form-control" name="enable_q" value="${questionnaireInstance?.enable_q}"/>
								</div>
							</div>
						</fieldset>
						<fieldset class="buttons">
							<br/>
							<g:actionSubmit class="btn btn-success" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />

						</fieldset>
					</g:form>


				</div>

			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>

</body>

</html>