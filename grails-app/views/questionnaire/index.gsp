<%@ page import="com.ef.app.qm.User_holidays" %>

<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'questionnaire.label', default: 'Questionnaire')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				${message(code: 'questionnaire.label.list', default: 'Questionnaire')}
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="col-lg-3" role="main">
					<g:link class="btn btn-primary btn-block" action="create">${message(code: 'questionnaire.label.new', default: 'Questionnaire')}</g:link>
					<br/>
				</div>
				<div class="dataTable_wrapper">
					<table class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
						<tr>
							<th>${message(code: 'questionnaire.questionnaireName.label')}</th>
						</tr>
						</thead>
						<tbody>

						<g:each in="${com.ef.app.qm.Questionnaire.list()}" status="i" var="questionnaireInstance">
							<tr class="${(i % 2) == 0 ? 'even gradeC' : 'odd gradeX'}">
								<td ><g:link  style="${(questionnaireInstance.enable_q)?'color: green;font-size: large;':''}" action="show" id="${questionnaireInstance.id}">${fieldValue(bean: questionnaireInstance, field: "questionnaireName")}</g:link></td>
							</tr>
						</g:each>

						</tbody>
					</table>
				</div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>

<script>
	$(document).ready(function() {
		$('#dataTables-example').DataTable({
			responsive: true
		});


	});
</script>

</body>

</html>
