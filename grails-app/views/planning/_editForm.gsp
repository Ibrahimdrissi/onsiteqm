<%@ page import="com.ef.app.qm.Visit; com.ef.app.qm.Space" %>

<div class="row form-group has-${hasErrors(bean: planningInstance, field: 'planningDate', 'error')}">
    <div class="col-lg-2" style="font-weight: bold" >
        <g:message code="planing.date.label" default="Planning Date" />
        <span class="required-indicator" style="color:red" >*</span>
    </div>
    <div class="col-lg-3">
        %{--<calendar:datePicker class="form-control"  name="planningDate" value="${planningInstance?.planningDate}" defaultValue="${new Date()}"   dateFormat="%d/%m/%Y"/>--}%
        %{--<g:datePicker  class="form-control" name="planningDate" precision="day"  value="${planningInstance?.planningDate}" default="none" noSelection="['': '']" />--}%
        <input data-field="datetime" type="text"
               title="${g.message(code: 'com.ef.qm.label.planningDate')}" data-toggle="tooltip"
               data-placement="left" name="planningDate_value"
               value="${planningInstance?.planningDate?.format("dd/MM/yyyy")}"
               class="form-control formTooltip datetimepicker"/>
    </div>
</div>
<div class="row form-group has-${hasErrors(bean: planningInstance, field: 'visit', 'error')}">
    <div class="col-lg-2" style="font-weight: bold" >
        <g:checkBox class="" name="urgent" id="urgent" checked="false"/> <g:message code="default.urgent.label" />

    </div>
</div>

<div class="row form-group has-${hasErrors(bean: planningInstance, field: 'visit', 'error')}">
    <div class="col-lg-2" style="font-weight: bold" >
        <g:message code="default.visit.label" default="Visit" />

    </div>
    <div class="col-lg-3">
        <g:select class="form-control" id="visit" name="visit.id" from="${com.ef.app.qm.Visit?.findById(planningInstance?.visit?.id)}" optionKey="id" optionValue="visitName" value="${planningInstance?.visit?.id}"/>
    </div>
</div>
<div class="row form-group has-${hasErrors(bean: planningInstance, field: 'space', 'error')}">
    <div class="col-lg-2" style="font-weight: bold" >
        <g:message code="com.ef.domain.name.space" default="Space" />

    </div>
    <div class="col-lg-3">
        <g:select class="form-control" id="space" name="space.id" from="${com.ef.app.qm.Space?.findById(planningInstance?.space?.id)}" optionKey="id" optionValue="spaceName" value="${planningInstance?.space?.id}" />
    </div>
</div>
<div id="changeDiv">
<div class="row form-group has-${hasErrors(bean: planningInstance, field: 'planningChangeComment', 'error')}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="planning.change.comment.label" default="Change Comment" />
	</div>
	<div class="col-lg-3">
		<g:textArea class="form-control" name="planningChangeComment" value="${planningInstance?.planningChangeComment}"/>
	</div>
</div>

<div class="row form-group has-${hasErrors(bean: planningInstance, field: 'planningChangeComment', 'error')}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="planning.change.joint.label" default="Change Joint" />
	</div>
	<div class="col-lg-3">
		<input type="file"  name="changeFile" class="form-control btn btn-primary"/>
	</div>
</div>
</div>
<script>
    $(document).ready(function () {
        $(".datetimepicker").datetimepicker({
            icons: {
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            showToday: false,
            inline: false,
            pickTime: false,
            todayBtn: true,
            locale: 'fr',
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            pickerPosition: "bottom-right",
            minView: 2,
            daysOfWeekDisabled: [0, 6]

        });
        /*$("#urgent").on("change",function () {
          if($(this).is(':checked')){
              $('#changeDiv').slideUp('slow');
          }else{
              $('#changeDiv').slideDown('slow');
          }
        });*/
    });
</script>


