<%@ page import="com.ef.app.qm.User;  org.apache.shiro.SecurityUtils" %>
<g:set var="loggedUser" value="${com.ef.app.qm.User.findByUserName(SecurityUtils.getSubject().getPrincipal())}"/>
<div class="modal-header">
    <h1><g:message code="com.ef.pcs.label.uploadingFile" default="File Uploading"/></h1>
</div>

<div class="modal-body">
    <g:form action="uploadCsv" method="post" enctype="multipart/form-data">
        <fieldset>
            <div class="form-group ">
                <label class="col-lg-3 control-label" for="upload cvs">
                    <g:message code="com.ef.app.qm.label.file.upload" default="Upload CSV"/>
                </label>

                <div class="col-lg-6">
                    <input type="file" name="myFile" class="form-control btn btn-primary"/>
                    <span class="help-inline label label-danger"></span>
                </div>
                <export:formats formats="['csv']" action="downloadPlannings" params="[]" class="btn btn-default pull-right"/>
            </div>
            %{--<g:message code="com.ef.app.qm.label.sample.csv" default="Sample.csv"/>--}%
            %{--<g:link uri="/Sample.csv"><g:message code="com.ef.app.qm.label.sample.csv" default="Sample.csv"/></g:link>--}%

        </fieldset>
        <br>
        <fieldset>
        <div class="col-lg-4"></div>
        <div class="col-lg-7">
            <a class="btn btn-warning" data-dismiss="modal"><g:message code="default.button.cancel.label"
                                                                       default="Cancel"/></a>
            <button type="submit" class=" btn btn-primary">
                <i class="glyphicon glyphicon-file"></i>
                <g:message code="default.button.upload.label" default="Upload"/>
            </button>
        </div>
        </fieldset>
    </g:form>
</div>