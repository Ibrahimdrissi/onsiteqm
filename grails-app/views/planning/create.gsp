
<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'com.ef.domain.name.planing', default: 'Planning')}" />
	<title><g:message code="default.create.label" args="${[message(code: 'com.ef.domain.name.planing', default: 'Planning')]}" /></title>
</head>

<body><br/>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<g:message code="default.create.label" args="${[message(code: 'com.ef.domain.name.planing', default: 'Planning')]}" />
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row" style="padding-bottom: 20px">
					<div class="col-lg-3" role="navigation">
						<g:link class="btn btn-primary btn-block" action="index"><g:message code="default.list.label" args="${[message(code: 'com.ef.domain.name.planing')]}" /></g:link>
					</div>
				</div>
				<div id="create-space" class="content scaffold-create" role="main">
					<g:hasErrors bean="${planingInstance}">
						<div class="alert alert-danger">
							<g:hasErrors bean="${planingInstance}">
								<div class="alert alert-danger">
									<g:eachError bean="${planingInstance}" var="error">
										<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
									</g:eachError>
								</div>
							</g:hasErrors>
						</div>
					</g:hasErrors>
					<g:form url="[resource:planingInstance, action:'save']" >
						<fieldset class="form">
							<g:render template="form"/>
						</fieldset>
						<fieldset class="buttons">
							<br/>
							<g:submitButton name="create" class="btn btn-success" value="${message(code: 'default.button.create.label', default: 'Create')}" />
						</fieldset>
					</g:form>
				</div>

			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
</body>

</html>

<script>
/*
	$(function(){
		$("#planningDate").keyup(function(){
			if(!$(this).val().match(/^\d+$/) || parseInt($(this).val(),10)<1 ){
				$(this).val("");
			}
		});
	});*/

</script>
