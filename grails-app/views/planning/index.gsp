<%@ page import="com.ef.app.qm.Space; com.ef.app.qm.User; com.ef.app.qm.Planning" %>

<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'com.ef.domain.name.planing', default: 'Planning')}" />
	<g:set var="userProfileName" value="${com.ef.app.qm.User.get(session?.user?.id)?.profile?.profilName?.trim()}" />

	<style>
		.dataTables_wrapper {
			position: relative;
			clear: both;
			width: 98% !important;
		}
	</style>
</head>

<body>

<br/>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<g:message code="default.list.label" args="[entityName]" />
			</div>
			<div class="panel-body" >

				<div class="row" >
					<div class="col-lg-12">
						<g:if test="${userProfileName?.equalsIgnoreCase("Chef de Cellule") || userProfileName?.equalsIgnoreCase("Cellule centrale")}">

							<div class="pull-right" align="center">
								<div class="row" style="padding-right: 20px;padding-bottom: 20px">
									<div class="btn-group">
										<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											Actions <span class="caret"></span>
										</button>
										<ul class="dropdown-menu pull-right" role="menu">
											<li><a href="../planning/create">Cr&eacute;er planning</a></li>
											<li role="separator" class="divider"></li>
											<li><a data-toggle="modal" href="#uploadPop">
												<i class="glyphicon glyphicon-plus"></i>
												<g:message code="com.ef.app.qm.uploadCsv.label" default="Upload CSV File"/>
											</a>
											</li>
										</ul>
									</div>
								</div>
							</div>


						</g:if>
					</div>
				</div>

				<div class="table-responsive" >
					<table class="table table-striped table-bordered table-hover" id="dataTables-planning">
						<thead>
						<tr>
							<th style="min-width: 150px !important;">Code de l'espace</th>
							<th style="min-width: 150px !important;">Nom de l'espace</th>
							<th style="min-width: 150px !important;">Utilisateur</th>
							<th style="min-width: 150px !important;">${message(code: 'default.visit.label', default: 'Visit')}</th>
							<th style="min-width: 150px !important;">${message(code: 'planning.planningDate.label', default: 'Date')}</th>
							<th style="min-width: 170px !important;">Etat</th>
							<th style="min-width: 150px !important;">Validateur</th>
							<th style="min-width: 150px !important;">Date Validation</th>
							<th style="min-width: 150px !important;">Date MDF</th>
						</tr>
						</thead>
						<tbody>
						<g:set var="planninglist"></g:set>
						<g:if test="${com.ef.app.qm.User.get(session.user?.id)?.profile?.profilName=="Cellule centrale" || com.ef.app.qm.User.get(session.user?.id)?.profile?.profilName=="Consultation"}" >
							<g:set var="planninglist" value="${Planning.list()}"></g:set>
						</g:if>
						<g:else>
							<g:set var="planninglist" value="${com.ef.app.qm.Planning.findAllBySpaceInList(com.ef.app.qm.Space.findAllByUserInList(com.ef.app.qm.User.findAllByTeam(com.ef.app.qm.User.get(session.user?.id)?.team)))}"></g:set>
						</g:else>
						<g:each in="${planninglist}" status="i" var="planningInstance">
							<g:if test="${planningInstance.planningState != Planning.CHANGED_STATE}">
								<tr class="${(i % 2) == 0 ? 'even gradeC' : 'odd gradeX'}">
									<td><g:link controller="planning" action="show" id="${planningInstance?.id}">${planningInstance?.space?.spaceCode.encodeAsHTML()}</g:link></td>
									<td ><g:link controller="planning" action="show" id="${planningInstance?.id}">${planningInstance?.space?.spaceName.encodeAsHTML()}</g:link></td>
									<td style="background-color:${planningInstance.planningState == Planning.WAITING_CONFIRMATION_STATE ?'#E36C0A !important;color:#fff !important;':'' }">${planningInstance?.space?.user?.firstName} ${planningInstance?.space?.user?.lastName}</td>
									<td style="background-color:${planningInstance.planningState == Planning.WAITING_CONFIRMATION_STATE ?'#E36C0A !important;color:#fff !important;':'' }">${planningInstance?.visit}</td><td style="background-color:${planningInstance.planningState == Planning.WAITING_CONFIRMATION_STATE ?'#E36C0A !important;color:#fff !important;':'' }"><g:formatDate format="dd-MM-yyyy" date="${planningInstance?.planningDate}"/></td>

									<td style="background-color:${planningInstance.planningState == Planning.WAITING_CONFIRMATION_STATE ?'#E36C0A !important;color:#fff !important;':'' }">
										<g:if test="${planningInstance?.planningState==Planning.VALIDATED_STATE}">
											<g:message code="com.ef.qm.planning.state.validated"></g:message>
										</g:if>
										<g:if test="${planningInstance?.planningState==Planning.CHANGED_STATE}">
											<g:message code="com.ef.qm.planning.state.changed" ></g:message>
										</g:if>
										<g:elseif test="${planningInstance?.planningState==Planning.WAITING_CONFIRMATION_STATE}">
											<g:message code="com.ef.qm.planning.state.waiting"></g:message>
										</g:elseif>
										<g:elseif test="${planningInstance?.planningState==Planning.REJECTED_STATE}">
											<g:message code="com.ef.qm.planning.state.rejected"></g:message>
										</g:elseif>
										<g:elseif test="${planningInstance?.planningState==Planning.EVALUATED_STATE}">
											<g:message code="com.ef.qm.planning.state.evaluated"></g:message>
										</g:elseif>
									</td>

									<td style="background-color:${planningInstance.planningState == Planning.WAITING_CONFIRMATION_STATE ?'#E36C0A !important;color:#fff !important;':'' }">
										${planningInstance?.user_validated?.firstName} ${planningInstance?.user_validated?.lastName}
									</td>

									<td style="background-color:${planningInstance.planningState == Planning.WAITING_CONFIRMATION_STATE ?'#E36C0A !important;color:#fff !important;':'' }">
										<g:formatDate format="dd-MM-yyyy HH:mm" date="${planningInstance?.date_validation}"/>
									</td>

									<td style="background-color:${planningInstance.planningState == Planning.WAITING_CONFIRMATION_STATE ?'#E36C0A !important;color:#fff !important;':'' }">
										<g:formatDate format="dd-MM-yyyy HH:mm" date="${planningInstance.date_updated}"/>
									</td>
								</tr>
							</g:if>
						</g:each>
						</tbody>
					</table>
				</div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>


<div id="uploadPop" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<g:render template="uploadCSV"/>
		</div>
	</div>
</div>
<script>
	$(document).ready(function() {
		$('#dataTables-planning').DataTable({
			responsive: true,
			"order": [[ 5, "asc" ]]
		});
	});
</script>

</body>

</html>