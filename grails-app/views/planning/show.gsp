<%@ page import="com.ef.app.qm.Evaluation; com.ef.app.qm.Team; com.ef.app.qm.Profile; com.ef.app.qm.User; com.ef.app.qm.Planning" %>

<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'com.ef.domain.name.planing', default: 'Planning')}" />
	<g:set var="userProfileName" value="${User.get(session?.user?.id)?.profile?.profilName?.trim()}" />
	<title><g:message code="default.show.label" args="${[message(code: 'com.ef.domain.name.planing',default: "Planning")]}" /></title>
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<g:message code="default.show.label" args="${[message(code: 'com.ef.domain.name.planing',default: "Planning")]}" />
			</div>
			<div class="panel-body">
				<div class="row" style="padding-right: 20px">
					<g:if test="${Planning.findBySpace(planningInstance.space)?.space?.user?.id==session.user.id && planningInstance?.planningState==Planning.VALIDATED_STATE}">

						<div class="pull-right" align="center">
							<div class="btn-group">
								<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									Actions <span class="caret"></span>
								</button>
								<ul class="dropdown-menu pull-right" role="menu">
									<li><a href="../../evaluation/create?planning=${planningInstance.id}">Evaluer</a></li>
									<g:if test="${com.ef.app.qm.Planning.findByPlanning(planningInstance)?.planningState!= Planning.WAITING_CONFIRMATION_STATE}">
										<li role="separator" class="divider"></li>
										<li><a href="../edit/${planningInstance.id}">Editer</a></li>
									</g:if>
								</ul>
							</div>
						</div>
					</g:if>
					<g:if test="${planningInstance?.planningState==Planning.EVALUATED_STATE && (planningInstance?.space?.user?.id==session?.user?.id || (User.findById(session?.user?.id)?.profile?.profilName=="Chef de cellule" && planningInstance?.space?.user.team.teamName==User.findById(session?.user?.id).team.teamName) || User.findById(session?.user?.id)?.profile?.profilName=="Cellule centrale")}">

						<div class="pull-right" align="center">
							<div class="btn-group">
								<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									Actions <span class="caret"></span>
								</button>
								<ul class="dropdown-menu pull-right" role="menu">
									<li><a href="../../evaluation/show/${com.ef.app.qm.Evaluation.findByPlanning(planningInstance).id}">Evaluation</a></li>
								</ul>
							</div>
						</div>
					</g:if>


				</div>
				<div id="show-space" class="content scaffold-show" role="main">

					<div class="row form-group">
						<div class="col-lg-2" style="font-weight: bold;padding-top: 8px;" >
							<g:message code="com.ef.domain.name.space" default="Space" />
						</div>
						<div class="col-lg-3">
							<pre>${planningInstance?.space?.spaceName.encodeAsHTML()}</pre>
						</div>
					</div>

					<div class="row form-group">
						<div class="col-lg-2" style="font-weight: bold;padding-top: 8px;" >
							<g:message code="planing.date.label" default="Planning Date" />
						</div>
						<div class="col-lg-3">
							<pre><g:formatDate format="dd-MM-yyyy" date="${planningInstance?.planningDate}"/></pre>
						</div>
						<g:if test="${planningInstance?.planningState==Planning.WAITING_CONFIRMATION_STATE  || planningInstance?.planningState==Planning.REJECTED_STATE}">
							<div class="col-lg-2" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="planing.previous.date.label" default="Previous Date" />
							</div>
							<div class="col-lg-3">
								<pre><g:formatDate format="dd-MM-yyyy" date="${planningInstance?.planning?.planningDate}"/></pre>
							</div>
						</g:if>
					</div>

					<div class="row form-group">
						<div class="col-lg-2" style="font-weight: bold;padding-top: 8px;" >
							<g:message code="default.visit.label" default="Visit" />
						</div>
						<div class="col-lg-3">
							<pre>${planningInstance?.visit}</pre>
						</div>
					</div>


					<g:if test="${planningInstance?.planningState!=Planning.VALIDATED_STATE}">
						<div class="row form-group">
							<div class="col-lg-2" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="planning.change.commit.label" default="Change Commit" />
							</div>
							<div class="col-lg-3">
								<g:textArea  class="form-control" name="planningChangeComment" value="${planningInstance?.planningChangeComment}" disabled="disabled"/>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-lg-2" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="planning.change.joint.label" default="Change Joint" />
							</div>
							<div class="col-lg-3">
								<pre><g:link uri="/${planningInstance?.planningChangeJoint}">${planningInstance?.planningChangeJoint}</g:link></pre>
							</div>
						</div>
					</g:if>
				</div>

				<div class="row" style="padding-top: 20px">
					<g:form url="[resource:planningInstance, action:'edit']" method="POST">
						<g:if test="${planningInstance?.planningState==Planning.WAITING_CONFIRMATION_STATE && (( userProfileName?.equalsIgnoreCase("Chef de Cellule") &&   planningInstance.space.user.id!=session.user.id &&  planningInstance.space.user.team==User.findById(session.user.id).team ) || (userProfileName?.equalsIgnoreCase("Cellule centrale")) && planningInstance.space.user.profile.profilName.equalsIgnoreCase("Chef de cellule") )  }">
							  <g:actionSubmit class="btn btn-success" action="approve" value="${message(code: 'default.button.approve.label')}" />
							  <g:actionSubmit class="btn btn-danger" action="disapprove" value="${message(code: 'default.button.disapprove.label')}" />
						</g:if>
					</g:form>
				</div>

			</div>
			<!-- /.panel-body -->
		    </div>


		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->

</body>

</html>

