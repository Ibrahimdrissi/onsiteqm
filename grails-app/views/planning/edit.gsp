<%@ page import="com.ef.app.qm.Planning" %>



<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'com.ef.domain.name.planing', default: 'Planning')}" />
	<g:set var="originalPlanning" value="${Planning.findByPlanning(planningInstance)}" />
	<title><g:message code="default.edit.label" args="${[message(code: 'com.ef.domain.name.planing',default: "Planning")]}" /></title>
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<g:message code="default.edit.label" args="${[message(code: 'com.ef.domain.name.planing',default: "Planning")]}" />
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row" style="padding-bottom: 20px">
					</div>
				<div id="edit-space" class="content scaffold-edit" role="main">

					<g:hasErrors bean="${planningInstance}">
						<div class="alert alert-danger">
							<g:eachError bean="${planningInstance}" var="error">
								<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
							</g:eachError>
						</div>
					</g:hasErrors>
					<g:form action="update1" method="post" enctype="multipart/form-data">
						<g:hiddenField name="version" value="${planningInstance?.version}" />
						<g:hiddenField name="id" value="${planningInstance?.id}" />
						<fieldset class="form">
							<g:render template="editForm"/>
						</fieldset>
						<fieldset class="buttons">
							<br/>
							<g:if test="${planningInstance?.planningState==Planning.WAITING_CONFIRMATION_STATE}">
								<g:actionSubmit class="btn btn-success" action="approve" value="${message(code: 'default.button.approve.label', default: 'Approve')}" />
								<g:actionSubmit class="btn btn-success" action="disapprove" value="${message(code: 'default.button.disapprove.label', default: 'Disapprove')}" />
							</g:if>
							<g:else>
								<g:actionSubmit class="btn btn-success" action="update1" value="${message(code: 'default.button.update.label', default: 'Update')}" />
								<g:link class="btn btn-warning" action="index"><g:message code="default.button.cancel.label" default="Cancel"/></g:link>
							</g:else>
						</fieldset>
					</g:form>
				</div>

			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
</body>

</html>
