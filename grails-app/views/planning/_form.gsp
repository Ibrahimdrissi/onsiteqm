<%@ page import="com.ef.app.qm.User; com.ef.app.qm.Visit; com.ef.app.qm.Space" %>

<div class="row form-group has-${hasErrors(bean: planningInstance, field: 'planningDate', 'error')}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="planing.date.label" default="Planning Date" />
		<span class="required-indicator" style="color:red" >*</span>
	</div>
	<div class="col-lg-3">
		%{--<calendar:datePicker class="form-control"  name="planningDate" value="${planningInstance?.planningDate}" defaultValue="${new Date()}"   dateFormat="%d/%m/%Y"/>--}%
		%{--<g:datePicker  class="form-control" name="planningDate" precision="day"  value="${planningInstance?.planningDate}" default="none" noSelection="['': '']" />--}%
		<input data-field="datetime" type="text"
			   title="${g.message(code: 'com.ef.pcs.label.startDate')}" data-toggle="tooltip"
			   data-placement="left" name="planningDate_value"
			   value="${new Date()?.format("dd/MM/yyyy")}"
			   class="form-control formTooltip datetimepicker"/>
		%{--<span class="input-group-addon">
			<span class="glyphicon glyphicon-calendar"></span>
		</span>--}%
	</div>
</div>

<div class="row form-group has-${hasErrors(bean: planningInstance, field: 'space', 'error')}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="com.ef.domain.name.space" default="Space" />

	</div>
	<div class="col-lg-3">
		<g:set var="SpaceList"/>
		<g:if test="${User.get(session.user?.id)?.profile?.profilName=="Cellule centrale"}">
			<g:set var="SpaceList" value="${Space.list()}" />
		</g:if>
		<g:else>
			<g:set var="SpaceList" value="${Space.findAllByUserInList(User.findAllByTeamAndIdNotEqual(User.get(session.user?.id)?.team,session.user?.id)).sort()}"/>
		</g:else>
		<g:select class="form-control space_choice"  id="space" name="space.id" from="${SpaceList}" noSelection="${['null':'Espace']}" optionKey="id" optionValue="spaceName"  value="${planningInstance?.space}"/>
	</div>
</div>
<div class="showresults">
	<div class="row form-group has-${hasErrors(bean: planningInstance, field: 'visit', 'error')}">
		<div class="col-lg-2" style="font-weight: bold" >
			<g:message code="default.visit.label" default="Visit" />

		</div>
		<div class="col-lg-3">
			<g:select class="form-control" id="visit" name="visit.id" from="" noSelection="${['null':'Visite']}" optionKey="id" optionValue="visitName" value="${planningInstance?.visit}" />
		</div>
	</div>
</div>

<script>
	$(".datetimepicker").datetimepicker({
		pickTime: false,
		icons: {
			date: "fa fa-calendar",
			up: "fa fa-arrow-up",
			down: "fa fa-arrow-down"
		},
		showToday: false,
		inline: false,
		todayBtn: true,
		locale:'fr',
		format: 'dd/mm/yyyy',
		autoclose: true,
		forceParse: false,
		pickerPosition: "bottom-right",
		minView: 2,
		daysOfWeekDisabled: [0,5,6],
		disabledDates: ["2015-08-08", "2014-08-05", "2014-08-06"]

	});
	$(".space_choice").change (function(){

	//alert('AA '+$(this).val());
		$.ajax({
			url: "VisiteForm",
			data: {val: $(this).val()},
			type: "GET",
			dataType: "html",
			success: function (data) {
				//var result = $('<div />').append(data).find('#showresults').html();
				//var htm= "";
				$('.showresults').html(data);
				//alert("data "+data);
			},
			error: function (xhr, status) {
				//alert("Sorry, there was a problem!");
			},
			complete: function (xhr, status) {
				//$('#showresults').slideDown('slow')
			}
		});


	});
</script>



