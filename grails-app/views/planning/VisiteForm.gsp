<%@ page import="com.ef.app.qm.Planning; com.ef.app.qm.Visit;" %>

<g:if test="${com.ef.app.qm.Planning.findAllBySpace(com.ef.app.qm.Space.findById(val))?.visit?.visitName?.size()>0}">
    <g:set var="VisitList" value="${Visit.findAllByVisitNameNotInList(com.ef.app.qm.Planning.findAllBySpace(com.ef.app.qm.Space.findById(val))?.visit?.visitName)}"/>
</g:if>
<g:else>
    <g:set var="VisitList" value="${com.ef.app.qm.Visit.list()}"/>
</g:else>


<div class="row form-group has-${hasErrors(bean: planningInstance, field: 'visit', 'error')}">
    <div class="col-lg-2" style="font-weight: bold" >
        <g:message code="default.visit.label" default="Visit" />

    </div>
    <div class="col-lg-3">
        <g:select class="form-control" id="visit" name="visit.id" from="${VisitList}" noSelection="${['null':'Visite']}" optionKey="id" optionValue="visitName" value="${planningInstance?.visit}" />
    </div>
</div>