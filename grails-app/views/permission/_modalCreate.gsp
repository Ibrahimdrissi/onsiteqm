<%@ page import="com.ef.app.qm.User; org.apache.shiro.SecurityUtils;" %>
<g:set var="loggedUser" value="${com.ef.app.qm.User.findByUserName(SecurityUtils.getSubject().getPrincipal())}"/>
<g:set var="userEntity" value="${message(code: 'com.ef.qm.entity.user', default: 'User')}"/>
<g:set var="roleEntity" value="${message(code: 'com.ef.qm.entity.role', default: 'Role')}"/>
<g:set var="permissionEntity" value="${message(code: 'com.ef.qm.entity.permission', default: 'Permission')}"/>
<div class="modal-header">
    <h1><g:message code="default.create.label" args="[permissionEntity]" /></h1>
</div>
<div class="modal-body">
    <g:form class="form-horizontal" action="create">
        <fieldset>
            <g:hiddenField name="createdBy.id" value="${loggedUser.id}"/>
            <g:hiddenField name="updatedBy.id" value="${loggedUser.id}"/>
            <g:hiddenField name="isManaged" value="false"/>

            <div class="form-group ${hasErrors(bean:permissionInstance,field:'name','error')}">
                <label class="col-lg-4 control-label"   for="name">
                    <g:message code="com.qm.cbr.label.name" default="Name"/>
                    <span class="required-indicator">*</span>
                </label>
                <div class="col-lg-6">
                    <input class="form-control"class="form-control"type="text" name="name" value="${permissionInstance?.name}" required="required">
                    <span class="help-inline label label-danger">${fieldError(bean:permissionInstance,field:'name')}</span>
                </div>
            </div>

            <div class="form-group ${hasErrors(bean:permissionInstance,field:'expression','error')}">
                <label class="col-lg-4 control-label"   for="expression">
                    <g:message code="com.ef.qm.label.expression" default="Expression"/>
                </label>
                <div class="col-lg-6">
                    <input class="form-control"class="form-control"type="text" name="expression" value="${permissionInstance?.expression}"
                           required="required"  class="formPopover"
                           data-content="${message(code: 'cbr.form.create.expression.hint')}"
                           placeholder="${message(code: 'cbr.form.create.expression.default')}">
                    <span class="help-inline label label-danger">${fieldError(bean:permissionInstance,field:'expression')}</span>
                </div>
            </div>

            <div class="form-group ${hasErrors(bean:permissionInstance,field:'description','error')}">
                <label class="col-lg-4 control-label"   for="description">
                    <g:message code="com.ef.cbr.label.description"/>
                </label>
                <div class="col-lg-6">
                    <textarea class="form-control" name="description" rows="3" >${permissionInstance?.description}</textarea>
                    <span class="help-inline label label-danger">${fieldError(bean:permissionInstance,field:'description')}</span>
                </div>
            </div>

            <div class="modal-footer row">
                <shiro:hasPermission permission="permission:list">
                    <g:link action="list" class="btn btn-default">
                        <i class="glyphicon glyphicon-circle-arrow-left"></i>
                        <g:message code="cbr.form.cancel.label"/>
                    </g:link>
                </shiro:hasPermission>
                <button type="submit" class="btn btn-primary">
                    <i class="glyphicon glyphicon-pencil glyphicon glyphicon-white"></i>
                    <g:message code="default.button.create.label" default="Create" />
                </button>
            </div>

        </fieldset>
    </g:form>

</div>