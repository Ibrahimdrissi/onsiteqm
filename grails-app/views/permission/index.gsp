
<html lang="en">

<head>
    <meta name="layout" content="layoutContent"/>
    <g:set var="entityName" value="${message(code: 'role.permission', default: 'Permission')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>

<br/>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <g:message code="default.list.label" args="[entityName]" />
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>${message(code: 'com.ef.qm.label.name',default: "Name")}</th>
                            <th>${message(code: 'com.ef.qm.label.description',default: "Description")}</th>
                            <th class="hidden"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:each in="${permissionInstanceList}" status="i" var="permissionInstance">
                            <tr class="${(i % 2) == 0 ? 'even gradeC' : 'odd gradeX'}">

                                <td>${permissionInstance?.name}</td>

                                <td>${fieldValue(bean: permissionInstance, field: "description")}</td>

                                <td></td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<script src="${resource(dir: 'theme/bower_components/jquery/dist/', file: 'jquery.min.js')}"></script>
<script src="${resource(dir: 'theme/bower_components/bootstrap/dist/js/', file: 'bootstrap.min.js')}"></script>
<script src="${resource(dir: 'theme/bower_components/metisMenu/dist/', file: 'metisMenu.min.js')}"></script>

<script src="${resource(dir: 'theme/bower_components/datatables/media/js/', file: 'jquery.dataTables.min.js')}"></script>
<script src="${resource(dir: 'theme/bower_components/datatables-plugins/integration/bootstrap/3/', file: 'dataTables.bootstrap.min.js')}"></script>

<script src="${resource(dir: 'theme/dist/js/', file: 'sb-admin-2.js')}"></script>
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>

</body>

</html>