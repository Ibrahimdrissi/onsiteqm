<%@ page import="com.ef.app.qm.User; org.apache.shiro.SecurityUtils" %>

<%--
  Author: Akeel
  Date: 12/20/12
  Time: 1:32 AM
--%>

<!doctype html>
<html>
<head>
  	<meta name="layout" content="layoutContent"/>
  	<g:set var="loggedUser" value="${com.ef.app.qm.User.findByUserName(SecurityUtils.getSubject().getPrincipal())}"/>
  	<g:set var="userEntity" value="${message(code: 'com.ef.cbr.entity.user', default: 'User')}"/>
  	<g:set var="roleEntity" value="${message(code: 'com.ef.cbr.entity.role', default: 'Role')}"/>
  	<g:set var="permissionEntity" value="${message(code: 'com.ef.cbr.entity.permission', default: 'Permission')}"/> 
  	<title><g:message code="default.edit.label" args="[permissionEntity]"/></title>
</head>

<body>
	<div class="row">
		<div class="col-lg-3">
			<g:render template="/templates/usermanagement" />
		</div>
			
		<div class="col-lg-9">
			<div class="page-header">
				<h1><g:message code="default.edit.label" args="[permissionEntity]" /></h1>
			</div>
			
			<g:form class="form-horizontal" action="edit">
				<fieldset>
					<g:hiddenField name="createdBy.id" value="${loggedUser.id}"/>
   					<g:hiddenField name="updatedBy.id" value="${loggedUser.id}"/>
   					<g:hiddenField name="version" value="${roleInstance?.version}"/>
   					<g:hiddenField name="id" value="${permissionInstance.id}"/>
   					<g:hiddenField name="isManaged" value="false"/>
				    
				    <div class="form-group ${hasErrors(bean:permissionInstance,field:'name','error')}">
						<label class="col-lg-4 control-label"   for="name">
							<g:message code="com.ef.cbr.label.name"/>
						</label>
						<div class="col-lg-6">
							<input class="form-control"class="form-control"type="text" name="name" value="${permissionInstance?.name}" required="required">
							<span class="help-inline label label-danger">${fieldError(bean:permissionInstance,field:'name')}</span>
						</div>
					</div>

					<div class="form-group ${hasErrors(bean:permissionInstance,field:'expression','error')}">
						<label class="col-lg-4 control-label"   for="expression">
							<g:message code="com.ef.cbr.label.expression"/>
						</label>
						<div class="col-lg-6">
							<input class="form-control"class="form-control"type="text" name="expression" value="${permissionInstance?.expression}"
								   required="required"  class="formPopover"
								   data-content="${message(code: 'cbr.form.create.expression.hint')}"
								   placeholder="${message(code: 'cbr.form.create.expression.default')}">
							<span class="help-inline label label-danger">${fieldError(bean:permissionInstance,field:'expression')}</span>
						</div>
					</div>
					
					<div class="form-group ${hasErrors(bean:permissionInstance,field:'description','error')}">
                        <label class="col-lg-4 control-label"   for="description">
                            <g:message code="com.ef.cbr.label.description"/>
                        </label>
                        <div class="col-lg-6">
                            <textarea class="form-control" name="description" rows="3" >${permissionInstance?.description}</textarea>
                            <span class="help-inline label label-danger">${fieldError(bean:permissionInstance,field:'description')}</span>
                        </div>                          
                    </div>
   
					<div class="form-actions">
						<shiro:hasPermission permission="permission:list">
							<g:link action="list" class="btn btn-default">
								<i class="glyphicon glyphicon-circle-arrow-left"></i>
								<g:message code="cbr.form.cancel.label"/>
							</g:link>
						</shiro:hasPermission>
						<shiro:hasPermission permission="permission:delete">
							<button type="submit" class="btn btn-danger confirmDelete">
								<i class="glyphicon glyphicon-trash glyphicon glyphicon-white"></i>
								<g:message code="default.button.delete.label" default="Delete" />
							</button>
						</shiro:hasPermission>
						<button type="submit" class="btn btn-primary">
							<i class="glyphicon glyphicon-pencil glyphicon glyphicon-white"></i>
							<g:message code="default.button.update.label" default="Update" />
						</button>
					</div>

				</fieldset>
			</g:form>
		</div>
	</div>
</body>
</html>