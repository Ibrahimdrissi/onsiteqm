<%@ page import="com.ef.app.qm.Team; com.ef.app.qm.User" %>

<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'team.label', default: 'com.ef.app.qm.Team')}" />
	<title><g:message code="default.list.label" args="${[message(code: 'com.ef.domain.name.team')]}" /></title>
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<g:message code="default.list.label" args="${[message(code: 'com.ef.domain.name.team')]}" />

			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="col-lg-3" role="main">
					<g:link class="btn btn-primary btn-block" action="create"><g:message code="default.new.label" args="${[message(code: 'com.ef.domain.name.team')]}" /></g:link>
					<br/>
				</div>
				<div class="dataTable_wrapper">
					<table class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
						<tr>
							<th>${message(code: 'team.teamName.label', default: 'com.ef.app.qm.Team')}</th>
							<th>${message(code: 'user.userName.label', default: 'Utilisateur Name')}</th>
						</tr>
						</thead>
						<tbody>
						<g:each in="${com.ef.app.qm.Team.list()}" status="i" var="team">
							<g:each in="${User.findAllByTeam(team)}" status="j" var="user">
								<tr class="${(j % 2) == 0 ? 'even gradeC' : 'odd gradeX'}">
									<td><g:link controller="team" action="show" id="${user.team.id}">${user.team.teamName}</g:link></td>
									<td><g:link controller="user" action="show" id="${user.id}">${user.userName}</g:link></td>
								</tr>
							</g:each>
							<g:if test="${com.ef.app.qm.User.findAllByTeam(team).size() == 0}">
								<tr class="${(i % 2) == 0 ? 'even gradeC' : 'odd gradeX'}">
									<td ><g:link controller="team" action="show" id="${team.id}">${team.teamName}</g:link></td>
									<td></td>
								</tr>
							</g:if>
						</g:each>


						</tbody>
					</table>
				</div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<script>
	$(document).ready(function() {
		$('#dataTables-example').DataTable({
			responsive: true
		});
	});
</script>

</body>

</html>
