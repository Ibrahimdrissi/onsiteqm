

<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'team.label', default: 'Team')}" />
	<title><g:message code="default.edit.label" args="${[message(code: 'com.ef.domain.name.team')]}" /></title>
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<g:message code="default.edit.label" args="${[message(code: 'com.ef.domain.name.team')]}" />
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row" style="padding-bottom: 20px">
					<div class="col-lg-2" role="navigation">
						<g:link class="btn btn-primary btn-block" action="index"><g:message code="default.list.label" args="${[message(code: 'com.ef.domain.name.team')]}" /></g:link>
					</div>
					<div class="col-lg-2" role="navigation">
						<g:link class="btn btn-primary btn-block" action="create"><g:message code="default.new.label" args="${[message(code: 'com.ef.domain.name.team')]}" /></g:link></div>
				</div>
				<div id="edit-team" class="content scaffold-edit" role="main">

					<g:form url="[resource:teamInstance, action:'update']" method="PUT" >
						<g:hiddenField name="version" value="${teamInstance?.version}" />
						<fieldset class="form">
							<g:render template="form"/>
						</fieldset>
						<fieldset class="buttons">
							<br/>
							<g:actionSubmit class="btn btn-success" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />

						</fieldset>
					</g:form>


				</div>

			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>



</body>

</html>

