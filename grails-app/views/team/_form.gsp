
<div class="row form-group has-${hasErrors(bean: teamInstance, field: 'teamName', 'error')}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="team.teamName.label" default="Team Name" />
		<span class="required-indicator" style="color:red" >*</span>
	</div>
	<div class="col-lg-3">
		<g:textField  class="form-control" name="teamName" value="${teamInstance?.teamName}"/>
	</div>
</div>

