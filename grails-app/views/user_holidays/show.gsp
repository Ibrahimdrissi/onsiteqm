

<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'user_holidays.label', default: 'User_holidays')}" />
	<title><g:message code="default.create.label" args="[entityName]" /></title>
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<g:message code="default.show.label" args="${[message(code: 'user_holiday.titre.label')]}" />
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row" style="padding-bottom: 20px">
					<div class="col-lg-2" role="navigation">
						<g:link class="btn btn-primary btn-block" action="index"><g:message code="default.list.label" args="${[message(code: 'user_holiday.titre.label')]}" /></g:link>
					</div>
					<div class="col-lg-2" role="navigation">
						<g:link class="btn btn-primary btn-block" action="create"><g:message code="default.new.label" args="${[message(code: 'user_holiday.titre.label')]}" /></g:link></div>
				</div>
				<div id="show-holiday" class="content scaffold-show" role="main">

					<g:if test="${user_holidaysInstance?.startDate}">
						<div class="row form-group">
							<div class="col-lg-2" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="user_holiday.startDate.label"  />
							</div>
							<div class="col-lg-3">
								<pre><g:formatDate format="dd-MM-yyyy" date="${user_holidaysInstance?.startDate}"/></pre>
							</div>
						</div>
					</g:if>

					<g:if test="${user_holidaysInstance?.endDate}">
						<div class="row form-group">
							<div class="col-lg-2" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="user_holiday.endDate.label"  />
							</div>
							<div class="col-lg-3">
								<pre><g:formatDate format="dd-MM-yyyy" date="${user_holidaysInstance?.endDate}"/></pre>
							</div>
						</div>
					</g:if>


					<g:if test="${user_holidaysInstance?.user}">
						<div class="row form-group">
							<div class="col-lg-2" style="font-weight: bold;padding-top: 8px;" >
								<g:message code="user_holiday.user.label"  />
							</div>
							<div class="col-lg-3">
								<pre>${user_holidaysInstance?.user?.firstName} ${user_holidaysInstance?.user?.lastName}</pre>
							</div>
						</div>
					</g:if>

				</div>

				<div class="row" style="padding-top: 20px">
					<g:form url="[resource:user_holidaysInstance, action:'delete']" method="DELETE">
						<div class="col-lg-2" role="navigation">
							<g:link class="btn btn-success btn-block" action="edit" resource="${user_holidaysInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
						</div>
						<div class="col-lg-2" role="navigation">
							<g:actionSubmit class="btn btn-danger btn-block" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
						</div>
					</g:form>

				</div>

			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>

</body>

</html>