<%@ page import="com.ef.app.qm.User_holidays" %>

<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'user_holiday.titre.label', default: 'user_Holiday')}" />
	<title><g:message code="com.expertflow.ma.onsiteqm.applicationName"/></title>
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<g:message code="default.list.label" args="${[message(code: 'user_holiday.titre.label')]}" />
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="col-lg-3" role="main">
					<g:link class="btn btn-primary btn-block" action="create"><g:message code="default.new.label" args="${[message(code: 'user_holiday.titre.label')]}" /></g:link>
					<br/>
				</div>
				<div class="dataTable_wrapper">
					<table class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
						<tr>
							<th>${message(code: 'user_holiday.user.label')}</th>
							<th>${message(code: 'user_holiday.startDate.label')}</th>
							<th>${message(code: 'user_holiday.endDate.label')}</th>

						</tr>
						</thead>
						<tbody>
						
						<g:set var="holidays_list"></g:set>
						<g:if test="${com.ef.app.qm.User.get(session.user?.id)?.profile?.profilName=="Animateur"}">
							<g:set var="holidays_list" value="${com.ef.app.qm.User_holidays.findAllByUser(com.ef.app.qm.User.findById(session.user?.id))}"></g:set>
						</g:if>
						<g:elseif test="${com.ef.app.qm.User.get(session.user?.id)?.profile?.profilName=="Chef de cellule"}" >
							<g:set var="holidays_list" value="${ com.ef.app.qm.User_holidays.findAllByUserInList(com.ef.app.qm.User.findAllByTeamOrId(com.ef.app.qm.User.get(session.user?.id)?.team,session.user?.id))}"></g:set>
						</g:elseif>
						<g:elseif test="${com.ef.app.qm.User.get(session.user?.id)?.profile?.profilName=="Cellule centrale"}">
							<g:set var="holidays_list" value="${com.ef.app.qm.User_holidays.findAllByUserInList(com.ef.app.qm.User.findAllByProfileNotEqualOrId(com.ef.app.qm.User.get(session.user?.id)?.profile,session.user?.id))}"></g:set>
						</g:elseif>

						<g:each in="${holidays_list}" status="i" var="holidayInstance">
							<tr class="${(i % 2) == 0 ? 'even gradeC' : 'odd gradeX'}">

								<td><g:link action="show" id="${holidayInstance.id}">${holidayInstance?.user?.firstName} ${holidayInstance?.user?.lastName}</g:link></td>

								<td><g:formatDate format="dd-MM-yyyy" date="${holidayInstance.startDate}"/></td>

								<td><g:formatDate format="dd-MM-yyyy" date="${holidayInstance.endDate}"/></td>


							</tr>
						</g:each>

						</tbody>
					</table>
				</div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>

<script>
	$(document).ready(function() {
		$('#dataTables-example').DataTable({
			responsive: true,
			"order": [[ 2, "asc" ]]
		});


	});
</script>

</body>

</html>