
<div class="row form-group has-${hasErrors(bean: user_holidaysInstance, field: 'user', 'error')}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="user_holiday.user.label" />
		<span class="required-indicator" style="color:red" >*</span>
	</div>

	<div class="col-lg-3">

		<g:set var="listUsers" value="${com.ef.app.qm.User.list()}" />

		<g:if test="${com.ef.app.qm.User.get(session.user?.id)?.profile?.profilName=="Animateur"}">
			<g:set var="listUsers" value="${com.ef.app.qm.User.findById(session.user?.id)}" />
		</g:if>
		<g:elseif test="${com.ef.app.qm.User.get(session.user?.id)?.profile?.profilName=="Chef de cellule"}" >
			<g:set var="listUsers" value="${com.ef.app.qm.User.findAllByTeamOrId(com.ef.app.qm.User.get(session.user?.id)?.team,session.user?.id)}" />
		</g:elseif>
		<g:elseif test="${com.ef.app.qm.User.get(session.user?.id)?.profile?.profilName=="Cellule centrale"}">
			<g:set var="listUsers" value="${com.ef.app.qm.User.findAllByProfileNotEqualOrId(com.ef.app.qm.User.get(session.user?.id)?.profile,session.user?.id)}"></g:set>
		</g:elseif>

		<g:select class="form-control" value="${user_holidaysInstance?.user?.id}" id="user" name="user.id" from="${listUsers}"  optionKey="id" optionValue="userName" />
	</div>
</div>

<div class="row form-group has-${dateError}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="user_holiday.startDate.label" />
		<span class="required-indicator" style="color:red" >*</span>
	</div>
	<div class="col-lg-3">

		<input data-field="datetime" type="text"
			   data-toggle="tooltip"
			   data-placement="left" name="startDate_value"
			   value="<g:formatDate format="dd/MM/yyyy" date="${user_holidaysInstance?.startDate}"/>"
			   class="form-control formTooltip datetimepicker"/>
	</div>
</div>

<div class="row form-group has-${dateError2}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="user_holiday.endDate.label" />
		<span class="required-indicator" style="color:red" >*</span>
	</div>
	<div class="col-lg-3">

		<input data-field="datetime" type="text"
			   data-toggle="tooltip"
			   data-placement="left" name="endDate_value"
			   value="<g:formatDate format="dd/MM/yyyy" date="${user_holidaysInstance?.endDate}"/>"
			   class="form-control formTooltip datetimepicker"/>
	</div>
</div>




<script>
	$(".datetimepicker").datetimepicker({
		pickTime: false,
		icons: {
			date: "fa fa-calendar",
			up: "fa fa-arrow-up",
			down: "fa fa-arrow-down"
		},
		showToday: false,
		inline: false,
		todayBtn: true,
		locale:'fr',
		format: 'dd/mm/yyyy',
		autoclose: true,
		forceParse: false,
		pickerPosition: "bottom-right",
		minView: 2,
		daysOfWeekDisabled: [0, 6],
		disabledDates: ["2015-08-08", "2014-08-05", "2014-08-06"]

	});


</script>
