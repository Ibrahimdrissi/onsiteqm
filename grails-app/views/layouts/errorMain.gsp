<%--
  Author: Akeel
  Date: 12/20/12
  Time: 1:32 AM
--%>

<!DOCTYPE HTML>
<html lang="fr">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=9" >
    <meta charset="utf-8">
    <title><g:layoutTitle default="${meta(name: 'app.name')}"/></title>
    <meta name="description" content="">
    <meta name="viewport" content="initial-scale = 1.0">
    %{--<meta http-equiv="X-UA-Compatible" content="IE=edge">--}%

    <style type="text/css" >
    #myUll{
        min-width: 100px;
        text-align: left;
    }

    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]><script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

     <link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon2.ico')}" type="image/x-icon">
     <g:layoutResources/>
     <g:layoutHead/>
</head>

<body>
%{--<g:render template="/templates/errorNav"/>--}%
<!-- Main Content Area -->
<div class="container">
    <div class="row">

        <div class='col-lg-12'>
            <g:if test="${flash.message}">
                <bootstrap:alert class="alert alert-${flash.type}">${flash.message}</bootstrap:alert>
            </g:if>
            <g:layoutBody/>

        </div>

    </div>
</div>


<g:layoutResources/>
</body>

</html>