<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title><g:message code="com.expertflow.ma.onsiteqm.applicationName"/></title>

	%{--<script src="${resource(dir: 'js/', file: 'tableToExcel.js')}"></script>--}%
	%{--<asset:javascript src="jquery-1.11.1.js"/>--}%
	<script src="${resource(dir: 'js/', file: 'jquery-1.11.1.js')}"></script>
	<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
	<link rel="apple-touch-icon" href="${assetPath(src: 'apple-touch-icon.png')}">
	<link rel="apple-touch-icon" sizes="114x114" href="${assetPath(src: 'apple-touch-icon-retina.png')}">


	<link rel="stylesheet" type="text/css"  href="${resource(dir: 'theme/bower_components/bootstrap/dist/css/', file: 'bootstrap.min.css')}" />
	<!-- MetisMenu CSS -->
	<link rel="stylesheet" type="text/css"  href="${resource(dir: 'theme/bower_components/metisMenu/dist/', file: 'metisMenu.min.css')}"  />
	<!-- DataTables CSS -->
	<link href="${resource(dir: 'theme/bower_components/datatables-plugins/integration/bootstrap/3/', file: 'dataTables.bootstrap.css')}" rel="stylesheet">
	<!-- DataTables Responsive CSS -->
	<link href="${resource(dir: 'theme/bower_components/datatables-responsive/css/', file: 'dataTables.responsive.css')}" rel="stylesheet">



	<!-- Custom CSS -->
	<link href="${resource(dir: 'theme/dist/css/', file: 'sb-admin-2.css')}" rel="stylesheet">
	<!-- Custom Fonts -->
	<link href="${resource(dir: 'theme/bower_components/font-awesome/css/', file: 'font-awesome.min.css')}" rel="stylesheet" type="text/css">
	<link href="${resource(dir: 'css/chosen/', file: 'chosen.css')}" rel="stylesheet" type="text/css">

	<style>
	div.calendar {
		width: 185px !important;
	}
	.calendar tbody .emptycell {
		visibility: visible !important;
	}
	.footrow{
		display: none !important;
	}


		.dataTables_scrollBody{
			overflow: overlay !important;
		}
	</style>

	<style>

	.backgroung_look{
		background-color: #337ab7;
	}
	.backgroung_affichage{
		background-color: #d9534f;
	}
	.backgroung_accueil{
		background-color: #5cb85c;
	}
	.backgroung_capacite{
		background-color: #f0ad4e;
	}

	</style>

</head>

<body>

<div id="wrapper">
	<!-- Navigation -->
	<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<g:link class="navbar-brand"  style="padding: 0 !important;" controller="home" action="index"><asset:image src="logo.png" alt="Grails"/></g:link>
		</div>
		<!-- /.navbar-header -->
		<ul class="nav navbar-top-links navbar-right">

		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
				<i class="fa fa-user fa-fw"> </i> ${session.user?.userName} <i class="fa fa-caret-down"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li>
					<g:link controller="auth" action="changePassword"><i class="fa fa-external-link fa-fw"></i>Changer Mot de passe</g:link>
				</li>
				<li>
					<g:link controller="user_holidays" action="index"><i class="fa fa-calendar "></i> <g:message code="com.ef.menu.User_holidays2" /></g:link>
				</li>
				<li>
					<g:link controller="auth" action="signOut"><i class="fa fa-sign-out fa-fw"></i> Logout</g:link>
				</li>
			</ul>
			<!-- /.dropdown-user -->
		</li>
			<!-- /.dropdown -->
		</ul>
		<!-- /.navbar-top-links -->

		<!-- /.navbar-static-side -->
	</nav>

	<div id="page-wrapper" style="margin: 0 !important;" >
		<g:layoutBody/>
	</div>
	<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

</body>

</html>
