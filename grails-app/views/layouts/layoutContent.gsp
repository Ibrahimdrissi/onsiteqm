<%@ page import="com.ef.app.qm.Questionnairegroup" %>
<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title><g:message code="com.expertflow.ma.onsiteqm.applicationName"/></title>

	<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
	<link rel="apple-touch-icon" href="${assetPath(src: 'apple-touch-icon.png')}">
	<link rel="apple-touch-icon" sizes="114x114" href="${assetPath(src: 'apple-touch-icon-retina.png')}">


	<link rel="stylesheet" type="text/css"  href="${resource(dir: 'theme/bower_components/bootstrap/dist/css/', file: 'bootstrap.min.css')}" />
	<!-- MetisMenu CSS -->
	<link rel="stylesheet" type="text/css"  href="${resource(dir: 'theme/bower_components/metisMenu/dist/', file: 'metisMenu.min.css')}"  />
	<!-- DataTables CSS -->
	<link href="${resource(dir: 'theme/bower_components/datatables-plugins/integration/bootstrap/3/', file: 'dataTables.bootstrap.css')}" rel="stylesheet">
	<!-- DataTables Responsive CSS -->
	<link href="${resource(dir: 'theme/bower_components/datatables-responsive/css/', file: 'dataTables.responsive.css')}" rel="stylesheet">
	<!-- Custom CSS -->
	<link href="${resource(dir: 'theme/dist/css/', file: 'sb-admin-2.css')}" rel="stylesheet">
	<!-- Custom Fonts -->
	<link href="${resource(dir: 'theme/bower_components/font-awesome/css/', file: 'font-awesome.min.css')}" rel="stylesheet" type="text/css">
	<link href="${resource(dir: 'css/chosen/', file: 'chosen.css')}" rel="stylesheet" type="text/css">
	<calendar:resources lang="en" theme="blue"/>

	%{--<asset:javascript src="jquery-1.11.1.js"/>--}%
	<script src="${resource(dir: 'js/', file: 'jquery-1.11.1.js')}"></script>
	%{--<asset:javascript src="moment.min.js"/>--}%
	<script src="${resource(dir: 'js/datetimepicker/', file: 'moment.min.js')}"></script>
	<script src="${resource(dir: 'js/datetimepicker/', file: 'bootstrap-datetimepicker.min.js')}"></script>
	%{--<asset:javascript src="datetimepicker/bootstrap-datetimepicker.min.js"/>
	<asset:javascript src="locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"/>
	<asset:javascript src="locales/bootstrap-datetimepicker.ru.js" charset="UTF-8"/>--}%
	%{--<asset:stylesheet href="datetimePicker/bootstrap-datetimepicker.css"/>--}%
	<link href="${resource(dir: 'css/datetimePicker/', file: 'DateTimePicker.css')}" rel="stylesheet" type="text/css">

	%{--<asset:stylesheet href="datetimePicker/DateTimePicker.css"/>--}%
	<link href="${resource(dir: 'css/datetimePicker/', file: 'bootstrap-datetimepicker.css')}" rel="stylesheet" type="text/css">

	<script src="${resource(dir: 'theme/bower_components/bootstrap/dist/js/', file: 'bootstrap.min.js')}"></script>
	<script src="${resource(dir: 'theme/bower_components/metisMenu/dist/', file: 'metisMenu.min.js')}"></script>
	<script src="${resource(dir: 'theme/dist/js/', file: 'sb-admin-2.js')}"></script>

	<script src="${resource(dir: 'theme/', file: 'jscolor.js')}"></script>

	<script src="${resource(dir: 'theme/bower_components/datatables/media/js/', file: 'jquery.dataTables.min.js')}"></script>
	<script src="${resource(dir: 'theme/bower_components/datatables-plugins/integration/bootstrap/3/', file: 'dataTables.bootstrap.min.js')}"></script>
	<r:require module="export"/>
	<style>
		div.calendar {
			width: 185px !important;
		}
		.calendar tbody .emptycell {
			visibility: visible !important;
		}
		.footrow{
			display: none !important;
		}
		.alert-top{
			margin-bottom: 0 !important;
		}
	</style>

	<style>

	.backgroung_look{
		background-color: #337ab7;
	}
	.backgroung_affichage{
		background-color: #d9534f;
	}
	.backgroung_accueil{
		background-color: #5cb85c;
	}
	.backgroung_capacite{
		background-color: #f0ad4e;
	}

	</style>

	<r:layoutResources/>
	<g:layoutHead/>
</head>

<body>

<div id="wrapper">
	<!-- Navigation -->
	<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<g:link class="navbar-brand"  style="padding: 0 !important;" controller="home" action="index"><asset:image src="logo.png" alt="Grails"/></g:link>
		</div>
		<!-- /.navbar-header -->
		<ul class="nav navbar-top-links navbar-right">

			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">
					<i class="fa fa-user fa-fw"> </i> ${session?.user?.userName} <i class="fa fa-caret-down"></i>
				</a>
				<ul class="dropdown-menu dropdown-user">
					<li>
						<g:link controller="auth" action="changePassword"><i class="fa fa-external-link fa-fw"></i>Changer Mot de passe</g:link>
					</li>
					<li>
						<g:link controller="user_holidays" action="index"><i class="fa fa-calendar "></i> <g:message code="com.ef.menu.User_holidays2" /></g:link>
					</li>
					<li>
						<g:link controller="auth" action="signOut"><i class="fa fa-sign-out fa-fw"></i> Se d&eacute;connecter</g:link>
					</li>
				</ul>
				<!-- /.dropdown-user -->
			</li>
			<!-- /.dropdown -->
		</ul>
		<!-- /.navbar-top-links -->

		<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse">
				<ul class="nav" id="side-menu">
					<li>
						<a href="#"><i class="fa fa-bar-chart-o fa-fw"></i><g:message code="com.ef.menu.Dashboard" /><span class="fa arrow"></span></a>
						<ul class="nav nav-third-level">
							<li >
								<g:link controller="home" action="dashboardBySpace"  >
									<i class="fa fa-dashboard fa-fw"></i> <g:message code="com.ef.menu.reporting.espace" />
								</g:link>
							</li>
							<li >
								<g:link controller="home" action="dashboardByTeam"  >
									<i class="fa fa-dashboard fa-fw"></i> <g:message code="com.ef.menu.reporting.cellule" />
								</g:link>
							</li>
							<li >
								<g:link controller="home" action="dashboardByRegion"  >
									<i class="fa fa-dashboard fa-fw"></i> <g:message code="com.ef.menu.reporting.region" />
								</g:link>
							</li>
							<li >
								<g:link controller="home" action="dashboardByUser" >
									<i class="fa fa-dashboard fa-fw"></i> <g:message code="com.ef.menu.reporting.user" />
								</g:link>
							</li>
							<li >
								<g:link controller="home" action="dashboardByDate" >
									<i class="fa fa-dashboard fa-fw"></i> <g:message code="com.ef.menu.reporting.date" />
								</g:link>
							</li>

						</ul>
					</li>

					<li>
						<g:link controller="evaluation" action="index"><i class="glyphicon glyphicon-list"></i> <g:message code="com.ef.menu.evaluation" /></g:link>
					</li>

					<li>
						<a href="#0"><i class="fa fa-bar-chart-o fa-fw"></i><g:message code="com.ef.menu.planning" /><span class="fa arrow"></span></a>
						<ul class="nav nav-third-level">
							<li>
								<g:link controller="planning" action="index"><i class="glyphicon glyphicon-calendar"></i> <g:message code="com.ef.menu.planning.list" /></g:link>
							</li>
						<g:if test="${com.ef.app.qm.User.findById(session?.user?.id).profile.profilName=="Animateur" || com.ef.app.qm.User.findById(session?.user?.id).profile.profilName=="Chef de cellule"}">
							<li>
								<g:link target="_blank" controller="reporting" action="myPlanning"><i class="glyphicon glyphicon-calendar"></i> <g:message code="com.ef.menu.planning.myplanning" /></g:link>
							</li>
							<li>
								<g:link target="_blank" controller="reporting" action="planningTeam"><i class="glyphicon glyphicon-calendar"></i> <g:message code="com.ef.menu.planning.cellule" /></g:link>
							</li>
						</g:if>
						<g:if test="${com.ef.app.qm.User.findById(session?.user?.id).profile.profilName=="Cellule centrale" || com.ef.app.qm.User.findById(session?.user?.id).profile.profilName=="Consultation"}">
							<li>
								<g:link target="_blank" controller="reporting" action="planning"><i class="glyphicon glyphicon-calendar"></i> <g:message code="com.ef.menu.planning.tous" /></g:link>
							</li>
						</g:if>
						</ul>
					</li>

					<li>
						<a href="#1"><i class="fa fa-bar-chart-o fa-fw"></i> <g:message code="com.ef.menu.reporting" /><span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li>
								<g:link controller="reporting"  action="global" target="_blank" ><i class="fa fa-bar-chart-o fa-fw"></i> <g:message code="com.ef.menu.reporting.global" /></g:link>
							</li>

							<!-- start li gestion by space -->
							<li>
								<a href="#2"><i class="fa fa-bar-chart-o fa-fw"></i><g:message code="com.ef.menu.reporting.espace" /><span class="fa arrow"></span></a>
								<ul class="nav nav-third-level">
								<!-- start list questionnaire for space -->
									<g:each var="questionnaire" in="${com.ef.app.qm.Questionnaire.list()}">
										<!-- test if questionnaire has group -->
										<g:if test="${questionnaire.questionnairegroups.size()>0}" >
											<li >
												<a href="#22">
													Questionnaire ${questionnaire.questionnaireName}
													<span class="fa arrow"></span>
												</a>
												<ul class="nav nav-third-level">
												<!-- start list group -->
													<g:each var="group" in="${Questionnairegroup.findAllByQuestionnaire(questionnaire)}">
														<!-- test if group has questions -->
														<g:if test="${group.questionses.size()>0}" >
															<li >
																<g:link controller="reporting" action="byGroup" params="[id: group?.id]" target="_blank"  ><i class="fa fa-dashboard fa-fw"></i> ${group.questionnairegroupName}</g:link>
															</li>
														</g:if>
														<!-- end test if group has questions -->
													</g:each>
												<!-- end list group -->
												</ul>
											</li>
										</g:if>
										<!-- end test if questionnaire has group -->
									</g:each>
								<!-- end list questionnaire for space -->
								</ul>
							</li>
							<!-- end li gestion by cellule -->
							<!-- start li gestion by cellule -->
							<li>
								<a href="#3"><i class="fa fa-bar-chart-o fa-fw"></i><g:message code="com.ef.menu.reporting.cellule" /><span class="fa arrow"></span></a>
								<ul class="nav nav-third-level">
									<g:each var="questionnaire" in="${com.ef.app.qm.Questionnaire.list()}">
										<g:if test="${questionnaire.questionnairegroups.size()>0}" >
											<li >
												<a href="#33">
													Questionnaire ${questionnaire.questionnaireName}
													<span class="fa arrow"></span>
												</a>
												<ul class="nav nav-third-level">
													<g:each var="group" in="${Questionnairegroup.findAllByQuestionnaire(questionnaire)}">
														<g:if test="${group.questionses.size()>0}" >
															<li >
																<g:link controller="reporting" action="byGroupAndCellule" params="[id: group?.id]" target="_blank"  ><i class="fa fa-dashboard fa-fw"></i> ${group.questionnairegroupName}</g:link>
															</li>
														</g:if>
													</g:each>
												</ul>
											</li>
										</g:if>
									</g:each>
								</ul>
							</li>
							<!-- start li gestion by space -->


						</ul>
					</li>
					<g:if test="${com.ef.app.qm.User.findById(session?.user?.id).profile.profilName=="Cellule centrale" || com.ef.app.qm.User.findById(session?.user?.id).profile.profilName=="Administrateur fonctionnel"}">
						<li>
							<a href="#4"><i class="glyphicon glyphicon-book"></i> <g:message code="com.ef.menu.Gestions" /><span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li>
									<g:link controller="profile" action="index"><i class="fa fa-dashboard fa-fw"></i> <g:message code="com.ef.menu.Profile" /></g:link>
								</li>
								<li>
									<g:link controller="team" action="index"><i class="fa fa-users"></i> <g:message code="com.ef.menu.Team" /></g:link>
								</li>
								<li>
									<g:link controller="space" action="index"><i class="glyphicon glyphicon-globe"></i> <g:message code="com.ef.menu.Spaces" /></g:link>
								</li>
								<li>
									<g:link controller="visit" action="index"><i class="glyphicon glyphicon-plane"></i> <g:message code="com.ef.menu.Visit" /></g:link>
								</li>
								<li>
									<g:link controller="holiday" action="index"><i class="glyphicon glyphicon-calendar"></i> <g:message code="com.ef.menu.Holiday" /></g:link>
								</li>
								<li>
									<g:link controller="user_holidays" action="index"><i class="glyphicon glyphicon-calendar"></i> <g:message code="com.ef.menu.User_holidays" /></g:link>
								</li>
								<li>
									<g:link controller="user" action="index"><i class="glyphicon glyphicon-user"></i> <g:message code="com.ef.menu.Utilisteurs" default="Utilisteurs" /></g:link>
								</li>

								<li>
									<a href="#5"><i class="fa fa-dashboard fa-fw"></i><g:message code="com.ef.menu.critere" /><span class="fa arrow"></span></a>
									<ul class="nav nav-third-level">
										<li>
											<g:link controller="questionnaire" action="index"><i class="fa fa-dashboard fa-fw"></i> <g:message code="com.ef.menu.critere.questionnaire" /></g:link>
										</li>
										<li>
											<g:link controller="questionnairegroup" action="index"><i class="fa fa-dashboard fa-fw"></i> <g:message code="com.ef.menu.critere.group" /></g:link>
										</li>
										<li>
											<g:link controller="questions" action="index"><i class="fa fa-dashboard fa-fw"></i> <g:message code="com.ef.menu.critere.question" /></g:link>
										</li>
									</ul>
								</li>

							</ul>
						</li>
					</g:if>
					%{--<li>
						<a href="#"><i class="glyphicon glyphicon-user"></i> <g:message code="com.ef.menu.usermangement.label" default="User Management" /><span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li>
								<g:link controller="user" action="index"><i class="glyphicon glyphicon-user"></i> <g:message code="com.ef.menu.Utilisteurs" default="Utilisteurs" /></g:link>
							</li>
							<li>
								<g:link controller="permission" action="index"><i class="fa fa-dashboard fa-fw"></i> <g:message code="com.ef.menu.permission" default="Permissions" /></g:link>
							</li>
							<li>
								<g:link controller="role" action="index"><i class="fa fa-dashboard fa-fw"></i> <g:message code="com.ef.menu.role" default="Roles" /></g:link>
							</li

						</ul>
						<!-- /.nav-second-level -->
					</li>--}%

				</ul>
			</div>
			<!-- /.sidebar-collapse -->
		</div>
		<!-- /.navbar-static-side -->

	</nav>
	<div id="csvUploading" class="loadingCsv hidden alert alert-info pull-right">
		<g:img dir="images" file="recycling.gif"/>

		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

		<g:message code="com.ef.app.qm.uploading.scv" default="Uploading CSV">

		</g:message>

	</div>
	<div id="page-wrapper">
		<g:if test="${flash.message}">
			<br/>
			<bootstrap:alert class="alert-top alert alert-${flash.type}">${flash.message}</bootstrap:alert>
		</g:if>
		<g:layoutBody/>
	</div>
	<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<script>
 /*   $(document).ready(function() {
     setInterval(function (){csvFileUpdating()} ,2000);
   });
   function readCookie(name) {
    var nameEQ = name + "=";
      var ca = document.cookie.split(';');
      for(var i=0;i < ca.length;i++) {
          var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
             if (c.indexOf(nameEQ) == 0)
              return c.substring(nameEQ.length,c.length);
      }
     return null;
   }
   function getCookie(name){
     var nameEq = name + "=";
      var cookies = document.cookie
   }
   function csvFileUpdating(){
      var flag  = readCookie("uploading");
      console.log("Cookie:"+flag)
        if(flag) {
           ${remoteFunction(method: 'get', controller: 'home', action: 'checkFileUploading', onSuccess: 'hideShowUploadingSign(data)', onFailure: 'fi()')};
        }
        if(flag == "null") {
          $('#csvUploading').removeClass("hidden");
          $('#csvUploading').hide();
        }
   }
   function hideShowUploadingSign(obj){
     console.log("Flag from server:"+obj.flag);
      if(obj.flag)
       $('#csvUploading').removeClass("hidden");
      else
        $('#csvUploading').hide();
   }*/
</script>


</body>

</html>
