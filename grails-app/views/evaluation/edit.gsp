<%@ page import="com.ef.app.qm.Evaluation; com.ef.app.qm.User; com.ef.app.qm.Questionnairegroup; com.ef.app.qm.Profile; com.ef.app.qm.Questions" %>

<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'evaluation.label', default: 'com.ef.app.qm.Evaluation')}" />
	<title><g:message code="default.edit.label" args="${[message(code: 'com.ef.domain.name.evaluation')]}" /></title>
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<g:if test="${evaluationInstance.evalStatus != com.ef.app.qm.Evaluation.REGISTERED_STATE}">
					<div class="row" >
						<div class="col-lg-7" >
							<g:if test="${evaluationInstance.planning.visit.id > 1 && com.ef.app.qm.Planning.findBySpaceAndVisit(evaluationInstance.planning.space,com.ef.app.qm.Visit.get((Long.parseLong(""+evaluationInstance.planning.visit.id))-1))?.planningState == com.ef.app.qm.Planning.EVALUATED_STATE }">
								<g:set var="currentVisitId" value="${evaluationInstance.planning.visit.id}" />
								<g:set var="previousPlanning" value="${com.ef.app.qm.Planning.findByVisitAndSpace(
										com.ef.app.qm.Visit.get((Long.parseLong(""+currentVisitId))-1),
										com.ef.app.qm.Space.get(evaluationInstance.planning.space.id))}" />
								<g:if test="${com.ef.app.qm.Evaluation.findByPlanning(previousPlanning).evalStatus != com.ef.app.qm.Evaluation.REGISTERED_STATE}" >
									<g:set var="previousNote" value="${com.ef.app.qm.Evaluation.findByPlanning(previousPlanning).score}" />
									<b>
										${com.ef.app.qm.Visit.get((Long.parseLong(""+currentVisitId))-1).visitName}:
										<g:formatDate format="dd-MM-yyyy"
													  date="${previousPlanning.planningDate}"/>; L'ancienne Note est
										<g:if test="${Double.parseDouble(previousNote) >= 4 }">
											<button style="" type="button" class="btn btn-success btn-circle">
												${previousNote}
											</button>
										</g:if>
										<g:elseif test="${Double.parseDouble(previousNote)  < 4 &&  Double.parseDouble(previousNote)  >= 2.5 }">
											<button style="" type="button" class="btn btn-warning btn-circle">
												${previousNote}
											</button>
										</g:elseif>
										<g:elseif test="${ Double.parseDouble(previousNote)   < 2.5 }">
											<button style="" type="button" class="btn btn-danger btn-circle">
												${previousNote}
											</button>
										</g:elseif>
									</b>
								</g:if>
							</g:if>
						</div>
						<div class="col-lg-5" >
							<b>
								${evaluationInstance.planning.visit.visitName}:
								<g:formatDate format="dd-MM-yyyy" date="${evaluationInstance.planning.planningDate}"/>
								${evaluationInstance.planning.space.spaceName}; Note
								<g:if test="${Double.parseDouble(evaluationInstance.score) >= 4 }">
									<button style="" type="button" class="btn btn-success btn-circle">
										${evaluationInstance.score}
									</button>
								</g:if>
								<g:elseif test="${Double.parseDouble(evaluationInstance.score)  < 4 &&  Double.parseDouble(evaluationInstance.score)  >= 2.5 }">
									<button style="" type="button" class="btn btn-warning btn-circle">
										${evaluationInstance.score}
									</button>
								</g:elseif>
								<g:elseif test="${ Double.parseDouble(evaluationInstance.score)   < 2.5 }">
									<button style="" type="button" class="btn btn-danger btn-circle">
										${evaluationInstance.score}
									</button>
								</g:elseif>
							</b>
						</div>
					</div>
				</g:if>
				<g:else>
					<b>
						${evaluationInstance.planning.visit.visitName}:
						<g:formatDate format="dd-MM-yyyy" date="${evaluationInstance.planning.planningDate}"/>
						${evaluationInstance.planning.space.spaceName}
					</b>
				</g:else>
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">

				<g:if test="${evaluationInstance.evalStatus == com.ef.app.qm.Evaluation.VALIDATED_STATE}">
					<div class="pull-right">
						<div class="btn-group">
							<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Actions <span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li id="btnExport" ><a href="#">Export</a></li>
							</ul>
						</div>
					</div>
				</g:if>
				<div class="row" style="padding-bottom: 20px">



				</div>


				<g:form url="[resource:evaluationInstance, action:'update']"  method="POST" enctype="multipart/form-data"  >
					<g:hiddenField name="version" value="${evaluationInstance?.version}" />
					<g:hiddenField name="initialState" value="${initialState}" />

					<fieldset class="form">
						<div class="row" >
							<g:each var="group" in="${com.ef.app.qm.Questionnairegroup.findAllByQuestionnaire(evaluationInstance.questionnaire)}">
								<div class="col-lg-12">
									<div class="panel panel-primary" style="border:0 !important;">
										<div class="panel-heading" style="color: #fff !important;background-color: ${group.color} !important;border:0 !important;" >
											${group.questionnairegroupName}
										</div>
										<div class="panel-body" style="border: 1pt solid ${group.color} !important;border-top: none !important;">

											<g:each var="Qs" in="${com.ef.app.qm.Questions.findAllByQuestionnairegroup(group)}">


												<div class="col-lg-6">
													<div class="panel panel-default" style="border:0 !important;">
														<div class="panel-heading" style="opacity: .65 !important;color: #fff !important;background-color: ${group.color} !important;border:0 !important;">
															${Qs.questionName}
														</div>
														<div class="panel-body" style="border: 1pt solid ${group.color} !important;border-top: none !important;">
															<pre style="max-height: 76px;overflow-y: auto" ><b><g:message code="evaluation.Points.libelle" /></b><br/>* ${Qs.questionPoints?.replaceAll(";",".\n* ")}</pre>
															<pre style="max-height: 76px;overflow-y: auto" ><b><g:message code="evaluation.maturity.libelle" /></b><br/>* ${Qs.questionMaturity?.replaceAll(";",".\n* ")}</pre>


															<div class="col-lg-12 ${evaluationInstance?.noteList[Qs.id] == ''?'has-error':''} " style="font-weight: bold" >
																<g:message code="evaluation.note.libelle" />
																<br/>
																<g:if test="${(com.ef.app.qm.Profile.findByProfilNameLike("%Cellule centrale%")?.id == com.ef.app.qm.User.get(session.user?.id)?.profile?.id && evaluationInstance?.evalStatus==1) || evaluationInstance?.planning?.space?.user?.id==session?.user?.id && (evaluationInstance?.evalStatus == com.ef.app.qm.Evaluation.REJECTED_STATE || evaluationInstance?.id==null || (evaluationInstance!=null && flash.message!=null )) }">
																	<g:field style="${(Integer.parseInt(""+evaluationInstance?.noteList[Qs.id]) == 0)?'display:none;':'display:block;'}" type="number" class="form-control notes" name="note_${Qs.id}" value="${(Integer.parseInt(""+evaluationInstance?.noteList[Qs.id]) == 0)?'0':evaluationInstance?.noteList[Qs.id]}"/>
																</g:if>
																<g:else>
																	<g:field style="${(Integer.parseInt(""+evaluationInstance?.noteList[Qs.id]) == 0)?'display:none;':'display:block;'}" readonly="readonly" type="number" class="form-control notes" name="note_${Qs.id}" value="${(Integer.parseInt(""+evaluationInstance?.noteList[Qs.id]) == 0)?'0':evaluationInstance?.noteList[Qs.id]}"/>
																</g:else>

															</div>
															<div class="col-lg-12" style="font-weight: bold" >
																<g:message code="evaluation.commentaireAnimateur.libelle" />
																<br/>

																<g:if test="${(com.ef.app.qm.Profile.findByProfilNameLike("%Cellule centrale%")?.id == com.ef.app.qm.User.get(session.user?.id)?.profile?.id && evaluationInstance?.evalStatus==1) || Profile.findByProfilNameLike("%Animateur%")?.id == com.ef.app.qm.User.get(session.user?.id)?.profile?.id && evaluationInstance?.planning?.space?.user?.id==session?.user?.id && (evaluationInstance?.evalStatus == com.ef.app.qm.Evaluation.REJECTED_STATE || evaluationInstance?.id==null || (evaluationInstance!=null && flash.message!=null )) }">
																	<g:textArea style="${(Integer.parseInt(""+evaluationInstance?.noteList[Qs.id]) == 0)?'display:none;':'display:block;'}" class="form-control" name="comm_animateur_${Qs?.id}" maxlength="500"  value="${(Integer.parseInt(""+evaluationInstance?.noteList[Qs.id]) == 0)?'0':evaluationInstance?.noteList["comm_animateur_"+Qs?.id]}"/>
																</g:if>
																<g:else>
																	<g:textArea style="${(Integer.parseInt(""+evaluationInstance?.noteList[Qs.id]) == 0)?'display:none;':'display:block;'}" disabled="disabled" class="form-control" name="comm_animateur_${Qs?.id}" maxlength="500"  value="${(Integer.parseInt(""+evaluationInstance?.noteList[Qs.id]) == 0)?'0':evaluationInstance?.noteList["comm_animateur_"+Qs?.id]}"/>
																</g:else>
															</div>
															<div class="col-lg-12" style="font-weight: bold" >
																<g:message code="evaluation.commentaireChef.libelle" />
																<br/>
																<g:if test="${(com.ef.app.qm.Profile.findByProfilNameLike("%Cellule centrale%")?.id == com.ef.app.qm.User.get(session.user?.id)?.profile?.id && evaluationInstance?.evalStatus==1) || Profile.findByProfilNameLike("%Chef de cellule%")?.id == com.ef.app.qm.User.get(session.user?.id)?.profile?.id && (evaluationInstance?.evalStatus==0 || evaluationInstance?.evalStatus==2) && ( com.ef.app.qm.User.findById(evaluationInstance.user.id).team.teamName== com.ef.app.qm.User.findById(session.user.id).team.teamName && com.ef.app.qm.User.findById(evaluationInstance.user.id).profile.id== Profile.findByProfilNameLike("%Animateur%").id || evaluationInstance.user.id==session.user.id )}">
																	<g:textArea style="${(Integer.parseInt(""+evaluationInstance?.noteList[Qs.id]) == 0)?'display:none;':'display:block;'}" class="form-control" name="comm_chef_${Qs?.id}" maxlength="500"  value="${(Integer.parseInt(""+evaluationInstance?.noteList[Qs.id]) == 0)?'0':evaluationInstance?.noteList["comm_chef_"+Qs?.id]}"/>
																</g:if>
																<g:else>
																	<g:textArea style="${(Integer.parseInt(""+evaluationInstance?.noteList[Qs.id]) == 0)?'display:none;':'display:block;'}" disabled="disabled" class="form-control" name="comm_chef_${Qs?.id}" maxlength="500"  value="${(Integer.parseInt(""+evaluationInstance?.noteList[Qs.id]) == 0)?'0':evaluationInstance?.noteList["comm_chef_"+Qs?.id]}"/>
																</g:else>
															</div>

															<div class="col-lg-12" style="font-weight: bold" >
																<g:message code="evaluation.commentaireCentrale.libelle" />
																<br/><g:if test="${Profile.findByProfilNameLike("%Cellule centrale%")?.id == com.ef.app.qm.User.get(session.user?.id)?.profile?.id && (com.ef.app.qm.Evaluation.findById(evaluationInstance?.id).evalStatus == 1 || com.ef.app.qm.Evaluation.findById(evaluationInstance?.id).evalStatus == 0 || flash?.message!=null && User.findById(evaluationInstance?.planning?.space?.user?.id).profile.id==Profile.findByProfilNameLike("%Chef de cellule%")?.id )}">
																<g:textArea style="${(Integer.parseInt(""+evaluationInstance?.noteList[Qs.id]) == 0)?'display:none;':'display:block;'}" class="form-control" name="comm_centrale_${Qs?.id}" maxlength="500"  value="${(Integer.parseInt(""+evaluationInstance?.noteList[Qs.id]) == 0)?'0':evaluationInstance?.noteList["comm_centrale_"+Qs?.id]}"/>
															</g:if>
															<g:else>
																<g:textArea style="${(Integer.parseInt(""+evaluationInstance?.noteList[Qs.id]) == 0)?'display:none;':'display:block;'}" disabled="disabled" class="form-control" name="comm_centrale_${Qs?.id}" maxlength="500"  value="${(Integer.parseInt(""+evaluationInstance?.noteList[Qs.id]) == 0)?'0':evaluationInstance?.noteList["comm_centrale_"+Qs?.id]}"/>
															</g:else>
															</div>

														</div>
													</div>
												</div>

											</g:each>

										</div>
									</div>
								</div>
							</g:each>
						</div>
						<g:render template="form"/>
					</fieldset>
					<fieldset class="buttons">
						%{--com.ef.app.qm.Profile.findByProfilNameLike("%Chef%")?.id == com.ef.app.qm.User.get(session.user?.id)?.profile?.id--}%

						<g:if test="${com.ef.app.qm.Profile.findByProfilNameLike("%Animateur%")?.id == com.ef.app.qm.User.get(session.user?.id)?.profile?.id && evaluationInstance?.evalStatus==2  || com.ef.app.qm.Profile.findByProfilNameLike("%Chef%")?.id == com.ef.app.qm.User.get(session.user?.id)?.profile?.id && com.ef.app.qm.User.findById(session?.user.id).team.teamName==com.ef.app.qm.User.findById(evaluationInstance?.user.id).team.teamName && (com.ef.app.qm.Profile.findByProfilNameLike("%Chef%")?.id != com.ef.app.qm.User.get(evaluationInstance?.user.id)?.profile?.id || evaluationInstance.evalStatus==2) || com.ef.app.qm.Profile.findByProfilNameLike("%Cellule centrale%")?.id == com.ef.app.qm.User.get(session.user?.id)?.profile?.id && (evaluationInstance?.evalStatus==1 || evaluationInstance?.evalStatus==2 || evaluationInstance?.evalStatus==0 && com.ef.app.qm.Profile.findByProfilNameLike("%Chef%")?.id == com.ef.app.qm.User.get(evaluationInstance?.user.id)?.profile?.id)}">
							<g:actionSubmit class="btn btn-success" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
						</g:if>
					</fieldset>
				</g:form>

				<table id="tables-total" style="display: none">

					<tr><td colspan="6" ></td></tr>
					<tr><td colspan="6" style="text-align:center" >Planning:<g:formatDate format="dd-MM-yyyy" date="${evaluationInstance?.planning?.planningDate}"/> Espace:${evaluationInstance?.planning?.space?.spaceName} Utilisateur:${evaluation?.user?.firstName} ${evaluation?.user?.lastName}; Note = ${evaluationInstance.score} </td></tr>
					<tr style="background-color: rgb(144, 237, 125);background: none" >
						<td style="padding: 5px;min-width: 350px" >#</td>
						<td style="padding: 5px;min-width: 400px"><g:message code="evaluation.question.label" /></td>
						<td style="padding: 5px"><g:message code="evaluation.note.label" /></td>
						<td style="padding: 5px;min-width: 250px"><g:message code="evaluation.evalAnimateurComment.label" /></td>
						<td style="padding: 5px;min-width: 250px"><g:message code="evaluation.evalSupComment.label" /></td>
						<td style="padding: 5px;min-width: 250px"><g:message code="evaluation.evalCentralComment.label" /></td>
					</tr>
					<g:each var="row" in="${utilityService.listAnswersByEvaluationToExport(evaluationInstance.id)}">
						<tr>
							<td style="padding: 5px;min-width: 350px" >${row.questionnairegroup_name}</td>
							<td style="padding: 5px;min-width: 400px">${row.question_name}</td>
							<td style="padding: 5px">${row.evalanswer_score}</td>
							<td style="padding: 5px">${row.evalanswer_animator_comment}</td>
							<td style="padding: 5px">${row.evalanswer_sup_comment}</td>
							<td style="padding: 5px">${row.evalanswer_centrale_comment}</td>
						</tr>
					</g:each>
					<tr><td colspan="6" ></td></tr>
					<tr style="background-color: rgb(144, 237, 125);background: none"><td><g:message code="evaluation.evalCentralComment.label" /></td>
						<td colspan="5" >${evaluationInstance?.evalAnimatorComment}</td></tr>
					<tr style="background-color: rgb(144, 237, 125);background: none"><td><g:message code="evaluation.commentaireChef.libelle" /></td>
						<td colspan="5" >${evaluationInstance?.evalSupComment}</td></tr>
					<tr style="background-color: rgb(144, 237, 125);background: none"><td><g:message code="evaluation.commentaireCentrale.libelle" /></td>
						<td colspan="5" >${evaluationInstance?.evalCentralComment}</td></tr>

				</table>

			</div>

		</div>
		<!-- /.panel-body -->
	</div>
	<!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>
<script src="${resource(dir: 'js/', file: 'jquery.btechco.excelexport.js')}"></script>
<script src="${resource(dir: 'js/', file: 'jquery.base64.js')}"></script>

<script >

	$(function(){

		$("#btnExport").click(function () {
			$('#tables-total').battatech_excelexport({
				containerid: 'tables-total'
			});
		})


		$(".notes").keyup(function(){
			if(!$(this).val().match(/^\d+$/) || parseInt($(this).val(),10)>5 || parseInt($(this).val(),10)<0 ){
				$(this).val("");
			}
		});
	});

</script>
</body>

</html>
