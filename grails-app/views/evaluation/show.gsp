<%@ page import="com.ef.app.qm.Planning; com.ef.app.qm.Space; com.ef.app.qm.Questions; com.ef.app.qm.Questionnairegroup" %>

<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'evaluation.label', default: 'Evaluation')}" />
	<title><g:message code="default.edit.label" args="${[message(code: 'com.ef.domain.name.evaluation')]}" /></title>
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="row" >
					<div class="col-lg-7" >
						<g:if test="${evaluationInstance.planning.visit.id > 1 && com.ef.app.qm.Planning.findBySpaceAndVisit(evaluationInstance.planning.space,com.ef.app.qm.Visit.get((Long.parseLong(""+evaluationInstance.planning.visit.id))-1))?.planningState == com.ef.app.qm.Planning.EVALUATED_STATE }">
							<g:set var="currentVisitId" value="${evaluationInstance.planning.visit.id}" />
							<g:set var="previousPlanning" value="${com.ef.app.qm.Planning.findByVisitAndSpace(
									com.ef.app.qm.Visit.get((Long.parseLong(""+currentVisitId))-1),
									com.ef.app.qm.Space.get(evaluationInstance.planning.space.id))}" />
							<g:if test="${com.ef.app.qm.Evaluation.findByPlanning(previousPlanning).evalStatus != com.ef.app.qm.Evaluation.REGISTERED_STATE}" >
								<g:set var="previousNote" value="${com.ef.app.qm.Evaluation.findByPlanning(previousPlanning).score}" />
								<b>
									${com.ef.app.qm.Visit.get((Long.parseLong(""+currentVisitId))-1).visitName}:
									<g:formatDate format="dd-MM-yyyy"
												  date="${previousPlanning.planningDate}"/>. L'ancienne Note est
									<g:if test="${Double.parseDouble(previousNote) >= 4 }">
										<button style="" type="button" class="btn btn-success btn-circle">
											${previousNote}
										</button>
									</g:if>
									<g:elseif test="${Double.parseDouble(previousNote)  < 4 &&  Double.parseDouble(previousNote)  >= 2.5 }">
										<button style="" type="button" class="btn btn-warning btn-circle">
											${previousNote}
										</button>
									</g:elseif>
									<g:elseif test="${ Double.parseDouble(previousNote)   < 2.5 }">
										<button style="" type="button" class="btn btn-danger btn-circle">
											${previousNote}
										</button>
									</g:elseif>
								</b>
							</g:if>
						</g:if>
					</div>
					<div class="col-lg-5" >
						<b>
							${evaluationInstance.planning.visit.visitName}:
							<g:formatDate format="dd-MM-yyyy" date="${evaluationInstance.planning.planningDate}"/>
							${evaluationInstance.planning.space.spaceName}
							<g:if test="${evaluationInstance.score != null }">. Note
								<g:if test="${Double.parseDouble(evaluationInstance.score) >= 4 }">
									<button style="" type="button" class="btn btn-success btn-circle">
										${evaluationInstance.score}
									</button>
								</g:if>
								<g:elseif test="${Double.parseDouble(evaluationInstance.score)  < 4 &&  Double.parseDouble(evaluationInstance.score)  >= 2.5 }">
									<button style="" type="button" class="btn btn-warning btn-circle">
										${evaluationInstance.score}
									</button>
								</g:elseif>
								<g:elseif test="${ Double.parseDouble(evaluationInstance.score)   < 2.5 }">
									<button style="" type="button" class="btn btn-danger btn-circle">
										${evaluationInstance.score}
									</button>
								</g:elseif>
							</g:if>
						</b>
					</div>

				</div>
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<g:if test="${evaluationInstance.evalStatus == com.ef.app.qm.Evaluation.VALIDATED_STATE}">
				<div class="pull-right">
					<div class="btn-group">
						<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Actions <span class="caret"></span>
						</button>
						<ul class="dropdown-menu pull-right" role="menu">
							<li id="btnExport" ><a href="#">Export</a></li>
						</ul>
					</div>
				</div>
				</g:if>
				<div class="row" style="padding-bottom: 20px">

					<div class="col-lg-2" role="navigation">
						<g:link class="btn btn-primary btn-block" action="index"><g:message code="default.list.label" args="${[message(code: 'com.ef.domain.name.evaluation')]}" /></g:link>
					</div>

				</div>
				<div id="edit-evaluation" class="content scaffold-edit" role="main">
					<g:hasErrors bean="${evaluationInstance}">
						<div class="alert alert-danger">
							<g:eachError bean="${evaluationInstance}" var="error">
								<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
							</g:eachError>
						</div>
					</g:hasErrors>

					<g:form url="[resource:evaluationInstance, action:'update']" method="PUT" >
						<g:hiddenField name="version" value="${evaluationInstance?.version}" />
						<fieldset class="form">
							<div class="row" >
								<g:each var="group" in="${com.ef.app.qm.Questionnairegroup.findAllByQuestionnaire(evaluationInstance.questionnaire)}">
									<div class="col-lg-12">
										<div class="panel panel-primary" style="border:0 !important;">
											<div class="panel-heading" style="color: #fff !important;background-color: ${group.color} !important;border:0 !important;" >
												${group.questionnairegroupName}
											</div>
											<div class="panel-body" style="border: 1pt solid ${group.color} !important;border-top: none !important;">

												<g:each var="Qs" in="${com.ef.app.qm.Questions.findAllByQuestionnairegroup(group)}">

													<div class="col-lg-6">
														<div class="panel panel-default" style="border:0 !important;">
															<div class="panel-heading" style="opacity: .65 !important;color: #fff !important;background-color: ${group.color} !important;border:0 !important;">
																${Qs.questionName}
															</div>
															<div class="panel-body" style="border: 1pt solid ${group.color} !important;border-top: none !important;">

																<pre style="max-height: 76px;overflow-y: auto" ><b><g:message code="evaluation.Points.libelle" /></b><br/>* ${Qs.questionPoints?.replaceAll(";",".\n* ")}</pre>
																<pre style="max-height: 76px;overflow-y: auto" ><b><g:message code="evaluation.maturity.libelle" /></b><br/>* ${Qs.questionMaturity?.replaceAll(";",".\n* ")}</pre>


																<div class="col-lg-12 ${evaluationInstance?.noteList[Qs.id] == ''?'has-error':''} " style="font-weight: bold" >
																	<g:message code="evaluation.note.libelle" />
																	<br/>
																	<g:field disabled="disabled" type="number" min="1" max="5" class="form-control notes" name="note_${Qs.id}" value="${(Integer.parseInt(""+evaluationInstance?.noteList[Qs.id]) == 0)?"":evaluationInstance?.noteList[Qs.id]}"/>
																</div>
																<div class="col-lg-12" style="font-weight: bold" >
																	<g:message code="evaluation.commentaireAnimateur.libelle" />
																	<br/>
																		<g:textArea disabled="disabled" class="form-control" name="comm_animateur_${Qs?.id}" maxlength="250"  value="${evaluationInstance?.noteList["comm_animateur_"+Qs?.id]}"  />
																</div>
																<div class="col-lg-12" style="font-weight: bold" >
																	<g:message code="evaluation.commentaireChef.libelle" />
																	<br/>
																		<g:textArea disabled="disabled" class="form-control" name="comm_chef_${Qs?.id}" maxlength="250"  value="${evaluationInstance?.noteList["comm_chef_"+Qs?.id]}" />
																</div>
																<div class="col-lg-12" style="font-weight: bold" >
																	<g:message code="evaluation.commentaireCentrale.libelle" />
																	<br/>
																	<g:textArea disabled="disabled" class="form-control" name="comm_centrale_${Qs?.id}" maxlength="250"  value="${evaluationInstance?.noteList["comm_centrale_"+Qs?.id]}" />
																</div>
															</div>
														</div>
													</div>

												</g:each>

											</div>
										</div>
									</div>
								</g:each>
							</div>
							<div class="row form-group ">
								<div class="col-lg-4" style="font-weight: bold" >
									<g:message code="evaluation.evalAnimatorComment.label" default="Eval Animator Comment" />
									<br/>
										<g:textArea disabled="disabled" class="form-control" name="evalAnimatorComment" maxlength="500" value="${evaluationInstance?.evalAnimatorComment}"/>
								</div>

								<div class="col-lg-4" style="font-weight: bold" >
									Commentaire chef de cellule
									<br/>
										<g:textArea disabled="disabled" class="form-control" name="evalSupComment" maxlength="500" value="${evaluationInstance?.evalSupComment}"/>
								</div>

								<div class="col-lg-4" style="font-weight: bold" >
									<g:message code="evaluation.evalCentralComment.label" default="Eval Central Comment" />
									<br/>
										<g:textArea disabled="disabled" class="form-control" name="evalCentralComment" maxlength="500" value="${evaluationInstance?.evalCentralComment}"/>
								</div>

								<g:if test="${evaluationInstance.filename != null }">

									<div class="col-lg-4" style="font-weight: bold;padding-top: 10px" >
										<a target="_blank" href="http://172.20.20.14:8080/telechargements/${evaluationInstance.filename}" >${(evaluationInstance.filename == null)?'':evaluationInstance.filename}</a>
									</div>
								</g:if>


							</div>
						</fieldset>
					</g:form>

					<table id="tables-total" style="display: none">

						<tr><td colspan="6" ></td></tr>
						<tr><td colspan="6" style="text-align:center" >Planning:<g:formatDate format="dd-MM-yyyy" date="${evaluation?.planning?.planningDate}"/> Espace:${evaluation?.planning?.space?.spaceName} Utilisateur:${evaluation?.user?.firstName} ${evaluation?.user?.lastName}; Note = ${evaluationInstance.score} </td></tr>
						<tr style="background-color: rgb(144, 237, 125);background: none" >
							<td style="padding: 5px;min-width: 350px" >#</td>
							<td style="padding: 5px;min-width: 400px"><g:message code="evaluation.question.label" /></td>
							<td style="padding: 5px"><g:message code="evaluation.note.label" /></td>
							<td style="padding: 5px;min-width: 250px"><g:message code="evaluation.evalAnimateurComment.label" /></td>
							<td style="padding: 5px;min-width: 250px">Commentaire chef de cellule</td>
							<td style="padding: 5px;min-width: 250px"><g:message code="evaluation.evalCentralComment.label" /></td>
						</tr>
						<g:each var="row" in="${utilityService.listAnswersByEvaluationToExport(evaluation.id)}">
							<tr>
								<td style="padding: 5px;min-width: 350px" >${row.questionnairegroup_name}</td>
								<td style="padding: 5px;min-width: 400px">${row.question_name}</td>
								<td style="padding: 5px">${row.evalanswer_score}</td>
								<td style="padding: 5px">${row.evalanswer_animator_comment}</td>
								<td style="padding: 5px">${row.evalanswer_sup_comment}</td>
								<td style="padding: 5px">${row.evalanswer_centrale_comment}</td>
							</tr>
						</g:each>
						<tr><td colspan="6" ></td></tr>
						<tr style="background-color: rgb(144, 237, 125);background: none"><td>Commentaire Animateur</td>
							<td colspan="5" >${evaluation?.evalAnimatorComment}</td></tr>
						<tr style="background-color: rgb(144, 237, 125);background: none"><td><g:message code="evaluation.commentaireChef.libelle" /></td>
							<td colspan="5" >${evaluation?.evalSupComment}</td></tr>
						<tr style="background-color: rgb(144, 237, 125);background: none"><td><g:message code="evaluation.commentaireCentrale.libelle" /></td>
							<td colspan="5" >${evaluation?.evalCentralComment}</td></tr>

					</table>

				</div>

			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>

<script src="${resource(dir: 'js/', file: 'jquery.btechco.excelexport.js')}"></script>
<script src="${resource(dir: 'js/', file: 'jquery.base64.js')}"></script>

<script >

	$(function(){

		$("#btnExport").click(function () {
			$('#tables-total').battatech_excelexport({
				containerid: 'tables-total'
			});
		});

		$(".notes").keyup(function(){
			if(!$(this).val().match(/^\d+$/) || parseInt($(this).val(),10)>5 || parseInt($(this).val(),10)<0 ){
				$(this).val("");
			}
		});
	});

</script>
</body>

</html>
