<%@ page import="com.ef.app.qm.Questionnaire; com.ef.app.qm.User; com.ef.app.qm.Questions; com.ef.app.qm.Profile; com.ef.app.qm.Questionnairegroup" %>

<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'evaluation.label', default: 'Evaluation')}" />
	<title><g:message code="default.create.label" args="${[message(code: 'com.ef.domain.name.evaluation')]}" /></title>
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="row" >
					<div class="col-lg-7" >
						<g:if test="${evaluationInstance.planning.visit.id > 1 && com.ef.app.qm.Planning.findBySpaceAndVisit(evaluationInstance.planning.space,com.ef.app.qm.Visit.get((Long.parseLong(""+evaluationInstance.planning.visit.id))-1)).planningState == com.ef.app.qm.Planning.EVALUATED_STATE }">
							<g:set var="currentVisitId" value="${evaluationInstance.planning.visit.id}" />
							<g:set var="previousPlanning" value="${com.ef.app.qm.Planning.findByVisitAndSpace(
									com.ef.app.qm.Visit.get((Long.parseLong(""+currentVisitId))-1),
									com.ef.app.qm.Space.get(evaluationInstance.planning.space.id))}" />
							<g:if test="${com.ef.app.qm.Evaluation.findByPlanning(previousPlanning).evalStatus != com.ef.app.qm.Evaluation.REGISTERED_STATE}" >
								<g:set var="previousNote" value="${com.ef.app.qm.Evaluation.findByPlanning(previousPlanning).score}" />

								<b>
									${com.ef.app.qm.Visit.get((Long.parseLong(""+currentVisitId))-1).visitName}:
									<g:formatDate format="dd-MM-yyyy"
												  date="${previousPlanning.planningDate}"/>. L'ancienne Note est
									<g:if test="${Double.parseDouble(previousNote) >= 4 }">
										<button style="" type="button" class="btn btn-success btn-circle">
											${previousNote}
										</button>
									</g:if>
									<g:elseif test="${Double.parseDouble(previousNote)  < 4 &&  Double.parseDouble(previousNote)  >= 2.5 }">
										<button style="" type="button" class="btn btn-warning btn-circle">
											${previousNote}
										</button>
									</g:elseif>
									<g:elseif test="${ Double.parseDouble(previousNote)   < 2.5 }">
										<button style="" type="button" class="btn btn-danger btn-circle">
											${previousNote}
										</button>
									</g:elseif>
								</b>

							</g:if>
						</g:if>
					</div>
					<div class="col-lg-5" style="padding-top: 3px !important;" >

							<b>
								<g:message code="default.create.label" args="${[message(code: 'com.ef.domain.name.evaluation')]}" />
							</b>
							${evaluationInstance.planning.visit.visitName}:
							${evaluationInstance.planning.space.spaceName};

					</div>

				</div>
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row" style="padding-bottom: 20px">
					<div class="col-lg-3" role="navigation">
						<g:link class="btn btn-primary btn-block" action="index"><g:message code="default.list.label" args="${[message(code: 'com.ef.domain.name.evaluation')]}" /></g:link>
					</div>
				</div>
				<div id="create-evaluation" class="content scaffold-create" role="main">
					<g:hasErrors bean="${evaluationInstance}">
						<div class="alert alert-danger">
							<g:eachError bean="${evaluationInstance}" var="error">
								<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
							</g:eachError>
						</div>
					</g:hasErrors>
					<g:form url="[resource:evaluationInstance, action:'save']"  enctype="multipart/form-data" >
						<fieldset class="form">
							<div class="row" >
								<g:each var="group" in="${com.ef.app.qm.Questionnairegroup.findAllByQuestionnaire((questionnaire != null)?questionnaire:com.ef.app.qm.Questionnaire.findByEnable_q(true))}">
									<div class="col-lg-12">
										<div class="panel panel-primary" style="border:0 !important;">
											<div class="panel-heading" style="color: #fff !important;background-color: ${group.color} !important;border:0 !important;" >
												${group.questionnairegroupName}
											</div>
											<div class="panel-body" style="border: 1pt solid ${group.color} !important;border-top: none !important;">

												<g:each var="Qs" in="${com.ef.app.qm.Questions.findAllByQuestionnairegroup(group)}">

													<div class="col-lg-6">
														<div class="panel panel-default" style="border:0 !important;">
															<div class="panel-heading" style="opacity: .65 !important;color: #fff !important;background-color: ${group.color} !important;border:0 !important;">
																${Qs?.questionName}
																<g:if test="${Qs.na}">
																	<div class="pull-right">
																		<g:checkBox style="margin: -6px;width: 29px;" class="form-control na_check" name="na_${Qs?.id}" />

																	</div>
																	<span style="float: right; margin-right: 10px;" >NA</span>
																</g:if>
															</div>
															<div class="panel-body" style="border: 1pt solid ${group.color} !important;border-top: none !important;">
																<pre style="max-height: 76px;overflow-y: auto" ><b><g:message code="evaluation.Points.libelle" /></b><br/>* ${Qs.questionPoints?.replaceAll(";",".\n* ")}</pre>
																<pre style="max-height: 76px;overflow-y: auto" ><b><g:message code="evaluation.maturity.libelle" /></b><br/>* ${Qs.questionMaturity?.replaceAll(";",".\n* ")}</pre>


																<div class="col-lg-12 ${evaluationInstance?.noteList[Qs?.id] == ''?'has-error':''} " style="font-weight: bold" >
																	<g:message code="evaluation.note.libelle" />
																	<br/>
																	<g:field type="number" class="form-control notes" name="note_${Qs?.id}" value="${evaluationInstance?.noteList[Qs?.id]}"/>
																</div>
																<div class="col-lg-12" style="font-weight: bold" >
																	<g:message code="evaluation.commentaireAnimateur.libelle" />
																	<br/>
																	<g:if test="${Profile.findByProfilNameLike("%Animateur%")?.id == com.ef.app.qm.User.get(session.user?.id)?.profile?.id }">
																		<g:textArea style="${evaluationInstance?.noteList['comm_animateur_error_'+Qs.id]}"  class="form-control" name="comm_animateur_${Qs?.id}" maxlength="500"  value="${evaluationInstance?.noteList["comm_animateur_"+Qs?.id]}" />
																	</g:if>
																	<g:else>
																		<g:textArea disabled="disabled" class="form-control" name="comm_animateur_${Qs?.id}" maxlength="500"  value="${evaluationInstance?.noteList["comm_animateur_"+Qs?.id]}"  />
																	</g:else>

																</div>
																<div class="col-lg-12" style="font-weight: bold" >
																	<g:message code="evaluation.commentaireChef.libelle" />
																	<br/>

																	<g:if test="${com.ef.app.qm.Profile.findByProfilNameLike("%Chef%")?.id == User.get(session.user?.id)?.profile?.id }">
																		<g:textArea style="${evaluationInstance?.noteList['comm_chef_error_'+Qs.id]}" class="form-control" name="comm_chef_${Qs?.id}" maxlength="500"  value="${evaluationInstance?.noteList["comm_chef_"+Qs?.id]}" />
																	</g:if>
																	<g:else>
																		<g:textArea disabled="disabled" class="form-control" name="comm_chef_${Qs?.id}" maxlength="500"  value="${evaluationInstance?.noteList["comm_chef_"+Qs?.id]}" />
																	</g:else>
																</div>


																<div class="col-lg-12" style="font-weight: bold" >
																	<g:message code="evaluation.commentaireCentrale.libelle" />
																	<br/>

																	<g:if test="${com.ef.app.qm.Profile.findByProfilNameLike("%Cellule centrale%")?.id == User.get(session.user?.id)?.profile?.id }">
																		<g:textArea class="form-control" name="comm_centrale_${Qs?.id}" maxlength="500"  value="${evaluationInstance?.noteList["comm_centrale_"+Qs?.id]}" />
																	</g:if>
																	<g:else>
																		<g:textArea disabled="disabled" class="form-control" name="comm_centrale_${Qs?.id}" maxlength="500"  value="${evaluationInstance?.noteList["comm_centrale_"+Qs?.id]}" />
																	</g:else>
																</div>



															</div>
														</div>
													</div>

												</g:each>

											</div>
										</div>
									</div>
								</g:each>
							</div>
							<g:render template="form"/>
						</fieldset>
						<fieldset class="buttons">
							<div class="row" >
								<div class="col-lg-10" >
									<g:submitButton name="create" class="btn btn-success" value="${message(code: 'default.button.create.label', default: 'Create')}" />
								</div>
								<div class="col-lg-1" >
									<g:submitButton name="create2" id="form_registered" class="btn btn-warning" value="${message(code: 'default.button.register.label', default: 'Enregistrer')}" />
								</div>
							</div>
						</fieldset>
					</g:form>
				</div>

			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<script >

	$(function(){
		$(".notes").keyup(function(){
			if(!$(this).val().match(/^\d+$/) || parseInt($(this).val(),10)>5 || parseInt($(this).val(),10)<=0 ){
				$(this).val("");
			}
		});

		$('.na_check').click(function(){
			if($(this).is(':checked'))
			{
				$(this).parent().parent().next().find( "input,textarea" ).css( "display", "none" );
				$(this).parent().parent().next().find( ".form-control").val('0');
			}
			else
			{
				$(this).parent().parent().next().find( "input,textarea" ).css( "display", "block" );
				$(this).parent().parent().next().find( ".form-control").val('');
			}
		});

	});

</script>



</body>

</html>
