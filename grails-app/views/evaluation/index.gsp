<%@ page import="com.ef.app.qm.User; com.ef.app.qm.Team; com.ef.app.qm.Profile; com.ef.app.qm.Evaluation" %>

<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'evaluation.label', default: 'Evaluation')}" />
	<title><g:message code="com.expertflow.ma.onsiteqm.applicationName"/></title>
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">

				<h4 class="panel-title">
				<g:message code="default.list.label" args="${[message(code: 'com.ef.domain.name.evaluation')]}" />
				<div class="pull-right">
						<div class="btn-group">
							<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
								Export
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li id="btnExport" ><a href="#">XLS</a></li>
							</ul>
						</div>
					</div>
				</h4>

			</div>
			<div class="panel-body" >
				<div class="table-responsive" >
					<table class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
						<tr>
							<th style="min-width: 150px !important;" >R&eacute;f&eacute;rence</th>
							%{--<th style="min-width: 150px !important;">Utilisateur</th>--}%
							<th style="min-width: 150px !important;">Rapport</th>
							<th style="min-width: 80px !important;">Visite</th>
							<th style="min-width: 150px !important;">Evaluateur</th>
							<th style="min-width: 150px !important;">${message(code: 'evaluation.evalStatus.label', default: 'Eval Status')}</th>
							<th style="min-width: 150px !important;">Date Cr&eacute;ation</th>
							<th style="min-width: 150px !important;">Validateur</th>
							<th style="min-width: 150px !important;">Date Validation</th>
							<th style="min-width: 170px !important;">Date MDF(Animateur)</th>
							<th style="min-width: 200px !important;">Date MDF(Chef de Cellule)</th>
							<th style="min-width: 200px !important;">Date MDF(Membre Centrale)</th>
							<th style="min-width: 200px !important;">Utilisateur (Derni&egrave;re MDF)</th>
						</tr>
						</thead>
						<tbody>
						<g:set var="evaluationlist"></g:set>
						<g:if test="${User.get(session.user?.id)?.profile?.profilName=="Animateur"}">
							<g:set var="evaluationlist" value="${Evaluation.findAllByUser(User.get(session.user?.id))}"></g:set>
						</g:if>
						<g:elseif test="${User.get(session.user?.id)?.profile?.profilName=="Chef de cellule"}" >
							<g:set var="evaluationlist" value="${ Evaluation.findAllByUserInList(User.findAllByTeam(User.get(session.user?.id)?.team))}"></g:set>
						</g:elseif>
						<g:elseif test="${User.get(session.user?.id)?.profile?.profilName=="Cellule centrale"}">
							<g:set var="evaluationlist" value="${Evaluation.list()}"></g:set>
						</g:elseif>
						<g:elseif test="${User.get(session.user?.id)?.profile?.profilName=="Consultation"}">
							<g:set var="evaluationlist" value="${Evaluation.list()}"></g:set>
						</g:elseif>
						<g:each in="${evaluationlist}" status="i" var="evaluationInstance">
							<tr class="${(i % 2) == 0 ? 'even gradeC' : 'odd gradeX'}"  >
								<td style="min-width: 150px" >
								<g:if test="${(evaluationInstance?.evalStatus == com.ef.app.qm.Evaluation.REGISTERED_STATE) && (evaluationInstance?.user?.id != com.ef.app.qm.User.findById(session?.user.id).id )}">
									<g:formatDate format="dd-MM-yyyy" date="${evaluationInstance.planning.planningDate}"/> ${evaluationInstance.planning.space.spaceName}
								</g:if>
								<g:elseif test="${evaluationInstance?.evalStatus == com.ef.app.qm.Evaluation.REGISTERED_STATE}">
									<g:link action="create_evaluationRegistred" params="[id: evaluationInstance.id,planning: evaluationInstance.planning.id ]" >
										<g:formatDate format="dd-MM-yyyy" date="${evaluationInstance.planning.planningDate}"/> ${evaluationInstance.planning.space.spaceName}
									</g:link>
								</g:elseif>
								<g:else >
									<g:link action="${(evaluationInstance?.user?.userName==session?.user?.userName && fieldValue(bean: evaluationInstance, field: "evalStatus") == '2' || fieldValue(bean: evaluationInstance, field: "evalStatus") == '0' && com.ef.app.qm.User.findById(evaluationInstance?.user.id).team==com.ef.app.qm.User.findById(session?.user.id).team && evaluationInstance?.user?.userName!=session?.user?.userName || (fieldValue(bean: evaluationInstance, field: "evalStatus") == '1' || fieldValue(bean: evaluationInstance, field: "evalStatus") == '0' && Profile.findByProfilNameLike("%Chef de cellule%")?.id == com.ef.app.qm.User.get(evaluationInstance.user?.id)?.profile?.id ) && Profile.findByProfilNameLike("%Cellule centrale%")?.id == com.ef.app.qm.User.get(session.user?.id)?.profile?.id)?'edit':'show' }" id="${evaluationInstance.id}">
										<g:formatDate format="dd-MM-yyyy" date="${evaluationInstance.planning.planningDate}"/> ${evaluationInstance.planning.space.spaceName}
									</g:link>
								</g:else>
								</td>
								<td>${evaluationInstance?.questionnaire?.questionnaireName}</td>
								%{--<td style="background-color:${evaluationInstance.evalStatus == 0 ?'#E36C0A !important;color:#fff !important;':'' }">${evaluationInstance?.planning?.space?.user?.firstName} ${evaluationInstance?.planning?.space?.user?.lastName}</td>--}%
								<td style="background-color:${evaluationInstance.evalStatus == 0 ?'#E36C0A !important;color:#fff !important;':'' }">
									${evaluationInstance.planning.visit.visitName}
								</td>
								<td style="background-color:${evaluationInstance.evalStatus == 0 ?'#E36C0A !important;color:#fff !important;':'' }">${evaluationInstance.user.userName}</td>

								<td style="background-color:${evaluationInstance.evalStatus == 0 ?'#E36C0A !important;color:#fff !important;':'' }">

									<g:if test="${fieldValue(bean: evaluationInstance, field: "evalStatus") == '0'}">
										En attente de validation
									</g:if>
									<g:elseif test="${fieldValue(bean: evaluationInstance, field: "evalStatus") == '1'}">
										Valid&eacute;
									</g:elseif>
									<g:elseif test="${(fieldValue(bean: evaluationInstance, field: "evalStatus") == '3') && (evaluationInstance?.user?.id == com.ef.app.qm.User.findById(session?.user.id).id )}">
										Enregistr&eacute;
									</g:elseif>
									<g:elseif test="${fieldValue(bean: evaluationInstance, field: "evalStatus") == '3'}">
										En cours
									</g:elseif>
									<g:else>
										Rejet&eacute;
									</g:else>
								</td>

								<td style="background-color:${evaluationInstance.evalStatus == 0 ?'#E36C0A !important;color:#fff !important;':'' }">
									<g:formatDate format="dd-MM-yyyy HH:mm" date="${evaluationInstance?.date_created}"/>
								</td>

								<td style="background-color:${evaluationInstance.evalStatus == 0 ?'#E36C0A !important;color:#fff !important;':'' }">
									${evaluationInstance?.user_validated?.firstName} ${evaluationInstance?.user_validated?.lastName}
								</td>

								<td style="background-color:${evaluationInstance.evalStatus == 0 ?'#E36C0A !important;color:#fff !important;':'' }">
									<g:formatDate format="dd-MM-yyyy HH:mm" date="${evaluationInstance?.date_validation}"/>
								</td>

								<td style="background-color:${evaluationInstance.evalStatus == 0 ?'#E36C0A !important;color:#fff !important;':'' }">
									<g:formatDate format="dd-MM-yyyy HH:mm" date="${evaluationInstance.date_animateur_updated}"/>
								</td>

								<td style="background-color:${evaluationInstance.evalStatus == 0 ?'#E36C0A !important;color:#fff !important;':'' }">
									<g:formatDate format="dd-MM-yyyy HH:mm" date="${evaluationInstance?.date_supervisor_updated}"/>
								</td>

								<td style="background-color:${evaluationInstance.evalStatus == 0 ?'#E36C0A !important;color:#fff !important;':'' }">
									<g:formatDate format="dd-MM-yyyy HH:mm" date="${evaluationInstance?.date_central_updated}"/>
								</td>

								<td style="background-color:${evaluationInstance.evalStatus == 0 ?'#E36C0A !important;color:#fff !important;':'' }">
									${evaluationInstance?.user_updated?.firstName} ${evaluationInstance?.user_updated?.lastName}
								</td>

							</tr>
						</g:each>

						</tbody>
					</table>
				</div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>

<script src="${resource(dir: 'js/', file: 'jquery.btechco.excelexport.js')}"></script>
<script src="${resource(dir: 'js/', file: 'jquery.base64.js')}"></script>

<script>
	$(document).ready(function() {
		$('#dataTables-example').DataTable({
			"lengthMenu": [[8, 25, 50, -1], [8, 25, 50, "Tous"]],
			responsive: true,
			"order": [[ 4, "asc" ]],
			//"ordering": false,
			//scrollY:        "500px",
			//scrollX:        true,
			//scrollCollapse: true,
		});

		$("#btnExport").click(function () {
			$("#dataTables-example").battatech_excelexport({
				containerid: "dataTables-example"
			});
		});

	});
</script>

</body>

</html>
