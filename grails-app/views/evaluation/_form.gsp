<%@ page import="com.ef.app.qm.Planning; com.ef.app.qm.Evaluation; com.ef.app.qm.User; com.ef.app.qm.Profile" %>


<div class="row form-group ">
	<div class="col-lg-4" style="font-weight: bold" >
		<g:message code="evaluation.evalAnimatorComment.label" default="Eval Animator Comment" />
		<br/>
		<g:if test="${(com.ef.app.qm.Profile.findByProfilNameLike("%Cellule centrale%")?.id == com.ef.app.qm.User.get(session.user?.id)?.profile?.id && evaluationInstance?.evalStatus==1) || Profile.findByProfilNameLike("%Animateur%")?.id == com.ef.app.qm.User.get(session.user?.id)?.profile?.id && evaluationInstance?.planning?.space?.user?.id==session?.user?.id && (evaluationInstance?.evalStatus == com.ef.app.qm.Evaluation.REJECTED_STATE || evaluationInstance?.id==null || (evaluationInstance!=null && flash.message!=null )) }">
			<g:textArea style="${message}" class="form-control" name="evalAnimatorComment" maxlength="750" value="${evaluationInstance?.evalAnimatorComment}"/>
		</g:if>
		<g:else>
			<g:textArea disabled="disabled" class="form-control" name="evalAnimatorComment" maxlength="750" value="${evaluationInstance?.evalAnimatorComment}"/>
		</g:else>

	</div>

	<div class="col-lg-4" style="font-weight: bold" >
		Commentaire chef de cellule
		<br/>

		<g:if test="${(com.ef.app.qm.Profile.findByProfilNameLike("%Cellule centrale%")?.id == com.ef.app.qm.User.get(session.user?.id)?.profile?.id && evaluationInstance?.evalStatus==1) || Profile.findByProfilNameLike("%Chef%")?.id == User.get(session.user?.id)?.profile?.id }">
			<g:textArea style="${message}" class="form-control" name="evalSupComment" maxlength="750" value="${evaluationInstance?.evalSupComment}"/>
		</g:if>
		<g:else>
			<g:textArea disabled="disabled" class="form-control" name="evalSupComment" maxlength="750" value="${evaluationInstance?.evalSupComment}"/>
		</g:else>
	</div>

	<div class="col-lg-4" style="font-weight: bold" >
		<g:message code="evaluation.evalCentralComment.label" default="Eval Central Comment" />
		<br/>

		<g:if test="${com.ef.app.qm.Profile.findByProfilNameLike("%Central%")?.id == User.get(session.user?.id)?.profile?.id }">
			<g:textArea style="${message}" class="form-control" name="evalCentralComment" maxlength="750" value="${evaluationInstance?.evalCentralComment}"/>
		</g:if>
		<g:else>
			<g:textArea disabled="disabled" class="form-control" name="evalCentralComment" maxlength="750" value="${evaluationInstance?.evalCentralComment}"/>
		</g:else>
	</div>

</div>
<div class="row form-group has-${hasErrors(bean: evaluationInstance, field: 'evalCentralComment', 'error')} ">
	<g:if test="${evaluationInstance?.id == null || evaluationInstance?.evalStatus==2 && flash.message==null }">
		<g:hiddenField class="form-control" name="evalStatus"  value="0"/>
	</g:if>
	<g:else>
		<g:if test="${com.ef.app.qm.Profile.findByProfilNameLike("%Chef de cellule%")?.id == com.ef.app.qm.User.get(session.user?.id)?.profile?.id && (evaluationInstance?.evalStatus==0 || evaluationInstance?.evalStatus==2 ) && com.ef.app.qm.Profile.findByProfilNameLike("%Animateur%")?.id==com.ef.app.qm.User.get(evaluationInstance?.user?.id)?.profile?.id && com.ef.app.qm.User.get(evaluationInstance?.user?.id)?.team?.id == com.ef.app.qm.User.get(session.user?.id)?.team?.id || com.ef.app.qm.Profile.findByProfilNameLike("%Cellule centrale%")?.id == com.ef.app.qm.User.get(session.user?.id)?.profile?.id &&( evaluationInstance?.evalStatus==0 || evaluationInstance?.evalStatus==2 )&& com.ef.app.qm.Profile.findByProfilNameLike("%Chef de cellule%")?.id == com.ef.app.qm.User.get(evaluationInstance?.user?.id)?.profile?.id }">
			<div class="col-lg-4" style="font-weight: bold" >
			<g:message code="evaluation.evalStatus.label" default="Eval Status" />
			<br/>
			<select class="form-control" name="evalStatus"  >
				<option ${(id_status != null || id_status == 1)?'selected':''} value="1"><g:message code="evaluation.status.validate" /></option>
				<option ${(id_status != null || id_status == 2)?'selected':''} value="2"><g:message code="evaluation.status.reject" /></option>
			</select>
				<input  name="checkstatWaitToConfirm" value="0" type="hidden"  />
		</div>
		</g:if>
		<g:elseif test="${com.ef.app.qm.Profile.findByProfilNameLike("%Cellule centrale%")?.id == com.ef.app.qm.User.get(session.user?.id)?.profile?.id && evaluationInstance?.evalStatus==1}">
			<g:hiddenField class="form-control" name="evalStatus"  value="1"/>
		</g:elseif>
		<g:else>
			<g:hiddenField class="form-control" name="evalStatus"  value="0"/>
		</g:else>
	</g:else>

	<g:if test="${evaluationInstance?.filename != null }">
		<div class="col-lg-4" style="font-weight: bold;padding-top: 10px" >
			<a target="_blank" href="http://172.20.20.14:8080/telechargements/${evaluationInstance.filename}" >${(evaluationInstance.filename == null)?'':evaluationInstance.filename}</a>
		</div>
	</g:if>

	<g:if test="${com.ef.app.qm.Profile.findByProfilNameLike("%Animateur%")?.id == com.ef.app.qm.User.get(session.user?.id)?.profile?.id || com.ef.app.qm.Profile.findByProfilNameLike("%Chef de cellule%")?.id == com.ef.app.qm.User.get(session.user?.id)?.profile?.id &&( evaluationInstance.planning.space.user.id == session.user?.id || com.ef.app.qm.Profile.findByProfilNameLike("%Chef de cellule%")?.id == com.ef.app.qm.User.get(evaluationInstance?.user?.id)?.profile?.id) || com.ef.app.qm.Profile.findByProfilNameLike("%Cellule centrale%")?.id == com.ef.app.qm.User.get(session.user?.id)?.profile?.id && evaluationInstance?.evalStatus==1 }" >
		<div class="col-lg-4" style="font-weight: bold" >
			<input type="file" name="evaluationJoint" class="form-control btn btn-primary" >
		</div>
	</g:if>
	<g:if test="">

	</g:if>


	<g:hiddenField class="form-control" name="evalNotification" type="number" value="1"/>

	<g:hiddenField value="${id_planning}"  name="id_planning" />

	<g:hiddenField value="${eval_id}"  name="eval_id" />

</div>
