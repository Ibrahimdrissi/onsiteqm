<%@ page import="org.apache.shiro.SecurityUtils" %>
<!doctype html>

<%--
  Author: Akeel
  Date: 5/27/11
  Time: 10:59 PM
--%>

<html>
<head>
    <meta name="layout" content="qm_main"/>
    <title><g:message code="com.ef.qm.welcome"/></title>
</head>

<body>

<div class="row">
       <div class="jumbotron">
            <div align="center">

                <h3>
                    <g:message code="com.ef.qm.dashboard.message1" default="Simple, Fast, Secure, Intuitive and Scalable are just to mention the few benefits of QM 0.1"/>
                </h3>
                <h4 class="panel-title ">
                    <a data-toggle="collapse"  class="btn btn-primary btn-xs pull-right" data-parent="#accordion" href="#collapseOne">
                        <g:message code="com.ef.qm.label.readMore" default="Read More"/>
                    </a>
                </h4>
            </div>
        </div>
        <div id="collapseOne" class="panel-collapse collapse">
            <div class="panel-body">
                <div class="panel panel-footer panel-info">
                    <h4>
                        <g:message code="com.ef.qm.dashboard.message2" default="QM 0.1 is our most brilliant Quality Management application, yet. bla bla bla"/>
                    </h4>
                </div>
                <div class="panel panel-footer panel-primary">
                    <h4>
                        <g:message code="com.ef.qm.dashboard.message3" default="QM 0.1 is our most brilliant Quality Management application, yet. bla bla bla"/>
                </h4>
                </div>
                <div class="panel panel-footer panel-info">
                    <h4>
                        <g:message code="com.ef.qm.dashboard.message4" default="QM 0.1 is our most brilliant Quality Management application, yet. bla bla bla"/>
                    </h4>
                </div>
                <div class="panel panel-footer panel-warning">
                    <h4>
                        <g:message code="com.ef.qm.dashboard.message5" default="QM 0.1 is our most brilliant Quality Management application, yet. bla bla bla"/>
                    </h4>
                </div>
                %{--<div class="panel panel-footer panel-primary">
                    <h4>
                        <g:message code="com.ef.cbr.dashboard.message6" default="CBR2.0 is our most brilliant 'Caller Based Routing' application, yet."/>
                    </h4>
                </div>--}%
            </div>
        </div>






</div>



</body>
</html>