<%@ page import="com.ef.app.qm.Team; com.ef.app.qm.Questions; com.ef.app.qm.Questionnairegroup; com.ef.app.qm.Space; com.ef.app.qm.Planning; com.ef.app.qm.Evalanswer; com.ef.app.qm.Evaluation; com.ef.app.qm.Visit" %>

<html lang="en">

<head>
	<meta name="layout" content="layout"/>
	<title>Projet TT</title>
</head>

<body>

<g:set var="questionsGroup" value="${com.ef.app.qm.Questionnairegroup.list()}" />

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header"><g:message code="com.ef.Dashboard.label"/></h1>
	</div>
	<!-- /.col-lg-12 -->
</div>

<div class="row">
	<div class="col-lg-8">
		<!-- /.row -->
		<div class="row">
			<div class="col-lg-4 col-md-6">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-calendar fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge">${Planning.list()?.size()}</div>
								<div><g:message code="com.ef.Dashboard.total"/></div>
							</div>
						</div>
					</div>
					<g:link controller="planning" action="index">
						<div class="panel-footer">
							<span class="pull-left"><g:message code="com.ef.Dashboard.planning"/></span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</g:link>
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
				<div class="panel panel-green">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-list-alt fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge">${Evaluation.list()?.size()}</div>
								<div><g:message code="com.ef.Dashboard.total"/></div>
							</div>
						</div>
					</div>
					<g:link controller="evaluation" action="index">
						<div class="panel-footer">
							<span class="pull-left"><g:message code="com.ef.Dashboard.evaluation"/></span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</g:link>
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
				<div class="panel panel-yellow">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-qrcode fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge">${Space.list()?.size()}</div>
								<div><g:message code="com.ef.Dashboard.total"/></div>
							</div>
						</div>
					</div>
					<g:link url="#" >
						<div class="panel-footer">
							<span class="pull-left"><g:message code="com.ef.Dashboard.espace"/></span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</g:link>
				</div>
			</div>
		</div>
		<!-- /.row -->

		<!-- /.panel GlobalEspace -->
		<a name="GlobalEspace" href="#GlobalEspace" ></a>
		<div class="panel panel-default">
			<!-- /.panel-heading -->
			<div class="panel-heading">
				<i class="fa fa-bar-chart-o fa-fw"></i> <g:message code="com.ef.menu.reporting.globalEspace"/>
				<div class="pull-right">
					<div class="btn-group">
						<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
							${(spaceId == null)?Space.list()[0]?.spaceName:(spaceId == 0)?'Tous':Space.get(spaceId)?.spaceName}
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu pull-right" role="menu">
							<g:each var="espace" status="i" in="${Space.list()}">
								<li><g:link url="${request.forwardURI}?spaceId=${espace?.id}#GlobalEspace"   >${espace?.spaceName}</g:link></li>
							</g:each>
							<li><g:link url="${request.forwardURI}?spaceId=0#GlobalEspace"   > <g:message code="com.ef.menu.liste.all"/></g:link></li>

						</ul>
					</div>
				</div>
			</div>
			<!-- /.panel-heading -->
			<!-- /.panel-body -->
			<div class="panel-body">
				<div id="containerGlobal" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel GlobalEspace -->

		<!-- /.panel GlobalCellule -->
		<a name="GlobalCellule" href="#GlobalCellule" ></a>
		<div class="panel panel-default">
			<!-- /.panel-heading -->
			<div class="panel-heading">
				<i class="fa fa-bar-chart-o fa-fw"></i> <g:message code="com.ef.menu.reporting.globalCellule"/>
				<div class="pull-right">
					<div class="btn-group">
						<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
							${(teamId == null)?Team.list()[0]?.teamName:(teamId == 0)?'Tous':Team.get(teamId)?.teamName}
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu pull-right" role="menu">
							<g:each var="team" status="i" in="${Team.list()}">
								<li><g:link url="${request.forwardURI}?teamId=${team?.id}#GlobalCellule"   >${team?.teamName}</g:link></li>
							</g:each>
							<li><g:link url="${request.forwardURI}?teamId=0#GlobalCellule"   > <g:message code="com.ef.menu.liste.all"/></g:link></li>

						</ul>
					</div>
				</div>
			</div>
			<!-- /.panel-heading -->
			<!-- /.panel-body -->
			<div class="panel-body">
				<div id="CelluleGlobal" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel GlobalCellule -->

		<!-- /.panel GlobalRegion -->
		<a name="GlobalRegion" href="#GlobalRegion" ></a>
		<div class="panel panel-default">
			<!-- /.panel-heading -->
			<div class="panel-heading">
				<i class="fa fa-bar-chart-o fa-fw"></i> <g:message code="com.ef.menu.reporting.globalRegion"/>
				<div class="pull-right">
					<div class="btn-group">
						<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
							${(region == null)?listRegion[0]?.region:(region == '0')?'Tous':region}
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu pull-right" role="menu">
							<g:each var="r" status="i" in="${listRegion}">
								<li><g:link url="${request.forwardURI}?region=${r?.region}#GlobalRegion"   >${r?.region}</g:link></li>
							</g:each>
							<li><g:link url="${request.forwardURI}?region=0#GlobalRegion"   > <g:message code="com.ef.menu.liste.all"/></g:link></li>

						</ul>
					</div>
				</div>
			</div>
			<!-- /.panel-heading -->
			<!-- /.panel-body -->
			<div class="panel-body">
				<div id="RegionGlobal" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel GlobalRegion -->



	<!-- /.panel -->
	</div>
	<!-- /.col-lg-8 -->
	<div class="col-lg-4">

		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="fa fa-calendar fa-fw"></i> <g:message code="com.ef.Dashboard.planning"/>
			</div>
			<div class="panel-body">
				<div id="morris-donut-chart"  ></div>
				<g:link controller="planning" action="index" class="btn btn-default btn-block" ><g:message code="com.ef.menu.planning" /></g:link>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="fa fa-list-alt fa-fw"></i> <g:message code="com.ef.Dashboard.evaluation"/>
			</div>
			<div class="panel-body">
				<div id="morris-donut-chart2"></div>
				<g:link controller="evaluation" action="index" class="btn btn-default btn-block" ><g:message code="com.ef.menu.evaluation" /></g:link>
			</div>
		</div>

	</div>
	<!-- /.col-lg-4 -->
</div>
<!-- /.row -->

<script>
	$(document).ready(function() {
		var listVisiteString = '${scoreList["listVisites"].toString().substring(1,scoreList["listVisites"].toString().size()-1)}';
		var listVisiteArray = listVisiteString.split(',');

		var id = 'containerGlobal';
		// http://localhost:8090/
		$('#'+id).highcharts({
			chart: { zoomType: 'x' },
			title: {text: ""},
			xAxis: {categories: listVisiteArray},
			exporting: {
				server: "http://ezzoumohammed.com/export/excel/",
				filename: "global",
				nbVisites: "${nbVisites}"
			},
			labels: {
				items: [{
					style: {
						left: '50px',
						top: '18px',
						color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
					}
				}]
			}
			,series: [{
				type: 'column',
				name: $('<div />').html( '<g:message code="evaluation.note.ponderation.libelle" />' ).text(),
				data: ${scoreList["listScores"]}
			},{
				type: 'spline',
				name: $('<div />').html( '<g:message code="evaluation.note.ponderation.libelle" />' ).text(),
				data: ${scoreList["listScores"]},
				marker: {
					lineWidth: 2,
					lineColor: Highcharts.getOptions().colors[0],
					fillColor: 'white'
				}
			}]
		});

	});
</script>

<script>
	$(document).ready(function() {
		var listVisiteString = '${scoreList["listVisites"].toString().substring(1,scoreList["listVisites"].toString().size()-1)}';
		var listVisiteArray = listVisiteString.split(',');

		var id = 'CelluleGlobal';
		// http://localhost:8090/
		$('#'+id).highcharts({
			chart: { zoomType: 'x' },
			title: {text: ""},
			xAxis: {categories: listVisiteArray},
			exporting: {
				server: "http://ezzoumohammed.com/export/excel/",
				filename: "global",
				nbVisites: "${nbVisites}"
			},
			labels: {
				items: [{
					style: {
						left: '50px',
						top: '18px',
						color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
					}
				}]
			}
			,series: [{
				type: 'column',
				name: $('<div />').html( '<g:message code="evaluation.note.ponderation.libelle" />' ).text(),
				data: ${scoreList["listScoresCellule"]}
			},{
				type: 'spline',
				name: $('<div />').html( '<g:message code="evaluation.note.ponderation.libelle" />' ).text(),
				data: ${scoreList["listScoresCellule"]},
				marker: {
					lineWidth: 2,
					lineColor: Highcharts.getOptions().colors[0],
					fillColor: 'white'
				}
			}]
		});

	});
</script>

<script>
	$(document).ready(function() {
		var listVisiteString = '${scoreList["listVisites"].toString().substring(1,scoreList["listVisites"].toString().size()-1)}';
		var listVisiteArray = listVisiteString.split(',');

		var id = 'RegionGlobal';
		// http://localhost:8090/
		$('#'+id).highcharts({
			chart: { zoomType: 'x' },
			title: {text: ""},
			xAxis: {categories: listVisiteArray},
			exporting: {
				server: "http://ezzoumohammed.com/export/excel/",
				filename: "global",
				nbVisites: "${nbVisites}"
			},
			labels: {
				items: [{
					style: {
						left: '50px',
						top: '18px',
						color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
					}
				}]
			}
			,series: [{
				type: 'column',
				name: $('<div />').html( '<g:message code="evaluation.note.ponderation.libelle" />' ).text(),
				data: ${scoreList["listScoresRegion"]}
			},{
				type: 'spline',
				name: $('<div />').html( '<g:message code="evaluation.note.ponderation.libelle" />' ).text(),
				data: ${scoreList["listScoresRegion"]},
				marker: {
					lineWidth: 2,
					lineColor: Highcharts.getOptions().colors[0],
					fillColor: 'white'
				}
			}]
		});

	});
</script>

<script>
	$(document).ready(function() {
		Morris.Donut({
			element: 'morris-donut-chart',
			data: [
				{
					label: '<g:message code="com.ef.Dashboard.planning.state.validated" default="Validated" />',
					value: ${Planning.findAllByPlanningState(Planning.VALIDATED_STATE).size()}
				}, {
					label: '<g:message code="com.ef.Dashboard.planning.state.waiting" default="Waiting confirmation" />',
					value: ${Planning.findAllByPlanningState(Planning.WAITING_CONFIRMATION_STATE).size()}
				}, {
					label: '<g:message code="com.ef.Dashboard.planning.state.rejected" default="Rejected" />',
					value: ${Planning.findAllByPlanningState(Planning.REJECTED_STATE).size()}
				}],
			colors: ['#5cb85c', '#f0ad4e', '#d9534f'],
			resize: true
		});

		Morris.Donut({
			element: 'morris-donut-chart2',
			data: [
				{
					label: '<g:message code="evaluation.status.create"  />',
					value: ${Evaluation.findAllByEvalStatus(Evaluation.CREATED_STATE).size()}
				}, {
					label: '<g:message code="evaluation.status.validate"  />',
					value: ${Evaluation.findAllByEvalStatus(Evaluation.VALIDATED_STATE).size()}
				}, {
					label: '<g:message code="evaluation.status.reject"  />',
					value: ${Evaluation.findAllByEvalStatus(Evaluation.REJECTED_STATE).size()}
				}],
			colors: ['#f0ad4e', '#5cb85c', '#d9534f'],
			resize: true
		});
	});
</script>

<script src="${resource(dir: 'theme/bower_components/raphael/', file: 'raphael-min.js')}"></script>
<script src="${resource(dir: 'theme/bower_components/morrisjs/', file: 'morris.min.js')}"></script>

</body>

</html>