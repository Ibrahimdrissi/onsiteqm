<div class="row form-group has-${dateError}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="holiday.holidayDate.label" default="Space Name" />
		<span class="required-indicator" style="color:red" >*</span>
	</div>
	<div class="col-lg-3">

		<input data-field="datetime" type="text"
			   data-toggle="tooltip"
			   data-placement="left" name="holidayDate_value"
			   value="<g:formatDate format="dd/MM/yyyy" date="${holidayInstance?.holidayDate}"/>"
			   class="form-control formTooltip datetimepicker"/>
	</div>
</div>

<div class="row form-group has-${hasErrors(bean: holidayInstance, field: 'holidayDescription', 'error')}">
	<div class="col-lg-2" style="font-weight: bold" >
		<g:message code="holiday.holidayDescription.label" default="Space Name" />
		<span class="required-indicator" style="color:red" >*</span>
	</div>
	<div class="col-lg-3">
		<g:hiddenField name="holidayDuration" min="1" class="form-control" type="number" value="1" ></g:hiddenField>
		<g:textField  name="holidayDescription" class="form-control" maxlength="250" value="${holidayInstance?.holidayDescription}"/>
	</div>
</div>


<script>
	$(".datetimepicker").datetimepicker({
		pickTime: false,
		icons: {
			date: "fa fa-calendar",
			up: "fa fa-arrow-up",
			down: "fa fa-arrow-down"
		},
		showToday: false,
		inline: false,
		todayBtn: true,
		locale:'fr',
		format: 'dd/mm/yyyy',
		autoclose: true,
		forceParse: false,
		pickerPosition: "bottom-right",
		minView: 2,
		daysOfWeekDisabled: [0, 6],
		disabledDates: ["2015-08-08", "2014-08-05", "2014-08-06"]

	});
</script>