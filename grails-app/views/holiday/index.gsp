<%@ page import="com.ef.app.qm.Holiday" %>

<html lang="en">

<head>
	<meta name="layout" content="layoutContent"/>
	<g:set var="entityName" value="${message(code: 'holiday.titre.label', default: 'Holiday')}" />
	<title><g:message code="com.expertflow.ma.onsiteqm.applicationName"/></title>
</head>

<body>

<br/>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<g:message code="default.list.label" args="${[message(code: 'holiday.titre.label')]}" />
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="col-lg-3" role="main">
					<g:link class="btn btn-primary btn-block" action="create"><g:message code="default.new.label" args="${[message(code: 'holiday.titre.label')]}" /></g:link>
					<br/>
				</div>
				<div class="dataTable_wrapper">
					<table class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
						<tr>
							<th>${message(code: 'holiday.holidayDescription.label')}</th>
							<th>${message(code: 'holiday.holidayDate.label')}</th>
						</tr>
						</thead>
						<tbody>
						<g:each in="${com.ef.app.qm.Holiday.list()}" status="i" var="holidayInstance">
							<tr class="${(i % 2) == 0 ? 'even gradeC' : 'odd gradeX'}">

								<td><g:link action="show" id="${holidayInstance.id}">${fieldValue(bean: holidayInstance, field: "holidayDescription")}</g:link></td>

								<td><g:formatDate format="dd-MM-yyyy" date="${holidayInstance.holidayDate}"/></td>

							</tr>
						</g:each>

						</tbody>
					</table>
				</div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>

<script>
	$(document).ready(function() {
		$('#dataTables-example').DataTable({
			responsive: true,
			"order": [[ 2, "asc" ]]
		});


	});
</script>

</body>

</html>
