﻿  $(document).ready(function() {

			$('.time').daterangepicker({
			"autoApply": true,
			"ranges" : {
              "Aujourd'hui": [moment(), moment()],
              'Hier': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
              'Les 7 derniers jours': [moment().subtract(6, 'days'), moment()],
              'Les 30 derniers jours': [moment().subtract(29, 'days'), moment()],
              'Le mois actuel': [moment().startOf('month'), moment().endOf('month')],
              'Le mois dernier': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
			"locale": {
				"format": "DD/MM/YYYY",
				"separator": "-",
				"applyLabel": "Valider",
				"cancelLabel": "Annuler",
				"fromLabel": "From",
				"toLabel": "To",
				"customRangeLabel": "Calendrier",
				"daysOfWeek": [
					"D",
					"L",
					"M",
					"M",
					"J",
					"V",
					"S"
				],
				"monthNames": [
					"Janvier",
					"Février",
					"Mars",
					"Avril",
					"Mai",
					"Juin",
					"Juillet",
					"Aout",
					"Septembre",
					"Octobre",
					"Novembre",
					"Décembre" 

				],
				"firstDay": 1
			},
		//	"startDate": moment(),
		//	"endDate": moment(),
			"opens": "left"
		}
		);

      });